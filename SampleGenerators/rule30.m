function out=rule30(in)
% RULE30 Wolfram's Rule30 cellular automata for random number generation.
%
out=uint64(0);
mxb=50; % max coding bit: log2(flintmax)-2
in=bitor(in,bitshift(bitand(1,in),mxb+1)); % copy lsb
for i=mxb:-1:1
    l=bitand(7,bitshift(in,int64(-i+1)));
    switch(l)
        case {4,3,2,1}
            out=bitor(out,bitshift(uint64(1),uint64(i)));
    end
end
    % case i=0
    l=bitshift(bitand(in,3),1)+bitand(uint64(1),bitshift(in,-mxb));
switch(l)
  case {4,3,2,1}
    out=bitor(out,1);
end
end
function testrule30
%%
clf
N=120;
 z=zeros(N,1,'int64');
 j=uint64(28888888888888888)*uint64(288);
 for i=1:N
  j=rule30(j);
  z(i)=j;
 end
 m=dec2bin(z)-'0';colormap([1 1 1;0 0 0]);imagesc(m);
 pause
d=double(z)*2^-51;
plot(d(1:2:end),d(2:2:end),'*');

%%
end
