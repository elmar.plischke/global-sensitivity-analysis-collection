function POINTS=sobolpoints( N, D, dir_file)
% ----- SOBOL POINTS GENERATOR BASED ON GRAYCODE ORDER -----------------
% INPUT: 
%   N         number of points  (cannot be greater than 2^32)
%   D         dimension  (make sure that the data file contains enough data!!)      
%   dir_file  the input file containing direction numbers
%
% OUTPUT:
%   A 2-dimensional array POINTS, where
%     
%     POINTS[i][j] = the jth component of the ith point,
%   
%   with i indexed from 0 to N-1 and j indexed from 0 to D-1
%
% ----------------------------------------------------------------------

% -----------------------------------------------------------------------------
% Licence pertaining to sobol.cc and the accompanying sets of direction numbers
% -----------------------------------------------------------------------------
% Copyright (c) 2008, Frances Y. Kuo and Stephen Joe
% All rights reserved.

% See http://web.maths.unsw.edu.au/~fkuo/sobol/

% C++ to matlab conversion by elmar.plischke@tu-clausthal.de

  if(nargin<3)
  % default database
  dir_file='new-joe-kuo-6.21201';
  end
  if(ispc), sep='\'; else sep='/'; end 
   full_dir_file= strcat( fileparts(mfilename('fullpath')), sep, ...
       'JoeKuo', sep, dir_file);
  infile=fopen(full_dir_file,'r');
  if (infile==-1) 
    error('Input file containing direction numbers cannot be found!\n');
  end
  
  buffer=fgetl(infile); % eat the first line
  
  % L = max number of bits needed 
  [~,L] = log2(N); 

  % C[i] = index from the right of the first zero bit of i
  C = ones(N,1,'uint32');
  for i=2:N
    value = i-1;
    while bitand(value , 1) 
      value = floor(value/2);
      C(i)=C(i)+1;
    end
  end
  % POINTS[i][j] = the jth component of the ith point
  %                with i indexed from 0 to N-1 and j indexed from 0 to D-1
  POINTS = zeros(N,D);

  % ----- Compute the first dimension -----
  
  % Compute direction numbers V[1] to V[L], scaled by pow(2,32)
  V = zeros(L+1,1,'uint32'); 
  for i=1:L, V(1+i) = bitshift(uint32(1),32-i); end % all m's = 1
  
  % Evaluate X[0] to X[N-1], scaled by pow(2,32)
  X = zeros(N+1,1,'uint32');
  for i=1:N 
    X(1+i) = bitxor( X(i) , V(1+C(i)) );
    POINTS(i,1)= double(X(1+i))/(2^32); % *** the actual points
    %        ^ 1 for first dimension
  end
  
  % ----- Compute the remaining dimensions -----
  for j=1:D-1
    
    % Read in parameters from file 
    dsa=fscanf(infile,'%d',3);%d=dsa(1);
    s=dsa(2);a=dsa(3);
    m0=fscanf(infile,'%d',s);m=[0;m0];
    % Compute direction numbers V[1] to V[L], scaled by pow(2,32)
    V = zeros(L+1,1);
    if (L <= s) 
      for i=1:L, V(1+i) = bitshift(m(1+i),32-i); end
    else 
      for i=1:s,   V(1+i) = bitshift(m(1+i),32-i); end
      for i=s+1:L
 	   V(1+i) = bitxor(V(1+i-s) , bitshift(V(1+i-s),-s)); 
 	   for k=1:s-1
        V(1+i) = bitxor(V(1+i), bitand(bitshift(uint32(a),-(s-1-k)), 1) * V(1+i-k));
       end
	  end
    end
    
    % Evaluate X[0] to X[N-1], scaled by pow(2,32)
    X(1) = 0; %  = zeros(N,1);
    for i=1:N 
      X(1+i) = bitxor( X(i) , V(1+C(i)) );
      POINTS(i,1+j)= double(X(1+i))/(2^32); % *** the actual points
      %        ^ 1+j for dimension (j+1)
    end
  end
  fclose(infile);
end
