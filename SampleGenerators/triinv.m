function x=triinv(ys,a,b,c)
% TRIINV inverse triangular distribution
x=zeros(size(ys));
for i=1:numel(ys)
    y=ys(i);
if(y<0 || y>1)
    x(i)=nan();
elseif( y <= (b-a)/(c-a)) 
    x(i)=a+sqrt(y*(c-a)*(b-a));
else
    x(i)=c-sqrt((1-y)*(c-a)*(c-b));
end %if
end %for
end