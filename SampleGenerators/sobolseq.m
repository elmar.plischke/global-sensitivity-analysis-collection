function values=sobolseq(realisations,dimension,sequence_count)
% SOBOLSEQ Implementation for Sobol generator.
% VAL=SOBOLSEQ(N,K) Generates N realisations of dimension K
%
% See
%   [Bratley+Fox, TOMS 14, 88 (1988)]
%   [Antonov+Saleev, USSR Comput. Maths. Math. Phys. 19, 252 (1980)]
%
%    Taken from the GNU Scientific Library GSL
%    Translated to MATLAB by elmar.plischke@tu-clausthal.de
%
%  Alternatively, net(sobolset(k),n)
%%
SOBOL_MAX_DIMENSION=40;
SOBOL_BIT_COUNT=30;

primitive_polynomials = [
  1,     3,   7,  11,  13,  19,  25,  37,  59,  47, ...
  61,   55,  41,  67,  97,  91, 109, 103, 115, 131, ...
  193, 137, 145, 143, 241, 157, 185, 167, 229, 171, ...
  213, 191, 253, 203, 211, 239, 247, 285, 369, 299  ...
];

degree_table = [
  0, 1, 2, 3, 3, 4, 4, 5, 5, 5, ...
  5, 5, 5, 6, 6, 6, 6, 6, 6, 7, ...
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, ...
  7, 7, 7, 7, 7, 7, 7, 8, 8, 8  ...
];

v_init= [ %[8][SOBOL_MAX_DIMENSION] =
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;...
    0, 0, 1, 3, 1, 3, 1, 3, 3, 1, 3, 1, 3, 1, 3, 1, 1, 3, 1, 3, 1, 3, 1, 3, 3, 1, 3, 1, 3, 1, 3, 1, 1, 3, 1, 3, 1, 3, 1, 3;...
    0, 0, 0, 7, 5, 1, 3, 3, 7, 5, 5, 7, 7, 1, 3, 3, 7, 5, 1, 1, 5, 3, 3, 1, 7, 5, 1, 3, 3, 7, 5, 1, 1, 5, 7, 7, 5, 1, 3, 3;...
    0, 0, 0, 0, 0, 1, 7, 9,13,11, 1, 3, 7, 9, 5,13,13,11, 3,15, 5, 3,15, 7, 9,13, 9, 1,11, 7, 5,15, 1,15,11, 5, 3, 1, 7, 9;...
    0, 0, 0, 0, 0, 0, 0, 9, 3,27,15,29,21,23,19,11,25, 7,13,17,...
    1,25,29, 3,31,11, 5,23,27,19,21, 5, 1,17,13, 7,15, 9,31, 9;...
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,...
     0,  0,  0, 37, 33,  7,  5, 11, 39, 63,...
    27, 17, 15, 23, 29,  3, 21, 13, 31, 25,...
     9, 49, 33, 19, 29, 11, 19, 27, 15, 25;...
     0,   0,  0,  0,  0,  0,    0,  0,  0,   0,...
     0,   0,  0,  0,  0,  0,    0,  0,  0,  13,...
    33, 115, 41, 79, 17,  29, 119, 75, 73, 105,...
     7,  59, 65, 21,  3, 113,  61, 89, 45, 107;...
    0, 0, 0, 0, 0, 0, 0, 0,  0,  0,...
    0, 0, 0, 0, 0, 0, 0, 0,  0,  0,...
    0, 0, 0, 0, 0, 0, 0, 0,  0,  0,...
    0, 0, 0, 0, 0, 0, 0, 7, 23, 39 ...
];
if(nargin<3),  sequence_count=0; end
%  last_denominator_inv=0;
  last_numerator_vec=zeros(1,SOBOL_MAX_DIMENSION);
  v_direction=zeros(SOBOL_BIT_COUNT,SOBOL_MAX_DIMENSION);

%% sobol init
if(dimension < 1 || dimension > SOBOL_MAX_DIMENSION), error('DIM out of bounds.'); end 
 

%% Initialize direction table in dimension 0.
  for k=1:SOBOL_BIT_COUNT, v_direction(k,1) = 1; end

%% Initialize in remaining dimensions.
  for i_dim=2:dimension
%%
    poly_index = i_dim;
    degree_i = degree_table(poly_index);
    includ=zeros(1,8);

    % Expand the polynomial bit pattern to separate
    % components of the logical array includ[].
%%
    p_i = primitive_polynomials(poly_index);
    for k = degree_i:-1:1;
      includ(k) = (mod(p_i, 2) == 1);
      p_i = floor(p_i/2);
    end

%% Leading elements for dimension i come from v_init[][].
    for j=1:degree_i;  v_direction(j,i_dim) = v_init(j,i_dim); end

    % Calculate remaining elements for this dimension,
    % as explained in Bratley+Fox, section 2.
%% 
    for j=(degree_i+1):SOBOL_BIT_COUNT;
      newv = v_direction(j-degree_i,i_dim);
      ell = 1;
      for k=1:degree_i; 
        ell = ell * 2;
        if(includ(k)), newv = bitxor(newv, (ell * v_direction(j-k,i_dim))); end
      end
      v_direction(j,i_dim) = newv;
    end
%%    
  end
%% Multiply columns of v by appropriate power of 2. 
  ell = 1;
  for j=(SOBOL_BIT_COUNT-1):-1:1;  
    ell = ell * 2;
    for i_dim=1:dimension; 
      v_direction(j,i_dim) = v_direction(j,i_dim) * ell;
    end
  end

%% 1/(common denominator of the elements in v_direction) 
  last_denominator_inv = 1.0 /(2.0 * ell);

%% final setup 
%  s_state->sequence_count = 0;
%  for(i_dim=0; i_dim<dimension; i_dim++) s_state->last_numerator_vec[i_dim] = 0;

values=zeros(realisations,dimension);
%%
for r = 1:realisations
%% sobol_get

%% Find the position of the least-significant zero in count.
  ell = 0;
  c = sequence_count;
  while(1), ell=ell+1;
    if(mod(c, 2) == 1),
	 c = floor(c/2);
    else 
	break;
    end
  end

%% Check for exhaustion. 
  if(ell > SOBOL_BIT_COUNT), error('BIT_COUNT exhausted.'); end

  for i_dimension=1:dimension;
    direction_i     = v_direction(ell,i_dimension);
    old_numerator_i = last_numerator_vec(i_dimension);
    new_numerator_i = bitxor(old_numerator_i, direction_i);
    last_numerator_vec(i_dimension) = new_numerator_i;
    values(r,i_dimension) = new_numerator_i * last_denominator_inv;
  end

  sequence_count=sequence_count+1;
 end
end

