function u=trappdf(x,a,b,c)
%TRAPPDF Distribution for trapezoidal distribution
% TRAPPDF(X,A,B,C) computes the density at X from trapezoidal 
% distribution on [A,B] with "slope" C, i.e. with support [A-C,B+C] 
% and plateau [A,B]
h=1/(c+(b-a));
u=0.*(x<=a-c)+0.*(x>b+c)+h*(x-(a-c))/c.*(x>a-c & x<=a)+...
    h*(x>a & x<=b)+ h*(b+c-x)/c.*(x>b & x<=b+c);
end