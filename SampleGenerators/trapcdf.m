function u=trapcdf(x,a,b,c)
%TRAPCDF Distribution for trapezoidal distribution
% TRAPCDF(X,A,B,C) computes the distribution at X from trapezoidal 
% distribution on [A,B] with "slope" C, i.e. with support [A-C,B+C] 
% and plateau [A,B]
h=1/(c+(b-a));
u=0.*(x<=a-c)+1.*(x>b+c)+h*(x-(a-c)).^2/(2*c).*(x>a-c & x<=a)+...
    h*(x-a+c/2).*(x>a & x<=b)+ (1-h/(2*c)*((b+c-x).^2)).*(x>b & x<=b+c);
end