function y=tripdf(xs,a,b,c)
% % TRIPDF triangular probability density
y=zeros(size(xs));
for i=1:numel(xs)
    x=xs(i);
if(x<=a)
    y(i)=0;
elseif( x <= b) 
    y(i)= 2*(x-a)/(c-a)/(b-a);
elseif( x <=c)
    y(i)=2*(c-x)/(c-a)/(c-b);
else
    y(i)=0;
end %if
end %for
end