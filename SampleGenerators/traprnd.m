function x=traprnd(n,a,b,c)
%TRAPRND Random numbers for a trapezoidal distribution
% TRAPRND(N,A,B,C) generates N random numbers from trapezoidal 
% distribution on [A,B] with "slope" C, i.e. with support [A-C,B+C] 
% and plateau [A,B]
x=(a-c)+rand(n,2)*[c;b+c-a];
end