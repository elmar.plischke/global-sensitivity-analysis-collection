function [Y,P]=iccorrelate(X,S,varargin)
% ICCORRELATE Iman Conover Correlation
%   ICCORRELATE(X,S) returns the reordered sample matrix X with (rank)
%   correlation matrix close to S

% Source: Mildenhall: Correlation and Aggregate Loss Distributions
% With An Emphasis On The Iman-Conover Method, 2005. Section 4.2

%X=rand(10,3);CX=X-ones(10,1)*mean(X);R=CX'*CX;s=sqrt(diag(R));S=R./(s*s');

% written by elmar.plischke@tu-clausthal.de
%%
%% default opts -- Parameter lists need double braces 'Params',{{3,4}}
opts=struct('Score','default','Params','none','UseQRP','no');
% With non-normal scores, correlation is not neccessarily respected
%%
[n,r]=size(X);
rr=size(S);
if(rr~=r), error('Correlation matrix size mismatch.'); end
if(any(diag(S)-1)), error('No correlation matrix'); end
%%
if ~isempty(varargin)
    if isa(varargin{1},'struct')
	 cmdOpts=varargin{1};
    elseif isa(varargin{1},'numeric')
      error(['Old syntax - consider ''UseQRP'',' num2str(varargin{1})]);
    else
    cmdOpts=struct(varargin{:});
	end
    members=fieldnames(opts);
    for i=1:length(members)
        o=members{i};
        if isfield(cmdOpts,o), opts.(o)=cmdOpts.(o);end
    end
end
%% Step 1: Moved
%% Step 2: stratified sample 
if strcmpi(opts.Score,'default')
% normal (van der Waerden score)
 v=-sqrt(2)*erfinv(1-(1:2:(2*n-1))/n)'; % avoid stat toolbox
else
% Step 2a: Alternative weights - needs stats toolbox
%v=(1:2:(2*n-1))'/(2*n);v=v-mean(v);
 if iscell(opts.Params), % cell to argument list
  v=icdf(opts.Score,(1:2:(2*n-1))'/(2*n),opts.Params{:});
 elseif isnumeric(opts.Params)
  v=icdf(opts.Score,(1:2:(2*n-1))'/(2*n),opts.Params);
 else
  v=icdf(opts.Score,(1:2:(2*n-1))'/(2*n));
 end
 v=v-mean(v);
end
%% Step 5: variance
mxx= v'*v/n; % mean(v)==0!
%% Step 4: score matrix M
M=zeros(n,r); M(:,1)=v; %repmat(v,1,r);
%% Step 6: random shuffle
if(ischar(opts.UseQRP))
 for i=2:r
    M(:,i)=v(randperm(n));
 end
else
% Step 6(variant): Use quasirandom permutation
if(isnumeric(opts.UseQRP))
if(opts.UseQRP==0)
 Z=sobolseq(n,r-1);
else
 Z=zeros(n,r-1);
 for(j=1:n), Z(j,:)=lptau(n*(opts.UseQRP-1)+j,r-1); end
end
else
    % treat as function handle
    Z=opts.UseQRP(n,r-1);
end
[dummy,idx]=sort(Z);
for i=2:r
    M(idx(:,i-1),i)=v;   
end
end
%% Step 7: correlation matrix E of M
E=M'*M/mxx;
%% Step 8: Choleski decomposition of E
F=chol(E);
%% Step 1: Choleski decomposition of S
C=chol(S);
%% Step 9: 
A=F\C;
%% Step 10: correlated scores
T=M*A;
%% Step 11: ranks of the elements of T
[R,I]=sort(T);
for i=1:r
    R(I(:,i),i)=1:n;
end
%% Step 12: Inject the rank order of T into X
[Y,J]=sort(X);
for i=1:r
    Y(:,i)=Y(R(:,i),i);
end
%% return permutation
if nargout>1
    P=zeros(n,r);
    for i=1:r
     P(:,i)=J(R(:,i),i);
    end
end
%%
end
