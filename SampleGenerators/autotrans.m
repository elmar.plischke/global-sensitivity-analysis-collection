function x=autotrans(u,distribparamlist,truncation)
%AUTOTRANS Transform unit hypercube using the given parameter list.
%dp={ { 'unif', { 3 ,4 }}, {'norm',{ 1}}, {'beta',{1,7}}}
%x=autotrans(rand(n,3),dp);
[n,k]=size(u);
if(length(distribparamlist)~=k), error('Arguments mismatch.'); end
x=zeros(n,k);
if(nargin<3 || isempty(truncation))
  for i=1:k
    dp=distribparamlist{i};
    distr=dp{1}; %% should check with exist()==2
    if(isa(distr,'function_handle'))
        p=dp{2};
        if(isempty(p))
         x(:,i)=distr(u(:,i));
        else
         x(:,i)=distr(u(:,i),p{:});
        end
    else
        p=dp{2};
        x(:,i)=feval([ distr 'inv'],u(:,i), p{:});
    end
  end
else
  for i=1:k
    dp=distribparamlist{i};
    distr=dp{1}; %% should check with exist()==2
    if(isa(distr,'function_handle'))
        warning(['Assuming quantile truncation for factor ' num2str(k)]);
        x(:,i)=distr(truncation(i,1)+u(:,i)*(truncation(i,2)-truncation(i,1)));
    else
        p=dp{2};
        tr=feval([ distr 'cdf'],truncation(i,:)', p{:});
        x(:,i)=feval([ distr 'inv'],tr(1)+u(:,i)*(tr(2)-tr(1)), p{:});
    end
  end
end
end