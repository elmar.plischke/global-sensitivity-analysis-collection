function y=tricdf(xs,a,b,c)
% TRICDF triangular cumulative distribution
y=zeros(size(xs));
for i=1:numel(xs)
    x=xs(i);
if(x<=a)
    y(i)=0;
elseif( x <= b) 
    y(i)=(x-a).^2/(c-a)/(b-a);
elseif( x <=c)
    y(i)=1-(x-c).^2/(c-a)/(c-b);
else
    y(i)=1;
end %if
end %for
end