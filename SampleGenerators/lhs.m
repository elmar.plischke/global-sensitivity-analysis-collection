function X=lhs(N,n)
%LHS Latin hypercube sample.
%   X=LHS(N,K) creates a K uniform samples of size N.
X=zeros(N,n);
for i=1:n
X(:,i) = (randperm(N)'-rand(N,1))/N;
end
return