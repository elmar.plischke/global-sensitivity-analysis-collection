%%%%%%% Example Session
if ~exist('cosi','file'), setupGSA; end
%% Load a simulation model -- see TestModels

ishigami % provides model(), trafo(), k
n=1024; % set sample size
%% Run Simulation Sensitivity -- see also SampleGeneration
% Compute first and total order sensitivity indices
[S,T]=ihssi(k,n,model,trafo,@sobolpoints)
%% Alternatively, data driven approach
%% Create sample

x=trafo(sobolpoints(n,k)); % QMC design
y=model(x);                % simulate once
%% analyse the I/O sample
% first order indices from available sample
S1a=easi(x,y,8,'Graphics') % spectral method with fast Fourier transform
snapnow
%%
% Requires a signal toolbox license
%S1b=cosi(x,y,8,'Graphics') % spectral method with discrete cosine transform
%snapnow
%%
clf
S2=csnsi(x,y,32,'More gfx')  % cumulative sum of normalized reordered output, 
                          % first order effects via polyline approximation.
						  snapnow
%%
Sij=zeros(k,k);
for i=1:k-1
    for j=i+1:k
     Sij(i,j)=xeasi(x,y,[i,j],['interactions ' num2str([i j])]); % approximative FAST analysis
     snapnow
    end
end
% second order indices
Sij
%% Moment-independent

ginimim(x,y,16,'Ishigami test function') % cdf based
snapnow
%%
l=betaKS3(x,y,16,'cdf-based') %cdf based
snapnow
%%
d=deltamim(x,y,16,'pdf-based')
snapnow
