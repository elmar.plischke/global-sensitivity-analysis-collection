% add subfolders to search path
sep = filesep; % if(ispc), sep='\'; else, sep='/'; end 
dir = fileparts(mfilename('fullpath'));

subdir=strcat('DataSensitivity',sep,...
 {'MomentIndependent','VarianceBased','DiscreteOutput'});
alldir=strcat(dir,sep,[subdir, {'PlotFun','ToolsUtils','SampleGenerators','TestModels',...
    'SimulatorSensitivity','ShapleyEffects'}]);

addpath(alldir{:});

