/*
 * Variance based sensitivity index using cosine trafo
 *
 */
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

const int M=10; // number of harmonics

// helferlein fuer qsort
int dcompare(const void *a,const void *b)
{
  double u=*(const double*)a;
  double v=*(const double*)b;
  if(u<v) return -1;
  else if(u>v) return 1;
  return 0;
}
inline double pow2(double x) { return x*x;}

// varianz mal samplesize
double nvar(double *x,int n)
{
  double *p=x;
  double  c=*p++; // pivot center for numerical stability
  double  v=0.0,m=0.0,d=0.0; // var mean diff
  for(int i=1;i<n;i++) { d=*p++ - c; m+=d;v+=d*d; }
  // printf("mean %g \n",c+m/(double)n);
  return (v-m*m/(double)n); /*/(double)(n-1);*/
}
double vsi(double *x,double *y, int n)
{
  int i,j;
  double coeffs[M];
  // sort y using x as a key
  double *xy=(double*)malloc(2*n*sizeof(double));
  double z;
  double *p=xy,*q=x,*r=y;
  for(i=0;i<n;i++) { *p++=*q++;*p++=*r++;}
  qsort((void*)xy,n,2*sizeof(double),dcompare);
  p=xy;
  // for(i=0;i<n;i++) {printf("%2d %5g %5g\n",i,*p,*p+1));p=p+2;}
  // cosine trafo
  for(j=0;j<M;j++) coeffs[j]=0.0;
  for(i=0;i<n;i++)
  {
    z=xy[2*i+1];
    for(j=0;j<M;j++) 
      coeffs[j]+= z*cos(M_PI*(double)((2*i+1)*(j+1))/(2.0*(double)n));
  }
  free(xy);
  z=0.0;
  for(j=0;j<M;j++) z+=pow2(coeffs[j]);
  return 2*z/n/nvar(y,n); 
}

int main()
{
  const int n=3000;
  int i;
  double x[3][n];	 
  double y[n];

  // ishigami: 3 parameter
  
  for(i=0;i<n;i++) 
    { 
	// Gleichverteilung auf (-pi,pi)
      x[0][i]=(double(rand())/double(RAND_MAX)-.5)*2.0*M_PI;
      x[1][i]=(double(rand())/double(RAND_MAX)-.5)*2.0*M_PI;
      x[2][i]=(double(rand())/double(RAND_MAX)-.5)*2.0*M_PI;
	  // 0.1 ist a, 7.0 ist b
      y[i]=sin(x[0][i])*(1.0+0.1*pow2(pow2(x[2][i])))+7.0*pow2(sin(x[1][i]));
    }
  //for(i=0;i<n;i++) 
  //  printf("%3d %6g %6g %6g %6g\n",i,x[0][i],x[1][i],x[2][i],y[i]);
  for(i=0;i<3;i++)
    {
      double z=vsi(x[i],y,n);
      printf("%5g %5g %5g\n",z, nvar(x[i],n), nvar(y,n));
    }
  return 0;
}
