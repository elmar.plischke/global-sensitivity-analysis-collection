"""
 cosi(x, y, M=6)
 
  Variance based first order sensitivity indices for 
  input matrix x and output vector y using m higher harmonics
  of the cosine transformation.
  
  Ref.: E. Plischke, How to compute variance-based sensitivity indicators with
        your spreadsheet software, EMS
"""
function cosi(x, y, M=6)


 n , k = size(x); 
# discrete cosine transformation weight matrix
 W =zeros(M,n)
 odds = pi*(1:2:2n-1)/(2n)
 for j in 1:M
    W[ j, : ] = cos.( collect(odds*j) )
 end

 Si = zeros(1,k)
 for j in 1:k
# cosine coefficients
  z = sqrt(2/n).*( W * y[sortperm(x[: ,j])]);
# Parseval: work directly on coefficients
  Si[j]  = sum(z .* z);
 end
#return value
 Ey=sum(y)/n;
 Vy=sum((y.-Ey).^2)/(n-1)
 Si./Vy/(n-1)
end
# The Ishigami model
function ishigami(x)  
	sin.(x[:,1]).*(0.1x[:,3].^4 .+ 1).+7sin.(x[:,2]).^2
end

# The input transformation for Ishigami model
ishigamiTrafo(u)=  pi*(2*u.-1)
ishigamiFactors = 4; # including dummy


using Statistics
using Plots
"""
 cusunoro(x, y, M=10)

 cumulative sum of normalized reordered output plot

 Plots the cumulative sum of the normalized reordered output and
 returns an estimate of the first order effects

 x: An input matrix,
 y: A output vector,
 M: Number of interpolation points (defaults to 10). Returns the first order indices.

 Ref.: E. Plischke, Adaptive Correlation Ratio Estimates 
       using Cusunoro Curves, RESS
"""
function cusunoro(x, y, M=10)
 n, k = size(x); 
 c = (1:n)/n
 y = (mean(y,dims=1).-y)./(n*sqrt.(var(y,dims=1)))
 Si = zeros(1,k)

 z = cumsum( y[sortperm(x[ :, 1])]) ;
 # mean squared gradient (with M-1 segments)
 ii = round.(Int,range(1, n, length=M));
 Si[1] = sum(diff(z[ii]).^2)*M;

 h=plot(c, z, xlabel="input quantiles", ylabel="deviation from mean")

 for j in 2:k
  z = cumsum( y[sortperm(x[ :, j])]) ;
  Si[j] = sum(diff(z[ii]).^2)*M;
  plot!(c, z)
 end
 display(h) # render gfx
 Si
end
;