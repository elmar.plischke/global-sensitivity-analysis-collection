
"""
Python implementation of E. Plischke EASI/COSI routines.
"""

import numpy
import scipy.fftpack

__author__ = "Jeremy Laforet"
__email__ = "jlaforet@utc.fr"
__version__ = "0.2"
__license__ = "GPL"


def easi(x, y, M=6):
    """
    Computes sensitivity indices by EASI method.

    :x: n x k inputs
    :y: n x j ouputs
    :M: max. harmonic cutoff (by default M = 6)
    Returns *Si* a vector of k x j sensitivity indices
    """
    n, k = x.shape
    if y.ndim == 1:
        y = y.reshape(y.shape[0], 1)

    nn, kk = y.shape
    if nn != n:
        raise ValueError('Input/Output dimensions mismatch!')

    indexes = numpy.argsort(x, axis=0)

    if n % 2:
        shuff = numpy.hstack(
            (numpy.hstack((range(0, n, 2), range(n - 2, 0, -2)))))
    else:
        shuff = numpy.hstack(
            (numpy.hstack((range(0, n, 2), range(n - 1, 0, -2)))))

    idx_period = [indexes[shuff, i] for i in range(k)]

    if kk == 1:
        yr = [y[idx_period[i]] for i in range(k)]
    else:
        yr = numpy.zeros((n, k, kk))
        for i in range(kk):
            yr[..., i] = y[:, i][idx_period]

    spect = abs(numpy.fft.fft(yr, axis=1)) ** 2 / n
    Vi = 2 * numpy.sum(spect[:, 1:M + 1], axis=1)
    V = numpy.sum(spect[:, 1:], axis=1)
    return Vi / V


def cosi(x, y, M=None):
    """
    Computes sensitivity indices by COSI method.

    :x: n x k inputs
    :y: n x j ouputs
    :M: max. harmonic cutoff
    Returns *Si* a vector of k x j sensitivity indices
    """
    n, k = x.shape
    if y.ndim == 1:
        y = y.reshape(y.shape[0], 1)

    nn, kk = y.shape
    if nn != n:
        raise ValueError('Input/Output dimensions mismatch!')

    indexes = numpy.argsort(x, axis=0)
    if kk == 1:
        yr = [y[indexes[:, i]] for i in range(k)]
    else:
        yr = numpy.zeros((n, k, kk))
        for i in range(kk):
            yr[..., i] = y[:, i][indexes]
    if not M:
        M = max(numpy.ceil(numpy.sqrt(n)), 3)
        print('COSI: Using {} coefficients.\n'.format(M))

    allcoeff = scipy.fftpack.dct(yr, axis=1, norm='ortho')
    V = numpy.sum(allcoeff[:, 1:]**2, axis=1)
    Vi = numpy.sum(allcoeff[:, 1:(M+1)]**2, axis=1)

    return Vi/V
