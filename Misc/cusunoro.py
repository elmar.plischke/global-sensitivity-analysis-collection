import numpy as np
import matplotlib.pyplot as plt

def cusunoro(x, y, M=10):
  ''' CUSUNORO cumulative sum of normalized reordered output plot

  Plots the cumulative sum of the normalized reordered output and
  returns an estimate of the first order effects

  x: An input matrix,
  y: A output vector,
  M: Number of interpolation points (defaults to 10). Returns the first order indices.

  Ref.: E. Plischke, Adaptive Correlation Ratio Estimates 
       using Cusunoro Curves, RESS
  '''
 n,k = x.shape; 

 y = (np.mean(y,dims=1)-y)/(n*np.sqrt(np.var(y,dims=1)))
 Si = np.zeros((1,k))

 z = np.cumsum( y[np.argsort(x[ :, 1])]) ;
 # mean squared gradient (with M-1 segments)
 ii = np.round(np.linspace(1, n, M));
 Si[1] = np.sum(np.diff(z[ii])^2)*M;

 c = np.arange(1.0,n)/n; # cdf
 h=plt(c, z, xlabel="input quantiles", ylabel="deviation from mean")

 for j in np.range(2,k):
  z = np.cumsum( y[np.argsort(x[ :, j])]) ;
  Si[j] = np.sum(np.diff(z[ii]).^2)*M;
  plt(c, z)
 
 display(h) # render gfx
 return (Si)
