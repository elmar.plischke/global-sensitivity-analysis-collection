function y=dwtcdf44(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 s = s - (cpv(d,-1)+ d )/4;
 d = d - s - cpv(s,1);
 s = s-(5*cpv(d,-2)-29*cpv(d,-1)-29*d+5*cpv(d,1))/128;
 s = 2*s;
 d = d/2;

 y = [ dwtcdf44(s) ; d];
end
end
