function [dCv,dCr,dVx,dVy,tAlfa]=dcov(x,y)
%DCOV Distance Covariance function
% DCV=DCOV(X,Y) returns the distance covariance between the vectors X and Y
%               X might also be a matrix
% [DCV,DCR,DVX,DVY,TALPHA]=DCOV(X,Y) additionally returns the
% distance correlation, dist. variance of X and Y and the niveau of the
% independent test (1-alpha/2)

% Ref.: G�BOR J. SZ�KELY,1 MARIA L. RIZZO AND NAIL K. BAKIROV: MEASURING 
% AND TESTING DEPENDENCE BY CORRELATION OF DISTANCES, 
% The Annals of Statistics 2007, Vol. 35, No. 6, 2769�2794
% Szekely Columbia workshop slides 2014: Unbiased estimator

% written by: elmar.plischke@tu-clausthal.de
[n,k]=size(x);
if(k==1)
% now blazingly fast with binary singleton expansion
 a=abs(bsxfun(@minus,x(:),x(:)'));
else    
 xx=sum(x.^2,2);
 a=sqrt(bsxfun(@plus,xx,xx')-2*(x*x')); 
end
b=abs(bsxfun(@minus,y(:),y(:)'));
%sum(a(:))/(n-1)/(n-2)
%sum(b(:))/(n-1)/(n-2)

A=bsxfun(@minus,bsxfun(@minus,a+sum(a(:))/(n-1)/(n-2),...
    sum(a,1)/(n-2)),sum(a,2)/(n-2));
B=bsxfun(@minus,bsxfun(@minus,b+sum(b(:))/(n-1)/(n-2),...
    sum(b,1)/(n-2)),sum(b,2)/(n-2));
dCv=sqrt(sum(mean( A.*B))/(n-3));

dVx=sqrt(max(0,sum(mean( A.^2))/(n-3)));
dVy=sqrt(max(0,sum(mean( B.^2))/(n-3)));
dCr=dCv/sqrt(dVx*dVy);
if(nargout>4)
% needs stats toolbox
    s2=sqrt(mean(a(:))*mean(b(:))/n);
    tAlfa=normcdf(dCv/s2);
end
end
