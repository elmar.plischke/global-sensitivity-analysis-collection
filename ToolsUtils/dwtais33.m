function y=dwtais33(x)
% discrete wavelet trafo average interpolation subdivision
% Alex Stoffel
[n,k]=size(x);
if(n<=4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 s = s + d;
 d = d + ( 16*cpv(s,-1) -8*s-cpv(s,1))/16;
 s = s + (cpv(d,-1) - cpv(d,1))/4;
 d = d/sqrt(2);
 s = s*sqrt(2);
 y = [ dwtais33(s) ; d];
end
end
