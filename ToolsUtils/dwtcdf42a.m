function y=dwtcdf42a(x)
% discrete wavelet trafo cohn daubechies feauveau
% Daubechies Sweldens
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 d = d + ( cpv(s,1) + s )/4;
 s = s + (s + cpv(s,-1));
 d = d - (cpv(d,1)+d)*3/16;
 d = 2*d;
 s = s/2;

 y = [ dwtcdf42a(s) ; d];
end
end
