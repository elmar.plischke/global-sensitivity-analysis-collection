function y=wht(x)
% WHT Walsh Hadamard Transform
  [n,k]=size(x);
  q=log2(n); 
  if (round(q)~=q), error('Size is not a power of two.'),end
  
  nn=2^(q-1);
  if (nn>1)
   y=[ wht( x(1: nn,:)+x(nn+(1: nn),:)) ; wht( x(1: nn,:)-x(nn+(1: nn),:)) ];
  elseif (nn==1)
   y=[ x(1,:)+x(2,:); x(1,:)-x(2,:)];
  else
   y=x; 
  end   
  return
end %function
