function y=dwtcdf22(x)
% discrete wavelet trafo cohn daubechies feauveau
% Uytterhoeven Thesis
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - .5*( s + cpv(s,1));
 s = s + .25*( cpv(d,-1)+ d );
 d = d/2;

 y = [ dwtcdf22(s) ; d];
end
end
