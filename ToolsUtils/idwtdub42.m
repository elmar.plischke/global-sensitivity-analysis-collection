function x=idwtdub42(y)
% inverse discrete wavelet trafo Deslauriers Dubuc 4/2, Binlet 9/7
[n,k]=size(y);
if(n<=4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtdub42(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
	s = s - ( cpv(d,-1)+ d )/4;
    d = d + ( 9*(s + cpv(s,+1))-(cpv(s,-1)+cpv(s,2)))/16;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtdub42(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
	s = s - ( cpv(d,-1)+ d )/4;
    d = d + ( 9*(s + cpv(s,+1))-(cpv(s,-1)+cpv(s,2)))/16;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
