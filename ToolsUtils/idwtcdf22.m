function x=idwtcdf22(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(n,k);
 if(k==1)
    s = idwtcdf22(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s - .25*( cpv(d,-1)+ d );
    d = d + .5*( s + cpv(s,1));
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf22(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s - .25*( cpv(d,-1)+ d );
    d = d + .5*( s + cpv(s,1));
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
