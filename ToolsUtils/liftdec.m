function [coeffs,lens]=liftdec(x,depth,wname)
%LIFTDEC Wavelet decomposition using lifting. 
% [C,L]=LIFTDEC(X,DEPTH,WNAME)
%
% See also wavedec, liftwave, lwt, wavenames
 if(isa(wname,'cell'))
     scheme=wname; 
 else
     scheme=liftwave(wname); 
 end
 coeffs=[];lens=length(x);
 s=x;
 for i=1:depth
  [s,d]=lwt(s,scheme);
  coeffs=[d; coeffs];lens=[length(d);lens];
 end
 coeffs=[s;coeffs]; lens=[length(s);lens];
end