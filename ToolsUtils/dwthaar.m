function y=dwthaar(x)
%DWTHAAR Haar wavelet transformation.
[n,k]=size(x);
 if(n==1) 
  y=x; 
 else
  if(k==1)
      if(mod(n,2)==0)
        sums=x(1:2:end)+x(2:2:end);
        diffs=x(1:2:end)-x(2:2:end);
      else
        sums=x(1:2:end)+x([2:2:end,end]);
        diffs=x(1:2:end)-x([2:2:end,end]);   
      end
  else
      if(mod(n,2)==0)
        sums=x(1:2:end,:)+x(2:2:end,:);
        diffs=x(1:2:end,:)-x(2:2:end,:);
      else
          sums=x(1:2:end,:)+x([2:2:end,end],:);
        diffs=x(1:2:end,:)-x([2:2:end,end],:);
      end
  end
  y=[ dwthaar(sums)/2; diffs/2];
 end
 return