function pred=mst_builtin(xy,gfx)
% MST_BUILTIN Euclidean Minimal Spanning Tree via internal graph mst
% slower than direct implementation for sizes >7000

% construct adjacency matrix
%xy=[x,y];
G=sum(xy.^2,2);
D=bsxfun(@plus,G,G')-2*(xy*xy');
g=graph(D);
[T,pred]=minspantree(g);

if(nargin>1) & ~isempty(gfx)
    i=find(pred==0);
    pred(i)=i;
    plot([xy(pred,1)';xy(:,1)'],[xy(pred,end)';xy(:,end)'],'r-','LineWidth',3);
    title(gfx)
end
end

function testmst
%%
ishigami
n=1000;
x=trafo(rand(n,k));
y=model(x);
for i=1:k
    subplot(1,k,i)
    tic,p=mst_prim_alt([x(:,i),y]);toc
    tic,q=mst_builtin([x(:,i),y]);toc
p(1)=1;
j=find(q==0);q(j)=j;
plot([x(p,i)';x(:,i)'],[y(p)';y'],'r-',...
    .1+[x(q,i)';x(:,i)'],.1+[y(q)';y'],'g-',...
    'LineWidth',3);
%pause(.2)
end
%%
end
