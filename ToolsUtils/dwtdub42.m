function y=dwtdub42(x)
% discrete wavelet trafo Deslauriers Dubuc 4/2, Binlet 9/7
% Alex Stoffel
[n,k]=size(x);
if(n<=4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - ( 9*(s + cpv(s,+1))-(cpv(s,-1)+cpv(s,2)))/16;
 s = s + ( cpv(d,-1)+ d )/4;
 d = d/2;

 y = [ dwtdub42(s) ; d];
end
end
