function y=dwtcdf55(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - s/5;
 s = s - (15*cpv(d,-1)+ 5*d)/24;
 d = d - (15*s+ 9*cpv(s,1))/10;
 s = s - (35*cpv(d,-2)-230*cpv(d,-1)-768*d+230*cpv(d,1)-35*cpv(d,2))/2304; 
 d = d/3;
 s = 3*s;

 y = [ dwtcdf55(s) ; d];
end
end
