function y=dwtsym(x)
% discrete wavelet trafo symlet
% Alex Stoffel
[n,k]=size(x);
if(n<=2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d -2.556583965520256*cpv(s,1);
 s = s -0.01903120704675512*d+0.3392439918648426*cpv(d,-1);
 d = d +1.05905726913757*s+5.87692797927752*cpv(s,1);
 s = s +0.03526079391665825*d-0.06598460741210342*cpv(d,-1);
 d = d -4.344017409218308*s;
 s = s * 3.195938969614102/sqrt(2);
 d = d / 3.195938969614102*sqrt(2);
 y = [ dwtsym(s) ; d];
end
end
