function y=dwtcdf31(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 s = s - cpv(d,-1)/3;
 d = d - (9*s + 3*cpv(s,1))/8;
 s = s + 4*d/9;
 d = sqrt(2)*d/3;
 s = 3*s/sqrt(2);

 y = [ dwtcdf31(s) ; d];
end
end
