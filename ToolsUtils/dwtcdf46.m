function y=dwtcdf46(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n); 
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:); 
 end
 s = s -( cpv(d,-1)+ d )/4;
 d = d - s - cpv(s,1);
 s = s-(-35*cpv(d,-3)+265*cpv(d,-2)-998*cpv(d,-1)-998*d+265*cpv(d,1)-35*cpv(d,2))/4096;
 s = 2*s;
 d = d/2;

 y = [ dwtcdf46(s) ; d];
end
end
