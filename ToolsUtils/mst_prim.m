function [p,d]=mst_prim(xy,gfx)
% MST_PRIM Minimal Spanning Tree after PRIM
% Source: Blum: Algorithmen, Oldenbourg 
[n,k]=size(xy);

Q=1:n;
d=inf(n,1);d(1)=0;
p=zeros(n,1);
%euclid=@(v)sum(v.^2);
while(~isempty(Q))
    [~,i]=min(d(Q));
    u=Q(i);Q(i)=[];
%    xyu=xy(u,:);
    w=arrayfun(@(v)sum((xy(u,:)-xy(v,:)).^2),Q)';
    ii=w<d(Q);
    d(Q(ii))=w(ii);
    p(Q(ii))=u;
end

if(nargin>1)
p(1)=1;plot([xy(p,1)';xy(:,1)'],[xy(p,2)';xy(:,2)'],'k-');
title(gfx);
end
d=sqrt(d);
end

