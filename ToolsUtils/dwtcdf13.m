function y=dwtcdf13(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - s;
 s = s + ( -cpv(d,-1)+ 8*d+cpv(d,1))/16;
 d = d/2;

 y = [ dwtcdf13(s) ; d];
end
end
