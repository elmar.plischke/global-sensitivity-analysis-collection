function y=dwtais53(x)
% discrete wavelet trafo average interpolation subdivision
% Alex Stoffel
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 s = s + d;
 d = d - ( 3*cpv(s,-2) + 22*cpv(s,-1) -64*s-22*cpv(s,1)+3*cpv(s,2))/256;
 s = s + (cpv(d,-1) - cpv(d,1))/4;
 d = d*sqrt(2);
 s = s/sqrt(2);
 y = [ dwtais53(s) ; d];
end
end
