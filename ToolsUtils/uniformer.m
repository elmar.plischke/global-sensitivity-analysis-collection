function [yunif,yunif2]=uniformer(y,y2) 
% UNIFORMER Transform to empirical cdfs.
% Ties are ignored.

if(nargin==2)
    y=[y,y2];
end
[n,k]=size(y);
yunif=zeros(n,k); 

[indx1,indx2]=sort(y);

for j=1:k 
  yunif(indx2(:,j),j)= ((1:n)-.5)/n;
end 

if(nargout==2)
    yunif2=yunif(:,end);yunif=yunif(:,1:end-1);
end

end 