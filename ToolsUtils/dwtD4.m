function y=dwtD4(x)
% Daubechies D4 wavelet trafo
% Daubechies Sweldens
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - sqrt(3)*s;
 s = s + (sqrt(3)*d + (sqrt(3)-2)*cpv(d,1))/4;
 d = d + cpv(s,-1);
% s = (sqrt(3)+1)/sqrt(2) * s;
% Mathematica - gleicher faktor, 2 zerfaellt 
 s = s/(sqrt(3)-1);
 d = (sqrt(3)-1) *d;
 y = [ dwtD4(s) ; d ];
 end
