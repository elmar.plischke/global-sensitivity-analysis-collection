function x=idwtcdf64(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf64(y(1:(n/2)))/(4*sqrt(2)); d=y(((n/2)+1):n)*(4*sqrt(2));
    s = s + ( 35*cpv(d,-2)- 195*cpv(d,-1)- 195*d +35*cpv(d,1))/1024;
    d = d + ( s + cpv(s,1) )*(4/3);
    s = s + ( cpv(d,-1)+ d )*(9/16);
    d = d + ( s+ cpv(s,1) )/6;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf64(y(1:(n/2),:))/(4*sqrt(2)); d=y(((n/2)+1):n,:)*(4*sqrt(2));
    s = s + ( 35*cpv(d,-2)- 195*cpv(d,-1)- 195*d +35*cpv(d,1))/1024;
    d = d + ( s + cpv(s,1) )*(4/3);
    s = s + ( cpv(d,-1)+ d )*(9/16);
    d = d + ( s+ cpv(s,1) )/6;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
