function x=idwtcdf42(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf42(y(1:(n/2)))/sqrt(8); 
    d = y(((n/2)+1):n)*sqrt(8);
    s = s-(3*cpv(d,-1)+3*d)/16;
    d = d + s + cpv(s,1);
    s = s +(d + cpv(d,-1))/4;
    x(2:2:n) = d;
	x(1:2:(n-1))= s; 
 else
    s = idwtcdf42(y(1:(n/2),:))/sqrt(8); 
    d = y(((n/2)+1):n,:)*sqrt(8);
    s = s-(3*cpv(d,-1)+3*d)/16;
    d = d + s + cpv(s,1);
    s = s +(d + cpv(d,-1))/4;
    x(2:2:n,:) = d;
	x(1:2:(n-1),:)= s;  
 end
end
end
