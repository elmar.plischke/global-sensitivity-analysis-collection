function x=idwthaar(y)
%IDWTHAAR Inverse Haar wavelet transformation.
  [n,k]=size(y);
  if(n==1), x=y;
  else
       N=pow2(ceil(log2(n)));
       if(N~=n)
           y=[y;zeros(N-n,k)];
           n=N;
       end
  if(k==1)
   x=  reshape([1;1]*idwthaar(y(1:(n/2)))',n,1);
   x=x+reshape([1;-1]*y((n/2+1):end)',n,1);
  else
   x=  kron(idwthaar(y(1:(n/2),:)),[1;1]);
   x=x+kron(y((n/2+1):end,:),[1;-1]);
  end
 %  x=x/sqrt(2);
  end
