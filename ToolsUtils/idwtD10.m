function x=idwtD10(y)
% inverse discrete wavelet trafo daubechies
[n,k]=size(y);
if(n<=8) 
  x=y; 
else
 x=zeros(n,k);
 if(k==1)
    s = idwtD10(y(1:(n/2)))/0.347389; 
    d = y(((n/2)+1):n)*0.347389;
    d=d-(0.172573*cpv(s,4)-1.10315*cpv(s,3));
    s=s-(0.0157993*cpv(d,-3)-0.0503964*cpv(d,-2));
    d=d-(3.03369*cpv(s,2)-7.59758*cpv(s,1));
    s=s-(0.0698843*cpv(d,-1)- 0.247729*d);
    d=d-3.77152*s;
    x(1:2:(n-1))= s;
    x(2:2:n) = d;
 else
     s = idwtD10(y(1:(n/2),:))/0.347389; 
    d = y(((n/2)+1):n,:)*0.347389;
    d=d-(0.172573*cpv(s,4)-1.10315*cpv(s,3));
    s=s-(0.0157993*cpv(d,-3)-0.0503964*cpv(d,-2));
    d=d-(3.03369*cpv(s,2)-7.59758*cpv(s,1));
    s=s-(0.0698843*cpv(d,-1)- 0.247729*d);
    d=d-3.77152*s;
    x(1:2:(n-1),:)= s;
    x(2:2:n,:) = d;    
 end
end

