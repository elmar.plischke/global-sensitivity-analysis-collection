function y=fyt(x,A)
% FYT Fast Yates transformation for vectors in natural (binary) order.  
% Y=FYT(X)   returns the interaction effects of a 2^d factorial design
% Y=FYT(X,A) uses the Kronecker kernel matrix A instead of [1 1 ; -1 1]

[n,k]=size(x);
if(nargin<2)
 A=[1,1;-1,1];
 h=2;
else
    h=size(A,1);
    if(h~=size(A,2))
     error('fyt: square matrix expected');
    end
end

d=round(log(n)/log(h));
krunch=reshape(x(:),h,(n*k)/h);
for i=1:d
    krunch=A*krunch;
    krunch=reshape(krunch.',h,(n*k)/h); % handle complex case
end
y=reshape(krunch,k,n).';
end


function testit
%%
% 3^2 design of Good
fyt([3 7 4 1 8 11 2 1 5]',[ 1 1 1; -1 0 1; 1 -2 1]) % 42 14 -6 -6 2 12 -18 -16 6
%%
end