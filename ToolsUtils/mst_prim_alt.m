function [p,d]=mst_prim_alt(xy,gfx)
% MST_PRIM_ALT Minimal Spanning Tree after PRIM
% Alternative version after profiling
% Source: Blum: Algorithmen, Oldenbourg 
[n,k]=size(xy);

Q=1:n;
d=inf(n,1);d(1)=0;
p=zeros(n,1);

while(~isempty(Q))
    [~,i]=min(d(Q));
    u=Q(i);Q(i)=[];
    for v=Q        
            w=sum((xy(u,:)-xy(v,:)).^2);
            if(w<d(v))
                d(v)=w;
                p(v)=u;
            end
    end
end
d=sqrt(d);
if(nargin>1)
p(1)=1;plot([xy(p,1)';xy(:,1)'],[xy(p,2)';xy(:,2)'],'r-','LineWidth',3);
title(gfx);
end
end

