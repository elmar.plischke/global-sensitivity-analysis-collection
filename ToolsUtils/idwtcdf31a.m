function x=idwtcdf31a(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = -2*idwtcdf31a(y(1:(n/2))); 
    d = -y(((n/2)+1):n);
	s = s + 4*cpv(d,2);
	d = d - (cpv(s,-2) + 3*cpv(s,-1))/8;
	s = s + 3*d;
    x(2:2:n) = d;
	x(1:2:(n-1))= s; 
 else
    s = -2*idwtcdf31a(y(1:(n/2),:)); 
    d = -y(((n/2)+1):n,:);
	s = s + 4*cpv(d,2);
	d = d - (cpv(s,-2) + 3*cpv(s,-1))/8;
	s = s + 3*d;
    x(2:2:n,:) = d;
	x(1:2:(n-1),:)= s;  
 end
end
end
