function d=dcov2(x,y)
% Distance Covariance
[n,k]=size(x);
[m,l]=size(y);
if(l>1) && (k>1), error('Sorry only vectors'); end
if(n~=m), error('Oops, mismatch'); end
%%
    A=abs(bsxfun(@minus,x,x'));
    B=abs(bsxfun(@minus,y,y'));
%    plot(A(:),B(:),'*');
   % d=sqrt(sum(A(:).*B(:))/n/(n-1)-sum(A(:))/n/(n-1)*sum(B(:))/n/(n-1));
    d=sqrt(mean(A(:).*B(:))-2*mean(A,1)*mean(B,2)/n+mean(A(:))*mean(B(:)));
%    disp([mean(A(:).*B(:)),mean(A,1)*mean(B,2)/n,mean(A(:)),mean(B(:))]);
end