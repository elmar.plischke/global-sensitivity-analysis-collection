function y=dwtcdf42(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 s = s - (cpv(d,-1) + d )/4;
 d = d - (s + cpv(s,1));
 s = s + (3*cpv(d,-1)+3*d)/16;
 d = d/sqrt(8);
 s = sqrt(8)*s;

 y = [ dwtcdf42(s) ; d];
end
end
