function x=idwtcdf26(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf26(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s - (5*cpv(d,-3)-39*cpv(d,-2)+162*cpv(d,-1)+162*d-39*cpv(d,1)+5*cpv(d,2))/512;
    d = d + .5*( s + cpv(s,1));
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf26(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s - (5*cpv(d,-3)-39*cpv(d,-2)+162*cpv(d,-1)+162*d-39*cpv(d,1)+5*cpv(d,2))/512;
    d = d + .5*( s + cpv(s,1));
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
