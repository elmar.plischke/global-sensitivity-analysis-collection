function y=dwtcdf62(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - ( s+ cpv(s,1) )/6;
 s = s - ( cpv(d,-1)+ d )*(9/16);
 d = d - ( s + cpv(s,1) )*(4/3);
 s = s + ( cpv(d,-1)+ d)*(5/32);
 s = 4*sqrt(2)*s;
 d = d/(4*sqrt(2));

 y = [ dwtcdf62(s) ; d];
end
end
