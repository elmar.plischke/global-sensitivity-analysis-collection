function x=idwtD6(y)
% inverse discrete wavelet trafo daubechies
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(n,k);
 if(k==1)
    s = idwtD6(y(1:(n/2)))/1.9182029462*sqrt(2); 
    d = y(((n/2)+1):n)*1.9182029462/sqrt(2); 
    d = d + 0.3896203900*s;
	s = s - ( 0.0284590896*d+ 0.4921518449*cpv(d,-1) );
	d = d + ( 1.565136796*cpv(s,1) - 0.3523876576*s);
	x(1:2:(n-1)) = s + 0.4122865950*d;
    x(2:2:n) = d; 
 else
    s = idwtD6(y(1:(n/2),:))/1.9182029462*sqrt(2); 
    d = y(((n/2)+1):n,:)*1.9182029462/sqrt(2);
    d = d + 0.3896203900*s;
	s = s - ( 0.0284590896*d+ 0.4921518449*cpv(d,-1) );
	d = d + ( 1.565136796*cpv(s,1) - 0.3523876576*s);
	x(1:2:(n-1),:) = s + 0.4122865950*d;
    x(2:2:n,:) = d; 
 end
end
end
