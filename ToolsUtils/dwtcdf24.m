function y=dwtcdf24(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - .5*( s + cpv(s,1));
 s = s -( 3*cpv(d,-2)-19*cpv(d,-1)- 19*d +3*cpv(d,1))/64;
 d = d/2;

 y = [ dwtcdf24(s) ; d];
end
end
