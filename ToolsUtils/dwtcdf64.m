function y=dwtcdf64(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - ( s+ cpv(s,1) )/6;
 s = s - ( cpv(d,-1)+ d )*(9/16);
 d = d - ( s + cpv(s,1) )*(4/3);
 s = s - ( 35*cpv(d,-2)- 195*cpv(d,-1)- 195*d +35*cpv(d,1))/1024;
 s = 4*sqrt(2)*s;
 d = d/(4*sqrt(2));

 y = [ dwtcdf64(s) ; d];
end
end
