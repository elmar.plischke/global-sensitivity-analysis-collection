function x=idwtcdf31(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = sqrt(2)*idwtcdf31(y(1:(n/2)))/3; 
    d = 3*y(((n/2)+1):n)/sqrt(2);
    s = s - 4*d/9;
    d = d + (9*s + 3*cpv(s,1))/8;
    s = s + cpv(d,-1)/3;
	x(2:2:n) = d;
    x(1:2:(n-1))= s;
 else
    s = sqrt(2)*idwtcdf31(y(1:(n/2),:))/3; 
    d = 3*y(((n/2)+1):n,:)/sqrt(2);
    s = s - 4*d/9;
    d = d + (9*s + 3*cpv(s,1))/8;
    s = s + cpv(d,-1)/3;
	x(2:2:n,:) = d;
    x(1:2:(n-1),:)= s;
 end
end
end
