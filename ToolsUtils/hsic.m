function h=hsic(x,y,Kx,Ky)
% HSCI Hilbert Schmidt Information Criterion

[n,k]=size(x);

% linear SVM
kernelSVM=@(x,y)y*x';
% polynomial SVM (t intercept, d degree)
kernelPLY=@(x,y,t,d)(y*x'+t)^d;
% RBF (radial basis function) kernel (sigma2 variance)
kernelRBF=@(x,y,sigma)...
exp(-(bsxfun(@plus,sum(x.*x,2),sum(y.*y,2)')-2*x*y')/sigma^2);
%exp(-bsxfun(@(x,y)sum( bsxfun(@rdivide,x-y,sigma).^2,2),x,y));
% MLP (multilayer perceptron) SVM (kappa scale, theta bias)
kernelMLP=@(x,y,kappa,theta)tanh(kappa*y*x'+theta);

if(nargin==2)
%sx=median(abs(bsxfun(@minus,median(x),x)));%/(0.675)*((4/(3*n))^(1/5));
%sy=median(abs(bsxfun(@minus,median(y),y)));%/(0.675)*((4/(3*n))^(1/5));
sx=mean(std(x)); %*((4/(3*n))^(1/5))
sy=std(y); %*((4/(3*n))^(1/5))
Kx=@(x1,x2)kernelRBF(x1,x2,sx);
Ky=@(y1,y2)kernelRBF(y1,y2,sy);
end

OmegaX=zeros(n,n);
OmegaY=zeros(n,n);
for(i=1:n)
        if(k>1)
            omegax=Kx(x(i,:),x(i:n,:));
        else
            omegax=Kx(x(i),x(i:n));
        end
      omegay=Ky(y(i),y(i:n));
      OmegaX(i,i:n)=omegax;
      OmegaX(i:n,i)=omegax;
      OmegaY(i,i:n)=omegay;
      OmegaY(i:n,i)=omegay;
%    if(k>1)
%        OmegaX(i,:)=Kx(x(i,:),x);
%    else
%        OmegaX(i,:)=Kx(x(i),x);
%    end
%    OmegaY(i,:)=Ky(y(i),y);
end
OmegaX=OmegaX*(eye(n,n)-ones(n,n)/n);
OmegaY=OmegaY*(eye(n,n)-ones(n,n)/n);
%mx=mean(OmegaX(:));my=mean(OmegaY(:));
%% trace OmegaX*(I-ones/n) * OmegaY*(I-ones/n)
h(1)=( OmegaX(:)'*OmegaY(:));
h(2)=h(1)./ sqrt( (OmegaX(:)'*OmegaX(:))...
 *(OmegaY(:)'*OmegaY(:)));
h(1)=h(1)/(n-1)^2;
%if(n>50)
%ii=randperm(n*n); 
%plot(OmegaX(ii(1:1000)),OmegaY(ii(1:1000)),'.');
%end
end
