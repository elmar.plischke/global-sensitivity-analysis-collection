function x=idwtD4(y)
% Daubechies Inverse D4 wavelet trafo
% Source: Daubechies Sweldens
[n,k]=size(y);
if(n<=2) 
  x=y; 
else
  x=zeros(n,k);
  if(k==1)
    d = y(((n/2)+1):n)/(sqrt(3)-1);
    s = (sqrt(3)-1)*idwtD4(y(1:(n/2)));    
    d = d - cpv(s,-1);
    s = s - (sqrt(3)*d + (sqrt(3)-2)*cpv(d,1))/4;
    d = d + sqrt(3)*s;
    x(1:2:(n-1)) = s;
    x(2:2:n)     = d;
  else
    d = y(((n/2)+1):n,:)/(sqrt(3)-1);
    s = (sqrt(3)-1)*idwtD4(y(1:(n/2),:));    
    d = d - cpv(s,-1);
    s = s - (sqrt(3)*d + (sqrt(3)-2)*cpv(d,1))/4;
    d = d + sqrt(3)*s;
    x(1:2:(n-1),:) = s;
    x(2:2:n,:)     = d;
  end
end
end
