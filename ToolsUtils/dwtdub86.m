function y=dwtdub86(x)
% discrete wavelet trafo Deslauriers Dubuc 8/6
% Alex Stoffel
[n,k]=size(x);
if(n<=8)
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d + (2^-11)*(5*cpv(s,-3)-49*cpv(s,-2) +245*cpv(s,-1)-1225*s...
    -1225*cpv(s,1)+245*cpv(s,2)-49*cpv(s,3) +5*cpv(s,4));
 s = s + (2^-9)*(3*cpv(d,-3)-25*cpv(d,-2) +150*cpv(d,-1)...
     +150*d-25*cpv(d,1)+3*cpv(d,2));
 d = d/2;

 y = [ dwtdub86(s) ; d];
end
end
