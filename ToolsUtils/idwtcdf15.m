function x=idwtcdf15(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf15(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s - ( 3*cpv(d,-2)-22*cpv(d,-1)+ 128*d+22*cpv(d,1)-3*cpv(d,2))/256;
    d = d + s;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf15(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s - ( 3*cpv(d,-2)-22*cpv(d,-1)+ 128*d+22*cpv(d,1)-3*cpv(d,2))/256;
    d = d + s;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
