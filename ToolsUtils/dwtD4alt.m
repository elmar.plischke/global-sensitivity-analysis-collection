function y=dwtD4alt(x)
% Daubechies D4 wavelet trafo
% Source: Daubechies Sweldens
[n,k]=size(x);
if(n==2) 
  y=x; 
else
  s = x(1:2:(n-1));
  d = x(2:2:n) - sqrt(3)* s([2:end,1]);
  s = s + sqrt(3)/4*d - (sqrt(3)-2)/4*d([end,1:end-1]);
  d = d + s([2:end,1]);
  s = s*(sqrt(3)+1)/sqrt(2);
  d = d*(sqrt(3)-1)/sqrt(2);
%  y = [ dwtD4alt(s); d];
  y =[ s; d];
end
end
