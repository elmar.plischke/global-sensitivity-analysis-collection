function x=idwtcdf11(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf11(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s - d/2;
    d = d + s;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf11(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s - d/2;
    d = d + s;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
