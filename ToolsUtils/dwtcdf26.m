function y=dwtcdf26(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - .5*( s + cpv(s,1));
 s = s +( 5*cpv(d,-3)-39*cpv(d,-2)+162*cpv(d,-1)+162*d-39*cpv(d,1)+5*cpv(d,2))/512;
 d = d/2;

 y = [ dwtcdf26(s) ; d];
end
end
