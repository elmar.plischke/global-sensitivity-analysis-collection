function y=dwtD10(x)
% discrete wavelet trafo daubechies 
% Maslen Abbot
[n,k]=size(x);
if(n<=8) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
d=d+3.77152*s;
s=s+(0.0698843*cpv(d,-1)- 0.247729*d);
d=d+(3.03369*cpv(s,2)-7.59758*cpv(s,1));
s=s+(0.0157993*cpv(d,-3)-0.0503964*cpv(d,-2));
d=d+(0.172573*cpv(s,4)-1.10315*cpv(s,3));
s=s*0.347389;
d=d/0.347389;
y = [ dwtD10(s) ; d];
end
end
