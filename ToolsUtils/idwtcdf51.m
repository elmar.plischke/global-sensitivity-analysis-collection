function x=idwtcdf51(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf51(y(1:(n/2)))/3; 
    d = 3*y(((n/2)+1):n);
    s = s - d/3;
    d = d + (15*s+ 9*cpv(s,1))/10;
    s = s + (15*cpv(d,-1)+ 5*d)/24;
    d = d + s/5;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf51(y(1:(n/2),:))/3; 
    d = 3*y(((n/2)+1):n,:);
    s = s - d/3;
    d = d + (15*s+ 9*cpv(s,1))/10;
    s = s + (15*cpv(d,-1)+ 5*d)/24;
    d = d + s/5;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
