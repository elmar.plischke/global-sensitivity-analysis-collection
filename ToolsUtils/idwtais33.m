function x=idwtais33(y)
% inverse discrete wavelet trafo average interpolation subdivision
[n,k]=size(y);
if(n<=4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtais33(y(1:(n/2)))/sqrt(2); 
    d = y(((n/2)+1):n)*sqrt(2);
	s = s - (cpv(d,-1) - cpv(d,1))/4;
	d = d - ( 16*cpv(s,-1) -8*s-cpv(s,1))/16;
	s = s - d;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtais33(y(1:(n/2),:))/sqrt(2); 
    d = y(((n/2)+1):n,:)*sqrt(2);
	s = s - (cpv(d,-1) - cpv(d,1))/4;
	d = d - ( 16*cpv(s,-1) -8*s-cpv(s,1))/16;
	s = s - d;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
