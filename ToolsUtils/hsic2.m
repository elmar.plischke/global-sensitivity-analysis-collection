function [t,tboot]=hsic2(x,y,s)
%HSIC Hilbert-Schmidt Independence Criterion 
%(inspired by Gretton 2007 implementation)
if(nargin<3), s=[-1,-1]; end

m=size(x,1);

if (s(1)==-1)
 k=min(100,m);
 G=sum(x(1:k,:).^2,2);
 dists=bsxfun(@plus,G,G')-2*x(1:k,:)*x(1:k,:)';
 dists=triu(dists,1);
 dists=dists(:);dists=dists(dists>0);
 s(1)=sqrt(0.5*median(dists));
end
if(s(2)==-1)
 k=min(100,size(y,1));
 G=sum(y(1:k,:).^2,2);
 dists=bsxfun(@plus,G,G')-2*y(1:k,:)*y(1:k,:)';
 dists=triu(dists,1);
 dists=dists(:);dists=dists(dists>0);
 s(2)=sqrt(0.5*median(dists));  
end
%disp(['Kernel bandwidths:    ' num2str(s)])
K = rbf_dot(x,x,s(1));
%[L,Ldist] = rbf_dot(y,y,s(2));
L = rbf_dot(y,y,s(2));

H = eye(m)-1/m*ones(m,m);

Kc = H*K*H;
t = 1/m^2*sum(sum(Kc'.*L));
%vec=@(X)X(:);
%edist= sqrt(vec(sqrt(Kdist)*H)'*vec(sqrt(Ldist)*H))/m

if(nargout>1)
 nboot=500;
 tboot=zeros(1,nboot);
 for i=1:nboot
    indx=randperm(m);
    tboot(i)=1/m^2*sum(sum(Kc'.*L(indx,indx)));
 end
%[t,tboot]=hsic2(x(:,4),y);hist(tboot,50),hold on,plot(t,0,'ro');hold off
end
end
function [r,dists]=rbf_dot(patterns1,patterns2,deg)

G = sum((patterns1.*patterns1),2);
H = sum((patterns2.*patterns2),2);
dists=bsxfun(@plus,G,H') - 2*patterns1*patterns2';
r = exp( -dists/2/deg.^2);
end