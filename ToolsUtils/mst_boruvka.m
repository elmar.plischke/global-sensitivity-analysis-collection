function [p,d]=mst_boruvka(x)
% MST_BORUVKA Minimal Spanning Tree after Boruvka

[n,k]=size(x);

oneton=1:n;
c=oneton;
p=zeros(n,1);
d=inf(n,1);

for i=1:(n-1)
    ci=c(i); % component
    if(ci==i)  % not touched
        ff=[1:i-1,i+1:n]; %oneton;ff(i)=[]; % index into complement
        iix=[];
    else
        cc=c==ci; % already part of a larger component
        ff=oneton(~cc);
        cc(i)=false;
        iix=find(cc);
    end
    xf=x(ff,:);    
    % find the minimal distance between component cc and complement

    [d,ll]=min(sum((x(i,:)-xf).^2,2));l=ff(ll); kk=i;
    if(~isempty(iix)),for ii=iix
        [dd,ll]=min(sum((x(ii,:)-xf).^2,2));
        if(dd<d), d=dd; l=ff(ll); kk=ii; end
    end,end
    % connect kk with l, reroot if parent is set
    s=kk;r=l;
    while(s~=0)
      q=p(s);p(s)=r;r=s;s=q;
    end
    % set length 
    d(kk)=d;
    % enlarge component
    cl=c(l);
    c(c==cl)=ci; 
    %gfx
    if(0)
    qq=p;
    rr=oneton;
    qq(qq==0)=rr(qq==0);
    plot([x(qq,1),x(:,1)]',[x(qq,2),x(:,2)]','ko-');drawnow
    end
end
end
end  
