function y=dwtcdf31a(x)
% discrete wavelet trafo cohn daubechies feauveau
% Uytterhoeven: Thesis
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 s = s - 3*d;
 d = d + (cpv(s,-2) + 3*cpv(s,-1))/8;
 s = s - 4*cpv(d,2);
 d = -d;
 s = -1/2*s;

 y = [ dwtcdf31a(s) ; d];
end
end
