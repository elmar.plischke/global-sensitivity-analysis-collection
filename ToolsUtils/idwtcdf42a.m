function x=idwtcdf42a(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf42a(y(1:(n/2)))*2; 
    d = y(((n/2)+1):n)/2;
    d = d + (cpv(d,1)+d)*3/16;
    s = s - (s + cpv(s,-1));
    d = d - ( cpv(s,1) + s )/4;
    x(2:2:n) = d;
	x(1:2:(n-1))= s; 
 else
    s = idwtcdf42a(y(1:(n/2),:))*2; 
    d = y(((n/2)+1):n,:)/2;
    d = d + (cpv(d,1)+d)*3/16;
    s = s - (s + cpv(s,-1));
    d = d - ( cpv(s,1) + s )/4;
    x(2:2:n,:) = d;
	x(1:2:(n-1),:)= s;  
 end
end
end
