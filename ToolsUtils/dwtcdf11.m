function y=dwtcdf11(x)
% discrete wavelet trafo cohn daubechies feauveau
% Uytterhoeven  Thesis
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - s;
 s = s + d/2;
 d = d/2;
 y = [ dwtcdf11(s) ; d];
end
end
