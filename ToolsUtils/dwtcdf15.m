function y=dwtcdf15(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - s;
 s = s + ( 3*cpv(d,-2)-22*cpv(d,-1)+ 128*d+22*cpv(d,1)-3*cpv(d,2))/256;
 d = d/2;
 y = [ dwtcdf15(s) ; d];
end
end
