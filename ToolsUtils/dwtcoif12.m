function y=dwtcoif12(x)
% discrete wavelet trafo coiflet12
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  s = x(1:2:n-1);
  d = x(2:2:n);
 else
  s = x(1:2:n-1,:);
  d = x(2:2:n,:);
 end
 d = d - 0.3952094886*s;
 s = s - 0.4865531265*cpv(d,-1)+0.3418203790*d;
 d = d + 0.1023563847*s+0.4940618204*cpv(s,1);
 s = s + 1.479728699*cpv(d,-1)+0.3648016173*d;
 d = d + 9.491856450*cpv(s,-1)-2.017253240*s;
 s = s - 0.02528002562*cpv(d,1)-0.02182215161*cpv(d,2);
 d = d - 12.46443692*cpv(s,-1);
 %s = 0.7403107249*s/sqrt(2);
 %d = 1.350784159*d*sqrt(2);
 s = s / 1.910297278;
 d = d * 1.910297278;
 y = [ dwtcoif12(s) ; d];
end
end
