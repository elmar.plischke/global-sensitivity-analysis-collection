function y=dwtcdf66(x)
% discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - ( s+ cpv(s,1) )/6;
 s = s - ( cpv(d,-1)+ d )*(9/16);
 d = d - ( s + cpv(s,1) )*(4/3);
 s = s + ( 63*cpv(d,-3)-469*cpv(d,-2)+ 1686*cpv(d,-1)+ ...
           1686*d -469*cpv(d,1) +63*cpv(d,2))/8192;
 s = 4*sqrt(2)*s;
 d = d/(4*sqrt(2));

 y = [ dwtcdf66(s) ; d];
end
end
