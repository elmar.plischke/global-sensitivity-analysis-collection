function x=liftrec(coeffs,lens,wname)
%LIFTREC Wavelet reconstruction using lifting
% X=LIFTDEC(C,L,WNAME)
%
% See also waverec, liftwave, ilwt, wavenames
 if(isa(wname,'cell'))
     scheme=wname; 
 else
     scheme=liftwave(wname); 
 end
 
 x=coeffs(1:lens(1));coeffs(1:lens(1))=[];lens(1)=[]; 
 while length(x)<lens(end)
    x=ilwt(x,coeffs(1:lens(1)),scheme);
    coeffs(1:lens(1))=[];lens(1)=[]; 
 end
end