function x=idwtcdf24(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf24(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s + ( 3*cpv(d,-2)-19*cpv(d,-1)- 19*d +3*cpv(d,1))/64;
    d = d + .5*( s + cpv(s,1));
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf24(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s + ( 3*cpv(d,-2)-19*cpv(d,-1)- 19*d +3*cpv(d,1))/64;
    d = d + .5*( s + cpv(s,1));
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
