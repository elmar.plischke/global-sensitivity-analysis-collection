function indx=c2i(coord,nBits)
%% C2I  coordinate to index using Hilbert curve
% IND=C2I(X,B) 
%  X: N x D matrix containing values between 0 and 2^B-1
%  B: Number of bits per dimension
%
% Derived from Doug Moore's implementation of the Butz algorithm
% Ken Musgrave: A Peano curve generation algorithm, Graphics Gems II,
% [The algorithm] "is based on bitwise boolean operations, 
% and is the only algorithm we have ever coded where we had 
% absolutely no insight into how it works." 
[nRuns,nDims]=size(coord);

if nDims >1
%%    
    nDimsBits=nDims*nBits;
    if(nDimsBits>64), error('Sorry, no bits left for you.'); end
    coords=zeros(nRuns,1,'uint64'); 
    % bitpack the data
    for d=nDims:-1:1
        coords=bitshift(coords,nBits);
        coords=bitor(coords,uint64(coord(:,d)));
    end 
%disp(dec2bin(coords))	
%%    
    if (nBits>1)
%%        
        ndOnes   = bitshift(1,nDims)-1; 
        nd1Ones  = bitshift(ndOnes,-1);
        nthbits  = (bitshift(1,nDimsBits)-1)/ndOnes;
        rotation = zeros(nRuns,1);
        flipBit  = zeros(nRuns,1,'uint64'); 
%%      
        coords = bittrans(coords,nDims,nBits);
        coords = bitxor(coords, bitshift(coords,-nDims));
%disp('bittrans');disp(dec2bin(coords))     
        indx=zeros(nRuns,1,'uint64');
        for b= nBits-1:-1:0
            bits = bitand( bitshift(coords, -b*nDims), ndOnes);
            bits = rotateRight( bitxor( flipBit, bits), rotation, nDims);
 %           disp('bits'),disp(dec2bin(bits))
            indx = bitor(bitshift(indx,nDims),bits);
	%		disp('indx'),disp(dec2bin(indx))

            flipBit = bitshift(uint64(1),rotation);
            % adjust rotation         
            bits = bitand( bits, bitand ( bitneg(bits) , nd1Ones));
            setBits=bits~=0;
            while(any(setBits))
                rotation(setBits) = rotation(setBits)+1;
                bits(setBits) = bitshift(bits(setBits),-1);
                setBits=bits~=0;
			end
            rotation = rotation+1;
            rotation(rotation >= nDims) = rotation(rotation >= nDims) - nDims;
        end
	%	disp(dec2bin(indx))
        indx = bitxor( indx, bitshift( nthbits,-1));
     %   dec2bin(indx)
    else
        indx = coords;
	end	
    d=1;
    while(d < nDimsBits)
        indx = bitxor( indx, bitshift(indx,-d));
        d = d*2;
    end
    
%%    
else
    % one dimension
    indx=coord;
end

    function bits=rotateRight(arg,nRots,nDims)
        bits = bitand( bitor( bitshift( arg, -nRots), ...
                              bitshift( arg, nDims-nRots)), ...
                       bitshift(1,nDims)-1);
    end
%  --- fails: k must be a type ---	
%	function bits=bitones(k)
%		bits =bitcmp(0, k);
%	end
		
    function bits=bitneg(arg)
        
        % Zweierkomplement
        if(arg==0)
            bits=zeros(1,1,'uint64'); %0; 
        else
            mask32=hex2dec('ffffffff');
            bits=bitcmp64(arg);
            % plus 1
            bitsl64=(bitand(bits,mask32));
            bitsl32=double(bitsl64)+1;
            %bits=bitxor(bits,bitsl64);
            bitsu64=bitshift(bits,32);
            if(bitsl32>mask32)
                bitsu32=double(bitsu64)+1;
            else
                bitsu32=double(bitsu64);
            end
            bits=bitor( bitshift(uint64(bitsu32),32), bitand( uint64(bitsl32), mask32));
        end
    end

        
end
%%
function coords=bittrans(inCoords,nDims,nBits)
%nDims, nBits

    nDims1 = nDims-1;
    inB = nBits;
    inFieldEnds = 1;
    inMask = bitshift(1,inB)-1;
    
    coords=zeros(size(inCoords),'uint64');
    utB = bitshift(inB,-1);
    while(utB~=0)
        shiftAmt = nDims1 * utB;
        utFieldEnds = bitor( inFieldEnds , bitshift( inFieldEnds, (shiftAmt+utB)));
        utMask = bitshift( utFieldEnds, utB) - utFieldEnds;
        utCoords = zeros(size(inCoords),'uint64');
        if(bitand(inB,1)==1)
            inFieldStarts = bitshift( inFieldEnds, inB-1);
            oddShift = 2*shiftAmt;
            for d=0:(nDims-1)
                in = bitand( inCoords, inMask);
                inCoords = bitshift( inCoords, -inB);
                coords = bitor( coords, bitshift( bitand( in, inFieldStarts),	oddShift));
                oddShift = oddShift+1;
                in = bitand( in, bitcmp64( inFieldStarts));
                in = bitand( bitor( in,  bitshift( in, shiftAmt)),  utMask);
                utCoords = bitor( utCoords, bitshift( in, (d*utB)));    
            end
        else
           for d=0:(nDims-1)
                in = bitand( inCoords, inMask);
                inCoords = bitshift( inCoords, -inB);
                in = bitand( bitor( in,  bitshift( in, shiftAmt)),  utMask);
                utCoords = bitor( utCoords, bitshift( in, (d*utB))); 
           end
        end
        inCoords = utCoords;
        inB = utB;
        inFieldEnds = utFieldEnds;
        inMask = utMask;
        utB = bitshift(inB,-1);
    end
    coords = bitor( coords, inCoords);
end

function bits=bitcmp64(arg)
    % bitcmp for uint64 is unsupported
    mask32= uint64(hex2dec('ffffffff'));
    all64 = bitor( bitshift(mask32,32), mask32);
    bits  = bitxor(arg,all64);
end
%x=rand(500,2)*16;
%i=c2i(floor(x),4);
%[z,q]=sort(i);
%plot(x(q,1),x(q,2),'-*');
