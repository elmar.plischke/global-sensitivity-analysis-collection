function y=dwtD6(x)
% discrete wavelet trafo daubechies 
% Deubechies Sweldens
[n,k]=size(x);
if(n==2) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 s = s - 0.4122865950*d;
 d = d + ( -1.565136796*cpv(s,1) + 0.3523876576*s);
 s = s + ( 0.0284590896*d+ 0.4921518449*cpv(d,-1) );
 d = d - 0.3896203900*s;
 s = s*1.9182029462/sqrt(2);
 d = d/1.9182029462*sqrt(2);

 y = [ dwtD6(s) ; d];
end
end
