function d=dcovrnd(x,y,R)
% DCOVRND distance covariance via randomization
if(nargin<3), R=1; end

[n,k]=size(x);
[m,l]=size(y);
if(n~=m), error('Oops, mismatch'); end

%if(l>1) && (k>1), error('Sorry only vectors'); end

 for r=1:R
  tick=randperm(n);
  dtick=randperm(n);
%% Euclidean norm: sum of squared distances
  dx=sqrt(sum([x-x(tick,:);x(tick,:)-x(dtick,:);x(dtick,:)-x].^2,2));
  dy=sqrt(sum([y-y(tick,:);y(tick,:)-y(dtick,:);y(dtick,:)-y].^2,2));

  d(r)=sqrt(max(0,mean(dx.*dy)+mean(dx)*mean(dy)-mean(dx.*dy([(2*n+1):end,1:(2*n)],:))...
    -mean(dx([(2*n+1):end,1:(2*n)],:).*dy)));
 end
end
