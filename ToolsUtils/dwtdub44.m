function y=dwtdub44(x)
% discrete wavelet trafo Deslauriers Dubuc 4/4, Binlet 13/7
% Alex Stoffel
[n,k]=size(x);
if(n==4) 
  y=x; 
else
 if(k==1)
  d = x(2:2:n);
  s = x(1:2:n-1);
 else
  d = x(2:2:n,:);
  s = x(1:2:n-1,:);
 end
 d = d - ( 9*(s + cpv(s,1))-(cpv(s,-1)+cpv(s,2)))/16;
 s = s + ( 9*(d + cpv(d,1))-(cpv(d,-1)+cpv(d,2)))/32;
 d = d/2;

 y = [ dwtdub44(s) ; d];
end
end
