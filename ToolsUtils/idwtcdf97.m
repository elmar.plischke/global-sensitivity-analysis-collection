function x=idwtcdf97(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf97(y(1:(n/2)))/1.149604398; 
    d = y(((n/2)+1):n)*1.149604398;
    s = s - 0.4435068522*(cpv(d,-1)+ d); 
    d = d - 0.8829110762*(s+ cpv(s,1));
    s = s + 0.05298011854*(cpv(d,-1)+ d);
    d = d + 1.586134342*(s+ cpv(s,1));
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf97(y(1:(n/2),:))/1.149604398; 
    d = y(((n/2)+1):n,:)*1.149604398;
    s = s - 0.4435068522*(cpv(d,-1)+ d); 
    d = d - 0.8829110762*(s+ cpv(s,1));
    s = s + 0.05298011854*(cpv(d,-1)+ d);
    d = d + 1.586134342*(s+ cpv(s,1));
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
