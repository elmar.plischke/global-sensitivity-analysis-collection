function x=idwtcdf46(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==4) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf46(y(1:(n/2)))/2; d=y(((n/2)+1):n)*2;
    s = s+(-35*cpv(d,-3)+265*cpv(d,-2)-998*cpv(d,-1)-998*d+265*cpv(d,1)-35*cpv(d,2))/4096;
    d = d + s + cpv(s,1);
	s = s + .25*(cpv(d,-1)+d);
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf46(y(1:(n/2),:))/2; d=y(((n/2)+1):n,:)*2;
    s = s+(-35*cpv(d,-3)+265*cpv(d,-2)-998*cpv(d,-1)-998*d+265*cpv(d,1)-35*cpv(d,2))/4096;
    d = d + s + cpv(s,1);
	s = s + .25*(cpv(d,-1)+d);
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s; 
 end
end
end
