function P = cpv(S, l)
%cyclic permutation of a vector 
%(Jensen & la Cour-Harbo, 2001)
[n,k]=size(S);
if(l==0)
    P=S;
end
if(k==1)
    if l > 0
        P = [S(l+1:end);S(1:l)];
    elseif l < 0
        P = [S(end+l+1:end);S(1:end+l)];
    end
else
    if l > 0
        P = [S(l+1:end,:);S(1:l,:)];
    elseif l < 0
        P = [S(end+l+1:end,:);S(1:end+l,:)];
    end  
end
end