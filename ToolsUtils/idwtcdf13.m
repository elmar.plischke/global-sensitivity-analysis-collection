function x=idwtcdf13(y)
% inverse discrete wavelet trafo cohn daubechies feauveau
[n,k]=size(y);
if(n==2) 
  x=y; 
else
 x=zeros(size(y));
 if(k==1)
    s = idwtcdf13(y(1:(n/2))); 
    d = y(((n/2)+1):n)*2;
    s = s - ( -cpv(d,-1)+ 8*d+cpv(d,1))/16;
    d = d + s;
    x(2:2:n) = d; 
    x(1:2:(n-1))= s;
 else
    s = idwtcdf13(y(1:(n/2),:)); 
    d = y(((n/2)+1):n,:)*2;
    s = s - ( -cpv(d,-1)+ 8*d+cpv(d,1))/16;
    d = d + s;
    x(2:2:n,:) = d; 
    x(1:2:(n-1),:)= s;
 end
end
end
