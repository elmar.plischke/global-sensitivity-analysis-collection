function Ti=janon(k,n,model,trafo,randomsource)
% JANON Total effects using Janon estimator
%

% written by elmar.plischke@tu-clausthal.de

if(nargin>=5) && ~isempty(randomsource)
    if ~isnumeric(randomsource)
        u=randomsource(n,2*k); 
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (2*k~=kk)
            error('SST: incompatible size of randomsource');
        end
        u=randomsource;
    end
else
%% use builtin QMC generator
    try
        u=net(sobolset(2*k),n);
    catch
        % for octave, port of Joe&Kuo 
        u=sobolpoints(n,2*k);
    end
%%
end
ua=u(:,1:k);ub=u(:,k+1:end);
xa=trafo(ua);
xb=trafo(ub);
ya=model(xa);
E2s=zeros(1,k+1);
Vys=zeros(1,k+1);
Fs=zeros(1,k);
E2s(end)=mean(ya.^2);
Vys(end)=var(ya)*(n-1)/n;;
for i=1:k
    xi=xa;xi(:,i)=xb(:,i);
    yi=model(xi);
    E2s(i)=mean(yi.^2);
    Vys(i)=var(yi)*(n-1)/n;
    Fs(i)=ya'*yi/n; % expectation of the squared conditional expectation
end
Ti=[(mean(E2s)-Fs)./mean(Vys);(E2s(1:end-1)-Fs)./Vys(1:end-1)];
end