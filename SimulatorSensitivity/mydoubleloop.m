function [Si,seps,names]=mydoubleloop(k,n,model,trafo,groups,opts_in)
% MYDOUBLELOOP Monte Carlo sensitivity analysis.
% SI=MYDOUBLELOOP(K,N,MODEL,TRAFO,GROUPS) evaluates the MODEL using input
% transformation TRAFO with K input parameters with N(1) inner and N(END)
% outer runs (quadrature points), requesting sensitivity conditional to the
% list of input parameter GROUPS (default {1,...K} ). 
%
% The function returns the following sensitivity measures per row
% Kolmogorov Smirnov            max | F_Y-F_Y|cond |
% Kuiper                        max ( F_Y-F_Y|cond ) - min ( F_Y-F_Y|cond )
% Borgonovo Delta               sum localmax - sum localmin
% Gini Anderson Darling         6 int ( F_Y-F_Y|cond )^2 dF_Y
% Variance Subset Importance    1-(V_Y|cond)/V_Y
% Wasserstein Kantorovich W 1   int | F^-1_Y-F^-1_Y|cond |  
% Wasserstein Kantorovich W 2   sqrt int ( F^-1_Y-F^-1_Y|cond )^2 
% Wasserstein Kantorovich W oo  max | F^-1_Y-F^-1_Y|cond |
% 25%/Median/75% Quantile Sensitivity

% written by Elmar.Plischke@tu-clausthal.de

% outer and inner are swapped

if nargin<4 || isempty(trafo), trafo=@(u)u; end % use identity
if nargin<5 || isempty(groups)
 groups=num2cell(1:k);
end
%names=struct('ks',1,'kuiper',2,'borgonovo',3,'gini',4,'pearsonCR',5, ...
%             'wasserstein1',6,'wasserstein2',7,'wassersteininf',8,...
%             'quartilelower',9,'quartilemedian',10,'quartileupper',11);

names={'ks','kuiper','borgonovo','gini','pearsonCR', ...
             'wasserstein1','wasserstein2','wassersteininf',...
             'quartilelower','quartilemedian','quartileupper'};
%% process options
% defaults
opts=struct('GfxTitle','',...
	        'GaussQuadrature',true,...
            'RandomSource',@sobolpoints,...
            'ShowEval',true,...
            'ShowCDF',false,...
            'SubPlot',[]);

if(nargin>=6) && ~isempty(opts_in)
    if isstruct(opts_in)
        members=fieldnames(opts);
        for i=1:length(members)
            o=members{i};
            if isfield(opts_in,o), opts.(o)=opts_in.(o);end
        end
    else
        opts.GfxTitle=opts_in;
    end
end

% Moving window for Delta CDF smoother
if(numel(n)>2)
    P=n(2);
else
    P=max( ceil(n(1)/(log10(n(1))+14)), 5);
end

u=opts.RandomSource(n(1),k);  
x=trafo(u);
y=model(x);evalc=n(1);
ys=sort(y);
Ey=mean(y);
Vy=var(y);

% Gaussian quadrature weights
if(opts.GaussQuadrature)
        m=n(end);
        beta=1/2*(1-(2*(1:(m-1))).^-2).^-.5;
        T=diag(beta,1)+diag(beta,-1);
        mm=1+floor(m/2);
        [vec,val]=eig(T);
        z=diag(val(mm:end,mm:end))';
        w=2*vec(1,mm:end).^2;
        zz=(1+[-z(end:-1:1),z])/2;
        ww=[w(end:-1:1),w]/2;
        if(rem(m,2))
          zz((m+1)/2)=[];ww((m+1)/2)=[];
        end
else
    %  Riemann equidistant
        zz=((1:n(end))-.5)/n(end); 
        ww=ones(1,n(end))/n(end);
end

locmax=@(x)x([true; x(2:end-1) > x(1:end-2) & x(2:end-1)>= x(3:end);true]);
locmin=@(x)x([true; x(2:end-1)<= x(1:end-2) & x(2:end-1) < x(3:end);true]);

cols=jet(n(end));

l=length(groups);
Meths=11; 
Si=zeros(Meths,l);
for g=1:length(groups)
 gg=groups{g};
 sep=zeros(Meths,n(end)^length(gg));
 if ~isempty(opts.GfxTitle),
     if(isempty(opts.SubPlot))
     subplot(round(sqrt(l)),ceil(sqrt(l)),g);
     else
         subplot(opts.SubPlot(1),opts.SubPlot(2),g);
     end
 end
 switch length(gg),
     case 1, zzz=zz;www=ww;
         % tensor product for groups / fully factorial
     case 2, [a,b]=ndgrid(1:n(end));
             zzz=zz([a(:),b(:)])';www=prod(ww([a(:),b(:)]),2);
     case 3, [a,b,c]=ndgrid(1:n(end));
             zzz=zz([a(:),b(:),c(:)])';www=prod(ww([a(:),b(:),c(:)]),2);
     case 4, [a,b,c,d]=ndgrid(1:n(end));
             zzz=zz([a(:),b(:),c(:),d(:)])';
             www=prod(ww([a(:),b(:),c(:),d(:)]),2);
     otherwise, error('Group length not supported.');
 end
    j=1;
    for z=zzz;
        % this is "wrong way" for pick and freeze 
        ui=u;ui(:,gg)=ones(n(1),1)*z';
        yi=model(trafo(ui));evalc=evalc+n(1);
        yis=sort(yi);
        
        binEdges    =  [-inf ; unique([y;yi]) ; inf]; %sort

        binCounts1  =  histc (y , binEdges, 1);
        binCounts2  =  histc (yi , binEdges, 1);

        sampleCDF1  =  cumsum(binCounts1)./sum(binCounts1);
        sampleCDF2  =  cumsum(binCounts2)./sum(binCounts2);
        deltaCDF    =  sampleCDF1-sampleCDF2;
            
        kui1        =  max( deltaCDF);
        kui2        =  max(-deltaCDF);
        gin         =  mean( (deltaCDF).^2 );
        
        sep(1,j) = max (kui1, kui2); % ks;
        sep(2,j) = kui1 + kui2;
        sep(4,j) = gin * 6;
        
        % moving average in disguises 
        m           = length (deltaCDF);
        sumCDF      = cumsum (deltaCDF);  
        %filterCDF   = ( sumCDF (min ((1:m)' + P, m))...
        %              - sumCDF (max ((1:m)' - P, 1)) ) ./ (2*P + 1);        
        %sep(3,j) = sum(locmax(filterCDF))-sum(locmin(filterCDF));
        % make filter length-aware
        indexHi     = min ((1:m)' + P, m);
        indexLo     = max ((1:m)' - P, 1);
        filterCDF   = ( sumCDF( indexHi)...
                      - sumCDF( indexLo) )./ (indexHi-indexLo);        
        sep(3,j) = sum(locmax(filterCDF))-sum(locmin(filterCDF));
       % superset importance
        sep(5,j) = (Ey-mean(yi)).^2/Vy;
        % Kantorovich power distances: working on order statistics
    % Sergey Bobkov and Michel Ledoux: ONE-DIMENSIONAL EMPIRICAL MEASURES,
    %   ORDER STATISTICS, AND KANTOROVICH TRANSPORT DISTANCES, 2016
        sep(6,j)=mean(abs(ys-yis)); 
        sep(7,j)=sqrt(mean((ys-yis).^2));
        sep(8,j)=max(abs(ys-yis));
    % Quantile sensitivity using partial expectations
        alfa=.25; m=floor(n(1)*alfa);ms=1:m;nm=(n(1)*alfa-m);
        sep(9,j) =( nm*(yis(m+1)-ys(m+1))+sum(yis(ms)-ys(ms)))/n(1);
        alfa=.50; m=floor(n(1)*alfa);ms=1:m;nm=(n(1)*alfa-m);
        sep(10,j)=( nm*(yis(m+1)-ys(m+1))+sum(yis(ms)-ys(ms)))/n(1);
        alfa=.75; m=floor(n(1)*alfa);ms=1:m;nm=(n(1)*alfa-m);
        sep(11,j)=( nm*(yis(m+1)-ys(m+1))+sum(yis(ms)-ys(ms)))/n(1);
        
        if ~isempty(opts.GfxTitle)
         if(~opts.ShowCDF)
          plot(ys,yis,'Color',cols(mod(j-1,n(end))+1,:)); 
          if(j==1)
           hold on
           xlabel('y unconditional');ylabel('y conditional');
           title([opts.GfxTitle ', conditioning on factor(s) ' num2str(gg)]);
            % view(45,90);
          end
         else
           plot(binEdges,deltaCDF,'Color',cols(mod(j-1,n(end))+1,:)); % filtercdf
           if(j==1)
            hold on
            xlabel('Output');ylabel('\Delta CDF');
           title([opts.GfxTitle ', conditioning on factor(s) ' num2str(gg)]);
          end
         end
        end
          
        j=j+1;
    end
    Si(:,g)=sep*www(:);
    if ~isempty(opts.GfxTitle),hold off;end
    if(nargout>=2), seps{g}=n(2)*bsxfun(@times,sep,www); end
end
if(opts.ShowEval)
    fprintf(1,'mydoubleloop: %d function evaluations\n',evalc);
end
end
