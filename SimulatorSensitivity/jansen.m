function [Si,STi,data]=jansen(k,n,model,trafo,rndsrc)
% JANSEN Compute sensitivity indices using winding stairs sampling
%       [SI,STI] = JANSEN(K,N,MODEL,TRAFO) computes main and total effects 
%       of the K-parametric function MODEL using N samples and the input 
%       transformation TRAFO.
%       [SI,STI,DATA] = JANSEN(K,N,MODEL,TRAFO) also returns the n x k+1 
%       data matrix
% see K. Chan, A. Saltelli, S. Tarantola: Winding Stairs, 2000
% Jansen M.J.W., Rossing W.A.H., and Daamen R.A., 1994

%     Written by Elmar Plischke, elmar.plischke@tu-clausthal.de
%%
if(0)
%%
%k=3;n=12;
%trafo=inline('x','x');
%model=inline('(x.^(ones(length(x),1)*[1,2,1]))*[1;-1;1]','x');
%%
end
%%
r=ceil(n/k-1);
n=k*r;
fprintf('JANSEN: Using %ld samples, %ld per index\n',n+k,r+1);
if(nargin<5)
us=rand(r+1,k);
else
    if(strcmp(class(rndsrc),'function_handle'))
        us=rndsrc(r+1,k);
    else
        [rr,kk]=size(rndsrc);
        if(rr==r+1 && kk==k)
          us=rndsrc; % check sizes
        else
            error('Size mismatch');
        end
    end
end
%winding stairs
T=triu(ones(k,k));
u=zeros(n+k,k);
u0=us(1,:);%rand(1,k);
ustart=u0;
for (i =1:r)
    u1=us(1+i,:); %rand(1,k);
    u( (1:k)+(i-1)*k,: )= bsxfun(@times,u0,T) + bsxfun(@times,u1,1-T);
    u0=u1;
 end
% create a real cyclic matrix
u( (1:k)+n,: )= bsxfun(@times,u0,T) + bsxfun(@times,ustart,1-T);
%%
u=u([2:end,1],:); % swap first entry to make default shifts work
%%
x=trafo(u);
y=model(x);
%%
w=reshape(y,k,r+1)'; % fill rows
%var(y)
%V=sum(sum( (w-ones(r+1,1)*sum(w)/(r+1)).^2))/(k*r);
Vs=var(w); % sum( (w-ones(r+1,1)*sum(w)/(r+1)).^2)/r;
%%
Si=zeros(1,k);
STi=zeros(1,k);
%ell=1:r;
%%
%Si(1)  = 1-sum((y(k*(ell-1)+1)-y(k*ell)).^2)/(2*r*V);
Si(1) = 1-sum((w(:,1)-w(:,k)).^2)/(2*(r+1)*Vs(1));
%STi(1) = sum( (y(k*ell)-y(k*ell+1)).^2)/(2*r*V);
STi(1)= sum( (w(:,k)-w([2:end,1],1)).^2)/(2*(r+1)*sqrt(Vs(1)*Vs(k)));
for (i = 2:k)
    %Si(i) = 1-sum( (y(k*(ell-1)+i)-y(k*ell+i-1)).^2)/(2*r*V);
    Si(i)= 1-sum( (w(:,i)-w([2:end,1],i-1)).^2)/(2*(r+1)*sqrt(Vs(i)*Vs(i-1)));
    %STi(i)=sum( (y(k*(ell-1)+i-1)-y(k*(ell-1)+i)).^2)/(2*r*V);
    STi(i)=sum( (w(:,i-1)-w(:,i)).^2)/(2*(r+1)*sqrt(Vs(i)*Vs(i-1)));
end
%%
if(nargout==3)
 data=[x,y];
end
%%
return



