function [Si,xy,perm] = rbd(k,n,function2evaluate,distTransform,gfx,cmdOpts)
%SI = RBD(K,N,MODEL,TRAFO,GFXTITLE,OPTS)
% First order indicies for a given model
% computed with �Fourier Amplitude
% Sensitivity Test� (FAST) using Random Balanced Design.
% Ideas follow 
% Saltelli et al., 'Global Sensitivity Analysis', p. 168, Wiley, 2008.
% Tarantola et al., 'Random balance designs...',p. 717-727, Reliab Eng Syst
%                    Safety 91, 2006.
% Input:
% K : no. of input factors
% N : no. of samples
% MODEL: function to evaluate
% TRAFO: distribution transformation, use @(x)x for
%                uniform [0,1] distribution
% GFXTITLE: create graphics with GFXTITLE title
% OPTS: structure containing the fields
%                Harmonics   [6]
%                Omega       [1] -- 0 activates Cosine Trafo
%                Permutation { ['rand'] 'qr' 'ic' 'icqr' 'icqrx' 'icqrs' 'freq'}
%                Debias      { 'on' ['off']}
%                Replicate   [0]
%                Strap       [0]
%                Sigma       { ['off'] kxk matrix }
%                QRandSrc    @sobolseq
% Output:
% SI: sensitivity indices
% XY: input/output sample (Nx(K+1))
% P:  permutation
% Written by elmar.plischke(at)tu-clausthal.de

% default options
opts=struct('Harmonics',6,'Omega',1,'Permutation','rand',...
	    'Debias','off','Replicate',0,...
	    'Strap',0, 'Sigma','off', 'QRandSrc',@sobolseq);
if(nargin>5)
    members=fieldnames(opts);
    for i=1:length(members)
        o=members{i};
        if isfield(cmdOpts,o), opts.(o)=cmdOpts.(o);end
    end
end

% Initialize scrambled Sobol Set
PointSet=scramble(sobolset(k-1),'MatousekAffineOwen');
%PointSet=scramble(haltonset(k-1),'RR2');

M=opts.Harmonics;     		% max. higher harmonic
omega=opts.Omega; 		% frequency
if(omega>0)
% create design points
 s=(2*(1:n)'-(n+1))/n;
 u=acos(-cos(pi*omega*s))/pi;
 orthoTrafo=@fft;
 scale=2; 
else
% omega<=0 codes cosine trafo
 u=(2*(1:n)'-1)/(2*n); % u=(1:n)/(n+1);
 orthoTrafo=@dct;
 scale=1;
 omega=1;
end
% create k random permutations from 1:n
switch lower(opts.Permutation)
 case 'qr', 
%if(strcmpi(opts.P,'on'))
% quasi-random permutations
 if(opts.Replicate==0)    
% [perm,indx]=sort(sobolseq(n,k));
 [perm,indx]=sort(opts.QRandSrc(n,k));
 else
 %r=opts.Replicate;
 %S=zeros(n,k);
 %for(i=1:n), S(i,:)=lptau(r+i,k); end % use M. Ratto Implementation
 S=sobolseq(n,k)+ones(n,1)*rand(1,k); % use random shift
 S=S-floor(S);  
 [perm,indx]=sort(S);
 end
 for(i=1:k)
  perm(indx(:,i),i)=1:n;
 end
case 'freq',
 perm=zeros(n,k);
 frequencies=[7,9,10,11,13,15,17];
 p=1:n;
 for(i=1:k)
  indx=frequencer(frequencies(i),n);
  perm(:,i)=p;
  p=p(indx);
 end
case 'ic', 
% Iman Conover
[dummy,perm]=iccorrelate(u*ones(1,k),eye(k,k));
case 'icqr',
% Iman Conover with Quasi-Random Permutation
[dummy,perm]=iccorrelate(u*ones(1,k),eye(k,k),'UseQRP',opts.Replicate);
case 'icqrs',
% Iman Conover with Scrampled Quasi-Random Permutation 
[dummy,perm]=iccorrelate(u*ones(1,k),eye(k,k),'UseQRP',@(n,k)net(PointSet,n));
case 'icqrx',
% Iman Conover with Quasi-Random Permutation, extended    
[dummy,perm]=sort(rand(n,k));
[dummy,perm1]=iccorrelate(u(perm),eye(k,k),'UseQRP',opts.Replicate);
for i=1:k,  perm(:,i)=perm(perm1(:,i),i);end
%case 'lhs2',
%% from a two-replicate Latin hypercube sample
%[dummy,P1]=sort(rand(floor(n/2),k));
%[dummy,P2]=sort(rand(ceil(n/2),k));
%perm=[P1;n+1-P2];
case 'qrdirect',  
[dummy,perm]=sort(opts.QRandSrc(n,k));
    % [dummy,perm]=sort(sobolseq(n,k));        
otherwise,
% random
 [dummy,perm]=sort(rand(n,k));
end
% create sample
%size(u),size(perm)
x=distTransform(u(perm));
% introduce correlation structure
if(isa(opts.Sigma,'numeric'))
%    x0=x;perm0=perm; % debug
 [x,perm2]=iccorrelate(x,opts.Sigma);
 for(i=1:k);perm(:,i)=perm(perm2(:,i),i); end
end 
% evaluate model
y=function2evaluate(x);
[nn,kk]=size(y); % allow for vector valued output
% pre-allocated vectors
yrs=zeros(n,k*kk);

% sorting based on s
 for i=1:k
  yrs(perm(:,i),i+((1:kk)-1)*k)=y;
 end
% keep the loop short and evaluate in vectorized form
 spectrum=(abs(orthoTrafo(yrs))).^2/n;
 if omega==ceil(omega) %isa(omega,'integer')
     if(M>0)
      Vi=sum(spectrum(1+omega*(1:M),:));
     else
         Vi=zeros(20,k);
         for m=1:20
         Vi(m,:)=sum(spectrum(1+omega*(1:m),:),1);
         end
     end
 else
 % non-integer frequency: pick floor and ceil
  Vi=sum(spectrum(1+ unique( [floor(omega*(1:M)), ceil(omega*(1:M))]),:));
 end
 V=sum(spectrum(2:n,:)); % mean estimate for V 
 Si=bsxfun(@rdivide,scale*Vi,V);
 
 if(strcmpi(opts.Debias,'on'))
  Si=(n*Si-scale*M)/(n-scale*M);
 end
 % if(0) % no!
 % if(omega>1)
  % % go for totals
  % if mod(n,omega), disp('Frequency is no divisor of sample-size.'); return;end
  % STi=zeros(1,k);
  % for i=1:k
   % % cleverly rearrange
   % z=reshape(yrs(:,i),(2*omega),n/(2*omega));
  % % flip each other direction
   % z(:,2:2:end)=z(end:-1:1,2:2:end);
  % % STi(i)=(n-1)*mean(var(z'))/V;
   % STi(i)=1-(n-1)*var(mean(z'))/V;
  % end
  % STi % de-biasing tricks needed?
 % end
 %end
 
 if(nargin>4 && ~isempty(gfx)) 
    subplot(1+k,1,1);
    plot(1:n,u(1:n),'.'); %,'MarkerSize',3);
    title([gfx ' - Random Balance Design']);xlabel('Index');
 %   legend(cellstr([char(ones(l,1)*'x_{'),num2str(group'),char(ones(l,1)*'}')]));
    a=axis;a(2)=n;axis(a);
	for(i=1:k)
    subplot(1+k,2,2+2*i-1);
    plot(1:n,yrs(:,(i-1)*kk+1),'k.');
    title([gfx ' - Output ']);xlabel('Index');
    a=axis;a(2)=n;axis(a);
    subplot(1+k,2,2+2*i);
    specshow(spectrum(:,(i-1)*kk+1),omega,M); % [omega,100],M);%add fake frequency
	end
 end

 l=opts.Strap;
 if(l>0 && omega==1)
 Sis=zeros(l,k);
 yrb=zeros(n,k);
 for j=1:l
  for(i=1:k)
   %idx=[];
   %while(isempty(idx))
   %   idx=find(rand(1,floor(n/2))<.5);
   %end
   %yrs([idx, n+1-idx],i)=yrs([n+1-idx, idx],i);
   idx=round(rand(n,1));
   yrb(:,i)=idx.*yrs(:,i)+(1-idx).*yrs(end:-1:1,i);
  end
  spectrum=(abs(fft(yrb))).^2/n;
  Sis(j,:)=2*sum(spectrum(1+(1:M),:))/V; 
 end
 Si=[Si;Sis];
 end

 if (nargout>1), xy=[x,y]; end
 if (l==0 && kk>1), Si=reshape(Si,k,kk)'; end
return

