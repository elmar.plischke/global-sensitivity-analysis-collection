function res=seqbifurc(k,model,trafo,group)
% sequential bifurcation experiments

% written by elmar.plischke@tu-clausthal.de
if(nargin<=3)
    group=round(linspace(0,k,4));group(1)=[];
else
    group=unique([group,k]);
end
if(nargin<=2 || isempty(trafo))
    trafo=@(x)x; 
end
% refine until largest single contributor has been identified
auto=true; %false; 
identify=10;id=1;
evallist=[];
%
l=length(group);
u=zeros(2*l,k);
for i=1:l
 u(i,:)   = [zeros(1,group(i)),ones(1,k-group(i))];
 u(l+i,:) = [ones(1,group(i)),zeros(1,k-group(i))];
end
%u(end,:)=ones(1,k);

x=trafo(.1+.8*u);
y=model(x);

evals=2*l;
y=[y(end);y];
b=(y(l+1+(1:l))-y(l+(1:l)) -(y((1:l)+1)-y(1:l)))/4;
res=[1:l;group];
if(~auto)
disp(res);disp(b');
end
%%
while(1)
    if(auto)
    %[~,newg]=max(abs(b));
     [~,ll]=sort(-abs(b));
     newg=ll(id);
    else
    newg=input([ 'Group to refine (1 - ' num2str(l) ') >> ']);
    end
    if(newg<1 || newg>l), break; end
    if(newg>1) 
        lower=group(newg-1)+1;
    else
        lower=1;
    end
    upper=group(newg)-1;
    if(lower<=upper)
        if(auto)
            newi=floor((lower+upper)/2);
        else
     newi=input([ 'End of lower subgroup (' num2str( lower) ...
        ' - ' num2str(upper) ') >> ']);
        end
     if(newi<lower || newi>upper), continue, end;
     ui=[zeros(1,newi),ones(1,k-newi);...
         ones(1,newi),zeros(1,k-newi)];
     xi= trafo(.1+.8*ui);
     yi= model(xi);evals=evals+2;
     
     %% insert
     group=[group(1:newg-1),newi,group((newg):end)];
     y=[y(1:(newg),:);yi(1,:);y((newg+1):(l+newg),:);...
         yi(2,:);y((l+newg+1):end,:)];
    % x=[x(1:(newg-1),:);xi(1,:);x((newg):(l+newg-1),:);...
    %     xi(2,:);x((l+newg):end,:)];
     l=length(group);
     b=(y(l+1+(1:l))-y(l+(1:l)) -(y((1:l)+1)-y(1:l)))/4;
     res=[1:l;group];
    if(~auto)
     disp(res);
     disp(b');
    end
    else
        if(auto), 
            evallist(end+1,:)=[newi,evals]; %,b(newg)];
            id=id+1;
            if(id>identify),  break; end
        else
            warning('Group consists of a single element.');
    
        end;
    end
    %image(x*256)
end
fprintf('SeqBifurc: %d model evaluations\n',evals);
if(auto)
    disp('factor  calls   sensitivity')
    disp(evallist);
    %fprintf('%8d %8d %f\n',evallist);
end
res=[res;b'];
end
function test
model99

MX=ones(k,k)*.9;MN=ones(k,k)*.1;
x=[triu(MX)+tril(MN,-1);triu(MN)+tril(MX,-1)];
y=model(x);

%w2y=@(j) ((j>=0) & (j<k)).*(k+1+j)+(j==k).*1+(j<0).*(1-j);
%b= ((y(w2y(1:k))-y(w2y(-(1:k))))-(y(w2y((1:k)-1))-y(w2y(1-(1:k)))))/4;

y(end+1)=y(1);
b=(y(k+1+(1:k))-y(k+(1:k)) - (y((1:k)+1)-y(1:k)))/4;

[~,indx]=sort(-abs(b));
bar(abs(b));
indx(1:10);

%%
x(end+1,:)=x(1,:);

group=11:22:99;
lo=x(1+group,:);
hi=x(k+1+group,:);
end
