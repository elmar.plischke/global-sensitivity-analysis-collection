function [Si,Ti,xy,Sisym,Tisym]=gleissi(k,n,model,trafo,randomsource,gfx)
% GLEISSI First and Total Effects Using Glen and Isaacs 2012

% create 2 sets of samples
if(nargin>=5)
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('GLEISSI: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use standard uniform random generator
    ua=rand(n,k);
    ub=rand(n,k);
%%
end

% use the provided transformation or identity mapping
if(nargin==3) || isempty(trafo)
    xa=ua;xb=ub;
else
%%
    xa=trafo(ua);xb=trafo(ub);
%%
end

%% model evaluation for reference sample runs
ya=model(xa);
yb=model(xb);
if(nargout>=3)
    xy=[xa,ya;xb,yb];
end
%% estimating mean and var
ma=mean(ya);
mb=mean(yb);
sa=sqrt(mean((ya-ma).^2));
sb=sqrt(mean((yb-mb).^2));
ga=(ya-ma)/sa;
gb=(yb-mb)/sb;
Si=ones(1,k);Ti=ones(1,k);
Sisym=ones(1,k);Tisym=ones(1,k);
for i = 1:k
%% replace ith parameter from run a with the one from run b
    xia=xa;xia(:,i)=xb(:,i);
    % evaluate model
    yia=model(xia);
    % and now the primed version
    xib=xb;xib(:,i)=xa(:,i);
    % evaluate model
    yib=model(xib);
%%      
    % save output if requested
    if(nargout>=3)
        xy=[xy;xia,yia;xib,yib];
    end
    % show scatterplots
    if(nargin>=6)
        subplot(2,k,i)
        plot([yia;yib],[yb;ya],'.'); %,'MarkerSize',2);
        title([ gfx ': main effect']);
        %linreg(yi,yb,[ gfx ': main effect']);
        xlabel(['y_{' num2str(i) '}']);ylabel('y')
        subplot(2,k,i+k)
        plot([yia;yib],[ya;yb],'.');% ,'MarkerSize',2);
        title([ gfx ': total effect']);
        %linreg(yi,ya,[ gfx ': total effect']);
        xlabel(['y_{' num2str(i) '}']);ylabel('y''')
    end
    % Glen Isaacs D3
    mja=mean(yia);sja=sqrt(mean((yia-mja).^2));
    gja=(yia-mja)/sja;
    mjb=mean(yib);sjb=sqrt(mean((yib-mjb).^2));
    gjb=(yib-mjb)/sjb;
    cdj=(gb'*gja+ga'*gjb)/(2*n);
    cd_j=(ga'*gja+gb'*gjb)/(2*n);
    pj=(ga'*gb+gja'*gjb)/(2*n);
    caj=(cdj-pj*cd_j)/(1-pj^2);
    ca_j=(cd_j-pj*cdj)/(1-pj^2);
% for main effects
    Si(i)=cdj-pj*ca_j/(1-caj*ca_j);
% for total effects
    Ti(i)=1-cd_j+pj*caj/(1-caj*ca_j);
    if(nargout>3)
        % symmetric Sobol' / Jansen formulas
        da=gja-ga;db=gjb-gb;
        Sisym(i)=.5*((gb'*da+ga'*db))/n;
        Tisym(i)=.5*(da'*da+db'*db)/(2*n);
    end
%%
end
function testgleis
%%
%addpath Broda
model=@(x)sin(x(:,1)).*(1.0+0.1*x(:,3).^4)+ 7.0*(sin(x(:,2))).^2;
trafo=@(u)(u-.5)*2*pi;
k=4;
    %
    %analyt=[.3139,.4424,0,0,.5576,.4424,.2437,0];
    %
    %
    VY=.1*pi^4/5+.1^2*pi^8/18+1/2+49/8;
    e1=((pi^4+50)/25)^2/(8*VY);
    e2=(7/2)^2/(2*VY);
    analyt=[e1,e2,0,0,1-e2,e2,1-e1-e2,0];
    %%
    costs1=[];costs2=[];
    valS1=[];valS2=[];valS3=[];
    valT1=[];valT2=[];valT3=[];
    R=30;
    for r=1:R
        I=randperm(2*k); % 1:2*k
    %%
    ns=2.^(4:15);

    for n=ns
        D=sobolpoints(2*n,2*k);
        %D=apply(@(m)sobolseq16384(m,2*k),1:2*n);
        %D=D(1:n,I); 
        D=D(n+1:end,I);
        %D2=sobolseq(2*n,2*k);
        [Si1,Ti1,xy]=ihssi(k,n,model,trafo,D);
        n1=size(xy,1);
        [Si2,Ti2,xy,Si3,Ti3]=gleissi(k,n,model,trafo,D);
        n2=size(xy,1);
        
        costs1(end+1)=n1;costs2(end+1)=n2;
        valS1(end+1,:)=sum(abs(Si1-analyt(1:4)));
        valS2(end+1,:)=sum(abs(Si2-analyt(1:4)));
        valS3(end+1,:)=sum(abs(Si3-analyt(1:4)));
        valT1(end+1,:)=sum(abs(Ti1-analyt(5:end)));
        valT2(end+1,:)=sum(abs(Ti2-analyt(5:end)));
        valT3(end+1,:)=sum(abs(Ti3-analyt(5:end)));
        
    end
    %%
%    subplot(1,2,1)
%    loglog(costs1,valS1,'-s',costs2,valS2,'-*');hold on
%        subplot(1,2,2)
%    loglog(costs1,valT1,'-s',costs2,valT2,'-*');hold on
%%
    end
%%
%resS1=[];resS2=[];resT1=[];resT2=[];
N=length(ns);
    resS1=mean(reshape(valS1,N,R),2);
    resS2=mean(reshape(valS2,N,R),2);
    resS3=mean(reshape(valS3,N,R),2);
    resT1=mean(reshape(valT1,N,R),2);
    resT2=mean(reshape(valT2,N,R),2);
    resT3=mean(reshape(valT3,N,R),2);
%%
   subplot(1,2,1)
   loglog(costs1(1:N),resS1,'-s',costs2(1:N),resS2,'-*',costs2(1:N),resS3,'-d');
   title('main effects');xlabel('model evaluations');
   ylabel('mean absolute error');legend('Sobol','Glen Isaacs','Sobol'' symm')
   subplot(1,2,2)
   loglog(costs1(1:N),resT1,'-s',costs2(1:N),resT2,'-*', costs2(1:N),resT3,'-d');
      title('total effects');xlabel('model evaluations');
   ylabel('mean absolute error');legend('Jansen','Glen Isaacs','Jansen symm')
%%
end
    
    
end
