function [Ti,V,VTi,misc]=totalsdep_pnf(x,model,densratios,R,lohi,debugmodel)
% TOTALSDEP_PNF Total effects using density ratios and derangements
%  [T,V]=TOTALSDEP_PNF(X,G,R) computes the unnormalized total 
%  effects T of the model G using the basic block sample X (n x d) 
%  and the list of ratio functions
%  R={  f(x) / fi(x(:,i)f(x(:,d\{i}) | i=1,...d }
% [T,V]=TOTALSDEP(X,G,R,B) uses multiple derangements (B replicates, default 1)
%  V is the output variance
% Ref: E. Borgonovo, E. Plischke, C. Prieur: Total Effects 
%      with Constrained Features, Stat. & Comput. (2024, forthcoming)

% written by elmar.plischke@tu-clausthal.de

   [n,d]=size(x);
   % if densratios are copula-based
   [xr,xi]=sort(x);
   for j=1:d
      xr(xi(:,j),j)=(2*(1:n)-1)/(2*n); % emp. cdf (without ties), avoid 0/1
   end
   if(nargin<5) || isempty(lohi)
    thres_lo=0.0;
    thres_hi=thres_lo;
   else
       assert(numel(lohi)==2,'lo/hi thresholds need vector of size 2');
       thres_lo=lohi(1);
       thres_hi=lohi(2);
       assert(thres_lo<=thres_hi,'low must be smaller than high');
   end
   if(nargin<3 || isempty(densratios))
       id=@(x,u)ones(size(x,1),1);
       densratios{1}=id;
       for i=2:d; densratios{i}=id; end
   end
% test with nearest neighbors
   if~strcmp(class(model),'function_handle')
    % assume vector, enable nearest neighbor estimation
    y=model;
    evals=0;
    thres_hi=inf;
   else
    y=model(x); evals=n;
   end
   dim_out=size(y,2);
   V=var(y,1); % normalize by n
   if(nargin<4 || isempty(R))
    R=1; % replicates
   end
   B=2500; % bootstrap replicates
   if(dim_out==1)
    Ti=zeros(R,d);
    VTi=zeros(R,d); % for error estimate
    sz=zeros(n,1);
   else
    Ti=zeros(R,d,dim_out);
    VTi=zeros(R,d*dim_out); % for error estimate
    sz=zeros(n,dim_out); % for bootstrapping the results
   end
  % further information from bootstrap
   Ti_debias=zeros(R,d*dim_out);
   VTi_boot=zeros(R,d*dim_out);
   Strap_all=zeros(R,d*dim_out,B);
   for r=1:R
    drm=derange(n);
    for j=1:d
     sz(:)=0; % clear 
     xx=x;xx(:,j)=x(drm,j);
     rr=xr;rr(:,j)=xr(drm,j);
     %% DEBUG factor j should be decorrelated
     %cc=corr(norminv(rr));cc(j,:) 
     %[min(xx);max(xx)]
     %% 
     if(r==1 && j==1)
     % first run: check for one or two parameter based version
	  try
       ww=densratios{j}(xx,rr);
       usetwo=true;
      catch
	   ww=densratios{j}(xx); % one parameter version
       usetwo=false;
      end
     elseif(usetwo)
      ww=densratios{j}(xx,rr);
     else
      ww=densratios{j}(xx);
     end

     % direct evaluation
     jj=ww>thres_hi;
     l=sum(jj);
     if(l>0)
      sz(jj,:)=ww(jj).*(y(jj,:)-model(xx(jj,:))).^2/2;
      evals=evals+l;
     end
     % nearest neighbors
     jj=ww<=thres_hi & ww>thres_lo;
     l=sum(jj);
     if(l>0)
         xxx=xx(jj,:);
         %yy=zeros(l,dim_out);
         %for m=1:l
         % [~,i_nn]=min(sum((x-xxx(m,:)).^2,2));
         % yy(m,:)=y(i_nn,:);
         %end
         %yy=y(knnsearch(x,xxx),:);
         yy=y(knnsearch(x,xxx,'Distance','seuclidean'),:); 
         sz(jj,:)=ww(jj).*(y(jj,:)-yy).^2/2;
         %% DEBUG
         if(nargin>5 && ~isempty(debugmodel))
         subplot(ceil(sqrt(d)),round(sqrt(d)),j)
         %plot3(yy,debugmodel(xx(jj,:)),ww(jj),'.');
         plot(yy,debugmodel(xx(jj,:)),'.');
         end
         %
         %%
     end
     %
     mu = mean(sz);
     if(dim_out==1)
      Ti(r,j)=mu;
     % permutation-1-dependence aware variance  [ 95% Cband: 1.96*sqrt()]
      VTi(r,j)=mean((sz-mu).^2)+2*mean((sz-mu).*(sz(drm)-mu));
     else
      Ti(r,j,:)=mu;
      VTi(r,j,:)=mean((sz-mu).^2)+2*mean((sz-mu).*(sz(drm,:)-mu));
     end
%     mean((sz-mu).^2) % variance
%     2*mean((sz-mu).*(sz(drm)-mu)) % expect non-zero
%     2*mean((sz-mu).*(sz(drm(drm))-mu)) % expect zero
     % plain bootstrap
     % mu_boot=bootstrp(B,@mean,sz);
     % block bootstrap, for dependent series
      Blen = ceil(n^(1/3)); % Hall et al., Lahiri
      nprime=floor(n/Blen); 
      bb=randi(n,B,nprime);
      if(dim_out==1)
       mu_bboot = sz(bb);
       for i=1:Blen-1
         bb=drm(bb); % next index in permutation cycle
         % bb=mod(bb,n)+1; % next realisation in sample
         mu_bboot = mu_bboot+sz(bb);
       end
       mu_boot=sum(mu_bboot,2)/(Blen*nprime);
     %
       Ti_debias(r,j)=2*mu-mean(mu_boot);
       VTi_boot(r,j)= sum( (mu_boot-mu).^2 )/(B-1)*Blen;
       Strap_all(r,j,:)=mu_boot;
      else
       warning('bootstrap for multivariate output unimplemented.');
      end
    end
   end
%
   disp(['Model evaluations ' num2str(evals) ])
   misc.evals=evals;
   misc.Ti_debias=Ti_debias;
   misc.VTi_boot=VTi_boot;
   misc.Strap_all=Strap_all;
end

function testme
%%
ishigami
n=4096;
x=trafo(sobolpoints(n,k)); % Ts == mean(Tb) under qmc
%x=trafo(rand(n,k));
%y=model(x);

constone=@(x,u)ones(size(x,1),1);
%%
fratios={constone,constone,constone,constone};

[T,V,VV,more]=totalsdep_pnf(x,model,fratios);
T./V
%%
gausscop=@(z,rho)1/sqrt(1-rho^2)*exp(-rho/(2*(1-rho^2))...
    *(rho*(z(:,1).^2+z(:,2).^2)-2*z(:,1).*z(:,2)));
k=3;
rhos=-.9:.1:.9;
Ts=zeros(length(rhos),k);T2s=zeros(length(rhos),k);
Tstrap=zeros(length(rhos),k);
Vs=zeros(size(rhos));
twice=false;
tic
for i=1:length(rhos)
   rho=rhos(i);
   S=eye(k,k);S(1,3)=rho;S(3,1)=rho;
   xc=trafo(normcdf(norminv(sobolpoints(n,k))*sqrtm(S)));
   %xc=trafo(normcdf(norminv(rand(n,k))*sqrtm(S)));
   % xc=iccorrelate(x,S);
   fratios={@(x,u)gausscop(norminv(u(:,[1,3])),rho),constone,...
             @(x,u)gausscop(norminv(u(:,[1,3])),rho),constone};
         %[Ts(i,:),Vs(i),Tb]=totalsdep(xc,model,fratios);
         [t1,v,t2,l]=totalsdep_pnf(xc,model,fratios,3);
      if(~twice)
         Ts(i,:)=mean(t1);
         T2s(i,:)=mean(t2);
       else
         Ts(i,:)=mean(l.Ti_debias);
         T2s(i,:)=mean(l.VTi_boot);
       end
       twice=~twice;

         Vs(i)=v;

        % Tstrap(i,:)=mean(Tb);
end
toc
if(twice), hold on; else clf; end
%subplot(1,2,1); plot(rhos,Ts,'-'); %,rhos,Tstrap,'--')
%subplot(1,2,2);
plot(rhos,Ts./Vs','-',rhos,(Ts+T2s)./Vs','-.',rhos,(Ts-T2s)./Vs','-.'); %,'-',rhos,Tstrap./Vs','--')
xlabel('correlation')
%%
end
