function [Si,Ti,V,info]=hoeff2si(k,n,model,trafo,randomsource)
% HOEFFSI Variance based sensitivity using Hoeffding U statistics variance
% and covariance estimators -- with error bars
% [SI,TI,V,info]=HOEFFSI(K,N,MODEL,TRAFO) computes first order effects SI and 
% total effects TI for the MODEL with K inputs using input transformation 
% TRAFO from unit hypercube of the basic sample block of size N. 
% The structure INFO contains the estimator variances. Requesting this 
% return value enables graphics.
% Divide by the output variance V to get relative values.
% 
% Costs: N(K+2) model evaluations
% See end of file for an example usage.

% inspired by Gamboa et al.
%%
% create 2 sets of samples
if(nargin<5), randomsource=[]; end
[ua,ub]=createsample(k,n,randomsource);

xa=trafo(ua);xb=trafo(ub);
ya=model(xa);yb=model(xb);evals=2;

% Hoeffding variance estimator 
V=zeros(1,3);
% Hoeffding estimator variance (however, without symmetry)
S=zeros(1,3);
for j=1:n 
 % all Hoeffding U statistics
 Phis=sum([ (ya(j)-ya).^2, (yb(j)-yb).^2,(ya(j)-ya).*(yb(j)-yb)])/(2*(n-1));
 V=V+Phis/n;
 % estimator variance via sum of squares
 S=S+Phis.^2/n;
end
VV=S-V.^2;

Si=zeros(1,k);
Ti=zeros(1,k);
VSi=zeros(1,k);
VTi=zeros(1,k);
for i=1:k
 xi=xa;xi(:,i)=xb(:,i);
 yi=model(xi);
 evals=evals+1;
 
 % Hoeffding covariance estimator
 ST=zeros(1,2);
 SST=zeros(1,2);
 for j=1:n
  yaa=ya(j)-ya;
  yai=yaa-(yi(j)-yi);
  % totals , first order effects
  Phis=sum([ yaa.*(yai),(yb-yb(j)).*yai])/(2*(n-1));
  ST=ST+Phis/n;
  SST=SST+Phis.^2/n;
 end
% adjust
 Ti(i) = ST(1);           Si(i) = ST(2);
 VTi(i)= SST(1)-ST(1)^2; VSi(i) = SST(2)-ST(2).^2;
end
V=[V(1);V(2)]-V(3);
if(nargout>3)
 info=struct('evals',evals*n,'VarV',VV,'VarS',VSi,'VarT',VTi);
% do gfx, if info is requested
% plot of estimates w/ confidence intervals
 bars=[V(2),Si;V(1),Ti]';
% split the barplot into two calls so to minimize the gap
 w=.4;
 h=bar((0:k)-w*(5/8),bars(:,1),w); 
 set(h,'FaceColor','r');
 hold on
 h=bar((0:k)+w*(5/8),bars(:,2),w);
 set(h,'FaceColor','b');
 % two-sided 5% under normal assumption
 errs1=1.96*2*sqrt(max(0,[VV(2),VSi]')/n); 
 errs2=1.96*2*sqrt(max(0,[VV(1),VTi]')/n);
 er = errorbar((0:k)-.25,bars(:,1),errs1,errs1);
 set(er,'Color', [0 0 0]);
 set(er,'LineStyle','none');
 er = errorbar((0:k)+.25,bars(:,2),errs2,errs2);
 set(er,'Color', [0 0 0]);
 set(er,'LineStyle','none');
 hold off
 a=axis;axis([-.5,k+.5, a(3), a(4)]);
 set(gca, 'XTick',0:k);
 ll=get(gca,'XTickLabel');ll{1}='Variance';set(gca,'XTickLabel',ll);
 legend('Main Effects','Total Effects')
end
end

function [ua,ub]=createsample(k,n,randomsource)
% create 2 sets of samples
if ~isempty(randomsource)
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('HOEFFSI: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use builtin QMC generator
    try
        u=net(sobolset(2*k),2^4+n);
    catch
        % for octave, port of Joe&Kuo 
        u=sobolpoints(2^4+n,2*k);
    end
    ua=u((2^4)+1:end,1:k);
    ub=u((2^4)+1:end,k+1:end);
%%
end
end

function testhoeffsi
%%
model=@(x)sin(x(:,1)).*(1.0+0.1*x(:,3).^4)+ 7.0*(sin(x(:,2))).^2;
trafo=@(u)(u-.5)*2*pi;
k=4;
n=1024;
[Si,Ti,V,evals]=hoeffsi(k,1024,model,trafo);
S=Si./V(2:-1:1)
T=Ti./V(1:2)
for r=1:5
[Si,Ti,V,evals]=hoeffsi(k,1024,model,trafo,@rand);
S=Si./V(2:-1:1)
T=Ti./V(1:2)
end
%%
% more tests
%%
R=round(1000/4);
n=64*4;
S=zeros(R,k);
T=zeros(R,k);
S2=zeros(R,k);
T2=zeros(R,k);
Vs=zeros(R,2);
for r=1:R
    u=rand(n,2*k);
[Si,Ti,V,evals]=hoeffsi(k,n,model,trafo,u);
S(r,:)=mean(Si./flipud(V));
T(r,:)=mean(Ti./V);
Vs(r,:)=V';
[Si,Ti]=ihssi(k,2*n,model,trafo,[u;u(:,[2*k:end,1:2*k-1])]);
S2(r,:)=Si;
T2(r,:)=Ti;
end
%%
anaSi=[.3139,.4424,0,0];
anaTi=[1-anaSi(2),anaSi(2),1-anaSi(1)-anaSi(2),0];
subplot(2,2,1);boxplot(S);hold on
stairs(.5:k+.5,[anaSi,anaSi(end)],'k:','LineWidth',3); hold off
subplot(2,2,2);boxplot(T);hold on;
stairs(.5:k+.5,[anaTi,anaTi(end)],'k:','LineWidth',3); hold off
subplot(2,2,3);boxplot(S2);hold on
stairs(.5:k+.5,[anaSi,anaSi(end)],'k:','LineWidth',3); hold off
subplot(2,2,4);boxplot(T2);hold on;
stairs(.5:k+.5,[anaTi,anaTi(end)],'k:','LineWidth',3); hold off
%%
end

function testhoeffxx
%%
ishigami
%%
% analytical values
V1_a=(1+.1*pi^4/5)^2/2;
V13_a=8*.1^2*pi^8/(5*3)^2;
V2_a=49/8;
%%

n=512
subplot(2,2,1)
[S,T,V,info]=hoeff2si(k,n,model,trafo,@rand)
title('Hoeffding SA with 95%CI, SRS 512') 
hold on;
stairs(-0.5:.5:4.5,[V1_a+V13_a+V2_a,V1_a+V13_a+V2_a,V1_a,V1_a+V13_a,V2_a,V2_a,0,V13_a,0,0,0],'k:','LineWidth',1);
hold off
% sneak in the missing analytical label
l=legend();l.String{3}='Analytic value';
subplot(2,2,2)
R=100;
Ss=zeros(R,k);
Ts=zeros(R,k);
Vs=zeros(R,2);
for r=1:R
[Ss(r,:),Ts(r,:),Vs(r,:)]=hoeff2si(k,n,model,trafo,@rand);
end
boxplot([Vs,Ss(:,1),Ts(:,1),Ss(:,2),Ts(:,2),Ss(:,3),Ts(:,3),Ss(:,4),Ts(:,4)])
title('Hoeffding SA, 100 replicates, SRS 512')
a=gca;
set(a,'XTick',1.5:2:9.5,'XTickLabel',{'Variance','1','2','3','4'})
hold on
stairs(0.5:1:10.5,[V1_a+V13_a+V2_a,V1_a+V13_a+V2_a,V1_a,V1_a+V13_a,V2_a,V2_a,0,V13_a,0,0,0],'k:','LineWidth',1);
hold off
%% and now the same with goda
subplot(2,2,3)
[~,~,~,T,varT]=shapleygodadual(k,n,model,trafo,0,'');
h=bar(1:k,T); set(h,'FaceColor','b');hold on
eb=errorbar(1:k,T,2*sqrt(varT),2*sqrt(varT));
set(eb,'LineStyle','none','Color','k');
V_a=V1_a+V13_a+V2_a;
stairs(0.5:k+0.5,[V1_a+V13_a,V2_a,V13_a,0,0]./V_a,'k:','LineWidth',1);
hold off
title('Goda Totals with 95%CI, SRS 512')
Ts=zeros(R,k);
for r=1:R
[~,~,~,Ts(r,:)]=shapleygodadual(k,n,model,trafo,0,'');
end
subplot(2,2,4)
boxplot(Ts);
title('Goda Totals, 100 replicates, SRS 512')
hold on
stairs(0.5:k+0.5,[V1_a+V13_a,V2_a,V13_a,0,0]./V_a,'k:','LineWidth',1);
hold off
%%
end
