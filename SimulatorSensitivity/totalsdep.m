function [Ti,V,Tb,Phis]=totalsdep(x,model,densratios,R)
% TOTALSDEP Total effects using density ratios
%  [T,V]=TOTALSDEP(X,G,R) computes the total effects T of the model G
%  using the basic block sample X (n x d) and the list of ratio functions
%  R={  f(x) / fi(x(:,i)f(x(:,d\{i}) | i=1,...d }
%  V is the output variance
% [~,~,TB]=TOTALSDEP(X,G,G,B) provides bootstrap information (B replicates, default 1000)
% Ref: E. Borgonovo, E. Plischke, C. Prieur: Total Effects 
%      with Constrained Features, Stat. & Comput. (2024, forthcoming)

% written by elmar.plischke@tu-clausthal.de

   [n,d]=size(x);
   % if densratios are copula-based
   [xr,xi]=sort(x);
   for j=1:d
      xr(xi(:,j),j)=(2*(1:n)-1)/(2*n); % emp. cdf (without ties), avoid 0/1
   end
%
   y=model(x); evals=n;
   V=var(y);
   Ti=zeros(1,d);
   if(nargout>2)
     if(nargin<4)
      R=1000; 
     end
      Tb=zeros(R,d);
   else 
      R=0;
   end
   if R>0, Phi=zeros(n,n); end % might be memory hungry for large sample sizes

   for j=1:d
      if(R>0), Phi(:)=0; end % clear
	  T=zeros(n,1);
      for i=1:n
%	     xx=[repmat(x(i,1:j-1),n,1),...
%		            x(1:n,j),...
%		     repmat(x(i,j+1:d),n,1)];
        xx=ones(n,1)*x(i,:);
        xx(:,j)=x(:,j);

%         rr=[repmat(xr(i,1:j-1),n,1),...
%		            xr(1:n,j),...
%		     repmat(xr(i,j+1:d),n,1)];
        rr=ones(n,1)*xr(i,:);
        rr(:,j)=xr(:,j); 
		 try
          ww=densratios{j}(xx,rr);
		 catch
		  ww=densratios{j}(xx); % one parameter version
		 end
         jj=ww>0; jj(i)=false; % only keep off-diag with pos. weight
        
		 z=ww(jj).*(y(i)-model(xx(jj,:))).^2/2;
         evals=evals+sum(jj);
         T(i)=sum(z)/(n-1);
		 if(R>0), Phi(i,jj)=z; end
      end
      ET=mean(T);
      Ti(j)=ET;
      
      if(R>0)
	     Phi=(Phi+Phi')/2;
	     for r=1:R
	        ii=randi(n,n,1);
	        Tb(r,j)=mean(sum(Phi(ii,ii)))/(n-1);
         end
      end
	  if(nargout>3), Phis{j}=Phi; end
   end
%
   disp(['Model evaluations ' num2str(evals) ])
end

function testme
%%
ishigami
n=256; % 512;
x=trafo(sobolpoints(n,k)); % Ts == mean(Tb) under qmc
%x=trafo(rand(n,k));
%y=model(x);

constone=@(x,u)ones(size(x,1),1);
%%
fratios={constone,constone,constone,constone};

[T,V,Tb]=totalsdep(x,model,fratios);
T./V
mean(Tb)/V
std(Tb)/V
%%
gausscop=@(z,rho)1/sqrt(1-rho^2)*exp(-rho/(2*(1-rho^2))...
    *(rho*(z(:,1).^2+z(:,2).^2)-2*z(:,1).*z(:,2)));
k=3;
rhos=-.9:.05:.9;
Ts=zeros(length(rhos),k);
Tstrap=zeros(length(rhos),k);
Vs=zeros(size(rhos));
tic
for i=1:length(rhos)
   rho=rhos(i);
   S=eye(k,k);S(1,3)=rho;S(3,1)=rho;
   xc=trafo(normcdf(norminv(sobolpoints(n,k))*chol(S))); % sqrtm(S))); % 
   % xc=iccorrelate(x,S);
   fratios={@(x,u)gausscop(norminv(u(:,[1,3])),rho),constone,...
             @(x,u)gausscop(norminv(u(:,[1,3])),rho),constone};
         %[Ts(i,:),Vs(i),Tb]=totalsdep(xc,model,fratios);
         [Ts(i,:),Vs(i)]=totalsdep(xc,model,fratios);
        % Tstrap(i,:)=mean(Tb);
end
toc
%%
%subplot(1,2,1); plot(rhos,Ts,'-'); %,rhos,Tstrap,'--')
%subplot(1,2,2); plot(rhos,Ts./Vs','-',rhos,Tstrap./Vs','--')
subplot(1,2,2); plot(rhos,Ts./Vs','LineWidth',3);hold off
xlabel('Rank correlation between x_1 and x_3')
ylabel('Sensitivity index');
title('Totals via Reweighting'); 
grid on
%%
end
