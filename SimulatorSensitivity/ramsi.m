function [Si,STi,xy]=ramsi(k,n,model,trafo,randomsource,gfx)
% RAMSI First and total effects using Azzini/Mara/Rosati Innovative Algorithm
%      [SI,STI]=RAMSI(K,N,MODEL,TRAFO) computes these sensitivity
%       measures for MODEL taking K parameters with distributions
%       given via TRAFO and for N base samples, hence using
%       2*N*(K+1) model evaluations.
%       [SI,STI]=IHSSI(K,N,MODEL,TRAFO,RSOURCE) uses RSOURCE as random
%       number source which is either a function taking 2 arguments
%       or a N x 2K matrix, e.g.
%       [SI,STI]=IHSSI(K,N,MODEL,TRAFO,@SOBOLSEQ) gives Sobol' algorithm


% create 2 sets of samples
if(nargin>=5)
    if ~isnumeric(randomsource)
%        ua=randomsource(n,k);
%        ub=randomsource(n,k);
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('IHSSI: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use standard uniform random generator
    ua=rand(n,k);
    ub=rand(n,k);
%%
end

% use the provided transformation or identity mapping
if(nargin==3)
    xa=ua;xb=ub;
else
%%
    xa=trafo(ua);xb=trafo(ub);
%%
end

%% model evaluation for reference sample runs
ya=model(xa);
yb=model(xb);
if(nargout==3)
    xy=[xa,ya;xb,yb];
end

Si=ones(1,k);STi=ones(1,k);
%%
for i = 1:k
%% replace ith parameter from run a with the one from run b
    xi=xa;xi(:,i)=xb(:,i);
    xj=xb;xj(:,i)=xa(:,i);

    % evaluate model
    yi=model(xi);
    yj=model(xj);
    % save output if requested
    if(nargout==3)
        xy=[xy;xi,yi;xj,yj];
    end
    % show scatterplots
    if(nargin>=6)
        subplot(2,k,i)
        plot([yi;yj],[yb;ya],'.'); %,'MarkerSize',2);
        title([ gfx ': main effect']);
        %linreg(yi,yb,[ gfx ': main effect']);
        xlabel(['y_{' num2str(i) '}']);ylabel('y')
        subplot(2,k,i+k)
        plot([yi;yj],[ya;yb],'.');% ,'MarkerSize',2);
        title([ gfx ': total effect']);
        %linreg(yi,ya,[ gfx ': total effect']);
        xlabel(['y_{' num2str(i) '}']);ylabel('y''')
    end

% new
 V     =   (ya-yb)'*(ya-yb)+(yi-yj)'*(yi-yj);
 Si(i) = 2*(ya-yi)'*(yj-yb)/V;
 STi(i)=  ((ya-yi)'*(ya-yi)+(yb-yj)'*(yb-yj))/V;
%%
end
end

function testram
  %%
  R=100;
  n=64; %128;
  ishigami
  for r=1:R
    u=rand(n,2*k);
    [S,T]=ramsi(k,n/2,model,trafo,u(1:n/2,:));
    SH(r,:)=S;
    TH(r,:)=T;
    [S,T]=ihssi(k,n,model,trafo,u);
    SS(r,:)=S;
    TS(r,:)=T;
  end
  subplot(2,2,1);boxplot(SH,'plotstyle','compact');axis([.5,k+.5,-.1,.8])
  subplot(2,2,2);boxplot(TH,'plotstyle','compact');axis([.5,k+.5,-.1,.8])
  subplot(2,2,3);boxplot(SS,'plotstyle','compact');axis([.5,k+.5,-.1,.8])
  subplot(2,2,4);boxplot(TS,'plotstyle','compact');axis([.5,k+.5,-.1,.8])
  %%
end