function [m,s,mstar,xy,T]=morrisEE(k,r,model,trafo,delta)
% MORRISEE Screening/elementary effects with fixed step size.
%      MYU=MORRIS(K,N,MODEL,TRAFO) computes these sensitivity
%       measures for MODEL taking K parameters with distributions
%       given via TRAFO and for N base samples, hence using
%       N*(K+1) model evaluations.

% this is elementary effects
if(nargin<=4), delta=0.1; end
D=ceil(1/delta)-1;
T=linspace(0,1,D+1); % avoid zeros and ones
xs=[];ys=[];
for i=1:r
   u0=T(1+floor(D*rand(1,k)));
   ud=diag(sign(rand(1,k)-.5))*delta;
   u=[u0;bsxfun(@plus,u0,ud)];  % add delta on u scale
   x=trafo(u);
%   x0=trafo(u0);
%   x=[x0;bsxfun(@plus,x0,ud)]; % add delta on x scale
   xs=[xs;x];
   y=model(x);ys=[ys;y];
   z(:,i)=(y(2:end)-y(1))./diag(ud);
   %t(:,i)=(y(2:end)-y(1)).^2./abs(diag(ud));
end
m=mean(z,2)';
s=std(z,[],2)';
mstar=mean(abs(z),2)';
T=mean(z.^2,2)'/2;
%sstar=std(abs(z),[],2);
xy=[xs,ys];
%% gfx
plot(abs(m),s,'o',mstar,s,'+')
xlabel('|\mu|,\mu^*')
ylabel('\sigma^2')
legend('|\mu|','\mu^*')
[~,ii]=maxk(s,6);
offset=diff(axis);
 text(mstar(ii)+offset(1)/100,...
      s(ii)-offset(3)/100,num2str(ii'),'FontSize',12)
%      s(ii)-offset(3)/100,names(ii),'FontSize',12)
 set(gca,'FontSize',12)
end