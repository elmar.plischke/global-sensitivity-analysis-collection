function [Myu,xy]=morris(k,n,model,trafo,randomsource,gfx)
% MORRIS Screening/elementary effects.
%      MYU=MORRIS(K,N,MODEL,TRAFO) computes these sensitivity
%       measures for MODEL taking K parameters with distributions
%       given via TRAFO and for N base samples, hence using
%       N*(K+1) model evaluations.


% create 2 sets of samples
if(nargin>=5 && ~isempty(randomsource))
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('MORRIS: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use standard uniform random generator
    ua=rand(n,k);
    ub=rand(n,k);
%%
end

% use the provided transformation or identity mapping
if(nargin==3)
    xa=ua;xb=ub;
else
%%
    xa=trafo(ua);xb=trafo(ub);
%%
end

%% model evaluation for reference sample runs
ya=model(xa);
%yb=model(xb);
if(nargout==2)
    xy=[xa,ya];
end

Myu=ones(3,k);STi=ones(1,k);
%Si_=ones(1,k);STi_=ones(1,k);
%%
for i = 1:k
%% replace ith parameter from run a with the one from run b
    xi=xa;xi(:,i)=xb(:,i);
    % evaluate model
    yi=model(xi);
    % save output if requested
    if(nargout==2)
        xy=[xy;xi,yi];
    end

	% difference quotient
	dydx=(yi-ya)./(xb(:,i)-xa(:,i));

    Myu(1,i)=mean(dydx);
    Myu(2,i)=mean(abs(dydx));
	Myu(3,i)=std(dydx);
%%
end
    % show scatterplots
    if(nargin>=6)
        plot(abs(Myu(1,:)),Myu(3,:),'b*'); %,'MarkerSize',2);
        hold on
        plot(Myu(2,:),Myu(3,:),'go'); %,'MarkerSize',2);
        [~,sens]=sort(-Myu(2,:)); % or 3?
        offset=diff(axis);
        l=min(6,k);
        text(abs(Myu(1,sens(1:l)))+offset(1)/100,...
            Myu(3,sens(1:l))-offset(3)/100,num2str(sens(1:l)'))
		hold off
        xlabel('|\mu|,\mu*');ylabel('\sigma');title(gfx); 
		legend('\mu','\mu*','Location','NorthWest')
    end	
end
