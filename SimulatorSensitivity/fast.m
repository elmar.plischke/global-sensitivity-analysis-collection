function Si = fast(k,function2evaluate,distTransform)
%SI = FAST(K)
% First order indicies for a given model
% computed with �Fourier Amplitude
% Sensitivity Test� (FAST).
% R. I. Cukier, C. M. Fortuin, Kurt E. Shuler,
% A. G. Petschek and J. H. Schaibly.
% �Study of the sensitivity of coupled reaction
% systems to uncertainties in rate coefficients.
% I-III Theory/Applications/Analysis�
% The Journal of Chemical Physics
% Input:
% K : no. of input factors
% Output:
% SI[] : sensitivity indices
% 
% Source:
% Per-Anders Ekstr�m: Eikos - A Simulation Toolbox for Sensitivity Analysis

% Other used variables/constants:
% OM[] : frequencies of parameters
% S[] : search curve
% X[] : coordinates of sample points
% Y[] : output of model
% OMAX : maximum frequency
% N : number of sample points
% AC[],BC[]: fourier coefficients
% V : total variance
% VI : partial variances
MI = 4; %: maximum number of fourier
% coefficients that may be
% retained in calculating the
% partial variances without
% interferences between the
% assigned frequencies
%
% Frequency assignment to input factors.
OM = SETFREQ(k);
% Computation of the maximum frequency
% �OMAX� and the no. of sample points �N�.
OMAX = OM(k);
N = 2*MI*OMAX+1;
% Setting the relation between the scalar
% variable �S� and the coordinates
% {X(1),X(2),...X(k)} of each sample point.
S = pi/2*(2*(1:N)-N-1)/N;
ANGLE = OM'*S;
X = 0.5+asin(sin(ANGLE'))/pi;
% Transform distributions from standard
% uniform to general.
X = distTransform(X);
% Do the �N� model evaluations.
Y = function2evaluate(X);
% Computation of Fourier coefficients.
AC = zeros(N,1); % initially zero
BC = zeros(N,1); % initially zero
q = (N-1)/2;
N0 = q+1;
for j=2:2:N % j is even
AC(j) = 1/N*(Y(N0)+(Y(N0+(1:q))+Y(N0-(1:q)))'* ...
cos(pi*j*(1:q)/N)');
end
for j=1:2:N % j is odd
BC(j) = 1/N*(Y(N0+(1:q))-Y(N0-(1:q)))'* ...
sin(pi*j*(1:q)/N)';
end
% Computation of the general variance �V�
% in the frequency domain.
V = 2*(AC'*AC+BC'*BC);
% Computation of the partial variances
% and sensitivity indices.
for i=1:k
Vi=0;
for j=1:MI
Vi = Vi+AC(j*OM(i))^2+BC(j*OM(i))^2;
end
Vi = 2*Vi;
Si(i) = Vi/V;
end
end
% Selection of a frequency set. Done
% recursively as described in:
% �A computational implementation of FAST�
% [McRae et al.]
function OM = SETFREQ(k)
OMEGA = [0 3 1 5 11 1 17 23 19 25 41 31 ...
23 87 67 73 85 143 149 99 119 ...
237 267 283 151 385 157 215 449 ...
163 337 253 375 441 673 773 875 ...
873 587 849 623 637 891 943 1171 ...
1225 1335 1725 1663 2019];
DN = [4 8 6 10 20 22 32 40 38 26 56 62 ...
46 76 96 60 86 126 134 112 92 ...
128 154 196 34 416 106 208 328 ...
198 382 88 348 186 140 170 284 ...
568 302 438 410 248 448 388 596 ...
216 100 488 166 0];
OM(1) = OMEGA(k);

for i=2:k
OM(i) = OM(i-1)+DN(k+1-i);
end
% to use the same frequencies as SimLab...
if k==2
OM = [5 9];
elseif k==3
OM = [1 9 15];
end
end