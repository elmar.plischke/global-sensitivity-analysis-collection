function [Si,STi,xy]=ihssi_vec(k,n,model,trafo,randomsource)
% IHSSI First and total effects using Ishigami-Saltelli-Homma.
%      [SI,STI]=IHSSI(K,N,MODEL,TRAFO) computes these sensitivity
%       measures for MODEL taking K parameters with distributions
%       given via TRAFO and for N base samples, hence using
%       N*(K+2) model evaluations.
%       [SI,STI]=IHSSI(K,N,MODEL,TRAFO,RSOURCE) uses RSOURCE as random
%       number source which is either a function taking 2 arguments
%       or a N x 2K matrix, e.g.
%       [SI,STI]=IHSSI(K,N,MODEL,TRAFO,@SOBOLSEQ) gives Sobol' algorithm

% Reference: I.M. Sobol', S. Tarantola, D. Gatelli, S.S. Kucherenko,
%            W. Mauntz: Estimating the approximation error when fixing 
%            unessential factors in global sensitivity analysis,
%            Reliab Eng Syst Safety 92 (2007), 957-960
%            A. Saltelli, P. Annoni, I. Azzini, F. Campolongo, M. Ratto, 
%            S. Tarantola: Variance based sensitivity analysis of model 
%			 output. Design and estimator for the total sensitivity index,
%            Comput. Phys. Comm. 181 (2010), 259-270

% create 2 sets of samples
if(nargin>=5)
    if ~isnumeric(randomsource)
%        ua=randomsource(n,k);
%        ub=randomsource(n,k);
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('IHSSI: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use standard uniform random generator
    ua=rand(n,k);
    ub=rand(n,k);
%%
end

% use the provided transformation or identity mapping
if(nargin==3)
    xa=ua;xb=ub;
else
%%
    xa=trafo(ua);xb=trafo(ub);
%%
end

%% model evaluation for reference sample runs
ya=model(xa);
yb=model(xb);
if(nargout==3)
    xy=[xa,ya;xb,yb];
end
%% estimating mean and var
%Ey=(mean(ya)+mean(yb))/2;
%za=ya-Ey;zb=yb-Ey;
Vy=var([ya;yb]);
%Vy=sum(za.*za+zb.*zb)/(2*n-1);
%Vy=(ya'*ya+yb'*yb)/(2*n)-Ey^2;

Si=ones(size(ya,2),k);STi=ones(size(ya,2),k);
%Si_=ones(1,k);STi_=ones(1,k);
%%
for i = 1:k
%% replace ith parameter from run a with the one from run b
    xi=xa;xi(:,i)=xb(:,i);
    % evaluate model
    yi=model(xi);
    % save output if requested
    if(nargout==3)
        xy=[xy;xi,yi];
    end
    % difference
	dy=yi-ya;
%% Sobol' fixing of unessential factors
    Si(:,i)=sum(yb.*dy)./(n*Vy);
%  % for totals, use Jansen's formula
    STi(:,i)=sum(dy.*dy)./(2*n*Vy);
%%
end
return
