function [K,M,l]=copdist(x,y,chatty)
%COPDIST Copula distance to independence plots.
% COPDIST(X,Y) plots U*V-C(U,V) where U and are the ranks of X and Y
%              and C is the empirical copula.

% written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);

just_rankcusunoro=false;
u=(1:n)/n;
[xr,index]=sort(x);
%v=empcdf(y);
[v,indey]=sort(y);v(indey)=u; % sorry no ties
vr=v(index);
%%
K=zeros(n,k);% Kup=zeros(n,k);Klow=zeros(n,k);
M=zeros(n,k); % scaled cusunoro curves
C=zeros(n,k); % empirical diagonal copula
for j=1:k
    for i=1:n
        w=vr(i,j);
        
        vv=vr(1:i,j);
        c=sum(vv<=w);
        C(i,j)=c/n;
        z=(c-i*w)/n;
        K(i,j)=z;
%        L(ceil(w*n),j)=z;
%        M(i,j)=sum(1-2*vv)/(n*sqrt(3));
        M(i,j)=(i-2*sum(vv))/(2*n)*sqrt(3);
      % z=sum(vr(1:i,j)<=w);
      % Z(i,j)=(z-1)/(n-1); % Kendall plot
    end
end
%%
%subplot(1,2,1);
%plot(sort(Z),(1:n)/n,[0,1],[0,1],':k');
%title('Kendall plot');
%subplot(1,2,1);
%set(gcf,'DefaultAxesColorOrder',jet(k));
% set(gca,'LineStyleOrder',{'-','--','-.'});
if(just_rankcusunoro)
plot(u,M); 
xlabel('u');ylabel('Deviation from rank mean');
else
cols=get(gcf,'DefaultAxesColorOrder');
markers='*ox+v^><';
for i=1:k
    plot(u,K(:,i),markers(1+mod(i-1,length(markers))),'MarkerSize',4,...
    'Color',cols(1+mod(i-1,size(cols,1)),:));
%l=boundary(u',K(:,i),.333);
%plot(u(l),K(l,i),...
%    'Color',cols(1+mod(i-1,size(cols,1)),:));
hold on;
end
set(gca,'ColorOrderIndex',1);
plot(u,M,'-','LineWidth',2); hold off
xlabel('u');ylabel('C(u,v)-uv');
end
%subplot(1,2,2);
%plot(u,L,'*','MarkerSize',2);
%xlabel('v');ylabel('C(u,v)-uv');
if(nargin>2)
%%
  m=7;
  ii=round(linspace(1,n,m+1));
  l.Tau=(4*sum(C)-(n+3))/(n-1);
  l.TigerRho=6*mean(K);
  l.TigerSigma=6*mean(abs(K));
  l.TigerDiscr=4*max(abs(K));
  l.Rho=3*l.Tau-2*l.TigerRho;
  l.RankCR=4*sum(diff(M(ii,:)).^2)*m;
l
%%
end
end
