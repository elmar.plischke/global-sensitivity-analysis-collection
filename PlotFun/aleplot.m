function [Tau,xy]=aleplot(X, fun, K)
% ALEPLOT Accumulated local effects plot.
% Minimal port from the R version
%
% Costs: NUMEL(X)*2 evaluations of FUN (vectorized)
N = size(X,1);  %sample size
d = size(X,2);  %number of predictor variables

if(nargin<3), K=40; end
assert(K<=N,'partition size too large.');

if(nargout>0)
    Tau = zeros(3,d);
    Y = fun(X); % enable tau-ALE
    EY = mean(Y);
    VY = var(Y);
%    xy=[X,Y];
end
for J = 1:d
%% for numerical or integer X[,J], calculate the ALE plot

    %find the vector of z values corresponding to the quantiles of X[,J]
    z= quantile(X(:,J),(0:1/K:1));  % vector of K+1 z values
    z = unique(z);   % necessary if X[,J] is discrete, in which case z could have repeated values 
    K = length(z)-1; %reset K to the number of unique quantile points
    % group training rows into bins based on z
    a1=cut(X(:,J), z);  % N-length index vector indicating into which z-bin the training rows fall
    X1 = X;
    X2 = X;
    X1(:,J) = z(a1);
    X2(:,J) = z(a1+1);
    yhat1 = fun(X1); 
    yhat2 = fun(X2); 
    Delta=yhat2-yhat1;  % N-length vector of individual local effect values

    %K-length vector of averaged local effect values
    
    Delta = accumarray(a1,Delta,[],@mean)'; %Delta = tapply(Delta, a1, @mean); 
    
    fJ = [0, cumsum(Delta)]; %K+1 length vector
    %%
    %now vertically translate fJ, by subtracting its average (averaged across X[,J])
    % frequency count of X[,J] values falling into z intervals
    b1=accumarray(a1,1)'; 
   % c1=[0,cumsum(a1)]; fJ=fJ-trapz(c1,fJ)/N;
    fJ = fJ - sum((fJ(1:K)+fJ(2:(K+1))).*b1)/(2*N);
    subplot(round(sqrt(d)),ceil(sqrt(d)),J);
    plot(z, fJ,'LineWidth',3);
    xlabel(['x_{',num2str(J),'}']);
    ylabel(['f_{',num2str(J),'}(x_{',num2str(J),'})']);

    if(nargout>0)
       % Test for computing tau ALE
    % Tauj = ((yhat1-yhat2).^2+(Y-yhat1).^2+(yhat2-Y).^2)/2;
    % Tauj = accumarray(a1,Tauj,[],@mean)';
    % %Tau(J) = sum(accumarray(a1,Tauj,[],@mean)'./b1);
    % Tau(J)=Tauj*diff(z)'; % cheap integral ?
   
    Tau(1,J)=sum( (Y-yhat1).^2+(yhat2-Y).^2+(yhat1-yhat2).^2)/(6*N*VY)*K^2;
    %Tau(J)=sum( (Y-yhat1).^2+(yhat2-Y).^2)/(4*N)*K^2;xy=[xy;X1,yhat1;X2,yhat2];
    %Tau(J) = mean((derange(yhat1)-Y).^2)/2; 
  %  Tau(J)=(Y.^2+yhat.^2 +EY)'*(yhat1+yhat2)/(N*VY);
   % Tau(2,J)=sum(Y+yhat1+yhat2-3*EY).^2/(9*(N-1))/VY; 
    Tau(2,J)=sum((yhat1+yhat2-2*Y).^2)/(6*N*VY)*K^2;
    Tau(3,J)=sum(accumarray(a1,(yhat1-yhat2).^2,[],@mean))/(2*VY); %sum((yhat1-yhat2).^2)/(2*VY)/K;
     hold on; plot(X(:,J),Y-mean(Y),'r.'); hold off
    end  
end
%disp(evals)
end

function y=cut(x,breaks)
y=zeros(size(x));
for b=breaks(1:end-1)
    y=y+1.*(x>=b);
end
y=y+1.*(x>breaks(end));
end

function z=tapply(x,i,f)
 z=[];
 for j=unique(i)', z(end+1)=f(x(i==j)); end
end

%%
function testale
%%
ishigami
X=trafo(rand(10000,k));
tau=aleplot(X,model);
%%
end
