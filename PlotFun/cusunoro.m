function [z,c,v]=cusunoro(x,y,param_names,do_2nd)
% CUSUNORO Shows CUSUNORO plot and 2nd moment CUSUNORO plots
% CUSUNORO(X,Y) plots the cumulative sum of the reordered normalized output
% CUSUNORO(X,Y,NAMES) specifies input and output names {'x_1',..,'x_k','y'}
% CUSUNORO(X,Y,NAMES,S) adds a plot for the variance based on second order 
%                   finite differences 

% written by elmar.plischke@tu-clausthal.de
test_DBD=0; % now defaults to Dhar Bergsma Dassios trick for variance

plotopts={'LineWidth',2}; 
set(gcf,'DefaultAxesLineStyleOrder',{'-','--','-.',':'});

[n,k]=size(x);
[xr,indx]=sort(x);
yr=y(indx);
Ey=mean(y);
w=(y-Ey).^2;
Vy=sum(w);
c=(0:n)/n;
% CUSUNORO curve
z=[zeros(1,k);cumsum(Ey-yr)/ sqrt(n*Vy)];
if(nargin==2 || nargin>2 && (isempty(param_names) || all(strcmp(param_names,'no_plot'))==0))
if(nargin==4), subplot(1,2+test_DBD,1); end
h=plot(c,z,plotopts{:});
if(nargin>2) && ~isempty(param_names)
 l=legend(h,param_names(1:end-1));
 try, l.AutoUpdate='off';end
 title(param_names{end});
else
  l=legend(cellstr(num2str((1:k)')));
  try, l.AutoUpdate='off'; end
end
l.NumColumns=ceil(k/5);
set(gca,'YGrid','on');
xlabel('input quantiles')
ylabel('deviation from mean'); % ('cumulative sums of normalized output')
end
% 2nd moment CUSUNORO
if(nargin==4)
    if(test_DBD)
wr=w(indx);
Ew=mean(w);
Vw=sum((w-Ew).^2);
%
%
v=[zeros(1,k);cumsum(Ew-wr)./sqrt(n*Vw)];
subplot(1,2+test_DBD,3);
plot(c,v,plotopts{:});
ylabel('deviation (second moments)');
set(gca,'YGrid','on');
xlabel('input quantiles')
    end
wr=(-2*yr+yr([1,1:end-1],:)+yr([2:end,end],:)).^2;
Ew=mean(wr);
Vw=sum(bsxfun(@minus,wr,Ew).^2);
v=[zeros(1,k);bsxfun(@rdivide,cumsum(bsxfun(@minus,Ew,wr)),sqrt(n*Vw))];
subplot(1,2+test_DBD,2);
plot(c,v,plotopts{:});
ylabel('deviation (second order finite differences)');
set(gca,'YGrid','on');
xlabel('input quantiles')


if~isempty(param_names), title(param_names{end}); end
else v=[];
end
end