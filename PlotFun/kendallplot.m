function [W,Z]=kendallplot(x,y,withlimits)
% KENDALLPLOT Visual Test for Independence using Copula 
% Genest and Boies

% plot(sqrt(W),(Z-W)*4) for a better resolution

original_requested=false;
copdist_requested=false;

[n,k]=size(x);
C=zeros(n,k);P=zeros(n,k);
[~,iy]=sort(y);xs=x(iy,:);
 for i=1:n  
%     iy=y(:)<=y(i);
%     for j=1:k
%        ix=xs(:,j)<=xs(i,j);
    ix=isxfun(xs,'<',xs(i,:));
%        C(i,j)=sum(ix(1:i))/n;
    C(i,:)=sum(ix(1:i,:))/n;
%        P(i,j)=(sum(ix).*i)/n^2;
    P(i,:)=sum(ix).*i/n^2;
%    end
 end
 Z=sort(C);%Z(end+1,:)=1;
 % Compute asymptotic under indep. K(w)=Pr(UV<w)=w.*(1-log(w))
 % W = n (n-1 i-1) int w K(w)^i-1 (1-K(w))^ n-i dK(w)
%  K=@(w)w.*(1-log(w));
%  M=100;
%  ws=linspace(0,1,M)';
%  ks=K(ws);ks(1)=0;
%  lws=-log(ws);lws(1)=0;
%  W=zeros(n,1);
%  for i=1:n
%      % bincoeff in octave
%   W(i)=n*nchoosek(n-1,i-1)*...
%    trapz(ws.*ks.^(i-1).*(1-ks).^(n-i).*lws)/(M-1);
%  end
 W=sort(P);
 if(copdist_requested)
 subplot(1,2,1);
 end
% original
if(original_requested)
 plot(W,Z,'-',[0,1],[0,1],'k:');
 xlabel('$uv$','Interpreter','latex')
 ylabel('$C(u,v)$','Interpreter','latex')
else
% modified
  set(gca,'LineStyleOrder',{'-','--','-.'});
  plot(sqrt(W),(Z-W)*4,[0,1],[0,0],'k:')
  %plot(sqrt(C),(C-P)*4,'.',[0,1],[0,0],'k:')
  xlabel('$\sqrt{uv}$','Interpreter','latex')
  ylabel('$4(C(u,v)-uv)$','Interpreter','latex') 
end 
 lg=strcat('x_{' ,num2str((1:k)') ,' }');
 legend(lg,'location','southeast','AutoUpdate','off');
% MW bounds
 if(nargin>2)
  z=linspace(0,1,20);hold on;
  plot(z,1-(2*z-1).^2,'k:',z/2,-z.^2,'k:');
  hold off
 end
if(copdist_requested)
subplot(1,2,2)
plot(linspace(0,1,n),(C-P)*4,'.');
 xlabel('$u$','Interpreter','latex')
ylabel('$4(C(u,v)-uv)$','Interpreter','latex')
end

end
 
function r=isxfun(a,o,b)
% inline singleton extension
switch(o)
    case '+', f=@plus;
    case '-', f=@minus;
    case '*', f=@times;
    case '/', f=@rdivide;
    case '<', f=@le; % less or equal
    case '>', f=@ge;
    case '=', f=@eq;
    otherwise, error('unkown op');
end
r=bsxfun(f,a,b);
end