function plotqq(beobachtung,verteilung,parameter1,parameter2,use_plot_in)
% PLOTQQ Quantil-Quantil-Plot 
%   PLOTQQ(BEOBACHTUNG,'VERTEILUNG',PARAMETER1,PARAMETER2,PLOT_CMD)
%   VERTEILUNG kann auch ein Funktionshandle einer inversen CDF sein.
n=length(beobachtung);
x=sort(beobachtung);

use_plot=@plot; 
%use_plot=@loglog; 
 
v=((1:n)-.5)/n; % Beobachtete Verteilung
if(ischar(verteilung))
    verteilung_txt=verteilung;
    if (nargin==2)
        if strcmp(verteilung,'norm')
         y=icdf(verteilung,v,0,1);
        else   
         y=icdf(verteilung,v);
        end
    elseif (nargin==3)
        y=icdf(verteilung,v,parameter1);
    elseif (nargin==4)
        y=icdf(verteilung,v,parameter1, parameter2);
    elseif (nargin==5)
        y=icdf(verteilung,v,parameter1, parameter2);
         use_plot = use_plot_in;
    else    
        error('PlotQQ: Nur bis zu 2 Parameter unterst�tzt');
    end
else
    verteilung_txt='vorgegebene';
    y=verteilung(v); % assume function handle
    if(nargin==5), use_plot = use_plot_in; end
end
mn=min(min(x),min(y));mx=max(max(x),max(y));


use_plot(y,x,'o',[mn,mx],[mn,mx],'k-');axis([mn,mx,mn,mx])
title(['QQ Plot gegen ' verteilung_txt ' Verteilung']);
ylabel('Beobachtete Werte');xlabel('Erwartete Werte');

%% test f�r quantile
quants=[0.001,.01,.05,.1,.5,.90,.95,.99, .999];
if(ischar(verteilung))
    if (nargin==2)
     if strcmp(verteilung,'norm')
    % icdf requires always parameters under matlab(?)
        rants=icdf(verteilung,quants,0,1);
     else
        rants=icdf(verteilung,quants);
     end
    elseif (nargin==3)
        rants=icdf(verteilung,quants,parameter1);
    elseif (nargin>=4)
        rants=icdf(verteilung,quants,parameter1, parameter2);
    end
else
    rants=verteilung(quants);
end
hold on
for r=rants
    use_plot([r,r],[mn,mx],'k:');
end
hold off
end

