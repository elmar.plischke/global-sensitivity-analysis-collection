function [wau,tau]=posuro(x,y,param_names)
% power sum of reordered output
[n,k]=size(x);
%reorder
[xs,ix]=sort(x);
ys=y(ix);
showcdf=false; % yz=sort(y);
% maximiles and minimiles
%F=(0:n)/n;
G=(1:n)/(n+1);
m=100;
as=log(0.5)./log(linspace(0.5,.99,m)');
%mxl=diff(F.^as,1,2)*ys;
%mnl=diff(1-(1-F).^as,1,2)*ys;
mxl=(as.*(G.^(as-1)))*ys;
mnl=(as.*((1-G).^(as-1)))*ys;
tau=linspace(0.01,.99,2*m-1);
wau=[mnl(end:-1:2,:);mxl]/n;
plot(tau,wau);
ylabel('power weighted output mean');
%plot(wau,tau);
%xlabel('power weighted output mean');
if(showcdf)
hold on;plot(yz,[1;-1]*linspace(0,1,n)+[0;1],'k:');hold off
end
if(nargin>2) && ~isempty(param_names)
    title(param_names{end});
    l=legend(param_names(1:end-1));
    set(l,'NumColumns',ceil(k/5));
end
end