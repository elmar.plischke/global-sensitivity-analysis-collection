function interactionplot(x,y,M,trafo,names)
% INTERACTIONPLOT visually detect interactions
% INTERACTIONPLOT(X,Y) produces scatterplots augmented with partial 
%                      dependence curves.
% INTERACTIONPLOT(X,Y,M,T,N) uses M curves, traforms the input via function
%                       T and uses names in the list N.
% Lines should be parallel if no interactions.
%  Modified for given data and multiple levels.
%  
% Ideas from 
% Jiju Antoni, "Design of Experiments for Engineers and Scientists", 
% Elsevier, 2nd ed, 2014:
% In order to determine whether two process parameters are interacting 
% or not, one can use a simple but powerful graphical tool called 
% interaction graphs. If the lines in the interaction plot are 
% parallel, there is no interaction between the process parameters 
% (Barton, 1990). This implies that the change in the mean response 
% from low to high level of a factor does not depend on the level 
% of the other factor. On the other hand, if the lines are 
% non-parallel, an interaction exists between the factors.
% A. Linder, Planen und Auswerten von Versuchen, Birkh�user, 1969: 
% Die Quadratsumme [] verschwindet demnach, wenn die Kurven der Werte f�r
% die verschiedenen Zeilen parallel verlaufen.[] Je st�rker dagegen die
% Unterschiede zwischen den Zeilen von Spalte zu Spalte voneinander
% abweichen, desto gr��er wird diese Quadratsumme; sie mi�t demnach die
% Wechselwirkung zwischen Zeilen und Spalten.

% written by Elmar Plischke, epl@tu-clausthal.de
 [n,k]=size(x);
 assert(size(y,1)==n,'dimensions mismatch.');

 if (nargin<3) || isempty(M), M=4; end
 if (nargin<4) || isempty(trafo)
  xs=sort(x);
  trafo=@(p)xs(ceil(n*p(:,1)),:);
 end 
 if nargin<5
  names={};
 end
 no_overlay=false;

 z=ones(n,k);
 ms=trafo((2*(1:M)'-1)./2/M*ones(1,k));
 for m=1:(M-1)
  z=z+bsxfun(@ge,x,trafo(m/M*ones(1,k)));
 end
 cnt=1;
 for l=1:(k-1)
  for m=(l+1):k
   %for i=1:M, for j=1:M
   % % conditional output mean given a checkerboard partition
   %  Ia(i,j)= mean(y(z(:,l)==i & z(:,m)==j));  
   % end; end
   Ia=accumarray(z(:,[l,m]),y,[],@mean,nan)

   subplot(k,k,(m-1)*k+l)
   if(~no_overlay)
    % scatterplot 
    plot(x(:,l),y,'.','MarkerSize',1,'Color',[.6 .6 .6]);
    hold on;
   end
   if(any(any(isnan(Ia)))), style='-o';
   else, style='-';end
   plot(ms(:,l),Ia,style,'LineWidth',3);
   hold off
   if(l==1)
    if(length(names)>k)
     ylabel(names{end});
    else
     ylabel('y');
    end
   end
 
  subplot(k,k,(l-1)*k+m)
  if(~no_overlay)
   plot(x(:,m),y,'.','MarkerSize',1,'Color',[.6 .6 .6]);
   hold on;
  end
  plot(ms(:,m),Ia',style,'LineWidth',3); 

  hold off 
  %  if(cnt==1),ylabel('y');end
%  xlabel(['x_{' num2str(m) '}']);
%  title([num2str(m) ' on ' num2str(l) ]);

end;end
  for l=1:k
    subplot(k,k,(l-1)*k+l);
    if(isempty(names))
     text(.5,.25,['x_{' num2str(l) '}'],'FontSize',12);
    else
     text(.5,.25,names{l},'FontSize',18);
    end
    axis off ; box off
end