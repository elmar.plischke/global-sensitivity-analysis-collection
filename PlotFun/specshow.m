function specshow(spectrum,ps,M,scale)
% SPECSHOW(FFTSPEC,FREQUENCIES)
% Show resonances in the spectrum
if(nargin<3), M=5; end
if(nargin<4), scale=2; end
% fake a legend
h=plot([1,2],[0,0],'b',[1,2],[0,0],'g',[1,2],[0,0],'r');
hold on;
%
ps=sort(ps);
V=sum(spectrum(2:end));
mx=min(2.4*max(spectrum(2:end))/V,1);
for  p=ps
 plot(p*[1,1],[0,mx],'b'); % allow for M==0
 for i=1:M    
    for q=ps
        if p<=q, continue; end
        for j=1:M
           for r=ps
               if q<=r, continue; end
               for k=1:M
                   % third order
                 plot(abs(i*p+j*q+k*r)*[1,1],[0,mx],'r');
                 plot(abs(i*p+j*q-k*r)*[1,1],[0,mx],'r');
                 plot(abs(i*p-j*q+k*r)*[1,1],[0,mx],'r');
                 plot(abs(i*p-j*q-k*r)*[1,1],[0,mx],'r');
               end
           end
            % second order
          plot(abs(i*p+j*q)*[1,1],[0,mx],'g');
          plot(abs(i*p-j*q)*[1,1],[0,mx],'g');           
        end
    end
    % first order
    if(i>1), plot(i*p*[1,1],[0,mx],'b'); end
 end
end
if any(M==0)
 last=1+3*(ps(end-1)-1);
else
 last=1+max(ceil(sum(ps)*M*1.1),32);
end 
plot(scale*spectrum(2:floor(end/2))/V,'k*') %last
axis([1,last,0,mx]);
legend(h,{'Direct influence','Pair interaction','Triple interaction'});
hold off
end

