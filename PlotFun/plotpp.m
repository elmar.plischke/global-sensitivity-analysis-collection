function plotpp(beobachtung,verteilung,parameter1,parameter2)
% PLOTPP Probabilistik-Plot 
%   PLOTPP(BEOBACHTUNG,'VERTEILUNG',PARAMETER1,PARAMETER2)
n=length(beobachtung);
x=sort(beobachtung);

v=(1:n)/n; % Beobachtete Verteilung
if (nargin==2)
    w=cdf(verteilung,x)
elseif (nargin==3)
    w=cdf(verteilung,x,parameter1);
elseif (nargin==4)
    w=cdf(verteilung,x,parameter1, parameter2);
else
    error('PlotPP: Nur bis zu 2 Parameter unterst�tzt');
end
%mn=min(min(x),min(y));mx=max(max(x),max(y));
ks=1.2/sqrt(n);
plot(v,w,'o',[0,1],[0,1],'k-',[-ks,1-ks],[0,1],'k:',[ks,1+ks],[0,1],'k:');
axis([0,1,0,1]);
title(['PP Plot gegen ' verteilung ' Verteilung']);
xlabel('Beobachtete Verteilung');ylabel('Erwartete Verteilung');
