function cusunorot(x,y,param_names,cmap)
% CUSUNOROT Time-dependent cumulative sum of reordered normalized output  
 [n,k]=size(x);
 [m,l]=size(y);
  
 [xs,ix]=sort(x);
 GfxCols=3; %round(sqrt(k));
 if(nargin==2 || nargin>2 && (isempty(param_names)))
     param_names=cellstr([char(ones(k,1)*'x_{'),num2str((1:k)'),...
         char(ones(k,1)*'}')]);
 end
 switch(l)
     case 1,
         cols=[0 0 .8]; 
     case 2,
         cols=[0,0,.8;.8,0,.3];
     case {3,4,5},
         cols=hsv(l);
     otherwise
         if(nargin>3)
            cols=cmap(l);
         else
            cols=jet(l);
         end
 end
 for i=1:k % do not change subplot too often
  subplot(ceil(k/GfxCols),GfxCols,i)
  for j=1:l
    if(l==1),z=y; else, z=y(:,j); end
    c=cumsum(mean(z)-z(ix(:,i)))/n/sqrt(var(z));
 
    plot(xs(:,i),c,'Color',cols(j,:)); 
    if(j==1), xlabel(param_names(i)); hold on,end
  end
   hold off
  end
 %h=colorbar('YTickLabelMode','manual',...
 %'YTickLabel', [ t(1) t(5) t(10) t(15) t(20) t(25)]);
 %
 end