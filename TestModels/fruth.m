% the Fruth et al. test function
k=6;
model=@(x)sin(-.5+x*[0;1;0;.9;0;-1.1])+cos(-.8+x*[-1.1;0;1;0;1.1;0]);
trafo=@(u)2*u-1;
