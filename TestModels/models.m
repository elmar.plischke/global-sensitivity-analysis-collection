%% A collection of one-line models

% Ginsbourger MCQMC 2014: influencing the extreme values
model1=@(x)sin(5*x(:,1))+5*x(:,1).*cos(x(:,2));k1=2;
% Nguyen MCQMC 2014
model2=@(x)log(1./sqrt(x(:,1))+1./sqrt(x(:,2)));k2=2;
% Tarantola MCQMC 2014: discontinuous
model3=@(x)sum(x,2)-sum(x<.5,2)/2;

% Branin Hoo(from Virtual Library of simulation experiments)
k4=2;
p4=[1,5.1/(2*pi)^2,5/pi,6,10,1/(8*pi)]; % default parameters
model4_=@(x,p)p(1)*(x(:,2)-p(2)*x(:,1).^2+p(3)*x(:,1)-p(4)).^2 ...
        +p(5)*(1-p(6))*cos(x(:,1))+p(5);
model4=@(x)model4_(x,p4);
trafo4=@(u)[-5+u(:,1)*(10+5),0+u(:,2)*(15-0)];
% Kucherenko MCQMC 2014: all bi=b, .5 ... 100
%model4=@(x,b)prod(bsxfun(@power,x,b),2);
% ... Ellipsoidal
model5=@(x)x.^2 *(1:size(x,2))';
% ...  Hartmann (negative)
model6_=@(x,a,A,P)exp(bsxfun(@minus,-x.^2*A+2*x*(A.*P),sum(A.*P.^2)))*a;
% Hartmann 6dim
a=[ 1.0, 1.2 , 3.0, 3.2]';
A=[10,3,17,3.5,1.7,8;.05,10,17,.1,8,14;3,3.5,1.7,10,17,8;17,8,.05,10,.1,14]';
P=1e-4*[1312,1696,5569,124,8283,5886;2329,4135,8307,3736,1004,9991;...
    2348,1451,3522,2883,3047,6650;4047,8828,8732,5743,1091,381]';
model6=@(x)model6_(x,a,A,P);k6=6;
%Hartmann 3dim
%A=[3 10 30; .1 10 35;3 10 30;.1 10 36]';
%P=1e-4*[3689 1170 2673;4699 4387 7470;1091 8732 5547;381 5743 8828]';
%Hartmann 4dim: cuts out from the 6dim

% Kucherenko Feil Shah Mauntz 2011 (some analytical values in the paper)
modelKFSM_1a=@(x)sum(cumprod(-x,2),2);
modelKFSM_2a=@(x)abs( (4*x(:,1)-2).*(4*x(:,2)-2)).* ...
    prod((abs(4*x(:,3:end)-2)+6.52)./(1+6.52),2);
modelKFSM_1b=@(x)prod((size(x,2)-x)./(size(x,2)-.5),2);
modelKFSM_2b=@(x)(1+1./size(x,2))^size(x,2)*prod(x.^(1/size(x,2)),2);
modelKFSM_3b=@(x)prod((abs(4*x-2)+6.52)./(1+6.52),2);
modelKFSM_1c=@(x)prod(abs(4*x-2),2);
modelKFSM_2c=@(x)2^size(x,2)*prod(x,2);
% Buzzard Xiu - CPC 2011 - Sparse grid, k=10, c,w random
%model_oscillatory=@(x,c,w)cos(2*pi*w(1)+x*c');
%model_productpeak=@(x,c,w)prod( bsxfun(@plus,c.^(-2),...
%                bsxfun(@minus,x,w).^2).^(-1),2);
%model_cornerpeak=@(x,c,w)(1+x*c').^(-1-numel(x(1,:)));
%model_gaussian=@(x,c,w)exp(-sum( bsxfun(@minus,x,w).^2*(c.^2)',2));
% Genz 1987 function family u in [0,1]^k, a > 0^k
% oscillatory
model_genz1=@(x,a,u)cos(2*pi*u(1)+x*a');
% product peak
model_genz2=@(x,a,u)prod(1./bsxfun(@plus,a.^(-2),bsxfun(@minus,x,u).^2),2);
% corner peak
model_genz3=@(x,a,u)(1+x*a').^(-(size(x,1)+1));
% Gaussian
model_genz4=@(x,a,u)exp(-bsxfun(@minus,x,u).^2*(a.^2)');
% C0 Function
model_genz5=@(x,a,u)exp(-abs(bsxfun(@minus,x,u))*a');
% Discontinuous
model_genz6=@(x,a,u)(x(:,1)<u(1)).*(x(:,2)<u(2)).*exp(x*a');

% artificial datasets from
%Conditional Density Estimation with Dimensionality Reduction via
%Squared-Loss Conditional Entropy Minimization
% Voot Tangkaratt,Ning Xie,Masashi Sugiyama
kart=7; % 6 is noise, 7 is switch
trafo_art=@(u)[norminv(u(:,1:6)*diag([1,1,1,1,1,.25])),u(:,7)];
model_arta=@(x)x(:,1).^2+x(:,2).^2+x(:,6);
model_artb=@(x)x(:,2)+x(:,2).^2+x(:,2).^3+x(:,6);
model_artc=@(x)(x(:,1).^2+x(:,6)).*(x(:,7)<=.85)+(2*x(:,6)-4).*(x(:,7)>.85);

% DaVeiga 
% Linkletter et al. 2006
k7=10;
model7=@(x)0.2*x*2.^(-(0:k7-1))';
% Si=STi: 3/4(1/4)^(i-1) /(1-(1/4)^10)
% Loppky et al. 2013
k8=10;
model8=@(x)6*x(:,1)+4*x(:,2)+5.5*x(:,3)+3*x(:,1).*x(:,2)...
    +2.2*x(:,1).*x(:,3)+1.4*x(:,2).*x(:,3)+x(:,4)...
    +0.5*x(:,5)+0.2*x(:,6)+0.1*x(:,7);

%% Wei Lu Song
k9=6;
trafo9=@(u)norminv(u);
trafo9c=@(u)iccorrelate(trafo9(u),eye(k9)+...
diag([0.9,0,0,0,0.9],1)+diag([0.9,0,0,0,.9],-1));

model9=@(x)x(:,1)/2+x(:,2).^2/4+x(:,3).^3/6+...
 x(:,5).*x(:,6).*exp(x(:,4))/16+...
 x(:,6).*exp(x(:,5))/12+exp(x(:,6))/8;
%% Chang Kruger Kustra Zhang: Canonical Correlation Analysis 2013
k10=10;
trafo10=@(u)[ (u(:,1)-.5)*2*pi,norminv(u(:,2:6),0,1),norminv(u(:,7:8),0,.1),norminv(u(:,9:10),0,.5)];
trafo10x=@(z)[sin(z(:,1))+z(:,7),z(:,2:5)];
model10y=@(z)[cos(z(:,1))+z(:,8),cos(z(:,2)+z(:,3))+z(:,9),z(:,4)+z(:,10),z(:,6)];
%z=trafo10(sobolseq(n,k10));x=trafo10x(z);y=model10y(z);

%% DeLozzo Marrel
k11=3;
%
a11=sinh(sqrt(3))/sqrt(3); b11=sqrt(sinh(2*sqrt(3))/2/sqrt(3)-a11^2);
c11=1/sqrt(.5-sin(4*sqrt(3))/8/sqrt(3));
trafo11=@(u)sqrt(12)*(u-.5);
model11a=@(x)x;
model11b=@(x)(exp(x)-a11)/b11;
model11c=@(x)c11*sin(2*x);
% an on/off vector [0 0 0] ... [ 1 1 1]
model11=@(x,a)a(1)*model11a(x(:,1))+a(2)*model11b(x(:,2))+a(3)*model11c(x(:,3));
% main = total: a_i^2 / sum_j a_j^2

model11d=@(x,a)model11b(x(:,1)).*(1-a*model11b(x(:,2)));
%% Crespi Data Generating Mechanism -  ESREL 2015
k12=2;
model=@(x)x(:,1).^2.*cos(x(:,1))-sin(3*x(:,1)).*exp(-x(:,1).^2)-cos(x(:,1).^2)+x(:,1).*(x(:,2)-1);
trafo=@(u)[-5.5+u(:,1)*11,norminv(u(:,2))];
%% Strong Oakley MDM
kSO=19;
means1SO=[1000,10,5.2,400,70,0.3,3.0,25,-.1,.5,1000,10,5.2,70,0.3,3.0,25,-.1,.5];
stds1SO=[1,2,1,200,10,.1,.5,10,.02,.2,1,2,1,10,.1,.5,10,.02,.2];
model1SO=@(x)10000*(prod(x(:,5:7),2)+prod(x(:,8:10),2))-(x(:,1)+prod(x(:,2:4),2));
means2SO=[1500,8,6.1,400,80,0.3,3.0,20,-.1,.5,1500,8,6.1,80,0.3,3.0,20,-.1,.5];
stds2SO=[1,2,1,200,10,.005,1,5,.02,.2,1,2,1,10,.05,1,5,.02,.2];
model2SO=@(x)10000*(prod(x(:,14:16),2)+prod(x(:,17:19),2))-(x(:,11)+prod(x(:,12:13).*x(:,4),2));
%trafo1SO=@(u)norminv(u,repmat(meansSO,size(u,1),1),repmat(stdsSO,size(u,1),1));
% 5,7,14,16 : all pairwise correlated 0.6 
% 6,15: correlated 0.6 
SSO=eye(kSO);
SSO(5,[7,14,16])=.6; SS0(7,[5,14,16])=.6; 
SSO(14,[5,7,16])=.6; SSO(16,[5,7,14])=.6;
SSO(6,15)=.6;SSO(15,6)=.6;
trafo1SO=@(u)bsxfun(@plus,means1SO,norminv(u)*chol(SSO)*diag(stds1SO));
trafo2SO=@(u)bsxfun(@plus,means2SO,norminv(u)*chol(SSO)*diag(stds2SO));
%u=sobolpoints(n,kSO);
%x1=trafo1SO(u);x2=trafo2SO(u)
%modelSO=@(x)max(model1SO(x1),model2SO(x2))
%% Oakley O'Hagan (2004)
kOO=15;
a1OO=[0.0118 0.0456 0.2297 0.0393 0.1177 0.3865 0.3897 ...
    0.6061 0.6159 0.4005 1.0741 1.1474 0.7880 1.1242 1.1982]';
a2OO=[0.4341 0.0887 0.0512 0.3233 0.1489 1.0360 0.9892 ...
    0.9672 0.8977 0.8083 1.8426 2.4712 2.3946 2.0045 2.2621]';
a3OO=[0.1044 0.2057 0.0774 0.2730 0.1253 0.7526 0.8570 1.0331 ...
    0.8388 0.7970 2.2145 2.0382 2.4004 2.0541 1.9845]';
MOO=[-2.2482886e-2 -1.8501666e-1  1.3418263e-1  3.6867264e-1  1.7172785e-1  1.3651143e-1 -4.4034404e-1 -8.1422854e-2 7.1321025e-1 -4.4361072e-1 5.0383394e-1 -2.4101458e-2 -4.5939684e-2 2.1666181e-1 5.5887417e-2;...
  2.5659630e-1  5.3792287e-2  2.5800381e-1  2.3795905e-1 -5.9125756e-1 -8.1627077e-2 -2.8749073e-1  4.1581639e-1  4.9752241e-1  8.3893165e-2 -1.1056683e-1  3.3222351e-2 -1.3979497e-1 -3.1020556e-2 -2.2318721e-1;...
 -5.5999811e-2  1.9542252e-1  9.5529005e-2 -2.8626530e-1 -1.4441303e-1  2.2369356e-1  1.4527412e-1  2.8998481e-1  2.3105010e-1 -3.1929879e-1 -2.9039128e-1 -2.0956898e-1  4.3139047e-1  2.4429152e-2  4.4904409e-2;...
  6.6448103e-1  4.3069872e-1  2.9924645e-1 -1.6202441e-1 -3.1479544e-1 -3.9026802e-1  1.7679822e-1  5.7952663e-2  1.7230342e-1  1.3466011e-1 -3.5275240e-1  2.5146896e-1 -1.8810529e-2  3.6482392e-1 -3.2504618e-1;...
 -1.2127800e-1  1.2463327e-1  1.0656519e-1  4.6562296e-2 -2.1678617e-1  1.9492172e-1 -6.5521126e-2  2.4404669e-2 -9.6828860e-2  1.9366196e-1  3.3354757e-1  3.1295994e-1 -8.3615456e-2 -2.5342082e-1  3.7325717e-1;...
 -2.8376230e-1 -3.2820154e-1 -1.0496068e-1 -2.2073452e-1 -1.3708154e-1 -1.4426375e-1 -1.1503319e-1  2.2424151e-1 -3.0395022e-2 -5.1505615e-1  1.7254978e-2  3.8957118e-2  3.6069184e-1  3.0902452e-1  5.0030193e-2;...
 -7.7875893e-2  3.7456560e-3  8.8685604e-1 -2.6590028e-1 -7.9325357e-2 -4.2734919e-2 -1.8653782e-1 -3.5604718e-1 -1.7497421e-1  8.8699956e-2  4.0025886e-1 -5.5979693e-2  1.3724479e-1  2.1485613e-1 -1.1265799e-2;...
 -9.2294730e-2  5.9209563e-1  3.1338285e-2 -3.3080861e-2 -2.4308858e-1 -9.9798547e-2  3.4460195e-2  9.5119813e-2 -3.3801620e-1  6.3860024e-3 -6.1207299e-1  8.1325416e-2  8.8683114e-1  1.4254905e-1  1.4776204e-1;...
 -1.3189434e-1  5.2878496e-1  1.2652391e-1  4.5113625e-2  5.8373514e-1  3.7291503e-1  1.1395325e-1 -2.9479222e-1 -5.7014085e-1  4.6291592e-1 -9.4050179e-2  1.3959097e-1 -3.8607402e-1 -4.4897060e-1 -1.4602419e-1;...
  5.8107658e-2 -3.2289338e-1  9.3139162e-2  7.2427234e-2 -5.6919401e-1  5.2554237e-1  2.3656926e-1 -1.1782016e-2  7.1820601e-2  7.8277291e-2 -1.3355752e-1  2.2722721e-1  1.4369455e-1 -4.5198935e-1 -5.5574794e-1;...
  6.6145875e-1  3.4633299e-1  1.4098019e-1  5.1882591e-1 -2.8019898e-1 -1.6032260e-1 -6.8413337e-2 -2.0428242e-1  6.9672173e-2  2.3112577e-1 -4.4368579e-2 -1.6455425e-1  2.1620977e-1  4.2702105e-3 -8.7399014e-2;...
  3.1599556e-1 -2.7551859e-2  1.3434254e-1  1.3497371e-1  5.4005680e-2 -1.7374789e-1  1.7525393e-1  6.0258929e-2 -1.7914162e-1 -3.1056619e-1 -2.5358691e-1  2.5847535e-2 -4.3006001e-1 -6.2266361e-1 -3.3996882e-2;...
 -2.9038151e-1  3.4101270e-2  3.4903413e-2 -1.2121764e-1  2.6030714e-2 -3.3546274e-1 -4.1424111e-1  5.3248380e-2 -2.7099455e-1 -2.6251302e-2  4.1024137e-1  2.6636349e-1  1.5582891e-1 -1.8666254e-1  1.9895831e-2;...
 -2.4388652e-1 -4.4098852e-1  1.2618825e-2  2.4945112e-1  7.1101888e-2  2.4623792e-1  1.7484502e-1  8.5286769e-3  2.5147070e-1 -1.4659862e-1 -8.4625150e-2  3.6931333e-1 -2.9955293e-1  1.1044360e-1 -7.5690139e-1;...
  4.1494323e-2 -2.5980564e-1  4.6402128e-1 -3.6112127e-1 -9.4980789e-1 -1.6504063e-1  3.0943325e-3  5.2792942e-2  2.2523648e-1  3.8390366e-1  4.5562427e-1 -1.8631744e-1  8.2333995e-3  1.6670803e-1  1.6045688e-1]';
trafoOO=@(u)norminv(u,0,1);
modelOO=@(x)x*a1OO+sin(x)*a2OO+cos(x)*a3OO+sum(x.*(x*MOO),2);
%% K function
modelkfun=@(x)sum(bsxfun(@times,(-1).^(1:size(x,2)),cumprod(x,2)),2);
%% Birnbaum Saunders fatigue distribution,  x(:,2) shape, x(:,3) scale (median)
trafoBS=@(u)[norminv(u(:,1)),2*u(:,2),u(:,3)];
modelBS=@(x)x(:,3).*(.5*x(:,1).*x(:,2)+sqrt(1+(.5*x(:,1).*x(:,2)).^2)).^2;
%% Luo Lu Xu:  Reproducing kernel technique for high dimensional model
%  representations 2014

% Friedman Stuetzle function
kFS=10;trafoFS=@(u)u;
modelFS=@(x)10*sin(pi*x(:,1).*x(:,2))+20*(x(:,3)-.5).^2+10*x(:,4)+5*x(:,5)+sum(x(:,6:end),2);
% with 4th order interactions
kLLX=10;trafoLLX=@(u)pi*u;
modelLLX=@(x)cos(x(:,1))+(sin(3.5*x(:,2))./sin(.5*x(:,2))-.5).*cos(x(:,3))+...
    (sin(6.5*x(:,4))./sin(.5*x(:,4))-.5).*cos(x(:,5)).*cos(x(:,6))+...
    (sin(9.5*x(:,7))./sin(.5*x(:,7))-.5).*cos(x(:,8)).*cos(x(:,9)).*cos(x(:,10));

%% Shahsavani Grimvall 2011
modelSG=@(x,a,b,c)prod(bsxfun(@times,b,...
    bsxfun(@power,.25+1/(2*pi)*asin(cos(2*pi*...
    bsxfun(@plus,x,c))),a)),2);
% a=ones(1,k)/4;b=(a+1).*2.^a;c=zeros(1,k);

%% a jump function
model_jump=@(x)0+all(x>.5,2); % all ANOVA effects 1/(2^dim-1)
% main dimension: dim*2^(dim-1)/(2^dim-1)
model_jump2=@(x)0+any(x>.5,2);
%% Friedman Popescu 2008
kFP=100;
trafoFP=@(u)floor(u*10); % discretize
modelFP=@(x)9*prod(-3*(1-x(:,1:3)).^2,2)-.8*exp(-2*(x(:,4)-x(:,5)))+...
    2*sin(pi*x(:,6)).^2-2.5*(x(:,7)-x(:,8));
% with noise term
modelFPnoise=@(x)9*prod(-3*(1-x(:,1:3)).^2,2)-.8*exp(-2*(x(:,4)-x(:,5)))+...
    2*sin(pi*x(:,6)).^2-2.5*(x(:,7)-x(:,8))+norminv(x(:,99),0,10);

%% Heteroskedastic model modified from J. Yin, S.H. Ng, K.M. Ng, Comput & Indust Eng 2011
kYNN=2;
trafoYNN=@(u)[10*u(:,1)-5,u(:,2)];
modelYNN=@(x)x(:,1).^2+norminv(x(:,2),0,sqrt(0.1*((x(:,1)>2)*49+1)));
%% Filter design, bandwidth and gain
% Husam Hamad, Sami Al-Hamdan, EJOR 2011
kHH=3; % 
trafoHH=@(u)1+9*u; % [1,10]
modelHH=@(x)[ 159*(1./x(:,1)+1./x(:,2))./x(:,3),  x(:,2)./(x(:,1)+x(:,2))];

%% Loeppky Williams Moore
kLWM=10;
trafoLWM=@(u)u;
modelLWM=@(x)6*x(:,1)+4*x(:,2)+5.5*x(:,3)+3*x(:,1).*x(:,2)+...
    2.2*x(:,1).*x(:,3)+1.4*x(:,2).*x(:,3)+x(:,4)+0.5*x(:,5)+...
    0.2*x(:,6)+0.1*x(:,7);

%% Khoromskij: Boltzman eq.
kK=3;
trafoK=@(x)[2*x(:,1)-1,1+32*x(:,2),1+6*x(:,3)];
modelK=@(x)abs(x(:,1)).^x(:,3).*sinc(abs(x(:,1)).*x(:,2));

%% Takeuchi et al.
kT=2;
trafoT1=@(u)[u(:,1)*2-1,norminv(u(:,2),0,.1*exp(2-2*u(:,1)))];
modelT1=@(x)sinc(x(:,1))+x(:,2);
trafoT2=@(u)[u(:,1)*2-1,u(:,2)];
modelT2=@(x)sinc(x(:,1))+norminv(x(:,2),0,0.1*exp(1-x(:,1)));
%% B-function of Y Shi, ZZ Lu, K Cheng, YC Zhou
kB=20;
modelB=@(x)sum(x(:,1:10).*x(:,11:20),2);
trafoB=@(u)bsxfun(@plus, [0 0 0 0 0 0 0 0 0 0 1 2 2 2 3 3 1.5 3 2 2], ...
    norminv(u)*diag([.5 .5 1 1 2 2 1 .5 1.5 2 2 2 1 1 1 3 3 3 5 5]));

%% Ytube of Saltelli Campolongo Cariboni
kY=3;
trafoY=@(u)bsxfun(@plus, [1e4 2e3 1e4], norminv(u)*diag([1900,200,850]));
modelY=@(x).7-2e-5*x(:,1)-(2e-4*abs(x(:,2))).^1.2-(2e-5*abs(x(:,3))).^2.1;
%% Bratley Fox Niederreiter
modelBFN=@(x)cumprod(x,2)*((-1).^(1:size(x,2))');

%% Hampton Doostan: Franke function
kF=2;
modelF=@(x)3/4*exp(-(9*x(:,1)-2).^2/4-(9*x(:,2)-2).^2/4)+...
           3/4*exp(-(9*x(:,1)+1).^2/49-(9*x(:,2)+1)/10)+...
           1/2*exp(-(9*x(:,1)-7).^2/4-(9*x(:,2)-3).^2/4)-...
           1/5*exp(-(9*x(:,1)-4).^2-(9*x(:,2)-7).^2);
       
%% David Mandel diss p44.
kM=2;
model=@(x)x(:,1).*x(:,2)+x(:,2).^2;
trafo=@(u,beta)[expinv(u(:,1),beta(1)),expinv(u(:,2),beta(2))];
%S1=beta(1)^2/(3beta(1)^2+20*beta(2)^2+8*beta(1)*beta(2))
%S2=(beta(1)^2+20*beta(2)^2+8*beta(1)*beta(2))/...
%    (3*beta(1)^2+20*beta(2)^2+8*beta(1)*beta(2))

%% Multi-index Monte Carlo: when sparsity meets sampling
kMIMC=5;
trafoMIMC=@(u)[u(:,1:3),u(:,4:5)*2-1];
modelMIMC=@(x)1+exp(2*x(:,4).*sin(pi*x(:,1)).*cos(pi*x(:,2)).*sin(pi*x(:,3))...
    +2*x(:,5).*cos(4*pi*x(:,1)).*sin(4*pi*x(:,2)).*sin(4*pi*x(:,3)));
% used for diffusion 
% - Nabla  * (model  Nabla u)=f  on [0,1]^3 
%                          u = 0 on boundary

% 0.57 due to interactions
kII=2;
modelII=@(x)x(:,1).*log(x(:,1)./x(:,2))+(1-x(:,1)).*log((1-x(:,1))./(1-x(:,2)));
%% Kucherenko Song
modelKS1_=@(x,a)x*a';trafoKS1_=@(u,mu,s)norminv(u,mu,s);
% Si,St= a.*s.^2/sum(a.*s.^2);
kKS1=4;
modelKS1=@(x)modelKS1_(x,[1,1,1,1]);
trafoKS1=@(u)trafoKS1_(u,[1,3,5,7],[1,1.5,2,2.5]);
kKS2=7;
modelKS2=@(x)prod(x(:,[1,3,5]),2)+prod(x(:,[1,3,6]),2)+prod(x(:,[1,4,5]),2)+...
    prod(x(:,[1,4,6]),2)+prod(x(:,[2,3,4]),2)+prod(x(:,[2,3,5]),2)+...
    prod(x(:,[2,5,6]),2)+prod(x(:,[2,4,7]),2)+prod(x(:,[2,6,7]),2);
KS2m=[2,3,.001,.002,.004,.005,.003];
KS2s=0.4214;
%KS2sig2=log(1+KS2s.^2./KS2m.^2);
%KS2mu=log(KS2m)-KS2sig2/2;
%trafoKS2=@(u)[logninv(u(:,1),KS2mu(1),sqrt(KS2sig2(1))),...
%    logninv(u(:,2),KS2mu(2),sqrt(KS2sig2(2))),...
%    logninv(u(:,3),KS2mu(3),sqrt(KS2sig2(3))),...
%    logninv(u(:,4),KS2mu(4),sqrt(KS2sig2(4))),...
%    logninv(u(:,5),KS2mu(5),sqrt(KS2sig2(5))),...
%    logninv(u(:,6),KS2mu(6),sqrt(KS2sig2(6))),...
%    logninv(u(:,7),KS2mu(7),sqrt(KS2sig2(7)))];
trafoKS2=@(u)[logninv(u(:,1),KS2m(1),KS2s),...
    logninv(u(:,2),KS2m(2),KS2s),...
    logninv(u(:,3),KS2m(3),KS2s),...
    logninv(u(:,4),KS2m(4),KS2s),...
    logninv(u(:,5),KS2m(5),KS2s),...
    logninv(u(:,6),KS2m(6),KS2s),...
    logninv(u(:,7),KS2m(7),KS2s)];
%% Francesco Giordano, Maria Lucia Parrella: Bias-corrected inference for
% multivariate nonparametric regression

kGP=11; % 11 codes noise;

modelGP1=@(x)5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP2=@(x)2*x(:,10)+5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP3=@(x)2*x(:,10).*x(:,2)+5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP4=@(x)2*prod(x(:,[10,1,2,3,4]),2)+5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP5=@(x)2*x(:,10).*x(:,1)+prod(x(:,2:4),2)+5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP6=@(x)2*x(:,10).^2.*x(:,1)+5*x(:,8).^2.*x(:,9).^2+norminv(x(:,11),0,.5);
modelGP7=@(x)2*(x(:,1)+1).^3+2*sin(10*x(:,2))+norminv(x(:,11),0,1);
%% Nassim Razaaly, Pietro Marco Congedo: Novel algorithm using Active Metamodel Learning and Importance Sampling

% Failure region
kRC=2;
trafoRC=@norminv;
modelRC=@(x).5*(x(:,1)-2).^2-1.5*(x(:,2)-5).^2-3;
% c=3,4,5
modelRC2=@(x,c)min(c-1-x(:,2)+exp(-x(:,1).^2/10)+(x(:,1)/5).^4,c^2/2-x(:,1).*x(:,2));
modelRC3=@(x)min([3+(x(:,1)-x(:,2)).^2/10-(x(:,1)+x(:,2))/sqrt(2),...
    3+(x(:,1)-x(:,2)).^2/10+(x(:,1)+x(:,2))/sqrt(2),...
    x(:,1)-x(:,2)+7/sqrt(2), -(x(:,1)-x(:,2))+7/sqrt(2)],[],2);
modelRC4=@(x)10-x(:,1).^2+5*cos(2*pi*x(:,1))-x(:,2).^2+5*cos(2*pi*x(:,2));
% 6D analytic example in the physical space: dynamic response of a non-linear oscillator
kRC2=6;
% x=[ m, c1, c2, r, F, t ]
trafoRC2=@(u)[norminv(u(:,1),1,.05),norminv(u(:,2),1,.1),norminv(u(:,3),.1,.01),...
    norminv(u(:,4),.5,.05),norminv(u(:,5),1,.2),norminv(u(:,6),1,.2)];
modelRC2=@(x)3*x(:,4)-abs(2*x(:,5)./abs(x(:,2)+x(:,3)).*sin(sqrt((x(:,2)+x(:,3))./x(:,1)).*x(:,6)/2));

%% Simulation studies from
% Fast smoothing parameter separation in multidimensional generalized P-splines
% Mar�a Xos� Rodr�guez-�lvarez, Dae-Jin Lee, Thomas Kneib, Mar�a Durb�n, Paul Eilers

kRA=3;
modelRA_=@(x)cos(2*pi*sqrt((x(:,1)-.5).^2+(x(:,2)-.5).^2));
% s= .1,.5,1
modelRA1=@(x,s)modelRA_(x)+norminv(x(:,3),0,s);
helpRA=@(y)exp(y)./(1+exp(y)); % ? misprint
modelRA2=@(x)1.*(x(:,3)<= helpRA( (modelRA_(x)+.2)/.5));

modelRA3=@(x)1.5*exp( bsxfun(@minus,x,[.2,.5,.9]).^2*[-1/5;-1/3;-1/4])+...
    0.5*exp( bsxfun(@minus,x,[.3,.7,.4]).^2*[-1/4;-1/2;-1/6])+...
    exp( bsxfun(@minus,x,[.1,.3,.7]).^2*[-1/5;-1/5;-1/4]);

%% Parity functions
%Residual variance estimation in machine learning
%Elia Liiti�inen, Michel Verleysen, Francesco Corona, Amaury Lendasse

k1LV=3;
trafoLV=@(u)norminv(u);
model1LV=@(x)sin(pi*x(:,1)).*sin(pi*x(:,2))+x(:,3)*sqrt(.1);
k2LV=5;
model2LV=@(x)sin(pi*x(:,1)).*sin(pi*x(:,2))/2+...
    sin(pi*x(:,3)).*sin(pi*x(:,4))/2+x(:,5)*sqrt(.1);
k3LV=5;
model3LV=@(x)(1+x(:,5)*sqrt(.1)).*sin(pi*x(:,1)).*sin(pi*x(:,2))+...
    (1+x(:,5)*sqrt(.1)).*sin(pi*x(:,3)).*sin(pi*x(:,4));

%% Atanassov test functions
modelA1=@(x)prod(x.^3+.75,2); % Schmidt Uhl, mean 1
modelA2=@(x)prod(abs(4*x-2),2); % Ross Arnold, mean 1
modelA3=@(x)sum(cumprod(bsxfun(@times,(-1).^(1:size(x,2)),x),2),2); % mean 1/3(1-(-1/2)^k)?
%% Swiler et al. Mixed Surrogate Models
%
kMSM=3;
trafoMSM=@(u)[1+floor(5*u(:,1)), 2*u(:,2)-1,2*u(:,3)-1];
MSMlevels=[7,12,.5,8,3.5];
modelMSM=@(x)sin(pi*x(:,3)).*(1+MSMlevels(x(:,1))')+7*sin(pi*x(:,2)).^2;
% Goldstein-Price
k=2;
trafoGP=@(u)[2*(floor(u(:,1)*3)-1),4*(u(:,2)-.5)];
modelGP=@(x)(1+(x(:,1)+x(:,2)+1).^2.*(19-14*x(:,1)+3*x(:,1).^2-14*x(:,2)+6*x(:,1).*x(:,2)+3*x(:,2).^2))...
    .*(30+(2*x(:,1)-3*x(:,2)).^2.*(18-32*x(:,1)+12*x(:,1).^2+48*x(:,2)-36*x(:,1).*x(:,2)+27*x(:,2).^2));
% 19-term fourth order polynomial, parameters 1&2 are discrete
kMSM2=4;
trafoMSM2=@(u)[80-30*(u(:,1)<2/3)-30*(u(:,1)<1/3),80-30*(u(:,2)<2/3)-30*(u(:,2)<1/3),...
  u(:,3)*100,u(:,4)*100];
modelMSM2=@(x)53.3108+0.184901*x(:,1)-5.02914e-6*x(:,1).^3+7.72522e-8*x(:,1).^4 ...
-0.0870775*x(:,2)-0.106959*x(:,3)+7.98772e-6*x(:,3).^3+0.00242482*x(:,4)+...
1.32851e-6*x(:,4).^3-0.00146393*x(:,1).*x(:,2)-0.00301588*x(:,1).*x(:,3)-...
0.00272291*x(:,1).*x(:,4)+0.0017004*x(:,2).*x(:,3)+0.0038428*x(:,2).*x(:,4)-...
0.000198969*x(:,3).*x(:,4)+1.86025e-5*x(:,1).*x(:,2).*x(:,3)-1.88719e-6*...
x(:,1).*x(:,2).*x(:,4)+2.50923e-5*x(:,1).*x(:,3).*x(:,4)-...
5.62199e-5*x(:,2).*x(:,3).*x(:,4);

%% Coppejans: On Kolmogorov's Representation of Functions 2003
kC=2;
trafoC=@(u)pi*u;
modelC1=@(x)sin(exp(x(:,1))/2+exp(x(:,2))/2); % >70% interaction
polyh=@(x)1.5*x+x.^2-2.9*x.^3+1.25*x.^4-.15*x.^5;
modelC2=@(x)-5+10*(exp(polyh(x(:,1)))+exp(polyh(x(:,2))))...
    ./(1+exp(polyh(x(:,1)))+exp(polyh(x(:,2))));
modelC3=@(x)exp(sin(x(:,1)+x(:,2))-cos(x(:,1))/3-2*cos(x(:,2))/3);
%% Lewis: Comments on Computer Experiments Technometrics 1996
% five-factor main-effects-and-interactions-model
kCCE=5;
trafoCCE=@(u)2*u-1; % symmetrical parameter values
modelCCE=@(x)x(:,1).*x(:,2)-x(:,1).*x(:,3)+x(:,2).*x(:,3)-x(:,2).*x(:,4)...
    +x(:,3).*x(:,4)-x(:,3).*x(:,5)+x(:,4).*x(:,5)-x(:,4).*x(:,1)...
    +x(:,5).*x(:,1)-x(:,5).*x(:,2);
% S_i=0 S_ij=.1 T_i=.4
mymodelCCE=@(x)prod(x(:,[1,2,3]),2)-prod(x(:,[1,2,4]),2)...
    +prod(x(:,[1,2,5]),2)+prod(x(:,[2,3,4]),2)-prod(x(:,[2,3,5]),2)...
+prod(x(:,[3,4,5]),2)-prod(x(:,[1,3,4]),2)-prod(x(:,[1,3,5]),2)...
+prod(x(:,[1,4,5]),2)-prod(x(:,[2,4,5]),2);
% S_i=0 S_ij=0 S_ijk=.1 T_i=.6

%% Goda: On the separability of multivariate functions
kG=50;

trafoG1=@(u)(2*u-1)*5.12;
modelG1=@(x)sum(x.^2-10*cos(2*pi*x)+10,2); % Rastrigin
trafoG2=@(u)(2*u-1)*2;
modelG2=@(x)sum(100*(x(:,1:end-1).^2-x(:,2:end)).^2+(x(:,1:end-1)-1).^2,2); % Rosenbrock

%% Razavi Gupta -- additive
kRG=6;
trafoRG=@(u)u;
modelRG=@(x)-sin(pi*x(:,1))-.3*sin(3.33*pi*x(:,1))+...
    -.76*sin(pi*(x(:,2)-.2))-.315+...
    -.12*sin(1.05*pi*(x(:,3)-.2))-.02*sin(95.24*pi*x(:,3))-.96+...
    -.12*sin(1.05*pi*(x(:,4)-.2))-.96+...
    -.05*sin(pi*(x(:,5)-.2))-1.02+...
    -1.08;
%% Ferrari-Trecate and Muselli (2002) pw. defined
kFTM=2;
trafoFTM=@(u)2*u-1;
modelFTM=@(x)(3+4*x(:,1)+2*x(:,2)).*(0.5*x(:,1)+0.29*x(:,2)>= 0).*(x(:,2)>=0)+...
            (-5-6*x(:,1)+6*x(:,2)).*(0.5*x(:,1)+0.29*x(:,2)<0).*(0.5*x(:,1)-0.29*x(:,2)<0)+...
(-2+4*x(:,1)-2*x(:,2)).* (0.5*x(:,1)-0.29*x(:,2)>= 0).*(x(:,2)<0);

%% Dette and Pepelyshev, Technometric 2010
trafo1=@(u)u;
% hyperballs with radii r= .5 and .4
% u(sum( (u-.5).^2 ) <r^2 ,:)=[]
kDP1=3;
modelDP1=@(x)100*(exp(-2./x(:,1).^1.75)+exp(-2./x(:,2).^1.5)+...
    exp(-2./x(:,1).^1.25));
kDP2=3;
modelDP2=@(x)4*(x(:,1)-2+8*x(:,2).*(1-x(:,2))).^2+(3-4*x(:,2)).^2+...
    16*sqrt(x(:,3)+1).*(2*x(:,3)-1).^2;
kDP3=8;
modelDP3=@(x)4*(x(:,1)-2+8*x(:,2).*(1-x(:,2))).^2+(3-4*x(:,2)).^2+...
    16*sqrt(x(:,3)+1).*(2*x(:,3)-1).^2+...
    sum((4:8).*log(1+sum(1+x(:,3)+cumsum(x(:,4:8),2),2)),2);

% borehole model: flow rate
% 2pi(H_u-H_l) / ( log(r/r_w)*(1+2LT_u/log(r/r_w)r_w^2K_w)+T_u/T_l )
% r_w .005 .15 radius of the borehole
% r 100 50000 radius of influence
% T_u 63070 115600 transmissity of the upper aquifer
% T_l 63.1 116 transmissity of the lower aquifer
% H_u 990 1110 potentiometric head of the upper aquifer
% H_l 700 820 potentiometric head of the lower aquifer
% L 1120 1680 length of the borehole
% K_w 9855 12045 hydraulic conductivity of the borehole

% long bar and spring: vertical displacement
% solve S=EA/l^3 ( z^2w^1.5zw^2+.5w^2)+K w for w
% E 4.8e5 5.2 e5 Young modulus of bar
% A 95,105 cross section area of bar
% l 2650 2750 length of unloaded bar
% z 24.5 25.5 coordinate of unloaded bar
% K .88 .92 stiffness of spring
% S -23 -22 vertical force

% high explosive charge implosion: radius of a steel cylinder (time dep)
% solve y'=(2/(R_I^2 f(y)^2) (v0^2/2-s/(2rho)g(y))).^-1/2
% f(y)=y^2/(1-lambda^2)*log( (y^2+1-lambda^2)/ y^2)
% g(y)=1/(1-lambda^2)(y^2+1-lambda^2)*log(y^2+1-lambda^2)-lambda^2log(lambda^2)
% R_I 1.5 initial outer radius of the cylinder being imploded
% lambda 2/3 ratio outer/inner cylinder radius
% rho=7.5 density relative to water
% v0=.3e5 initial velocity
% T = 5e5 end of time
% s 1.85e10 2.25e10 yield of stress of steel
% m .29 .35 mass ratio
% u0 1.4e10 1.7e10 detonation energy 
% Neddermeyer: v= m sqrt ( 2u_0/((1+m))

% Dimov: nonsmooth integrand
modelDNSI=@(x)sum(abs(x-.5).^(-1/3),2);
modelDNSI2=@(x)sum(abs(x-.8).^(-1/3),2);
kDSI=4;
% smooth integrand
modelDSI=@(x)exp(x(:,1)+x(:,2)*2).*cos(x(:,3))./(1+x(:,2)+x(:,3)+x(:,4));
modelDSI2=@(x)x(:,1).*x(:,2).^2.*exp(x(:,1).*x(:,2)).*sin(x(:,3)).*cos(x(:,4));
% computational irregularities
modelDCI=@(x,a)(1+x*a').^(-size(x,2)+1);
%a=rand(size(x))*600/size(x,2)^2;
%a=[5 5 5 5 4]'
%a=[3 2 2 3 2 3 3 4 2 3 3 2 2 3 3 4 3 3]/27

%% from the Da Veiga et al. SIAM book
% arctangent temporal
modelDV1=@(x)atan(x(:,1)).*cos(x(:,3))+atan(x(:,2)).*sin(x(:,3)); % x(:,3) is t
trafoDV1=@(u)[-7,-7,0]+u.*[14,14,2*pi];
kDV1=3;
% Bratley
modelDV2=modelBFN;
% HeterDisc 
modelDV3=@(x)(x(:,1)<10).*(sin(pi*x(:,1)/5)+2/10*cos(4*pi*x(:,1)/5)+x(:,3)/100.*(x(:,2)-10))+...
    (x(:,1)>=10).*(x(:,3)/100.*(x(:,2)-10)+1/10*(x(:,4)-20));
trafoDV3=@(u)u*20;
%kDV3 >=4
% DV4: ishigami (external)
% DV5: linklatter = model7
% DV6: Morris (external)
% DV7: Sobol' g (external)


