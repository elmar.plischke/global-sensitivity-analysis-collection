% Hart Gremaud

k=3;
model=@(x)x*[1.5;1.25;1];
trafo=@(u,rho)iccorrelate(norminv(u),[1,rho,rho;rho,1,rho;rho,rho,1]);

n=1024;
u=sobolpoints(n,k);

Si=[];
rho=0:.099:1;

for r=rho
   x=trafo(u,r);
   y=model(x);
   Si=[Si;cosi(x,y,8)];
end
plot(rho,Si);


%%
k=6;
n=1024;
%n=4096;
trafo=@(u)[(97+6*u(:,1))/10, (2716+168*u(:,2))/100,(194+12*u(:,3))/75,...
    (4+2*u(:,4))/5,(4+2*u(:,5))/5,(4+2*u(:,6))/5];
    %-9+(4+2*u(:,4))/5,-9+(4+2*u(:,5))/5,25+(4+2*u(:,6))/5];
% from mfiles provided by the authors
trafo2=@(u)bsxfun(@plus,[10,28,8/3,1,1,1],...
    bsxfun(@times,[3/10,3*7/25,8/100,.2,.2,.2],2*u-1));    
lorenz=@(t,x,p)[p(1)*(x(2)-x(1)) ; x(1)*(p(2)-x(3))-x(2); x(1)*x(2)-p(3)*x(3)];

y=zeros(n,1);
%z=zeros(n,2);
u=sobolpoints(n,k);
p=trafo2(u);
%at=lorenz63(); 
%at{6}{1}=@(u)(4+2*betainv(u,1,4))/5;
%p=autotrans(u,at);
clf
cols=jet(64);
% for compatibility
opts = odeset('RelTol',1e-5);
for i=1:n
[t,x]=ode45(@(t,x)lorenz(t,x,p(i,:)),[0,1],p(i,4:6)',opts);
%[t,x]=ode45(@(t,x)Lorenz_ODE(t,x,p(i,:)),[0,1],p(i,4:6)',opts);
plot3(x(:,1),x(:,2),x(:,3),'Color',cols(ceil(u(i,6)*64),:));hold on
%ii=find(t>.25,1);
%z(i,:)=[x(ii,2),x(ii,2)];
y(i)=x(end,3)./x(end,2); % x(end,3)/x(end,1)
end
hold off
%%
try % someone stealing signal toolbox licenses
    cosi(p,y,8,'gfx')
catch
    easi(p,y,8,'gfx')
end
%%
at=lorenz63(); 
[Si,St,XY_a]=ihssi(length(at),4096,@lorenz63,@(u)autotrans(u,at),@sobolpoints,'Lorenz Sensitivity');
Si,St
% alfa_3 is last parameter
at{6}{1}=@(u)(4+2*betainv(u,1,4))/5;
[Si,St,XY_b]=ihssi(length(at),4096,@lorenz63,@(u)autotrans(u,at),@sobolpoints,'Lorenz Sensitivity');
Si,St

%%
g_func=@(x,a)prod((abs(4*x-2)+ones(size(x,1),1)*a)./(1+ones(size(x,1),1)*a),2);
n=2048;
k=10;
[Si,St,XY]=ihssi(k,n,@(x)g_func(x,0:(k-1)),@(u)u,@sobolpoints,'g func');
Si,St
phi_pdf=ones(n,k);
%%
tree=fitrtree(XY(1:n,1:end-1),XY(1:n,end),'MinLeafSize',25);
%view(tree,'Mode','graph')
%%
R=200;
s=rand(R,k);
z=predict(tree,s);
plot(g_func(s,0:(k-1)),z,'*');
%%

