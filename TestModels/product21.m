% Borgonovo 21 product example
k=21;
trafo=@(u)logninv(u,1,1);
model=@(x)prod(x.^(ones(size(x,1),1)*...
    [4 4 4 4 4 4 4 2 2 2 2 2 2 2 1 1 1 1 1 1 1]),2);
%delta1..7 = .112, delta8..14=.053 delta15..21=.026 