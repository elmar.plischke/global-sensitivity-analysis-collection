%% examples from Derennes et al.

% sum of gaussian
k=2;
trafo=@(u)[norminv(u(:,1)),norminv(u(:,2),0,sqrt(5))];
model=@(x)sum(x,2);

n=5000; %n=4096;
x=trafo(rand(n,k)); % sobolseq(n,k)); % 
y=model(x);

disp('analytic values: 0.1436, 0.5382')
opts=struct('PartitionSize',32,'KDEstimator','diffusion',...
'QuadraturePoints',4096,'KSLevel',0);
Di=deltamim(x,y,opts)
[~,Di2]=betaKS3(x,y,32)
[Di3,Di3a]=deltawei(x,y,5)

%% affine transformation 
k=4;
S=[1,1/2,1/3,1/4;1/2,1,1/2,1/3;1/3,1/2,1,1/2;1/4,1/3,1/2,1];
trafo=@(u)norminv(u)*chol(S);
model=@(x)x*[1.7;1.8;1.9;2.0];

n=4096;
x=trafo(sobolseq(n,k)); % rand(n,k)); % 
y=model(x);

disp('!0.2857 0.3620 0.3792 0.3176!')

[Di3,Di3a]=deltawei(x,y,5)

%% risk assessment model
faulttree
 
 n=4096;
x=trafo(sobolseq(n,k)); % rand(n,k)); % 
y=model(x);

[Di3,Di3a]=deltawei(x,y,5)

%%
trafo=@(u)logninv(u);
model=@(x)prod(x,2);

 n=4096;
 
for k=3:5
x=trafo(sobolseq(n,k)); % rand(n,k)); % 
y=model(x);
disp(k);
[Di3,Di3a]=deltawei(x,y,5)
[~,Di2]=betaKS3(x,y,32)
end

 
