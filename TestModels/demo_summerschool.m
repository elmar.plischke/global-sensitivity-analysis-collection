cvetkovic

ns=floor(logspace(2.5,4.7,30));
for i=1:length(ns)
[Si,STi]=efast(k,ns(i)*k,model1,@(u)autotrans(u,trafo1));
Sis(i,:)=Si;
STis(i,:)=STi;
[Si,STi]=ihssi(k,ns(i),model1,@(u)autotrans(u,trafo1));
SisIHS(i,:)=Si;
STisIHS(i,:)=STi;
[Si,STi]=ihssi(k,ns(i),model1,@(u)autotrans(u,trafo1),@sobolpoints);
SisSob(i,:)=Si;
STisSob(i,:)=STi;
end
%%
a=[ns(1),ns(end),0,1];
subplot(3,1,1)
semilogx(ns,Sis,'-',ns,STis,'--');axis(a);
title('Attenuation Index: eFAST');xlabel('sample size');ylabel('fraction of variance');
subplot(3,1,2)
semilogx(ns,SisIHS,'-',ns,STisIHS,'--');axis(a);
title('Attenuation Index: Sobol (Random)');xlabel('sample size');ylabel('fraction of variance');
subplot(3,1,3)
semilogx(ns,SisSob,'-',ns,STisSob,'--');axis(a);
title('Attenuation Index: Sobol (LPtau)');xlabel('sample size');ylabel('fraction of variance');
%%
for i=1:k; lgd{i}=trafo1{i}{3}; end; legend(lgd)
%%
pause
%%
for i=1:length(ns)
Si=rbd(k,ns(i)*k,model1,@(u)autotrans(u,trafo1));
Sis(i,:)=Si;
x=autotrans(rand(ns(i),k),trafo1);y=model1(x);
Si=easi(x,y,8);
SisEA(i,:)=Si;
x=autotrans(sobolpoints(ns(i),k),trafo1);y=model1(x);
Si=easi(x,y,8);
SisEASob(i,:)=Si;
end
%%
a=[ns(1),ns(end),0,.4];
subplot(3,1,1)
semilogx(ns,Sis,'-');axis(a);
title('Attenuation Index: RBD');xlabel('sample size');ylabel('fraction of variance');
subplot(3,1,2)
semilogx(ns,SisEA,'-');axis(a);
title('Attenuation Index: EASI (Random Sample)');xlabel('sample size');ylabel('fraction of variance');
subplot(3,1,3)
semilogx(ns,SisEASob,'-');axis(a);
title('Attenuation Index: EASI (LPtau)');xlabel('sample size');ylabel('fraction of variance');