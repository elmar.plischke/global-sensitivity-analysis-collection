%% examples from gamboa et al.

%% exponential
model=@(x)exp(x(:,1)+2*x(:,2));
trafo=@(u)norminv(u,0,1);
% Sobol .0118 .3748
% CvM   .1145 .5693
k=2;
%
n=8192;
x=trafo(net(sobolset(k),n+1));
x(1,:)=[]; % delete first point 
y=model(x);
easi(x,y,40,'Bla')
ginimim(x,y,32,'bla')
flavoursi(x,y,[10,32],'bla')
%% gaussian plume model
% Q emission rate
% u wind speed in x direction
% stack height h
% delta relative plume rise

% height at ground level
Q=@(p)p(:,1); % first parameter
u=@(p)p(:,3);
H=@(p)p(:,4); % [ 1 2 10 20] % @(p)p(:,3)*(1+p(:,4));
K=@(p)p(:,2); % Eddy diffusion constant
C=@(x,y,p)Q(p)./(2*pi*K(p).*x)*exp(-u(p).*(y.^2+H(p).^2)./(4*K(p).*x));

k=4;
trafo=@(u)u*diag([10,10,10,4])+[0,0,0,1]; % uniform [0,10] for first three, treat height differently


xs=linspace(0,8,200);
ys=linspace(-4,4,200);
P=trafo(sobolpoints(2048,k));
Q=[];
for p=P'
for i=1:length(xs)
    for j=1:length(ys)
        F(i,j)=C(xs(i),ys(j),p');
    end
end
Q(end+1,:)=F(:)';
%if(mod(size(Q,1),200)==0),surf(xs,ys,F','EdgeColor','none');drawnow;end
end
%%
%Si=easi(P,Q,32);
%avoid out of memory. Change if sizes of xs,ys are changed
i=1:4000:40000
for j=i,Si(j+(0:3999),:)=cosi(P,Q(:,j+(0:3999)),32); end
%%
names={'emission rate','wind speed','diffusion','altitude'}
for i=1:k
    subplot(3,2,i)
    imagesc(reshape(Si(:,i),length(ys),length(xs))')
    colorbar
    title(['Sensitivity for ' names{i}]);axis off
end
%%
subplot(3,2,6)
imagesc(reshape(sum(Si,2),length(ys),length(xs))'), title('sum of main effects'),colorbar
axis off
subplot(3,2,5)
V=var(Q);
imagesc(reshape(log10(V),length(ys),length(xs))'), title('log10 output variance'),colorbar
axis off
