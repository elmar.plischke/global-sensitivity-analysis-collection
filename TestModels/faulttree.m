% Faulttree model
% Iman, Lu et al.

k=7;
%errorfactor=2;
% Tatsuya Sakurahara et al.: alternatively
% errorfactor=[2 2 4.3 1.6 1.6 1.2 2.0];
means=[ 2 3 .001 .002 .004 .005 .003];
sigma=log(errorfactor)/1.645;
mu=log(means)-sigma.^2/2;

%trafo=@(u)exp(norminv(u,mu,sigma));
trafo=@(u)exp(bsxfun(@minus,mu,sqrt(2)*bsxfun(@times,sigma,erfinv(1-2*u))));

model=@(x)x(:,1).*( (x(:,3)+x(:,4)).*(x(:,5)+x(:,6)))+ ...
    x(:,2).*(x(:,3).*(x(:,4)+x(:,5))+(x(:,5)+x(:,7)).*(x(:,4)+x(:,6)));

if(0)
%%
% indicator matrix top-event/cut set
T=logical([ 1 1 1 1 0 0 0 0 0 0; ...
            0 0 0 0 1 1 1 1 1 1; ...
            1 1 0 0 1 1 0 0 0 0; ...
            0 0 1 1 1 0 1 0 1 0; ...
            1 0 1 0 0 1 1 1 0 0; ...
            0 1 0 1 0 0 0 1 0 1; ...
            0 0 0 0 0 0 0 0 1 1]);
[kk,m]=size(T);
z=prod(x(:,T(:,1)),2);
for l=2:m
    z=z+prod(x(:,T(:,l)),2);
end
%%
end
%% common cause failure / rare event approximation
model_ccf=@(x)x(:,1)+x(:,2).*x(:,3)+((1-.1)*x(:,4)).^4+.1*x(:,4);
%means_ccf=[1e-5,1e-3,1e-3,5e-3];
%ef_ccf=2;
means_ccf=[1e-3,1e-3,1e-3,1e-3];
ef_ccf=[1.2 4.3 3.6 4.8]; % alternative 
sigma_ccf=log(ef_ccf)/1.645;
mu_ccf=log(means_ccf)-sigma_ccf.^2/2;
trafo_ccf=@(u)exp(bsxfun(@minus,mu_ccf,sqrt(2)*bsxfun(@times,sigma_ccf,erfinv(1-2*u))));