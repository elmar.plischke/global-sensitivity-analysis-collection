%% Fed-batch bioreactor for penicillin production
% Yunfei Chu, Juergen Hahn, Computers and Chemical Engineering 48 (2013) 280�292

% written by elmar.plischke@tu-clausthal.de

% y=[ X biomass concentration
%     S substrate concentration 
%     P product concentration 
%     V reactor volume]

Sin=200;
y0=[1 .5 0 150]';
Fvec=[0 1 0 0  1 1 1 1 1 0 0 1 1 0 0 0 0 0 0]; % at least STEPS entries
F=@(t).2*Fvec(floor(t/10)+1);

k=6;
trafo=@(u) (u+.5)*diag([.02,.05,5,.5,1.2,.004]);

n=2048;
%x=trafo(sobolpoints(n,k));
x=trafo(net(sobolset(k),n+1));x(1,:)=[];
STEPS=18;
res=zeros(n,STEPS);

    mum=@(i)x(i,1);
    Km=@(i)x(i,2);
    Ki=@(i)x(i,3);
    YX=@(i)x(i,4);
    YP=@(i)x(i,5);
    nu=@(i)x(i,6);
    
    mu=@(s,i)mum(i)*s/(Km(i)+s+s.^2/Ki(i));
    
    oderhs=@(t,y,i)[ (mu(y(2),i)-F(t)./y(4))*y(1); ...
               (-mu(y(2),i)/YX(i)-nu(i)/YP(i))*y(1)+F(t)./y(4)*(Sin-y(2)); ...
               nu(i)*y(1)-F(t)./y(4)*y(3);...
               F(t)];
for i=1:n
    [t,z]=ode45(@(t,y)oderhs(t,y,i),0:10:(10*STEPS),y0);
    res(i,:)=z(2:end,3)';
end
[Si,V]=cosi(x,res,-8); % do not normalize by output variance
plot(10:10:(STEPS*10),sqrt(Si),10:10:(STEPS*10),sqrt(V),'k--');
xlabel('time');ylabel('Absolute contribution to output deviation');
legend('Kinetic constant \mu_m','Kinetic constant K_m',...
'Inhibition constant K_i', ...
'Biomass yield Y_X','Product yield Y_P','Parameter \nu');
title('Fed-batch bioreactor for penicillin production: Product concentration')
