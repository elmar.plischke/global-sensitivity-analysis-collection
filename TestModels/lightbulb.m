%% response surface model for a lightbulb design model

% Su, Nelder, Wolbert, Spence: APPLICATION OF GENERALIZED LINEAR MODELS TO
% THE DESIGN IMPROVEMENT OF AN ENGINEERING ARTEFACT
% QUALITY AND RELIABILITY ENGINEERING INTERNATIONAL, VOL. 12. 101-1 I2 (1996)

k=4;
distribs={ {'tri',{1,4,7},'distance discharge tube/long pole'},...
    {'tri',{2,4,6},'distance discharge tube/short pole'},...
    {'tri',{.5,.6,.7},'diameter of wire cross section'},...
    {'tri',{2,2.5,3},'radius curved part of short pole'}};

trafo =@(u) autotrans(u,distribs);
% four stresses: Table X
model = @(x) ones(length(x),1)*[26.81 76.1 -36.0 60.4]+...
    x(:,1)*[-11.25 36.6 3.3 23.0]+x(:,2)*[4.3 12.4 23.8 -3.8]+...
    x(:,3)*[140.61 976.4 654.8 631.2]+x(:,4)*[-12.1 -47.6 -4.4 -26.2]+...
    x(:,1).*x(:,3)*[31.66 -136.7 -20.6 -79.7]+...
    x(:,2).*x(:,3)*[-10.57 -22.6 -45.2 4.8]+...
    x(:,1).*x(:,4)*[.17 4.5 .9 2.6]+...
    x(:,1).^2*[1.2 -4.7 .7 -2.6]+x(:,3).^2*[-34.98 -624 -464.5 -278.2]+...
    x(:,1).^2.*x(:,3)*[-3.06 10.5 1.5 5.7];
