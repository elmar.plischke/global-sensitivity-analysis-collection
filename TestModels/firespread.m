% Fire spread model
% EUNHYE SONG, BARRY L. NELSON, AND JEREMY STAUM

% input description for use with autotrans()
dp={ {'logn', { 2.19,.517},'\delta', 'Fuel depth','cm'},...
     {'logn', { 3.31,.294},'\sigma', 'Fuel particle area to volume ratio','1/cm'},...
     {'logn', { 8.48,.063},'h', 'Fuel particle low heat content','kcal/kg'},...
     {'logn', { -.592,.219},'\rho_P', 'Oven-dry particle density','g/cm^3'},...
     {'norm', { 1.18,.377},'m_l', 'Moisture content of live fuel','g/g'},...
     {'norm', { .19,.047},'m_d', 'Moisture content of dead fuel','g/g'},...
     {'norm', { .049,.011},'S_T', 'Fuel particle total mineral content','g/g'},...
     {'logn', { 1.936+1.0174,.5569},'U', 'Wind speed at midflame height (strong wind factor)','km/h'},...
     {'norm', { .38,.186},'tan\Phi', 'Slope','[]'},...
     {'logn', { -2.19,.66},'P', 'Dead fuel loading to total fuel loading','[]'}};
 % unit conversion
 % formulas require delta [ft], sigma [1/ft] h [Btu/lb] rhoP [lb/ft^3] U [ft/min]
 scalex=@(x)x*diag([1/30.48,30.48,.4536*4.184/1.0548,30.48^3/453.6,1,1,1,1/(6*.003048),1,1]);
 truncate=[-inf,inf;3/.6,inf;-inf,inf;-inf,inf;0,inf;-inf,inf;0,inf;...
     -inf,inf;0,inf;-inf,1];
 trafo=@(u)autotrans(u,dp,truncate);

 % postprocess
 tobedeleted=@(x)x(:,5)<0 | x(:,7)<0 | x(:,9)<0 | x(:,10)>1 | x(:,2)<3/.6;
% x(tobedeleted(x),:)=[];

 k=length(dp);
% submodels     
% w0 =@(x)1./(1.0+exp((15.-x(:,1))/2))/4.8824;         % fuel loading; kgm-2 % lb/ft^2 needed?
% w0 =@(x)1./(1.0+exp((15.-30.48*x(:,1))/2));
 w0 =@(x)1./(1.0+exp((15.-30.48*x(:,1))/2))/4.8824;
 Gammamax=@(x)x(:,2).^1.5./(495.0+.0594*x(:,2).^1.5); %maximum reaction velocity; min-1
 betaop=@(x)3.348*x(:,2).^-0.8189;                    % optimum packing ratio
% A =@(x)133.0*x(:,2).^-0.8189;
 A =@(x)133.0*x(:,2).^-0.7913;             % Albini 1976
 thetaast=@(x)(301.4-305.87*(x(:,5) -x(:,6)) + 2260*x(:,6))./(2260*x(:,5));
 theta=@(x)min(1,max(thetaast(x),0));
 muM=@(x)exp(-7.3*x(:,10).*x(:,6)-(7.3*theta(x)+2.13).*...
     (1-x(:,10)).*x(:,5));                          % moisture damping coefficient
 muS=@(x)0.174*x(:,7).^-0.19;                       % mineral damping coefficient
 %muS=@(x)min(0.174*x(:,7).^-0.19,1);  % as in the code
 C =@(x)7.47*exp(-0.133*x(:,2).^0.55);
 B =@(x)0.02526*x(:,2).^0.54;
 E =@(x)0.715*exp(-3.59e-4*x(:,2));
 wn=@(x)w0(x).*(1-x(:,7));                          % net fuel loading
 rhob=@(x)w0(x)./x(:,1);                            % ovendry bulk density
 epsi=@(x)exp(-138./x(:,2));                        % effective heating number
 Qig=@(x)130.87+1054.43*x(:,6);                     % heat of preignition
  %Qig =@(x) (401.41 + x(:,6) * 2565.87) * 0.4536/1.060;
 beta=@(x)rhob(x)./x(:,4);                          % packing ratio
% Gamma=@(x)Gammamax(x).*beta(x)./betaop(x).*A(x).*exp(A(x).*...
%     (1-beta(x)./betaop(x)));                       % optimum reaction velocity
 Gamma=@(x)Gammamax(x).*(beta(x)./betaop(x)).^A(x).*exp(A(x).*...
     (1-beta(x)./betaop(x)));                       % optimum reaction velocity
 xi =@(x)exp((0.792 + 0.681*sqrt(x(:,2))).*(beta(x)+ 0.1))./...
     (192.0 + 0.2595*x(:,2));                         % propagating flux ratio
 PsiW=@(x)C(x).*(x(:,8)).^B(x).*...
     (beta(x)./betaop(x)).^-E(x);                   % wind coefficient
 PsiS=@(x)5.275.*beta(x).^-.3.*(x(:,9)).^2;         % slope factor
 IR=@(x)Gamma(x).*wn(x).*x(:,3).*muM(x).*muS(x);    % reaction intensity
 
 % rate of fire-spread
 model_imperial=@(x)IR(x).*xi(x).*(1+PsiW(x)+PsiS(x))./(rhob(x).*epsi(x).*Qig(x));
 model=@(x)model_imperial(scalex(x))*0.508; % Change from ft/min to cm/s
 
 %%
 if(1)
     %%
     %k=length(dp);
     n=2^14;
     for i=1:k
         nm{i}=dp{i}{3};
     end
     %% recreate figure
     [Shi,Vi,St]=shapmob(k,n,model,trafo);
     [~,~,~,moreZ]=shapmobdep(k,n,model,trafo,eye(k,k));
     STi=[0,0,1,1]'+St./[Vi;Vi];
     %%
     subplot(3,3,1)
     bar(STi([2,3],:)');
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
     axis([0,k+1,0,.41]);title('Independence: First Order');
     subplot(3,3,2)
     bar(STi([1,4],:)');
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
     axis([0,k+1,0,.41]);title('Independence: Total');
     subplot(3,3,3)
     bar((Shi./Vi)');
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
       axis([0,k+1,0,.41]);title('Independence: Shapley');
     %%
     C=eye(k,k);C(6,8)=-.3;C(8,6)=-.3;
     [Shw,Vw,St,more0]=shapmobdep(k,n,model,trafo,C);
     STi=St./Vw;
     %%
     subplot(3,3,4)
     bar(STi(2,:));
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
     axis([0,k+1,0,.41]);title('Weak dependence: First Order');
     subplot(3,3,5)
     bar(STi(1,:));
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
       axis([0,k+1,0,.41]);title('Weak dependence: Total');
     subplot(3,3,6)
     bar((Shw./Vw)');
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
       axis([0,k+1,0,.41]);title('Weak dependence: Shapley');
            %%
     C=eye(k,k);C(6,8)=-.8;C(8,6)=-.8;
     [Shs,Vs,St,more]=shapmobdep(k,n,model,trafo,C);
     STi=St./Vs;
     %%
     subplot(3,3,7)
     bar(STi(2,:));
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
     axis([0,k+1,0,.41]);title('Strong dependence: First Order');
     subplot(3,3,8)
     bar(STi(1,:));
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
       axis([0,k+1,0,.41]);title('Strong dependence: Total');
     subplot(3,3,9)
     bar((Shs./Vs)');
     a=gca;
     set(a,'XTick',1:k);set(a,'XTickLabel',nm);
       axis([0,k+1,0,.41]);title('Strong dependence: Shapley');
       %%
 end
 if(exist('more'))
 %% pairwise interactions under strong dependency
 clf
 ii=[];for i=1:k,ii=[ii,((i-1)*(k+1)+2):(i*k)]; end
 m=1:length(ii);
 YTick={}; for i=1:(k-1);for j=(i+1):k, YTick{end+1}=[nm{i} ', ' nm{j}]; end, end
 %subplot(1,3,1)
h=moreZ.ShapOwen; % was ticked
Sh2=h(ii)/Vi(1);
h=more0.ShapOwen;
Sh2_weak=h(ii)/Vw;
h=more.ShapOwen;
Sh2_strong=h(ii)/Vs;
plot(m-.5,Sh2,'+',m-.5,Sh2_weak,'<',m-.5,Sh2_strong,'>'); %Sh2_lo,m,'<',Sh2_hi,m,'>')

set(gca,'XTick',1:length(ii));set(gca,'XTickLabel',YTick)
 set(gca,'XTickLabelRotation',-72)
  set(gca,'FontSize',14)
title('Shapley-Owen for pairwise interaction'); 
legend({'No Dependence','Weak Dependence','Strong Dependence'});

%%
 end