% Example from Hans Bruun Nielsen DACE slides
model=@(x)prod(exp(bsxfun(@times,x,1:size(x,2))).*...
	     cos(2*bsxfun(@times,x,1:size(x,2))),2);
trafo=@(u)pi*(u-3/4);
% k=2 or 3
