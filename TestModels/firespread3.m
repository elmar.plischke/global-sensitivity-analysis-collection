function [ros,sdr,ews,ri]=firespread3(x)
% Rothermel�s wildland fire model of Liu Jiminez Hussaini �kten Goodrick
% using Albertini
% Gam optimum reation velocity
% wn net fuel loading
% heat fuel low heat content
% etaM moisture damping ccoef
% etaS mineral damping coef
% xi propagating flux ratio
%rhob ovendry bulk density
%eta effective heating number
%Qig heat of pre-ignition
% theta split angle between upslope direction and wind direction
%PhiS slope factor
%PhiW wind factor

% named parameters
xcell=num2cell(x,1);
[w0d1,w0d2,w0d3,d0lh,w0lw,d,sigd1,sigd2,sigd3,siglh,siglw,heat,rhop,md1,md2,md3,mlh,mlw]=xcell{:};


PhiC=sqrt((PhiS+PhiW.*cos(theta)).^2+(PhiW.*sin(theta)).^2);
% reaction intensity
ri=Gam.*wn.*heat.*etaM.*etaS;
%rate of spread
ros=ri.*xi.*(1+PhiC)./(rhoB.*eta.*Qig);