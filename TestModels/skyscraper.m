% skyscraper function
% piecewise constant output
model2=@(x,p)sum(floor(p*x),2); % p: number of floors
model=@(x)model2(x,size(x,2));
% with interaction
model3=@(x,p)prod(ceil(sqrt(p*x)),2);