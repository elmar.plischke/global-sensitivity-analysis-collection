% flood model from Roustant et al. 2013
k=8;
% Maximal annual flow rate 	[m3/s]  Gumbel(1013,558) on (500,3000)
% Strickler coeff 			[-]		Normal(30,8) on (15,+oo)
% River downstream level	[m]		Triangular(49,50,51)
% River upstream level		[m]		Triangular(54,55,56)
% Dyke height			    [m]     Uniform(7,9)
% Bank level				[m]		Triangular(55,55.5,56)
% Length of river stretch   [m]		Triangular(4990,5000,5010)
% River width				[m]		Triangular(295,300,305)

truncs=[ 1-evcdf(-500,-1013,558),  1-evcdf(-3000,-1013,558); ...
              normcdf(15,30,8),1;
			  0,1;  0,1; 0,1; 0,1; 0,1; 0,1]';
truncate=@(u)ones(size(u,1),1)*truncs(1,:)+u.*(ones(size(u,1),1)*(truncs(2,:)-truncs(1,:)));			  
trafo=@(u)[-evinv(1-u(:,1),-1013,558),norminv(u(:,2),30,8),triinv(u(:,3),49,50,51),...
           triinv(u(:,4),54,55,56),7+2*u(:,5),triinv(u(:,6),55,55.5,56),...
		   triinv(u(:,7),4990,5000,5010),triinv(u(:,8),295,300,305)];

% maximal annual overflow	[m]
 model=@(x)x(:,3)+(x(:,1)./(x(:,8).*x(:,2).*sqrt((x(:,4)-x(:,3))./x(:,7)))).^0.6-x(:,5)-x(:,6); 
% costs  					[MEuro]
 cost=@(x,y)(y>0)+(0.2+0.8*(1-exp(-1000./y.^4))).*(y<=0)+max(x(:,5),8)/20;