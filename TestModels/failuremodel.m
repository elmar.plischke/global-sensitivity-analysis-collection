% failure model

k=4;
trafo=@(u)[4+u(:,1)*(6-4),5+u(:,2)*(10-5), u(:,3:4)];

timetofail=@(x)[expinv(x(:,3),x(:,1)),expinv(x(:,4),x(:,2))];

% serial parallel replace
model=@(x)[min(timetofail(x),[],2),max(timtofail(x),[],2),sum(timetofail(x),2)];

