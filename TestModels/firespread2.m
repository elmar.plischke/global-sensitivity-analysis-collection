function retval=firespread2(X)
% Fire spread model
% EUNHYE SONG, BARRY L. NELSON, AND JEREMY STAUM
% ported from the R code found on eunhyesong.info

% This function implements the firespread model from Rothermel (1972) for the purpose of global sensitivity analysis.
% For detailed description, refer to Song, Nelson, Staum (2016): https://epubs.siam.org/doi/10.1137/15M1048070
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Returns the rate of spread and the reaction intensity of fire
% delta: fuel depth
% sigma: fuel particle area-to-volume ratio
% h: fuel particle low heat content
% rho: ovendry particle density
% ml: moisture content of the live fuel
% md: moisture content of the dead fuel
% S: fuel particle total mineral content
% U: wind speed at midflame height
% tanphi: slope
% P: dead fuel loading vs. total loading
  
  w0 = 1./(1+exp((15 - X(:,1))/2)) /4.8824;
  delta = X(:,1)/30.48;
  sigma = X(:,2)*30.48;
  h = X(:,3)*0.4536/0.25;
  %rho = X(:,4)/0.48824;
  rho = X(:,4)/0.48824*30.48;
  ml = X(:,5);
  md = X(:,6); 
  S = X(:,7);
  U = X(:,8)/0.018288;
  tanphi = X(:,9);
  P = X(:,10);
  
  rmax = sigma.^1.5./(495 + 0.0594*sigma.^1.5);
  bop = 3.348*sigma.^(-0.8189);
  A =133*sigma.^(-0.7913);
  thestar = (301.4 - 305.87*(ml - md) + 2260 * md)./(2260 * ml);
  theta = min(1, max(thestar, 0));
  muM = exp(-7.3 * P .* md - (7.3* theta + 2.13) .* (1-P) .* ml);
  
  muS = min(0.174*S.^(-0.19),1);
  C = 7.47*exp(-0.133*sigma.^0.55);
  B = 0.02526*sigma.^0.54;
  E = 0.715*exp(-3.59*10^(-4)*sigma);
  wn = w0.*(1-S);
  rhob = w0./delta;
  eps = exp(-138./sigma);
  Qig = (401.41 + md * 2565.87) * 0.4536/1.060;
  beta = rhob./rho;
  r = rmax.*(beta./bop).^A.*exp(A.*(1-beta./bop));
  xi = (192 +0.2595.*sigma).^(-1).*exp((0.792+0.681*sigma.^0.5).*(beta+0.1));
  phiW = C.*U.^B.*(beta./bop).^(-E);
  phiS = 5.275*beta.^(-0.3).*tanphi.^2;
  
  % Reaction intensity
  IR = r.*wn.*h.*muM.*muS;
  % Rate of spread
  R = IR.*xi.*(1 + phiW + phiS)./(rhob.*eps.*Qig);
  
  retval=R*30.48/60;
end