% The Anscombe Dataset
% Data set  1-3    1    2     3     4    4
% Variable   x     y    y     y     x    y
obs=[1     10.0  8.04  9.14  7.46  8.0  6.58;...
     2      8.0  6.95  8.14  6.77  8.0  5.76;...
     3     13.0  7.58  8.74 12.74  8.0  7.71;...
     4      9.0  8.81  8.77  7.11  8.0  8.84;...
     5     11.0  8.33  9.26  7.81  8.0  8.47;...
     6     14.0  9.96  8.10  8.84  8.0  7.04;...
     7      6.0  7.24  6.13  6.08  8.0  5.25;...
     8      4.0  4.26  3.10  5.39 19.0 12.50;...
     9     12.0 10.84  9.13  8.15  8.0  5.56;...
    10      7.0  4.82  7.26  6.42  8.0  7.91;...
    11      5.0  5.68  4.74  5.73  8.0  6.89];

subplot(2,2,1);
plot(obs(:,2),obs(:,3),'*');
subplot(2,2,2);
plot(obs(:,2),obs(:,4),'*');
subplot(2,2,3);
plot(obs(:,2),obs(:,5),'*');
subplot(2,2,4);
plot(obs(:,6),obs(:,7),'*');
%%
if(0)
x=obs(:,2);y=obs(:,4);
[xs,is]=sort(x);ys=y(is); % sortieren for die grafik
D=[0*xs+1,xs];
yhut=D*(D\ys);
clf
plot(xs,ys,'+',xs,yhut,'-');
end

if(1)
a=[3.5 19.5 2.5 13.5];
subplot(2,2,1);
hold on;
axis(a);
xlabel('x_1');ylabel('y_1');
plot(a(1:2),[1,a(1);1,a(2)]*([ones(11,1),obs(:,2)]\obs(:,3)),'k:');
subplot(2,2,2);
hold on;
axis(a);
xlabel('x_2');ylabel('y_1');
plot(a(1:2),[1,a(1);1,a(2)]*([ones(11,1),obs(:,2)]\obs(:,4)),'k:');
subplot(2,2,3);
hold on;
axis(a);
xlabel('x_3');ylabel('y_1');
plot(a(1:2),[1,a(1);1,a(2)]*([ones(11,1),obs(:,6)]\obs(:,7)),'k:');
subplot(2,2,4);
hold on;
axis(a);
xlabel('x_4');ylabel('y_4');
plot(a(1:2),[1,a(1);1,a(2)]*([ones(11,1),obs(:,2)]\obs(:,5)),'k:');
end