function [Si,Si2,indx]=elsa(x,y,group,m,gfx)
%% ELSA Elastic Sensitivity indices using search curves
%       and Chatterjee-type / three makes company estimators
% [S,C]=ELSA(X,Y,GROUP,M) estimates the (subset) sensitivity of the group
%       GROUP using the modulus parameter M: if scalar then a plowtrack with
%       frequencies M^i is used; if vector, M(i) are used as frequencies
%       using a ping pong curve (default M=11). [experimental M<=0]
%       S returns variance-based, C Gini/Chatterjee type estimators.
%       ELSA(X,Y,GROUP,M,'GFX') produces a visualization.
%
% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
assert(nn==n, 'Input/output sizes mismatch!');
% assert(kk==1, 'Only single output supported.');
assert(all((group>=1) & (group<=k)), 'Index out of range.');

l=length(group);
verbose=false; %true; %false; % dis-/enable path statistics
rank_transform=true; % false; % input is uniform
skip_sort=false;

if(l>1) % higher order effects
%% rank-transform the input
if(rank_transform)
 %r=zeros(n,l); % test for sub-cube information
 rr=zeros(n,l); 
 [~,indxx] = sort(x(:,group));
 for i=1:l
     rr(indxx(:,i),i)= ((1:n))/(n);
 end
else
 rr=x(:,group);
end
% plot(rr(:,1),rr(:,2),'.'); pause
%%
 H=1; % max. harmonics -- unused
%% initialise "frequencies"
if(nargin<4) || isempty(m)
    m=11;
end

if(m(1)==0)
    % Hilbert curve
    % m=floor(log2(flintmax)/l); % no of bits per dim.
    m=floor(32/l);
    z=c2i((rr-1/n)*(2^m),m);
elseif(isnan(m(1)))
    assert(l==2,'only 2D curves.')
   %z=hilbertcode(rr);
    z=peanocode(rr);
elseif(m(1)<0)
    % special permutations
    skip_sort=true;
    switch(-m(1))
        case 1,
    % MST/TSP solution
            [p,startend]=mst2tsp(rr);

            % convert tree to path
            indx=zeros(n,1);
            s=startend(1); % from the start
            i=1;indx(i)=s;
        % do until ...
            s=p(s);i=i+1;indx(i)=s;
            while(s~=p(s)) % until we hit the root
                s=p(s);i=i+1;indx(i)=s;
            end
            s=startend(2); % and from the end
            i=n;indx(i)=s;
            while(s~=p(s))
                s=p(s);i=i-1;indx(i)=s;
            end
        case 2,
   % cross path elimination
            indx=xpath(rr);
        case 3,
            [~,indx]=sort(max(rr,[],2));
            indx=xpath(rr,indx);
        case 4,
            [~,indx]=sort(prod(rr,2));
            indx=xpath(rr,indx);
        case 5,
            [~,indx]=sort(rr(:,1));
            indx=xpath(rr,indx);
    end
elseif length(m)==1
    % plowtrack curve
    P=m; % P=2*m+1;
    z=floor((rr*P)*(1-eps)); % scale and round
    sgns=(-1).^z;
    s=sgns(:,end);
    z(:,1)=rr(:,1)*(P-1); % first dimension with fractional information
    for i=l-1:-1:1
        % change directions
        z(s==-1,i) = P-1-z(s==-1,i);
        s=s.*sgns(:,i);
    end
    z=z*(P.^(0:(l-1))');
elseif length(m)==l
% ping pong curve
%% extended Euclidean algorithm: results could be precomputed/reused
 ms=prod(m);
 M=ms./m;
 [g,~,v]=gcd(m,M);
 if(any(g~=1)), error('Commensurate moduli'); end

 r=floor((rr.*m)*(1-eps)); % scale and round
%% make even
 idx=mod(r,2)==1;
 r(idx)=-r(idx)-1;
 rr(idx)=-rr(idx);
 zz=mean(rr-r,2); % remainder [0,1]

%% chinese remainder
 z= mod( r*(M.*v)', 2*ms) +zz; % plus sub-cube info
 idx=find(z>ms);     % handle large numbers
 z(idx)=2*ms-z(idx); % by flipping back
 %%
end
else
    % first order effects
    z=x(:,group); % use input as sorting key
end
if(~skip_sort)
 [~,indx]=sort(z);
end
Ey=mean(y);
Vy=var(y);

% multioutput 
ys=y(indx,:);

%%
if(nargin>4 && ~isempty(gfx))
if(l>1)
 subplot(1,2,1)
 default=get(gcf,'DefaultAxesColorOrder');
 if(n<=10000)
 % Rainbow colors
  set(gcf,'DefaultAxesColorOrder',hsv(n-1));

  if(l==3)
     plot3([x(indx(1:end-1),group(1)),x(indx(2:end),group(1))]',...
           [x(indx(1:end-1),group(2)),x(indx(2:end),group(2))]',...
           [x(indx(1:end-1),group(3)),x(indx(2:end),group(3))]',...
           '-o');
     zlabel(['x_{' num2str(group(3)) '}']);
  else
     plot([x(indx(1:end-1),group(1)),x(indx(2:end),group(1))]',...
         [x(indx(1:end-1),group(2)),x(indx(2:end),group(2))]',...
         '-o');
  end
 else
  if(l==3)
     plot3(x(indx,group(1)),x(indx,group(2)),x(indx,group(3)),'-o');
     zlabel(['x_{' num2str(group(3)) '}']);
  else
     plot(x(indx,group(1)),x(indx,group(2)),'-o');
  end
 end
% colororder(gcf,'default');
% hold off
 %h=plot(x(indx,group(1)),x(indx,group(2)),'-o');
 %if(n>2000), set(h,'MarkerSize',2); end
 xlabel(['x_{' num2str(group(1)) '}'])
 ylabel(['x_{' num2str(group(2)) '}'])
 subplot(1,2,2)
 set(gcf,'DefaultAxesColorOrder',default)
end

plot(1:n,ys);
xlabel('index');ylabel('output');
end
%%

if(kk>1)
    % just 1 option
    Si=1-mean(diff(ys).^2)./(2*Vy);
else
w=ys(1:end-2,:).^2+ys(2:end-1,:).^2+ys(3:end,:).^2 ...
    -(ys(1:end-2,:)+ys(2:end-1,:)+ys(3:end,:)).^2/3;
    % all three estimators
    Si=[1-mean(w)./(2*Vy), 1-mean(diff(ys).^2)./(2*Vy), (mean(ys(1:end-1).*ys(2:end))-Ey.^2)./Vy];
end
if(nargout>1 && kk==1)
    % do Gini CvM as well
     [yr,indxy] = sort(y); % assume 1d
     yr(indxy)  = (1:n)/(n+1); % empirical cdf, avoiding 1
     rs=yr(indx);
     % also use Yitzhaki |X-Y|= X+Y-2min(X,Y)
     Si2=[1-3*mean(abs(rs(2:end)-rs(1:end-1))), ...
         6*mean(min(rs(2:end),rs(1:end-1)))-2];
end
%% if chatty
 if(l>1)  && verbose
  pathstats(rr,indx);
 end
end
%%
function [p,leaf]=mst2tsp(x)
%% MST2TSP Approximate Travelling Salesperson via Minimum Spanning Tree
%%
%% from: Converting MST to TSP Path by Branch Elimination
%% Pasi Fränti, Teemu Nenonen and Mingchuan Yuan, Appl. Sci. 2021, 11, 177.

% no code to be very proud of
p=mst_prim_alt(x);root=1;p(root)=root; % node 1 defaults to root here
n=size(x,1);
leaf=1:n;
leaf(p(2:end))=[];
if(sum(p(2:end)==root)==1), leaf=union(leaf,root); end % root is leaf
depth=zeros(n,1);
branch=[];
backtrack=cell(n,1);
for i=1:n, j=p(i); if(j~=i), backtrack{i}=j; end; end % add all connections
for l=leaf
    m=p(l);
    while(m~=l)
     depth(m)=depth(m)+1;
     backtrack{m}(end+1)=l;
     if(depth(m)>1), branch(end+1)=m; break; end
     l=m;m=p(l);
    end
end
if(length(backtrack{root})==2),branch=setdiff(branch,root); end % root is no branch
jjj=unique(branch);
while(true)
% largest(smallest?) branch
dmax=0; % inf; %
bp=0;bq=0;
for j=jjj % ([1,end])
%j=jjj(1); % always pick next
succ=backtrack{j};
   for k=succ
    d=sum( (x(j,:)-x(k,:)).^2);
    if(d>dmax), dmax=d; bp=j; bq=k; end
    %if(d<dmax), dmax=d; bp=j; bq=k; end
   end
end
% reroot at bp
q=bp;
r=p(q);
p(q)=q; % new root
while(r~=q)
    s=p(r); p(r)=q;
    q=r;r=s;
end

p(bq)=bq; % second tree
%unlink
lnk=backtrack{bp};
backtrack{bp}=setdiff(lnk,bq);
if(length(lnk)==3)
    % remove from branch list
    jjj=setdiff(jjj,bp);
end
lnk=backtrack{bq};
backtrack{bq}=setdiff(lnk,bp);
if(length(lnk)==2)
    % add to leaf list
    leaf=union(leaf,bq);
elseif(length(lnk)==3)
       jjj=setdiff(jjj,bq);
end
% color the trees
color=zeros(1,n);
color(bp)=1;
color(bq)=2;

for l=leaf
    m=l;
    while color(m)==0, m=p(m); end
    c=color(m);
    m=l;
    while color(m)==0, color(m)=c;m=p(m); end
end
%%
red=color(leaf)==1;
autumn=leaf(red); % all red leaves
spring=leaf(~red); % all green leaves

d=inf; l1=0;l2=0;
lena=sum(red);
lens=sum(~red); %length(spring)
if(lena<lens)
    len=lena;
    for l=autumn
        [dd,ll]=min(sum((x(l,:)-x(spring,:)).^2,2));
        if(dd<d)
            l1=l;l2=spring(ll);
            d=dd;
        end
    end
else
    len=lens;
    for l=spring
        [dd,ll]=min(sum((x(l,:)-x(autumn,:)).^2,2));
        if(dd<d)
            l1=l;l2=autumn(ll);
            d=dd;
        end
    end
end
%  l1 belongs to smaller subtree, reroot and connect
q=l1;
r=p(q);
p(q)=l2; % connect
while(r~=q)
    s=p(r); p(r)=q;
    q=r;r=s;
end

% update leaf, backtrack
if(len==1)
 leaf=setdiff(leaf,l2);
else
 leaf=setdiff(leaf,[l1,l2]);
end
backtrack{l1}(end+1)=l2;
backtrack{l2}(end+1)=l1;
if(length(leaf)<3), break; end
%cnt=length(jjj);
%if(mod(cnt,25)==0),fprintf('%d branches to go.\n',cnt);end
end

end

function indx=xpath(x,indx)
%% Cross path dismanteling Pan Xia for TSP approximation
% Costs: Multipass O(n^2) ...
 n=size(x,1);
 seglen=@(x,i,j)sqrt(sum((x(i,:)-x(j,:)).^2));
 if(nargin==1 || isempty(indx))
  [~,indx]=sort(sum(x,2)); % use coordinate sum for a initial path
 end
 % save segleni in a vector and update when swapping
 seglens=zeros(1,n);
 seglens(1)=seglen(x,indx(1),indx(2));
 seglens(2)=seglen(x,indx(1),indx(2));
 things_to_do=true;
 first_run=true;
 while things_to_do
    things_to_do=false;
    for i=1:n-1
     p1=indx(i);p2=indx(i+1);
     segleni=seglens(i);
     for j=i+2:n-1
        q1=indx(j);q2=indx(j+1);
        if(~first_run)
          seglenj=seglens(j);
        else
         seglenj=seglen(x,q1,q2);
         seglens(j)=seglenj;
        end
        seglenX1=seglen(x,p1,q1);
        seglenX2=seglen(x,p2,q2);
        if(segleni+seglenj ...
                > seglenX1+seglenX2 )
            indx(i+1:j)=indx(j:-1:i+1);

            seglens(i:j)=[seglenX1,seglens(j-1:-1:i+1),seglenX2];
            p2=q1;
            segleni=seglenX1;
            things_to_do=true;
        end
     end
     first_run=false;
    end
end

end
%%
function pathstats(x,indx)
 d=sqrt(sum(diff(x(indx,:)).^2,2));
 fprintf('***************\nLength %.4g, Max %.4g, Min %.4g, Mean %.4g, Std %.4g\n***************\n',...
    sum(d),max(d),min(d),mean(d),std(d));
end

%%
function testme
%%
 ishigami
 n=1024; % 4096
 R=100;
 types = 5;
 resa=zeros(R,types,3);
 resb=zeros(R,types,2);

 resc=zeros(R,k,k); % for hdmrsi
h=waitbar(0,'Simulation in progress')
 for r=1:R
     waitbar(r/R,h);
% x=trafo(rand(n,k));
x=trafo(net(scramble(sobolset(k),'MatousekAffineOwen'),n));
 y=model(x);

 [a1,b1]=elsa(x,y,[ 1 3],-2);
 [a2,b2]=elsa(x,y,[ 1 3],-1);
 [a3,b3]=elsa(x,y,[ 1 3],0);
 [a4,b4]=elsa(x,y,[ 1 3],11);
 [a5,b5]=elsa(x,y,[ 1 3],[23 25]);

resa(r,:,:)=[a1;a2;a3;a4;a5];
resb(r,:,:)=[b1;b2;b3;b4;b5];
resc(r,:,:)=hdmrsi(uniformer(x),y,[7,7]);

 end
 delete(h)
 disp('Curves');disp('means')
 squeeze(mean(resa,1))
 disp('stddev')
 squeeze(std(resa,1))

 disp('HDMR');disp('mean,std')
 mean(resc(:,1,3))
 std(resc(:,1,3))

%%
end

function testelsa2
%%
ishigami
 n=4096
 k=4;
 Sij_Var3=zeros(k,k); % 3 term variance -- Broto Bachoc
 Sij_Jans=zeros(k,k); % successive Jansen
 Sij_Prod=zeros(k,k); % product formulation of Klein et al.
 Gij_Chat=zeros(k,k); % Chatterjee: abs. rank difference
 Gij_Yitz=zeros(k,k); % Yitzhaki: min of ranks

 x=trafo(rand(n,k));
 y=model(x);

  for i=1:k
     for j=1:k
         % unique() pre-sorts, so do it directly
         if(i==j)
           [sij,gij]=elsa(x,y,i);
         else
           % second return argument is Gini/Chatterjee
           [sij,gij]=elsa(x,y,[i j]);
         end
         Sij_Var3(i,j)=sij(1); Sij_Jans(i,j)=sij(2); Sij_Prod(i,j)=sij(3);
         Gij_Chat(i,j)=gij(1); Gij_Yitz(i,j)=gij(2);
     end
  end
fun=@(x)x-diag(x)-diag(x)'+2*diag(diag(x)); % remove individual effects from pairs
subplot(2,3,1);imagesc(fun(Sij_Var3));colorbar;title('Var3');
subplot(2,3,2);imagesc(fun(Sij_Jans));colorbar;title('Jansen');
subplot(2,3,3);imagesc(fun(Sij_Prod));colorbar;title('Product');
subplot(2,2,3);imagesc(fun(Gij_Chat));colorbar;title('Chatterjee');
subplot(2,2,4);imagesc(fun(Gij_Yitz));colorbar;title('Min Rank');
 %%
end

%%
function bugme
%% hunting bugs in hi-dim plowtrack
M=12;
[xx,yy,zz]=meshgrid(linspace(0,1,M));
z=[xx(:),yy(:),zz(:)];
elsa(z,prod(z,2),[1 2 3],M,'gfx'); % seems ok
%
M=[5 6 7];S=3;
[xx,yy,zz]=meshgrid(linspace(0,1,M(1)*S),linspace(0,1,M(2)*S),linspace(0,1,M(3)*S));
z=[xx(:),yy(:),zz(:)];
elsa(z,prod(z,2),[1 2 3],M,'gfx')

%%
end

function curvemean
%%
R=128;
n=4096; %2048;
X1=zeros(R,n);
X2=zeros(R,n);
for r=1:R
 %x=normcdf(randn(n,2)*[1,.8;0,1]);
 %x=rand(n,2);
 x=net(scramble(sobolset(2),'MatousekAffineOwen'),n);
 [~,~,ii]=elsa(x,sum(x,2),[ 1 2],nan); %[21 13]); % 0);
 X1(r,:)=x(ii,1)';
 X2(r,:)=x(ii,2)';
end
subplot(1,2,1)
plot(mean(X1),mean(X2),'-')
subplot(1,2,2)
plot(1:n,sqrt(var(X1)+var(X2)))
%%
end

%% compare against klt
function elsaklt
%%
 ishigami
 n=4096;
 u=rand(n,k);
 %u=net(scramble(sobolset(k),'MatousekAffineOwen'),n);
 x=trafo(u);
 y=model(x);

 [Si1,~,ii]=elsa(u,y,[1 3],nan);Si1
 c=abs(fft(y(ii))).^2; % circulant matrix, for convenience
 c(1)=0; % without mean
 Z=toeplitz(real(ifft(c))/(n-1));
 [V,D]=eigs(Z,60,'lm'); % ,struct('issym',true)); % decreasingly ordered?
 yhat=V*V'*y(ii);
 Si1KL=mean(yhat.^2)/var(y)
 subplot(1,2,1)
plot(yhat,y(ii),'.')
subplot(1,2,2)
plot(sort(diag(D),'descend'))

%%
end
