function [Sij,Si]=sissi(x,y,group,m,gfx)
%% SISSI Computes sensitivity indices (first and higher effects) 
%       using the ping-pong curve
%   SIJ=SISSI(X,Y,GROUP) computes the combined effects of the indices
%                        GROUP. 
%   SIJ=SISSI(X,Y,GROUP,MODULI) additionally specifies the moduli for 
%                the construction of the ping-pong curve.
%   SIJ=SISSI(X,Y,GROUP,[],'TITLE') creates a graphical output.
%   [SIJ,SI]=SISSI(X,Y,GROUP) also returns the main effects for GROUP.
%
% See also: XEASI, LISSI, QSUSI
%
% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'),end
if any((group<1) | (group>k)), error('Index out of range.'), end

l=length(group);
H=4; % max. harmonics
scale=1/6; % threshold for unimportant contributions
%% initialise "frequencies"
if(nargin<4) || isempty(m)
%%    
    m_defaults=[13 15 16 17 19];
    m=m_defaults(1:l);
%%
end
%% extended Euclidean algorithm: results could be precomputed/reused
 ms=prod(m);
 if(H*sum(m)>n), fprintf('Running low on samples.');end
 M=ms./m;
 [g,u,v]=gcd(m,M);
 while(any(g~=1))
     warning('Commensurate moduli');
     m=m+1; % retry
     ms=prod(m);
     if(2*H*ms>n), fprintf('Running low on samples.');end
     M=ms./m;
     [g,u,v]=gcd(m,M);
 end
%% rank-transform and scale the input
r=zeros(n,l);
rr=zeros(n,l); % test for sub-cube information
[xr,indxx] = sort(x(:,group));
for i=1:l
     rr(indxx(:,i),i)= ((1:n))*m(i)/(n+1);
     r(indxx(:,i),i) = floor(((1:n))*m(i)/(n+1));
end
%% make even
 idx=mod(r,2)==1;
 r(idx)=-r(idx)-1;
 rr(idx)=-rr(idx);
 zz=mean(rr-r,2); % remainder [0,1]
 
%% chinese remainder
 z= mod( r*(M.*v)', 2*ms) +zz; % plus sub-cube info
 idx=find(z>ms);     % handle large numbers 
 z(idx)=2*ms-z(idx); % by flipping back
%% close the curve by traversing it back and forth 
 if mod(n,2)==0
% even no. of samples
    shuffle=[1:2:(n-1), n:-2:2];
 else
% odd no. of samples
    shuffle=[1:2:n, (n-1):-2:2];
 end
%shuffle=1:n
 %%
[~,indx]=sort(z);
F=fft(y(indx(shuffle))); % save for regression curve
spectrum=abs(F.^2);
spectrum=2*spectrum/sum(spectrum(2:end));
%idx=find(spectrum(2:end/2)>=scale/sqrt(n));
idx=find(spectrum(2:(H*sum(m)))>=scale/sqrt(n));

df=0;%
%df=length(idx); % bias correction
Sij=(n*sum(spectrum(1+idx))-df)/(n-df); % sum of first and higher order effects
% First order effects
Si=zeros(1,l);
for i=1:l
    Si(i)=sum(spectrum(1+(1:H)*M(i)));
end
%% some graphics
if(nargin==5)
%%    
    subplot(3,1,1);
    plot(1:n,x(indx(shuffle),group),'.'); %,'MarkerSize',3);
    title([gfx ' - Inputs (sorted)']);xlabel('Index');
    legend(cellstr([char(ones(l,1)*'x_{'),num2str(group'),char(ones(l,1)*'}')]));
    a=axis;a(2)=n;axis(a);
    subplot(3,1,2);
    plot(1:n,y(indx(shuffle)),'k.');
    title([gfx ' - Output (reordered)']);xlabel('Index');
    hold on;
    % regression curve
    G=zeros(n,1);freqsel=[1,1+idx',(n+1)-idx'];
    G(freqsel)=F(freqsel);
    plot(1:n,real(ifft(G)),'r');
    hold off;
    a=axis;a(2)=n;axis(a);
    subplot(3,1,3);
    specshow(spectrum,M,H); %m
    title([gfx ' - Power Spectrum of Output']);
    xlabel('Frequency');ylabel('Variance Contribution');
%%
end
%%
end
%%

function test
%%
trafo=@(u)2*u-1;
model=@(x)x(:,1).^3.*x(:,2).*(1+x(:,1).*x(:,2));

x=trafo(rand(10000,2));
y=model(x);

for i=0:2:16
    [Sij,Si]=sissi(x,y,[2,1],i+[1 11],'Bla')
    pause(2); 
end
%%
end
