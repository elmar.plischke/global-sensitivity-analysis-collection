function [Si,Ti,Hi,ST]=ninasi(x,y,opts,gfx,K)
% NINASI Nearest Neighbor Sensitivity.

% Devroye, Gy�rfi, Lugosi, Walk: A nearest neighbor estimate 
% of the residual variance, Electronic Journal of Statistics 2018

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
pnorm=@(y)y.^2; % abs(), .^2 .^8
if(nargin>2) && size(opts,2)==k 
    l=size(opts,1); 
else
    l=0;
    opts=[];
end
if(nargin>4) && K>1
   min_or_sort=@sort;
   flatten=@(hh,eve,m)reshape((2*hh(1:m,:)-eve)',1,m*size(hh,2));
else
   K=1;
   min_or_sort=@min;
   flatten=@(hh,eve,m)2*hh-eve;
end

I=zeros(n,K*(2*k+l));E2Y=mean(y).^2;VY=var(y);EY2=mean(y.^2);

for i=1:n
 even=mod(i,2)==0;
 z=pnorm(bsxfun(@minus,x((2-even):2:n,:),x(i,:)));
 [~,ii]=min_or_sort([z,sum(z,2)-z,z*opts']);
% if(K==1)
%  I(i,:)=2*ii-even;
% else
  %[~,ii]=sort([z,sum(z)-z,z*opts']);
  I(i,:)=flatten(ii,even,K);
% end
end
%size(I),size(y)
ST=y'*y(I)/n;
Si=(ST(1:k)-E2Y)/VY;
m=(k+1):(2*k);
Ti(1,:)=1+(E2Y-ST(m))/VY;
%% Experimental
Ti(2,:)=mean(bsxfun(@minus,y,y(I(:,m))).^2)/2/VY;
%Ti(3,:)=y'*bsxfun(@minus,y,y(I(:,m)))/n/VY;
%Ti(4,:)=(EY2-ST(m))/VY;
%%
if(l>0), Hi=(ST((2*k+1):end)-E2Y)/VY; 
elseif(K>1) Hi=[]; 
end
if(K>1), ST=[ones(K,1),(1:K)']\reshape((ST-E2Y)/VY,numel(ST)/K,K)'; end

if(nargin>3) && ~isempty(gfx)
 for i=1:(2*k+l)
     subplot(round(sqrt((2*k+l))),ceil(sqrt((2*k+l))),i)
     %hold on;
     plot(y,y(I(:,i)),'.');
     if(i<=k), title(['Main ' num2str(i)]), 
     elseif(i<=2*k), title(['Total ' num2str(i-k)]); 
     else, title(['Higher ' num2str(find(opts(i-2*k,:)))]);
     end
 end
end
end

function testnina
%%
ishigami
res=[];
R=50;
second=[1,1,0,0;1,0,1,0;1,0,0,1;0,1,1,0;0,1,0,1;0,0,1,1];
for n=[50,200,1000,5000,10000]
    %x=trafo(rand(n,k));y=model(x);
    for r=1:R
        x=trafo(iccorrelate(sobolpoints(n,k),eye(k)));y=model(x);
        [Si,Ti,Sb2]=ninasi(x,y,second);
        %jj=randperm(n);
        %[Si,Ti]=ninasi(x(jj,:),y(jj));
        res=[res; n,r,Si,Ti(2,:),Sb2];
    end
end
%%
z=reshape(res(:,3:10),R,5*2*k); % something's odd here
boxplot(z,'PlotStyle','compact','Color','bbbbbgggggrrrrrkkkkkbbbbbgggggrrrrrkkkkk');
a=gca;
chr0={'50','200','1k','5k','10k'};
chr={ chr0{:} chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:} };
a=gca;
a.XTick=1:40;
a.XTickLabel=chr;
a.XTickLabelRotation=90;
a=axis;
hold on
h=plot([0,1],[a(2) a(2)],'b',[0,1],[a(2) a(2)],'g',[0,1],[a(2) a(2)],'r',[0,1],[a(2) a(2)],'k');
legend(h,{'x_1','x_2','x_3','x_4'},'AutoUpdate','off')
for i=1:7
    plot(.5+[5 5]*i,[a(3),a(4)],'k:')
end
an=[.3139,.4424,0,0,.5576,.4424,.2437,0];
for i=1:8
    plot((i-1)*5+[.5 5.5],an(i)*[1,1],'--k')
end
title('Nearest Neighbor estimates of first and total effects, 50 random replicates per box')
ylabel('Variance contribution')
xlabel('Sample size')
%%
end

function testnina2
%%
ishigami
n=7500;
x=trafo(rand(n,k));
y=model(x);
G=[0,1]; % Gray code: Appendix D in Maxwell: Bebop Boolean Boogie
for i=2:k
    G=[G,fliplr(G);zeros(1,size(G,2)),ones(1,size(G,2))];
end
G=G(:,2:end); % remove zero
[S,T,H]=ninasi(x,y,G');

l=convertStringsToChars(replace(string(num2str(G'))," ",""));
stem(H);
a=gca;
a.XTick=1:((2^k)-1);a.XTickLabel=l;
a.XTickLabelRotation=90;
%% Shapley values via M�bius Trafo: need poset inclusion matrix
% Mazur: Combinatorics
%Zeta=speye(2^k-1);
iii=[];jjj=[];vvv=[];
for i=1:2^k-1 
    for j=1:2^k-1
         g(j)=all(G(:,i)<=G(:,j));
    end
    jj=find(g);
    iii=[iii,i*ones(size(jj))];
    jjj=[jjj,jj];
    vvv=[vvv,ones(size(jj))];
end
Zeta=sparse(iii,jjj,vvv);
mob=full(H/Zeta)./sum(G);
g=logical(G);
for i=1:k
    Shap(k)=sum(mob(g(i,:)));
end
Shap
%%
end