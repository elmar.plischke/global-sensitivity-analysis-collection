function [SS,TT]=ninjasi(x,y)
%NINJASI Sensitivity Analysis with Nearest Neighbor
% Using Owen's triple split data
% (and Evens / Jones Gamma regression -- no)
[n,k]=size(x);
E2Y=mean(y).^2;VY=var(y);
pnorm=@(y)y.^2;
iiA=1:3:n;
iiB=2:3:n;
iiC=3:3:n;
I=zeros(n,2*k);
%D=zeros(n,2*k);
J=zeros(n,2*k);
%E=zeros(n,2*k);
for i=1:n
    switch mod(i,3)
      case 1, ii1=iiB;ii2=iiC;
      case 2, ii1=iiC;ii2=iiA;
      case 0, ii1=iiA;ii2=iiB;
    end
        z=pnorm(bsxfun(@minus,x(ii1,:),x(i,:)));
        [d,ii]=min([sum(z,2)-z,z]);
        I(i,:)=ii1(ii);
        %D(i,:)=d; % sqared distances
        z=pnorm(bsxfun(@minus,x(ii2,:),x(i,:)));
        [d,ii]=min([sum(z,2)-z,z]);
        J(i,:)=ii2(ii);
        %E(i,:)=d;
end
size(I)
%ST=y'*(y(I)+y(J))/(2*n);
%S=(ST(1:k)-E2Y)/VY;
jj=[2:n,1]'; %[iiB;iiC;iiA];
kk=[n,1:n-1]';
S(1,:)=mean((y-y(I)).*(y(J(:,[(k+1):end,1:k]))-y(jj)))/VY;
S(2,:)=mean((y-y(J)).*(y(I(:,[(k+1):end,1:k]))-y(kk)))/VY;
T(1,:)=mean((y-y(I)).^2)/2/VY;
T(2,:)=mean((y-y(J)).^2)/2/VY;
%T(3,:)=mean((2*y-(y(J)+y(I))).^2)/8/VY;
%T(4,:)=mean((y-(D.*y(J)+E.*y(I))./(D+E)).^2)/2/VY;
%L=mean(D);M=mean(E);
SS=[S(:,1:k);1-T(:,(k+1):end)];
TT=[T(:,1:k);1-S(:,(k+1):end)];
end