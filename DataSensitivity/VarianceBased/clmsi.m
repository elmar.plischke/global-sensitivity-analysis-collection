function Si=clmsi(x,y,l,gfx)
%% CLMSI Compute sensitivity indices using Correlation ratio methods.
%   SI=CLMSI(X,Y) computes the main effects using a piecewise linear fit. 
%
%   SI=CLMSI(X,Y,L) additionally specifies the partition size L, if L<0 then 
%   the requested number of realisations per subsample is -L. 
%   L defaults to CEIL(SQRT(LENGTH(X))). 

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'); end

%% default partitioning: n^1/2 samples in n^1/2 partitions
if(nargin<3) || (isempty(l))
 l=ceil(sqrt(n)); 
 fprintf('CLMSI: partition size %d\n',l); 
end
if(l<0)
    % requested number of realisations per subsample
    l=ceil(n/-l);
 fprintf('CLMSI: partition size %d\n',l); 
end
%%
Si=zeros(1,k);
EY=mean(y);

for i=1:k
%% Rank-transform input for partitioning
 [xx,index]=sort(x(:,i));
 xx(index)=((1:n)-.5)*l/n;
% sensitivity index for parameter i
 D=[]; % construct design matrix
 e=[]; % end of subsample (for spline approach)
 for j=1:l
  D=[D, (xx>=(j-1)) & (xx<j) ];
  if(j<l)
    q=j*n/l;r=floor(q);
    e=[e, x(index(r),i)+(q-r)*(x(index(r+1),i)-x(index(r),i)) ];
  end
  end 
 %yhat=D*(D\y);
 
 % conditional linear fit
 Dlin=[D, D.* (x(:,i)*ones(1,l))];
 if(1)
     % use sort-of-continuous spline
     E=sqrt(n)*eye(l-1,l);
     E=E-E(:,[end,1:end-1]); %  Additional conditions penalising the gap
     E=[Dlin; E,diag(e)*E];  %  ... 1 -1 ... e(i) -e(i) ... (= 0 + error) 
     yhat=Dlin*(E\[y;zeros(l-1,1)]);
 else
    yhat=Dlin*(Dlin\y);
 end
 if (nargin==4)
     if k>1
 subplot(2,ceil(k/2),i);
     end
 zz=x(index,i);
 %plot(x(:,i),y,'*',zz,yhat(indx),'-',zz,yhatlin(indx),':'); 
 plot(zz,y(index),'.',zz,yhat(index),'-','LineWidth',2); %,zz,yhatlin(index),':');
 xlabel(['x_{',num2str(i),'}']);ylabel('y');
% legend('local const model'); %,'local linear model'); %title(gfx);
 title(gfx);
 end
 Si(i)= sum( (yhat -EY).^2)/sum( (y-EY).^2);
 %%
end
%% Use degree-of-freedom correction
%Si=((n-1)*Si-(2*l-1))./(n-2*l); % Kelley's formula
return

