function Si=susi(x,y,l,gfx)
%% SUSI Compute sensitivity indices using Correlation Ratio methods.
%   SI=SUSI(X,Y) computes the main effects using a piecewise constant fit. 
%
%   SI=SUSI(X,Y,L) additionally specifies the partition size L, if L<0 then 
%   the requested number of realisations per subsample is -L. 
%   L defaults to CEIL(SQRT(LENGTH(X))). 
%   
%   SI=SUSI(X,Y,[],'GFXTITLE') provides graphical feedback.

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'); end

%% default partitioning: n^1/2 samples in n^1/2 partitions
if(nargin<3) || (isempty(l))
 l=ceil(sqrt(n)); 
 fprintf('SUSI: partition size %d\n',l); 
end
if(l<0)
    % requested number of realisations per subsample
    l=ceil(n/-l);
 fprintf('SUSI: partition size %d\n',l); 
end
if(l==0)
    fprintf('SUSI: Switching to discrete mode\n');
    discrete_mode=1;
else
    discrete_mode=0;
end
%%
Si=zeros(1,k);
EY=mean(y);
%%
if(~discrete_mode)
[xs,index]=sort(x);
end
xr=zeros(n,1);
warn_once=0;
 for i=1:k
%% Rank-transform input for partitioning
if(~discrete_mode)
 %% empirical cdf (scaled by L) with ties
smallcnt=0;samecnt=1;val=xs(1,i);
for(j=2:n)
    nval=xs(j,i);
    if(val==nval), 
      samecnt=samecnt+1;
    else
      val=nval;
      xr(index(smallcnt+(1:samecnt),i))=(smallcnt+0.5*samecnt)*l/n;
      smallcnt=smallcnt+samecnt;samecnt=1;
    end
end
% fill last entry/run
xr(index(smallcnt+(1:samecnt),i))=(smallcnt+0.5*samecnt)*l/n;
 %for j=1:n
 %   xx(index(j))= (sum( zz(1:j)<zz(j) ) + 0.5* sum( zz==zz(j) ))*l/n;
 %end
else
    % discrete mode
    [xs,~,xr]=unique(x(:,i));
    l=length(xs);
end
%% sensitivity index for parameter i
 D=[]; % construct design matrix
 for j=1:l
  indx=(xr>(j-1)) & (xr<=j);
  if(any(indx))
   D=[D, indx ];
  else
   if(~warn_once),disp('SUSI: Empty partition class detected.');warn_once=1;end
  end
 end 
 yhat=D*(D\y);
 % conditional linear fit
 %Dlin=[D, D.* (x(:,i)*ones(1,l))];
 %yhat=Dlin*(Dlin\y);  
 if (nargin==4)
 subplot(floor(k/2+.5),2,i);
 if(~discrete_mode)
 plot(xs(:,i),y(index(:,i)),'.',xs(:,i),yhat(index(:,i)),'-','LineWidth',2);
 else
      for j=1:l
     ii=xr==j;firstnb=find(ii);
     plot(j+rand(sum(ii),1)-.5,y(ii),'.',j,yhat(firstnb(1)),'o');
     hold on
      end
      hold off
 end
 xlabel(['x_{',num2str(i),'}']);ylabel('y');
% legend('local const model'); %,'local linear model'); %title(gfx);
 title(gfx);
 end
 Si(i)= sum( (yhat -EY).^2)/sum( (y-EY).^2);
 %%
end
%% Use degree-of-freedom correction
%Si=((n-1)*Si-(l-1))./(n-l); % Kelley's formula
return

