function [Ti,num1,den1,num0,den0]=jenni(x,y,R,gfx)
% JENNI Nearest Neighbor Estimates of Total Effects, Jackknife Debiasing
%%
[n,k]=size(x);
%biasf=@(n)n;sqrt(n); % bias is a function of 1/biasf(samplesize)
if(nargin<3 || isempty(R))
R=25;
end
if(nargin<4), gfx=''; end 

num0=zeros(1,k);den0=var(y);
%num1=zeros(n,k);num2=zeros(n,k);
num1=zeros(R,k);num2=zeros(R,k);
%den1=zeros(R,k);den2=zeros(R,k);
for j=1:k
    notj=1:k;notj(j)=[];
    p=mst_prim_alt(x(:,notj));
    p(1)=find(p==1,1); % loop at root
    num0(j)=y'*(y-y(p))/n;
%    num1(1,j)=num0(j);
%    p(1)=1; % remove root
%    num2(1,j)=y'*(y-y(p))/n;
    if(~isempty(gfx))
        subplot(ceil(sqrt(k)),round(sqrt(k)),j);plot(y,y(p),'.');
    end
    for i=1:R
        ii=randperm(n);
        i1=ii(1:floor(2*n/3)); % Jackknife from Gentle: Stat Comput
        %i2=ii(floor(n/2)+1:end);
        [p1,d1]=mst_prim_alt(x(i1,notj));p1(1)=1;
        %[p2,d2]=mst_prim_alt(x(i2,notj));p2(1)=1;
        num1(i,j)=3*y(i1)'*(y(i1)-y(i1(p1)))/(2*n); % 2/3 sample
        den1(i,j)=var(y(i1));
        %num2(i,j)=2*y(i2)'*(y(i2)-y(i2(p2)))/n;
        %den2(i,j)=var(y(i2));
    end

% % perform a jackknife by re-routing the tree
% p(1)=0;%num2(1,:)=num0;
% pp=p;
%  for i=2:n
%      p=pp; % reset
%      s=i;r=0;firstq=p(s);
%      while(s~=0)
%      q=p(s);p(s)=r;r=s;s=q;
%      end
%      p(i)=firstq; % loop back at the root
%      num1(i,j)=y'*(y-y(p))/n;
%      p(i)=i; % erase at the root
%      num2(i,j)=y'*(y-y(p))/n;
%   %   p(i)=0;
%  end
 end

%T12=mean([num1;num2])./mean([den1;den2]);
Ti=[num0./den0;...
    mean(num1,1)./mean(den1,1);...
    (3*num0./den0-2*(mean(num1,1)./mean(den1,1)))]; 
%; mean(num1)./mean(den1);(3*num0-2*mean(num1))./den0]; 
 %T12; 2*num0./den0-1/2*(mean(num1)/mean(den1)+mean(num2)/mean(den2))];
 % Miller: Jackknife for quotients
end
