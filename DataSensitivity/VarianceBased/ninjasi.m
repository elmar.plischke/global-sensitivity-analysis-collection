function [S,T,M]=ninjasi(x,y)
%NINJASI Sensitivity Analysis with Nearest Neighbor
% Using Owen's triple split data
[n,k]=size(x);
E2Y=mean(y).^2;VY=var(y);
pnorm=@(y)y.^2;
iiA=1:3:n;
iiB=2:3:n;
iiC=3:3:n;
I=zeros(n,k);
J=zeros(n,k);
C=zeros(n,k);
for i=1:n
    switch mod(i,3)
      case 1, ii1=iiB;ii2=iiC;
      case 2, ii1=iiC;ii2=iiA;
      case 0, ii1=iiA;ii2=iiB;
    end
        z=pnorm(bsxfun(@minus,x(ii1,:),x(i,:)));
        %[~,ii]=min(z*(ones(k)-eye(k)));
        [~,ii]=min(sum(z,2)-z);
        C(i,:)=x(i,:)-x(ii1(ii)+((1:k)-1)*n);
        I(i,:)=ii1(ii);
        z=pnorm(bsxfun(@minus,x(ii2,:),x(i,:)));
        [~,ii]=min(z);
        J(i,:)=ii2(ii);
end
%ST=y'*(y(I)+y(J))/(2*n);
%S=(ST(1:k)-E2Y)/VY;
jj=[2:n,1]; %[iiB;iiC;iiA];

% Owen 1/n ( f(x:x)-f(x:z))'(f(x:y)-f(y:y))
S=mean((y-y(I)).*(y(J)-y(jj(:))))/VY;

% 1/2n ( f(x:x)-f(x:z))'(f(x:x)-f(x:z))
T(1,:)=mean((y-y(I)).^2)/VY/2;
T(2,:)=1-mean((y-y(J)).*(y(I)-y(jj(:))))/VY;
%mean((y([n,1:n-1]')-y(J)).^2)/VY/2
m=(y-y(I))./C; % Morris OAT
M(1,:)=mean(m);
M(2,:)=mean(abs(m));
M(3,:)=std(m);
end