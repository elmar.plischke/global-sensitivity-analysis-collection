function [Si,Sj,Ti,Tj,Vy]=nanasi(x,y)
% NANASI Nearest Neighbor Sensitivity with symmetric 
% and asymmetric versions.

% Confidence Intervals for a Class of Residual Variance Estimators (Draft)
% Elia Liiti�inen and Francesco Corona and Belle Selene Xia

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
K=2; % first and second NN
pnorm=@(y)y.^2; % abs(), .^2 .^8

idx=zeros(n,4*k);
for i=1:n
 z=pnorm(bsxfun(@minus,x,x(i,:)));
 z(i,:)=nan(); % always sort last
 [~,ii]=mink([z sum(z,2)-z], 2); % pick subdimensions
 idx(i,:)=[ii(1,:),ii(2,:)];
end

Vy=var(y);
Si=1-mean( (y(idx(:,1:k))-y).^2 )/(2*Vy); 
Sj=1-mean( (y(idx(:,1:k))-y).*(y(idx(:,2*k+(1:k)))-y) )/Vy;

Ti=mean( (y(idx(:,k+(1:k)))-y).^2)/(2*Vy);
Tj=mean( (y(idx(:,k+(1:k)))-y).*(y(idx(:,3*k+(1:k)))-y) )/Vy;
end

