function T=ninnasi(x,y,M)
% Nearest Neighbor sensitivity using knnsearch
if(nargin<3) || isempty(M), M=1; end
vec=@(v)v(:);
[n,k]=size(x);
[~,jj]=sort(rand(n,1)); % random permutation
j1=jj(1:floor(n/2));
x1=x(j1,:);
j2=jj(floor(n/2)+1:end);
x2=x(j2,:);
f=zeros(2,k);
for i=1:k
    ii=1:k;ii(i)=[]; % all dimensions but index i
    jj=knnsearch(x(:,ii),x(:,ii),'K',1+M); % first index finds identity
    f(1,i)=mean(vec(y(jj(:,2:end)).*y)); % without the identity match
    jj=knnsearch(x1(:,ii),x2(:,ii),'K',M);
%    ll=knnsearch(x2(:,ii),x1(:,ii),'K',M);
%    f(2,i)=mean(vec(y([j1(jj);j2(ll)]).*y([j2;j1])));
f(2,i)=mean(vec(y(j1(jj)).*y(j2)));
end
T=(mean(y.^2)-f)./var(y);
end