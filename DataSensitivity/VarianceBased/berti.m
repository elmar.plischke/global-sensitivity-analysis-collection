function [ Ti, Tj ] = berti(x,y ,bits, gfx)
%BERTI Total Sensitivity using Hilbert search curves.
%   T=BERTI(X,Y) computes total effects.

[n,k]=size(x);
if(nargin<3 || isempty(bits)), bits=min(10,floor(64/k)); end
if(nargin<4)
gfx=''; % 'Gfx';
end
VY=var(y);

Ti=zeros(1,k);
Tj=zeros(1,k);
u=zeros(n,k);

[xs,ii]=sort(x);
for i=1:k
 u(ii(:,i),i)=((1:n)-.5)/n;
end
for i=1:k
    ii=1:k;ii(i)=[];
    [~,ic]=sort(c2i(u(:,ii)*2^bits-1,bits));
    Ti(:,i)= mean( diff(y(ic)).^2 )/(2*VY);
    if(~isempty(gfx))
        subplot(ceil(sqrt(k)),round(sqrt(k)),i);
        plot(y(ic(1:end-1)),y(ic(2:end)),'.');
        xlabel('y');ylabel('y+');
        title([ gfx ', totals for x_{' num2str(i) '}']) 
    end
    if(nargout>1)
     xc=xs(ic,ii); % x along the curve
        if(0)
        % no improvement noticable (maybe buggy)
    % with pair of nearest neighbor detection
  
     pre=sum((xc(4:end,:)-xc(3:end-1,:)).^2,2)-sum( (xc(3:end-1,:)-xc(1:end-3,:)).^2,2)>0;
     pst=sum((xc(4:end,:)-xc(2:end-2,:)).^2,2)-sum( (xc(2:end-2,:)-xc(1:end-3,:)).^2,2)<0;
     prei=[false;false;pre;true];
     prei1=[false;pre;true;false];
     prei2=[pre;true;false;false];
     
     psti=[true;pst;false;false];
     psti1=[false;true;pst;false];
     psti2=[false;true;pst];
     
     cnti=~(prei | psti); % prae and post are nonexclusive(?)
     cnti1=[cnti(2:end);false];
     cnti2=[false;cnti(1:end-1)];
     
     Tj(i)=(mean( (y(ic(prei1))-y(ic(prei))).*(y(ic(prei2))-y(ic(prei))))+...
         mean( (y(ic(psti1))-y(ic(psti))).*(y(ic(psti2))-y(ic(psti))))+...
         mean( (y(ic(cnti1))-y(ic(cnti))).*(y(ic(cnti2))-y(ic(cnti)))))/VY/4;
     % factor 4 unaccounted for ?!?
        end
        % next try: just decide if to use predecessor or successor
        dx=sum( diff(xc).^2,2);
        jj=diff(dx)>0;
        pre=[false; jj ; true];
        pre1=[jj;true;false];
        pst=~pre;
        pst1=[false;true;~jj];
        Tj(i)=(y(ic(pre))'*(y(ic(pre))-y(ic(pre1)))+ ...
            (y(ic(pst))'*(y(ic(pst))-y(ic(pst1)))))/n/VY;
    end
end
end

function bertitest
%% Hilbert search curve vs. nearest neighbor
ishigami
n=4000;
R=50;
NSi=zeros(R,k);
NTi=zeros(R,k);
Ntim=zeros(R,1);
MTi=zeros(R,k);
Mtim=zeros(R,1);
for r=1:R
    x=trafo(rand(n,k));y=model(x);
    tic;[S,T]=ninasi(x,y);NSi(r,:)=S;NTi(r,:)=T;Ntim(r)=toc;
    tic;[T]=berti(x,y);MTi(r,:)=T;Mtim(r)=toc;
end
%%
clf
boxplot([NTi,MTi])
%%

end