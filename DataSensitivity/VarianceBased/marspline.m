function [B,a0,lof0]=marspline(x,y,Mmax,Tmax)
% Multi-Adaptive Regression Splines by Friedman
% Algorithm 2
% follows closely the pseudo-code in the article

% written by elmar.plischke@tu-clausthal.de
n=size(x,2);
% base size
if(nargin<3 || isempty(Mmax))
    Mmax=n^2*10;
end
% number of tests
if(nargin<4 || isempty(Tmax))
    Tmax= inf(); %  % enable random subset selection for Tmax<size(x,1)
end
vkm = true(n,Mmax+1);
B{1}=@(x)ones(size(x,1),1); % feature
M=2;
while M<=Mmax
    disp([ num2str(M) ' base functions (out of ' num2str(Mmax) ')']);
    lof0=inf(); % lack of fit
    a0=zeros(1,M+1);
    for m=1:M-1
        jj=B{m}(x)>0;
        if(Tmax<length(jj))
            jj=jj(randperm(length(jj),Tmax));
        end        
        ii=find(vkm(:,m))';
        if(Tmax<length(ii))
            ii=ii(randperm(length(ii),Tmax));
        end
        for v=ii % 1:n 
            %%
            for t=x(jj,v)'
                [a,lof]=minLOF1(B,t,M,m,v,x,y);
                if(lof<lof0), lof0=lof;m0=m;v0=v;t0=t; a0=a; end
            end
            %%
        end
    end
    B{M}=@(x)B{m0}(x).*max(x(:,v0)-t0,0); 
    B{M+1}=@(x)B{m0}(x).*max(t0-x(:,v0),0);
    vkm(:,M) = vkm(:,m0); % best guess
    vkm(:,M+1) = vkm(:,m0);
    vkm(v0,M+[0 1]) = false;
    M=M+2;
end
end
function B=mars_recursive_split(x,y,Mmax)
% Algorithm 1
n=size(x,2);

B{1}=@(x)ones(size(x,1),1);

for M=2:Mmax
    lof0=inf(); % lack of fit
    a0=zeros(1,M);
    for m=1:M-1
        
        jj=B{m}(x)>0;
        for v=1:n
            %%
            for t=x(jj,v)'
                [a,lof]=fminsearch(@(a)LOF1(a,B,t,M,m,v,x,y),a0);
                if(lof<lof0), lof0=lof;m0=m;v0=v;t0=t; a0=a; end
            end
            %%
        end
    end
    B{M}=@(x)B{m0}(x).*(x(:,v0)<t0);
    B{m0}=@(x)B{m0}(x).*(x(:,v0)>t0);
end
end

function r=LOF(a,B,t,M,m,v,x,y)
z=0;
% g function: p.w. constant
for i=1:length(B)
    if(i~=m)
     z=z+a(i)*B{i}(x);
    else
        z=z+B{m}(x).*(a(m).*(x(:,v)>t)+a(M).*(x(:,v)<t));
    end
end
% loss function
r= mean((y-z).^2);
end

function r=LOF1(a,B,t,M,m,v,x,y)
z=0;
%
% g function: p.w. polynomial
for i=1:M-1
     z=z+a(i)*B{i}(x);
end
z=z+B{m}(x).*(a(M).*max(x(:,v)-t,0)+a(M+1).*max(t-x(:,v),0));

% loss function
r= mean((y-z).^2);
end

function [a,r]=minLOF1(B,t,M,m,v,x,y)
% via regression
% Currently design matrix is rebuilt for each call
% Friedman suggests to solve LS via Cholesky and save
% decomposition between calls

D=zeros(size(x,1),M+1);

% g function: p.w. polynomial
for i=1:M-1
     D(:,i)=B{i}(x);
end
D(:,M)=B{m}(x).*max(x(:,v)-t,0);
D(:,M+1)=B{m}(x).*max(t-x(:,v),0);
a=D\y;
try
 warning off last % no more "singular to working precision" messages
end
% loss function
r= mean((y-D*a).^2);
end

function testMARSPLINE
%%
ishigami
n=1024;
%x=trafo(rand(n,k));
x=trafo(sobolpoints(n,k));
y=model(x);
for i=1:k
  subplot(2,ceil(k/2),i)
  [B,a,l]=marspline(x(:,i),y,10);
  z=0;
  for j=1:length(B)
    z=z+B{j}(x(:,i))*a(j);
  end
  plot(x(:,i),y,'.',x(:,i),z,'o');
  title(['Sensitivity ' num2str(1-l/var(y))])
  xlabel(['x_{' num2str(i) '}']);
end
%%
  [B,a,l]=marspline(x(:,[1 3]),y,40);
  disp(['Sensitivity of (1,3):' num2str(1-l/var(y))])
  u=linspace(-pi,pi,20);
  [uu,vv]=meshgrid(u,u);
  xx=[uu(:),vv(:)];
  zz=0;
  for j=1:length(B)
    zz=zz+B{j}(xx)*a(j);
  end
  clf
  surf(uu,vv,reshape(zz,20,20));
  %%

end

