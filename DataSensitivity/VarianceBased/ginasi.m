function Ti=ginasi(x,y,p,gfx)
% GINASI K-Nearest Neighbor Sensitivity using GAMMA approach.

% Devroye, Györfi, Lugosi, Walk: A nearest neighbor estimate 
% of the residual variance, Electronic Journal of Statistics 2018
% Antonia J. Jones: New tools in non-linear modelling and prediction 2004

% Written by elmar.plischke@tu-clausthal.de
global sym_count; % switch plot symbols on hold
[n,k]=size(x);
pnorm=@(y)y.^2; % abs(), .^2 .^8
if(nargin<=2), p=10;  end
if(nargin<=3), gfx=''; end

VY=var(y);
vec=@(v)v(:)';
gamma=zeros(n,k*p);
delta=zeros(n,k*p);
for i=1:n
 even=mod(i,2)==0;
 z=pnorm(bsxfun(@minus,x((2-even):2:n,:),x(i,:)));
 [di,ii]=sort(sum(z,2)-z);jj=2*ii-even;
 
 gamma(i,:)=vec((y(i)-y(jj(1:p,:))).^2);
 delta(i,:)=vec(di(1:p,:)); % already squared
end
%%
G=reshape(mean(gamma)'/2,p,k);
D=reshape(mean(delta),p,k);
if(p~=1)
for j=1:k
    beta(j,:)=[ones(p,1),D(:,j)]\G(:,j);
    if(~isempty(gfx))
        subplot(1,k,j)
        if(ishold)
            sym='os^v<>';
            if(j==1)
             if(isempty(sym_count)), sym_count=2; 
             else sym_count=mod(sym_count,length(sym))+1; end; 
            end
            str=['k' sym(sym_count) ];
        else
            sym_count=[];
            str='ko';
        end
        plot(D(:,j),G(:,j)/VY,'-',0,beta(j,1)/VY,str);
        xlabel('k-NN squared distance'); ylabel('total effect');
    end
end
Ti=beta(:,1)'/VY;
else
    Ti=G/VY;
end
end

function testnina
%%
ishigami
res=[];
R=50;
second=[1,1,0,0;1,0,1,0;1,0,0,1;0,1,1,0;0,1,0,1;0,0,1,1];
for n=[50,200,1000,5000,10000]
    %x=trafo(rand(n,k));y=model(x);
    for r=1:R
        x=trafo(rand(n,k));y=model(x);
        [Si,Ti,Sb2]=ninasi(x,y,second);
        %jj=randperm(n);
        %[Si,Ti]=ninasi(x(jj,:),y(jj));
        res=[res; n,r,Si,Ti,Sb2];
    end
end
%%
z=reshape(res(:,3:end),R,5*2*k);
boxplot(z,'PlotStyle','compact','Color','bbbbbgggggrrrrrkkkkkbbbbbgggggrrrrrkkkkk');
a=gca;
chr0={'50','200','1k','5k','10k'};
chr={ chr0{:} chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:}  chr0{:} };
a=gca;
a.XTick=1:40;
a.XTickLabel=chr;
a.XTickLabelRotation=90;
a=axis;
hold on
h=plot([0,1],[a(2) a(2)],'b',[0,1],[a(2) a(2)],'g',[0,1],[a(2) a(2)],'r',[0,1],[a(2) a(2)],'k');
legend(h,{'x_1','x_2','x_3','x_4'},'AutoUpdate','off')
for i=1:7
    plot(.5+[5 5]*i,[a(3),a(4)],'k:')
end
an=[.3139,.4424,0,0,.5576,.4424,.2437,0];
for i=1:8
    plot((i-1)*5+[.5 5.5],an(i)*[1,1],'--k')
end
title('Nearest Neighbor estimates of first and total effects, 50 random replicates per box')
ylabel('Variance contribution')
xlabel('Sample size')
%%
end
