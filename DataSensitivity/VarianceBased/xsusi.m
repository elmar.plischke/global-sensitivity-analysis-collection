function Si=xsusi(x,y,indx,M)
%% XSUSI Compute sensitivity indices (higher effects) 
%%       using a checkerboard partitioning method

% These are combined effects, if index==[i,j] then Si+Sj+Sij is returned
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'),end
if any((indx<1) | (indx>k)), error('Index out of range.'), end

li=length(indx);
%if(li==k), error('XSUSI: Refusing to compute one.'), end

%
EY=mean(y);
if(nargin<4)
M = floor(n^(1/(2*li))); % no. of partitions per dimension BAD!
fprintf('XSUSI: Using partition size %ld\n',M); 
end
%if(M<5), 
%    disp('Sensitivity Indices: Running low on samples.'); 
%end
%% indices using interactions from 'indx'
D0=[];
for dim=indx
 %% Rank-transform input for partitioning
 [xx,indxx]=sort(x(:,dim));
 xx(indxx)=((1:n)-.5)*M/n;
% sensitivity index for parameter i
 D=[]; % construct design matrix
 for j=1:M
  D=[D, (xx>=(j-1)) & (xx<j) ];
 end
% dim , D(1:10,:)
 if(isempty(D0))
  D0=D; 
 else
   [nd,kd]=size(D0);
   D1=D0.*D; %repmat(D,1,kd/M);
  for i=2:M
   D1=[ D1, D0.* D(:, [i:M, 1:(i-1)])];
  end
  %sum(D1)
  if(any(sum(D1)==0)), disp('oops, zeros.'); end
  D0=D1;
 end
 end
 % throw away zeros
 D0(:,sum(D0)==0)=[];
 % compute regression
yhat=D0*(D0\y);
%if(li>=2)
%plot3(x(:,indx(1)),x(:,indx(2)),y,'*');
%hold on
%plot3(x(:,indx(1)),x(:,indx(2)),yhat,'r+');
%hold off
%xlabel(['x_{', num2str(indx(1)),'}']);
%ylabel(['x_{', num2str(indx(2)),'}']);
%zlabel('y');
%end
Si= sum( (yhat -EY).^2)/sum( (y-EY).^2);
end
