function Sij=qeasi(x,y,M,nobiascorrection)
% QEASI First and second order effects via search curves.
%  QEASI provides a wrapper to XEASI.
% SIJ=QEASI(X,Y) computes a matrix SIJ with first order effects on the diagonal and second order effects
%               in the off-diagional entries. Due to the asymmetric frequency selection SIJ(i,j) 
%               may differ from SIJ(j,i).
%    QEASI(X,Y,M,NB) uses a frequency spacing of M. The presence of parameter NB disables bias correction 
%               (in case of QMC sampling) 
[n,k]=size(x);
if(nargin==2 || isempty(M))
    M=6;
end
if(nargin<4)
    DF=2*M*(2*M-1);
    DM=M;
else
    DM=0;DF=0;
end
Sij=zeros(k,k); 
for i=1:k
    Si=xeasi(x,y,[i],M);
    Sij(i,i)=(n*Si-2*DM)/(n-2*DM);  
    for j=(i+1):k
            Sij(i,j)=(n*xeasi(x,y,[i,j],M)-DF)/(n-DF);
            Sij(j,i)=(n*xeasi(x,y,[j,i],M)-DF)/(n-DF);
    end
end
end
