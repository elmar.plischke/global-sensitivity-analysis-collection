function Sij=lissi(x,y,group,m,gfx)
%% LISSI Computes sensitivity indices (first and higher effects) 
%       using the space-filling Hilbert curve
%   SIJ=LISSI(X,Y,GROUP) computes the combined effects of the indices
%                        GROUP. 
%   SIJ=LISSI(X,Y,GROUP,M) additionally specifies the depth of the 
%                Hilbert curve corresponding to a partition of 2^M per dimension.
%   SIJ=LISSI(X,Y,GROUP,[],'TITLE') creates a graphical output.

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
l=length(group);

if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'),end
if any((group<1) | (group>k)), error('Index out of range.'), end

%% initialise depth
if(nargin<4) || isempty(m)
%%    
    m=[4 4];
%%
end
%% rank-transform and scale the input
r=zeros(n,l,'int32');
for i=1:l
     [xr,indxx] = sort(x(:,group(i)));
     r(indxx,i) = floor(((0:n-1)*2^m(1))/n); % 0...(2^m)-1
end
%% coordinate to index mapping using Hilbert curve
 z=c2i(r,m(1));
 %%
[dummy,indx]=sort(z);
yr=y(indx);

%% wavelet decomposition using wavelet toolbox
wname='db2';
[F,lens]=wavedec(yr,m(end)-1,wname);
G=zeros(size(F));
G(1:lens(1))=F(1:lens(1));
%df=lens(1)-1; % degree of freedom for bias correction
df=0;
yhat=waverec(G,lens,wname);
%% compute sensitivity indices (sum of first and higher order effects)
EY=mean(y);
Sij=(n*sum( (yhat -EY).*(yr-EY))./ sum( (y-EY).*(y-EY))-df)/(n-df);
 
%% some graphics
if(nargin==5)
%%    
    subplot(3,1,1);
    plot(1:n,x(indx,group),'.'); %,'MarkerSize',3);
    title([gfx ' - Inputs (sorted)']);xlabel('Index');
    legend(cellstr([char(ones(l,1)*'x_{'),num2str(group'),char(ones(l,1)*'}')]));
    a=axis;a(2)=n;axis(a);
    subplot(3,1,2);
    plot(1:n,yr,'k.');
    title([gfx ' - Output (reordered)']);xlabel('Index');
    hold on;
    % regression curve
    plot(1:n,yhat,'r');
    hold off;
    a=axis;a(2)=n;axis(a);
    h=subplot(3,1,3);
    mdw1dstem(h,F,lens);title([gfx ' - Wavelet Decomposition']);
    a=axis;a(2)=n;axis(a);xlabel('Index');ylabel('');
%%
end
%%
end
%%