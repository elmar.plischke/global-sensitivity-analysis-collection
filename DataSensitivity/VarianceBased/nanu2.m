function [T,ts]=nanu2(x,y,standardize,quick)
% NANU Total effects with updating nearest neigbors.
%  T=NANU(X,Y) returns total effects.
% [T,TS]=NANU(X,Y,STD) standardizes the inputs if STD is true, and 
%       additionally returns the (unscaled) intermediate estimates
%       when building up the nearest neighbor graph.

% written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
if(nargin<3 || isempty(standardize))
standardize=true; % make round unit balls
end
if(nargin<4 || isempty(quick))
    quick=n>20000; % don't compute total est for each realization
end
if (standardize)
    x=(x-mean(x))./std(x);
end
ts=zeros(n-1,k);
%J=repmat((1:n)',1,k);
z=repmat(y,1,k); 
od=inf(n,k); % old distances
if(~quick)
for i=2:n
    dx=(x(i,:)-x(1:i-1,:)).^2;
    di=sum(dx,2)-dx;   % distances to new point
    jj=od(1:i-1,:)>di; % indices to be updated
    for j=1:k
     kk=find(jj(:,j));
     od(kk,j)=di(kk,j);
     z(kk,j)=y(i);                   % copy the output
    end
    [od(i,:),ni]=min(di,[],1);        % NN for new point
    z(i,:)=y(ni);
%    ts(i-1,:)=sum( (y(1:i)-y(J(1:i,:))).^2)/(2*i);    % Jansen estimator

    ts(i-1,:)=sum( (y(1:i)-z(1:i,:)).^2)/(2*i);    % Jansen estimator
end
else
    % ts outside loop
for i=2:n
    dx=(x(i,:)-x(1:i-1,:)).^2;
    di=sum(dx,2)-dx;   % distances to new point
    jj=od(1:i-1,:)>di; % indices to be updated
    for j=1:k
     kk=find(jj(:,j));
     od(kk,j)=di(kk,j);
     z(kk,j)=y(i);                   % copy the output
    end
    [od(i,:),ni]=min(di,[],1);        % NN for new point
    z(i,:)=y(ni);
end
ts=cumsum((y-z).^2)./(2*(1:n)');
end
% Efron Stein debiasing 
% Assume point est. (last value) is biased towards true value as
% mean estimator towards point est.
T=(2*ts(end,:)-mean(ts(1:end-1,:)))/var(y);
end
% for r=1:300;r,ii=randperm(n);T(r,:)=nanu(x(ii,:),y(ii)); end