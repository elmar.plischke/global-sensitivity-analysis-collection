function Si=crasi(x,y,M,gfx)
% CRASI adaptive correlation ratio computing first order sensitivity indices
%   SI=CRASI(X,Y) computes first order indices, 
%   SI=CRASI(X,Y,M) additionally sets number of extrema (default 5),
%               M<0 requests piecewise cubic Hermitean interpolation
%   SI=CRASI(X,Y,M,GFX) or SI=CRASI(X,Y,[],GFX) produces graphical
%   illustration 
%
% written by elmar.plischke@tu-clausthal.de
%%
[n,k]=size(x);
[xr,indx]=sort(x);
yr=y(indx);
Ey=mean(y);
Vy=sum((y-Ey).^2);
yplus=[zeros(1,k);cumsum(yr-Ey)/sqrt(Vy)];
%%
if(nargin<3) || isempty(M)
    M=5; % use maximal M pairs of inner points
end
%% requesting interpolation
if(M<0)
    do_interpol=1;
    M=abs(M);
else
    do_interpol=0;
end
%% optimal partition
innerp=zeros(2*M,k);
yrect=yplus;innerp=[];
for i=1:M
    [valx,indx]=max(yrect);
    [valn,indn]=min(yrect);

    innerp(2*i-[ 1 0],:)=[indx;indn];
    for j=1:k
        yrect(:,j)=rectify(yrect(:,j),indx(j),indn(j));
    end
   hold on;plot(linspace(0,1,n+1),yrect);
end
innerp=[ones(1,k);sort(innerp);ones(1,k)*(n+1)];
%%
if(nargin==4)
%% show graphics
  yout=zeros(2*M+2,k);
  legname=cell(1,k);
  for i=1:k
  yout(:,i)=yplus(innerp(:,i),i);
  legname{i}=['x_' num2str(i)];
  end
  if(~ishold)
% two-sided, niveau given by n^-.5
    alfa=1-1/(2*sqrt(n));
    %bound=norminv(allfa); %icdf('normal',alfa,0,1); 
    bound=-sqrt(2)*erfcinv(2*alfa); % avoid stat toolbox 
    xs=[0:4,linspace(5,n-5,20), n-4:n]; % for round edges
    %xs=0:n;
    ys=sqrt( bound* (1-((2*(xs)-n)/n).^2));
    fill([xs,fliplr(xs)], [ys,-fliplr(ys)],[.8,.8,.8],'EdgeColor','none');hold on;
  end
  h=plot(0:n,yplus);
  legend(h,legname);
  if(~do_interpol)
      plot(innerp-1,yout,':o');
  else
      plot(innerp-1,yout,'o');
  end
  hold off;
 title(gfx);
 a=axis;a(2)=n;axis(a);
 xlabel('index (sorted)');
 ylabel('cumulative sum of normalized output')
%%
end
%%
Si=zeros(1,k);
for i=1:k
%%
    u=unique(innerp(:,i)); % avoid boundary extrema {1,n} occurring twice
    idu=n./diff(u);
    m=length(u);
    if(~do_interpol)
%    Si(i)= (n*sum((diff(yplus(u,i)).^2./(diff(u))))-m)/(n-m); 
%    Si(i)= sum((diff(yplus(u,i)).^2./(diff(u))))
        Si(i)=(idu'*(diff(yplus(u,i)).^2)-m)/(n-m); % unbias
    else
    if(0)
    % CRAP    
    % do Newton interpolation
    % horner scheme
      %  c=yplus(u,i);
      %  for (l=1:(m-1))
      %    for (j = m:-1:(l+1))
      %      c(j)=(c(j)-c(j-1))/(u(j)-u(j-l));
      %    end
      %  end
     % horner to polynomial
      %  p = c(m);
      %  for (l=(m-1):-1:1) 
      %      %p=polyadd(c(l), polymul([1,-u(l)],p));
      %      p=conv(poly(u(l)),p);p(end)=p(end)+c(l);
      %  end
        p = polyfit(u,yplus(u,i),m-1); % replaces Newton
        % integrate the square of the derivative
        %dp=polydiff(p);
        dp=polyder(p);
        %q=polyint(polymul(dp,dp));
        q=polyint(conv(dp,dp));
        %Si(i)=[1,-1]*polyeval(q,[n;0]);
        Si(i)=[1,-1]*polyval(q,[n;0]);
    end
    % use piecewise cubic Hermite interpolation polynomial
        pp=pchip(u,yplus(u,i));
        [breaks,coefs,num]=unmkpp(pp);
        si=0;
        % integrate the square of the derivative
        for l=1:num
            dp= polyder(coefs(l,:)); % breaks(l) is shifted to 0
            q = polyint(conv(dp,dp));
            si= si + polyval(q,breaks(l+1)-breaks(l)); % -polyval(q,0) (=0)       
        end
        % bias correction
        Si(i)=(n*si-m)/(n-m);
        
        if(nargin==4)
            hold on
            ns=0:n;
            plot(ns,ppval(pp,ns),'k:');
            %plot(ns,polyval(p,ns),'k:',ns,interp1(u,yplus(u,i),ns,'pchip'),'k--');
            hold off
        end
    end
%%
end
end


function yr=rectify(y,a,b)
n=length(y);
ya=y(a);yb=y(b);
if(a<b)
 z=[ (1:a)*ya/a, (1:b-a)*(yb-ya)/(b-a)+ya, (n-b-1:-1:0)*yb/(n-b)]';
else
 z=[ (1:b)*yb/b, (1:a-b)*(ya-yb)/(a-b)+yb, (n-a-1:-1:0)*ya/(n-a)]';
end
yr=y-z;
end

% mit dreh
function yr=alt_rectify(y,a,b)
n=length(y);
ya=y(a);yb=y(b);
if(a<b)
 yr=[ y(1:a)-(1:a)'*ya/a; (1:b-a)'*(yb-ya)/(b-a)+ya-y((a+1):b); y(b+1:n)-(n-b-1:-1:0)'*yb/(n-b)];
else
 yr=[ y(1:b)-(1:b)'*yb/b; (1:a-b)'*(ya-yb)/(a-b)+yb-y((b+1):a); y(a+1:n)-(n-a-1:-1:0)'*ya/(n-a)];
end
end

%% polynomials in action
%function p=polyadd(p1,p2)
% n1=length(p1);n2=length(p2);
% if(n1>n2)
%  p=p1+[zeros(1,n1-n2),p2];
% else
%  p=p2+[zeros(1,n2-n1),p1];
% end
%end
%
%function p=polymul(p1,p2)
%%% de-normalize
% q1=polyadd(p1,zeros(size(p2)));
% q2=polyadd(p2,zeros(size(p1)));
%%%
% Q=q1.'*q2;
% % sum over the anti-diagonals
% [n,k]=size(Q);
% R=Q(:,n:-1:1); % fliplr
% p=[];
% for(i = n-1:-1:-(n-1))
%  s=sum(diag(R,i));
%  if(isempty(p)) 
%   if(s~=0) 
%    p=s;
%   end
%   else
%    p=[p,s];
%  end
% end
%%% 
%end
%
%function q=polydiff(p)
%  N=length(p)-1;
%  if(N<1)
%    q=[];
%  else
%    %D=diag(N:-1:1); D(N+1,N)=0;q=p*D;
%    q=p(1:N).*(N:-1:1);
%  end
%end
%
%function q=polyint(p)
%  n=length(p);
%  if(n<1)
%    q=[];
%  else
%    q=[ p./(n:-1:1) , 0];
%  end
%end
%
%function y=polyeval(p,x)
%  N=length(p)-1;
%  m=numel(x);
%  if(m==1)
%    y=(x.^(N:-1:0))* p';
%   else
%    y=(repmat(x(:),1,N+1).^repmat(N:-1:0,m,1)) * p.';
%  end
%end
