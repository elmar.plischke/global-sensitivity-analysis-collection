function [T,ts]=nanu(x,y,standardize)
% NANU Total effects with updating nearest neigbors.
%  T=NANU(X,Y) returns total effects.
% [T,TS]=NANU(X,Y,STD) standardizes the inputs if STD is true, and 
%       additionally returns the (unscaled) intermediate estimates
%       when building up the nearest neighbor graph.

% written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
if(nargin<3 || isempty(standardize))
standardize=true; % make round unit balls
end
if (standardize)
    x=(x-mean(x))./std(x);
end
l=size(y,2);
if(l==1)
    ts=zeros(n-1,k);
else
    ts=zeros(n-1,k,l);
end
for j=1:k
a=1:k;a(j)=[]; % all but j
J=(1:n)';
do=inf(n,1); % old distances
for i=2:n
    if(mod(i,1000)==0), fprintf('Factor %d:%d, Iteration %d:%d\n',j,k,i,n); end
    di=sum((x(i,a)-x(1:i-1,a)).^2,2); % distances to new point
    jj=find(do(1:i-1)>di);            % update
    if(~isempty(jj))
    J(jj)=i;
    do(jj)=di(jj);
    end
    [do(i),J(i)]=min(di);                  % NN for new point
    if(l==1)
    ts(i-1,j)=sum( (y-y(J)).^2)/i;    % Jansen estimator
    else
        ts(i-1,j,:)=sum( (y-y(J,:)).^2)/i;
    end
end
end
% Efron Stein debiasing 
% Assume point est. is biased towards true value as
% mean estimator towards point est.
if(l==1)
T=(2*ts(end,:)-mean(ts(1:end-1,:)))/var(y)/2;
else
    T=squeeze(2*ts(end,:,:)-mean(ts(1:end-1,:,:)))./var(y)/2;
end
end
% for r=1:300;r,ii=randperm(n);T(r,:)=nanu(x(ii,:),y(ii)); end