function [Si,Valt]=bregmansi(x,y,varargin)
% BREGMANSI Sensitivity from Bregman functions (Generalized Correlation
% Ratios)
% [S,V]=BREGMAN(X,Y,M,MODE) returns the sensitivities of Y given the 
% columns of X using a harmonic basis with M harmonics (default 7) 
% and Bregman function MODE (default 'quadratic')

[n,k]=size(x);
%% parse args
% defaults
    M=5; % for haar wavelet
    % M=7; % for cosine transformation
    mode='brier';
    newmode=false;
    offset=0;
    gfx='';
%    
l=varargin;

s=nextarg(l);
if ~isempty(s)
if ~ischar(s)
    M=s;
else
    mode=s;
    newmode=true;
end
end
if ~newmode
    s=nextarg(l);
    if ~isempty(s), mode=s;end
end

%%     
[xs,ix]=sort(x);
ys=y(ix);
%c=dct(ys);
%c((M+1):end,:)=0;
%yhat=idct(c);
c=dwthaar(ys);
c((1+2^M):end,:)=0;
yhat=idwthaar(c);
ymean=mean(y);
%%
%for i=1:k
%    subplot(ceil(sqrt(k)),round(sqrt(k)),i);
%    plot(xs(:,i),yhat(:,i),'-');
%end
%%
switch(lower(mode))
    case {'quadratic','brier'}  % Imai Encyclopedia of Maths
    f=@(y)y.^2;
    check=@(y)true;
    case {'log','logarithmic','kl','kullback'}
    f=@(y)nan2null(y.*log(y));
    check=@(y)all(y>=0);
    case {'burg','itakura','saito'}
    f=@(y)-log(y);
    check=@(y)all(y>0);
    case 'mixed'
    ab=nextarg(l); 
    f=@(y)y.^(ab(1))-y.^(ab(2));
    check=@(y)ab(2)~=1 | any(y>=0);
    case {'interval','bit','logistic'}
    ab=nextarg(l);
    f=@(y)nan2null( (y-ab(1)).*log(y-ab(1))+ (ab(2)-y).*log(ab(2)-y) );
    check=@(y)all(y>=ab(1)) &  all(y<=ab(2));
    case 'hockey' % Freshly invented
        a=nextarg(l); 
        f=@(y)max(y,a);
        check=@(y)true;
    case {'exp','exponential'} % Chen Chen Reo
         f=@(y)exp(y);
        check=@(y)true;
    case {'inv','inverse'}
        a=nextarg(l); 
        f=@(y)1./(y-a);
    check=@(y)all(y>a);
    case {'power','pow'}
        a=nextarg(l);
        f=@(y)-y.^a/(a*(1-a));
        check=@(y)all(y>0);
    case {'dualbit','duallogistic'} % Bregman Voronoi diagrams, Boissonnat Nielsen Nock
        f=@(y)log(1+exp(y));
        check=@(y)true;
    case 'hellinger'
        f=@(y)sqrt(1-y.^2);
        check=@(y)all(y>=-1) & all(y<=1);
    otherwise
        disp(l)
        error('Mode is none of brier, log, burg, mixed, interval, hockey.');
end
if(~check(y)), warning('Range check failed. Proceed at own risk.'); end
if(~all(check(yhat))), warning('Range check for regression curve failed'); end  

Si=mean(f(yhat))-f(ymean);
Valt=mean(f(y))-f(ymean);
gfx=nextarg(l);
nextarg({}); % end of arg parsing
if ~isempty(gfx)
for i=1:k
subplot(ceil(sqrt(k)),round(sqrt(k)),i)
plot(x(:,i),y,'.',xs(:,i),yhat(:,i),'-','LineWidth',3);
xlabel(['x_{' num2str(i) '}']);
ylabel('y');
title(gfx);
end
end
end

function m=nextarg(l)
 persistent argcounter;
 
 m={}; 
 if(isempty(l)), clear('argcounter'); return; end
 if(isempty(argcounter)), argcounter=1; end
 if(argcounter<=length(l)) 
     m=l{argcounter};
     argcounter=argcounter+1;
 end
end

function z=nan2null(z)
 z(isnan(z))=0;
end