function Si=mausi(x,y,M,gfx)
%% MAUSI Compute sensitivity indices using moving average.
%   SI=MAUSI(X,Y) computes the main effects. 
%

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
%if kk~=1, error('Only single output supported.'); end

 [xr,indexx]=sort(x);
%%
 if mod(n,2)==0
% even no. of samples
    shuffle=[1:2:(n-1), n:-2:2];
 else
% odd no. of samples
    shuffle=[1:2:n, (n-1):-2:2];
 end
%% create quasi-periodic input of period 1
% index=indexx(shuffle,:);
index=indexx;

 if kk==1
% sort output
    yr=y(index);
 else
    yr=zeros(n,k*kk);
    for i=1:kk
        z=y(:,i);
        yr(:,(i-1)*k+(1:k))=z(index);
    end
 end
 
%% moving average
% Spannweite 3
% yhat=[1/2*(yr(1,:)+yr(2,:)); ...
%       1/3*(yr(2:end-1,:)+yr(1:end-2,:)+yr(3:end,:)); ...
%       1/2*(yr(end-1,:)+yr(end,:))];
% Spannweite 5
% yhat=[1/2*(yr(1,:)+yr(2,:)); ...
%       1/3*(yr(1,:)+yr(2,:)+yr(3,:)); ...
%       1/5*(yr(1:end-4,:)+yr(2:end-3,:)+yr(3:end-2,:)+yr(4:end-1,:)+yr(5:end,:)); ...
%       1/3*(yr(end-2,:)+yr(end-1,:)+yr(end,:)); ...
%       1/2*(yr(end-1,:)+yr(end,:))];
%% Spannweite 2M+1
if(nargin<3) || (isempty(M))
    M=12;
end
%% Cyclic moving average
yhat=yr;
for i=1:M
    yhat = yhat + yr( [ end-i:end, 1:end-i-1],:)+ yr([ (i+1):end, 1:i],:);
end
yhat=yhat/(2*M+1);
%% crappy kernel estimator
%yhat=yr*nchoosek(2*M,M)/4^M;
%for i=1:M
%    yhat = yhat + nchoosek(2*M,(i-1))/4^M*(yr( [ end-i:end, 1:end-i-1],:)+ yr([ (i+1):end, 1:i],:));
%end
%%
if (nargin==4)
%%
 for i=1:k
  subplot(floor(k/2+.5),2,i);
  zz=x(index(:,i),i);
  plot(zz,y(index(:,i)),'.',zz,yhat(:,i),'-','LineWidth',2); 
  xlabel(['x_{',num2str(i),'}']);ylabel('y');
  title(gfx);
 end
%%
end
%%
Ey=mean(y);
Cyhat=yhat-ones(n,1)*mean(yhat);

%% 
%Si0=sum( (yhat-Ey).^2)./sum( (y-Ey).^2)
%Si1=1- sum((yhat-yr).^2)./sum( (y-Ey).^2)
%Si2=sum( Cyhat.*(yr-Ey))./sum( (y-Ey).^2)
% Doksum & Samarov
Si = (sum( Cyhat.*(yr-Ey))).^2 ./ ( sum( Cyhat.^2).* sum( (y-Ey).^2));
% S= var(yhat)./var(y)
end
