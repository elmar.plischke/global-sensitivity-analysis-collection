function [Sij,Sti]=qsusi(x,y,l,gfx)
%% QSUSI Compute sensitivity indices using Correlation ratio methods.
%   [SIJ,STI]=QSUSI(X,Y) computes first and second order effects using a piecewise 
%   constant fit, additionally estimating (approximate) total effects. 
%   On return, SIJ contains the first order effects on the diagonal, and the 
%   second order effects in the upper part. The lower part is occupied by
%   multiple correlation ratios. These values are bias-corrected.
%   STI contains lower and upper estimates of the total effects by summing
%   the appropriate first and second order effects.
%
%   QSUSI(X,Y,L) additionally specifies the partition size L, if L<0 then 
%   the requested number of realisations per subsample is -L. 
%   L defaults to CEIL(SQRT(LENGTH(X))/7). 

% Optimised for large sample sizes using sparse design matrix
% Written by elmar.plischke@tu-clausthal.de

% for i=2:30; 
%  [Sij,Sti]=qsusi(x,y,i,'Test'), diff(Sti), drawnow;
%  pause(.5);
% end
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'); end

%% default partitioning: (0.02n)^1/2 partitions
if(nargin<3) || (isempty(l))
 l=max(ceil(sqrt(n)/7),4); 
 fprintf('QSUSI: partition size %d\n',l); 
end
if(l<0)
    % requested number of realisations per subsample
    l=ceil(n/-l);
 fprintf('QSUSI: partition size %d\n',l); 
end
%%
Ey = mean(y);
Vy = sum( (y-Ey).^2);

%% Rank-transform input for partitioning
[xx,indx]=sort(x);
for i=1:k
 xx(indx(:,i),i)=((1:n)-.5)*l/n;
end
clear indx;

%% 
%D=[];
%D=zeros(n,k*l);
D=spalloc(n,k*l,n*k); % use sparse storage
%% Design matrix for partition j
 for j=1:l
  %D=[D, (xx>=(j-1)) & (xx<j)];
   D(:,(j-1)*k+(1:k))=(xx>=(j-1)) & (xx<j);
 end 
clear x xx;

%% sensitivity indices
Sij = zeros(k,k);
Z= (full (D'*spdiag(y-Ey)*D)).^2 ./ (full(D'*D) + eps); %avoid 0/0
%Z= (full (D'*spdiag(y-Ey)*D)).^2 .* (full(D'*D) );
% faster than D'*diag(y-Ey)*D
%Z= (full (D'*( D.* ((y-Ey)*ones(1,l*k))) )).^2./(full(D'*D) + eps); %avoid 0/0
%whos
for  i=1:k
    indx=(0:l-1)*k;
    for j=i:k
        G=Z( i+indx, j+indx );
        Sij(j,i)=sum(sum(G))/Vy;
        
        if(nargin==4)
            if(i==j)
                subplot (k,k,(i-1)*k+i);
                plot(diag(G)/Vy);
                a=axis; a(1)=1; a(2)=l; axis(a);
            else
                subplot (k,k,(i-1)*k+j);
                image(G,'CDataMapping','scaled');
                set(gca,'XTick',[],'YTick',[]);
                subplot (k,k,(j-1)*k+i);
                %% product of marginals
                P=diag(Z(i+indx,i+indx))*diag(Z(j+indx,j+indx))';
                image(P,'CDataMapping','scaled');
                set(gca,'XTick',[],'YTick',[]);
            end
        end
    end
end
%Sij
%% bias correction
df = l*( eye(k,k)+ l*tril(ones(k,k),-1)) - 1;
Sij=(n*Sij-df)./(n-df);
%% 
for i=1:k
    for j=i+1:k
        Sij(i,j)=Sij(j,i)-Sij(i,i)-Sij(j,j);
    end
end
%% lower and upper bounds
if(nargout>1)
Sti = zeros(2,k);
Zij=triu(Sij);
for i=1:k
    itot=1:k;itot(i)=[];
    Sti(1,i)=sum(Zij(i,:)+Zij(:,i)')-Zij(i,i);   
    Sti(2,i)=1-sum(sum(Zij(itot,itot)));
end
end

return
end

function D=spdiag(x)
[n,k]=size(x);
D=spconvert([ (1:n)'*[1,1], x ]);
end
