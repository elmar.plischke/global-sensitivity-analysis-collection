function [Si,Si_ip]=csnsi(x,y,M,gfx)
% CSNSI correlation ratio computing first order sensitivity indices
%       interpolating cusunoro (cumulative sum of normalized reordered 
%       output) curves
%   SI=CSNSI(X,Y) computes first order indices, 
%   SI=CSNSI(X,Y,M) additionally sets number of intervals,
%               M<0 requests number of realisations per interval
%               a vector M specifies a partition (0...1)
%   SI=CSNSI(X,Y,M,GFX) or SI=CSNSI(X,Y,[],GFX) produces a graphical
%   illustration 
%
% written by elmar.plischke@tu-clausthal.de
%%
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'); end

%% CUSUNORO curves
[xr,indx]=sort(x);
yr=y(indx); % reorder
Ey=mean(y);
Cy=y-Ey;    % center
sy=sqrt(n*(Cy'*Cy)); % normalizer
yplus=[zeros(1,k);cumsum(Ey-yr)/sy]; % cumulative sum
%%
if(nargin<3) || isempty(M)
    M=floor(sqrt(n)); % set number of partitions
end
% requesting nr. realisations

if(isscalar(M))
    if(M>0)
        innerp=ones(k,1)*floor(linspace(1,n+1,M+1));
    else
        innerp=ones(k,1)*(1:-M:(n+1));
    end
else
    [mm,ll]=size(M);
    if(mm==k)
        innerp=M;
    else
        innerp=ones(k,1)*(1+floor(M*n));
    end
end
innerp=[ones(1,k);sort(innerp');ones(1,k)*(n+1)];
%%
if(nargin==4)
%% show graphics
%  yout=zeros(2*M+2,k);

  if(iscell(gfx))
      legname=gfx(1:end-1);
      gfx=gfx{end};
  else
      legname=cell(1,k);
      for i=1:k
        legname{i}=['x_{' num2str(i) '}'];
      end
  end
%
  clear yout
  for i=1:k
   yout(:,i)=yplus(innerp(:,i),i);
  end
%  subplot(1,2,1)
  if(~ishold)
% two-sided, niveau given by n^-.5
    alfa=1-1/(2*sqrt(n));
    bound=-sqrt(2)*erfcinv(2*alfa); % avoid stat toolbox 
    xs=[0:4,linspace(5,n-5,20), n-4:n]; % for round edges
    %xs=0:n;
    ys=sqrt( bound* (1-((2*xs-n)/n).^2));
    fill([xs,fliplr(xs)]/n, [ys,-fliplr(ys)]/sqrt(n),[.8,.8,.8],'EdgeColor',[.8,.8,.8]);hold on;
  end
  h=plot((0:n)/n,yplus);
  l=legend(h,legname); 
  try
      set(l,'Location','southwest');
      set(l,'NumColumns',2);
      set(l,'AutoUpdate','off');
  end
  set(gca,'ColorOrderIndex',1); % reset colors

  plot((innerp-1)/n,yout,'o');
 
  hold off;
 title(gfx);
 xlabel('input contribution');
 ylabel('deviation form mean'); %('cumulative sum of normalized output')
 set(gca,'YGrid','on');
%%
end
%%
Si=zeros(1,k);
if(nargout==2)
    Si_ip=zeros(2,k);
end
for i=1:k
%%
    u=unique(innerp(:,i)); % avoid boundary extrema {1,n+1 } occurring twice
    idu=n./diff(u);

    Si(i)=idu'*(diff(yplus(u,i)).^2);
%    Si(i)=sum(diff(yplus(u,i)).^2);

    if(nargout==2)
        pp=pchip(u,yplus(u,i));  % piecewise cubic hermitean 
        if(nargin==4)
            xx=linspace(0,1,20);
            hold on;plot(xx,ppval(pp,xx*n),'k:');hold off
        end
        [breaks,coefs,num]=unmkpp(pp);
        si=0;
        % integrate the square of the derivative
        for l=1:num
            dp= polyder(coefs(l,:)); % breaks(l) is shifted to 0
            q = polyint(conv(dp,dp));
            si= si + polyval(q,breaks(l+1)-breaks(l)); % -polyval(q,0) (=0)       
        end
        Si_ip(1,i)=n*si;
        %*****
        pp=spline(u,yplus(u,i)); % cubic spline
        [breaks,coefs,num]=unmkpp(pp);
        si=0;
        % integrate the square of the derivative
        for l=1:num
            dp= polyder(coefs(l,:)); % breaks(l) is shifted to 0
            q = polyint(conv(dp,dp));
            si= si + polyval(q,breaks(l+1)-breaks(l)); % -polyval(q,0) (=0)       
        end
        Si_ip(2,i)=n*si;
    end
%%
end
end
