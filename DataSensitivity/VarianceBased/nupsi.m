function [Si,Ti,V,L]=nupsi(x,y)
% NUPSI Updating Nearest Neighbor estimator of sensitivity effects.

[n,k]=size(x);
V=var(y);

Y=y(1).^2*ones(1,2*k);
if(nargout>3)
 L=zeros(n,2*k);
 L(1,:)=Y;
end
for i=2:n
    dx=(x(i,:)-x(1:i-1,:)).^2;
    di=[sum(dx,2)-dx, dx];   % distances to new point
    [~,jj]=min(di,[],1);        % NN for new point
    yi=y(i);
    Y=(1-1/i)*Y+yi/i*(yi-y(jj)');
    if(nargout>3),     L(i,:)=Y; end
end
Ti=Y(1:k);Si=V-Y(k+1:end);
end

% might want to try 
% Vs=(cumsum(y.^2)./(1:n)')-(cumsum(y)./(1:n)').^2
% instead of V

function testnupsi
%% 
ishigami
n=10000;
nvis=10:100:n;
subplot(1,2,1)
R=50;
res=zeros(R,k*2);
for r=1:R
    r
    x=trafo(rand(n,k));
    y=model(x);
    [S,T,V,L]=nupsi(x,y);
    res(r,:)=L(end,:);
    plot(nvis,L(nvis,:)./V); hold on
    set(gca,'ColorOrderIndex',1);if(mod(R,5)==0),drawnow;end
end
legend({'T_1','T_2','T_3','T_4','1-S_1','1-S_2','1-S_3','1-S_4'},'NumColumns',2)
xlabel('Sample size')
hold off
%%
subplot(1,2,2);
res_debias=2*L(end,:)-mean(L(800:n-1,:));
boxplot(res,'plotstyle','compact');hold on
V=13.8445;
analyt=[.3139,.4424,0,0,.5576,.4424,.2437,0]*V; 
plot([.5,1.5;1.5,2.5;2.5,3.5;3.5,4.5;4.5,5.5;5.5,6.5;6.5,7.5;7.5,8.5]', ...
    [analyt([ 5 5 ; 6 6; 7 7; 8 8]);V-analyt([ 1 1 ; 2 2; 3 3; 4 4])]');
plot((1:8)+.33,res_debias,'ko');
title('Absolute sensitivity indices for n=10k');
hold off
%%
end