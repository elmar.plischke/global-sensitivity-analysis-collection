function [Si,STi,XY]=xfast(k,n,model,trafo,gfx)
% XFAST compute sensitivity indices using full frequency decomposition
%        [SI,STI]=XFAST(K,N,MODEL,TRAFO)
%

% written by elmar.plischke@tu-clausthal.de

% frequency selection
for M=6:-1:1
P=2*M+1;
freq=P.^((k-1):-1:0);
if n> 2*sum(freq)*M
    break; end
end
if((P^k-1)>n)
    disp('XFAST: Running low on samples'); 
    Si=zeros(1,k); STi=Si; return
end

fprintf('XFAST: maximum harmonic M=%d, basic frequency P=%d\n',M,P);

% sample construction
u=zeros(n,k);
s=linspace(0,1,n+1)';s(1)=[]; % avoid double data point
phi=rand(1,k)*pi;             % random shift
for i=1:k
    u(:,i)=1/pi*acos(cos(phi(i)+2*pi*freq(i)*s)); %G(s,P^(i-1));
end
% data transformation and model evaluation
x=trafo(u);
y=model(x);
if (nargout==3), XY=[x,y]; end

% first order effects
spectrum=(abs(fft(y))).^2/n;
 V=sum(spectrum(2:n));
Si=zeros(1,k);
for i=1:k
    Si(i)=2*sum(spectrum(1+(1:M)*freq(i)))/V;
end

% total effects
 STi=zeros(1,k);
% using signed p-adic decomposition
maxfreq=((P^k)-1)/2;
index=1:maxfreq;
padic=zeros(k,((P^(k-1))-1)/2);
for i=1:k % while any(index)
    i1=mod(index,P);
    i2=floor(index./P);
    ic=find(i1>M); % carry bits
    i1(ic)=i1(ic)-P;
    i2(ic)=i2(ic)+1;
    padic(i,:)=find(i1==0); % cleared bits
    index=i2;
end
% compute total indices
for i=1:k
    STi(k+1-i)=1-2*sum(spectrum(1+padic(i,:)))/V;
end
% some graphics
if(nargin==5)
%% gfx test
    group=1:k;l=k;
    figure(gcf)
    subplot(3,1,1);
    plot(x,'.');
    title(gfx)
    xlabel('Index');
    ylabel('Inputs');
    legend(cellstr([char(ones(l,1)*'x_{'),num2str(group'),char(ones(l,1)*'}')]));
    subplot(3,1,2);
    plot(y,'k.');
    title(gfx);
    xlabel('Index');
    ylabel('Output');
    subplot(3,1,3);
    specshow(spectrum,freq,M);
    title('Power Spectrum of Output');
    xlabel('Frequency');
    ylabel('Rel. Var. Contrib.');  
%%
end



