function Si=kltsi(x,y,M,gfx)
%KLTSI Sensitivities using Karhunen Lo�ve Transformation
% SI=KLTSI(X,Y) computes first order effects
% SI=KLTSI(X,Y,M) uses subspace dimension M  (default 2)
% SI=KLTSI(X,Y,M,'GFX') additionally produces a plot

% written by elmar.plischke@tu-clausthal.de
if(nargin<3) || isempty(M)
    M=2;
end
if(nargin<4)
    gfx='';
end
SPURIOUS=40; % no. of sign changes
firsttime=1; % output waring message only once

[n,k]=size(x);
%%
[xs,xi]=sort(x);
Ey=mean(y);
Vy=var(y);
z=y-Ey; % center
zs=z(xi); % and reorder
%%
Si=zeros(1,k);
for j=1:k
%
 if(exist('svds'))
  if(~exist('OCTAVE_VERSION'))
 % MATLAB > R2014a needed, octave has no support for functional form
   [~,s,v]=svds(@(x,tf)Afun(x,tf,zs(:,j)),[2*n-1,n],...
         max(M,6),'largest',struct('tol',5e-3));
   d=diag(s)/n;
  else
   C=zeros(2*n-1,n);
   for i=1:n
    C((i-1)+(1:n),i)=zs(:,j);
   end
   [~,s,v]=svds(C,max(M,6),'L',struct('tol',5e-3));
   d=diag(s).^2/n;
  end
 else
  % same as above, but now with full svd
  C=zeros(2*n-1,n);
  for i=1:n
   C((i-1)+(1:n),i)=zs(:,j);
  end
  [~,s,v]=svd(C,'econ');
  d=diag(s).^2/n;
 end
 % reconstruct the signal,
 for m=M:-1:1
  alfa=v(:,1:m)'*zs(:,j);
  zz=v(:,1:m)*alfa;
 %% check for sign changes
  if( sum( (zz(2:end).*zz(1:end-1)) < 0) > SPURIOUS)
   if(firsttime), disp('Spurious contribution detected.'); firsttime=0; end
   if(m==1) zz=0*zz; end
  else break; % done
  end
 end
%%
 if(~isempty(gfx))
  subplot(ceil(k/2),2,j)
  plot(xs(:,j),Ey+zs(:,j),'.',xs(:,j),Ey+zz,'-','MarkerSize',2',...
    'LineWidth',2);
  ylabel('y');
  xlabel([ 'x_{' num2str(j) '}']);
  title(gfx);
 end
%%
 Si(1,j)=var(zz)/Vy;
%Si(2,j)=sum(d(1:M))/Vy;
%%
end
end
% helper function for svds (functional form)
function z=Afun(x,flags,y)
 switch(lower(flags))
    case 'notransp',
        z=conv(y,x);
    otherwise,
        z=conv(x,y(end:-1:1),'valid'); % overlapping part of convolution
 end
end
