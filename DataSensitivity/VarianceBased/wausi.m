function [Si,V]=wausi(x,y,M,gfx,waveletname)
%% WAUSI Calculation of sensitivity indices from given data.
%     SI = WAUSI(X,Y) returns the sensitivity indices for input arguments X 
%     and output arguments Y (per line) based upon a truncated wavelet
%	  decomposition.
%     SI = WAUSI(X,Y, M, GFX, WNAME) allows to specify the depth M of the
%     wavelet decomposition, a title GFX for optional graphics output 
%     (of first output), and a wavelet filter name. 
%     Names starting with 'wave-' use the wavelet WNAME (see WFILTERS). 
%     Names starting with 'lift-' are handled by a lifting scheme 
%     (see WAVENAMES). 
%     Nmes without prefix assume that functions prefixed with dwt and idwt
%     are available (power-of-2 sample size assumption).
%
%     WNAME defaults to 'haar'.
%
%     Check also dwtmode('per')
%%
%     Written by Elmar Plischke, elmar.plischke@tu-clausthal.de

 [n,k]=size(x);
 [nn,kk]=size(y);
 if nn~=n, error('Input/output sizes mismatch!'), end
%% coefficient selection
if (nargin==2) || (isempty(M))
 M=min(floor(log2(n))-5,3);
 fprintf('WAUSI: Using 2^%d coefficients.\n',M);
end

if(M<0)
    M=-M;
    dontshuffle=0;
else
    dontshuffle=1;
end
%% data processing
 [xr,index]=sort(x);
if(~dontshuffle)
 if mod(n,2)==0
% even no. of samples
    shuffle=[1:2:(n-1), n:-2:2];
 else
% odd no. of samples
    shuffle=[1:2:n, (n-1):-2:2];
 end
% create quasi-periodic input of period 1
 indexqper=index(shuffle,:);
else
    indexqper=index;
end

 if kk==1
% sort output
    yr=y(indexqper);
 else
    yr=zeros(n,k*kk);
    for i=1:kk
        z=y(:,i);
        yr(:,(i-1)*k+(1:k))=z(indexqper);
    end
 end
%% default: haar wavelet
if(nargin<5)
    waveletname = 'haar';
end
%dwt  = inline(['dwt'  waveletname '(x)'],'x');
%idwt = inline(['idwt' waveletname '(x)'],'x');
%% test for lifting scheme
if length(waveletname)>=6 && strcmpi(waveletname(1:5),'lift-')
    waveletname=waveletname(6:end);
    wdec=@liftdec;
    wrec=@liftrec;
elseif length(waveletname)>=6 && strcmpi(waveletname(1:5),'wave-')
    waveletname=waveletname(6:end);
    wdec=@wavedec;
    wrec=@waverec;
else
    if(floor(log2(n))~=log2(n)), error('No power-of-2 sample size'); end
    wdec=@builtinwdec;
    wrec=@builtinwrec;
end
%%
yhat=zeros(n,k*kk);
df=zeros(1,k*kk);

for i=1:(k*kk)
% wavelet decomposition 
[coeff , lens]=wdec(yr(:,i),M,waveletname);
% truncate 
%coeff=[ allcoeff(1:lens(1),:); zeros(length(allcoeff)-lens(1),1) ];
coeff(lens(1)+1:end)=0;
df(i)=lens(1)-1; % respect mean
% reconstruct
yhat(:,i)=wrec(coeff,lens, waveletname);
end
%Ehaty=mean(yhat);
%% compute sensitivity indices
EY=mean(y);
nV=sum( (y-EY).^2);
V=nV/n;
if(kk~=1)
    EYY=kron(EY,ones(1,k));
   %%
   %vec=@(x)x(:);
   %plot(1:(k*kk),sum( (yhat -EYY).*(yr-EYY)),1:(k*kk),kron(nV,ones(1,k)))
   %%
   Si=  reshape(sum( (yhat -EYY).*(yr-EYY)),k,kk)'./ nV';
else
    Si=  sum( (yhat -EY).*(yr-EY))./ nV;
end
    
%% 
if (nargin>=4) && (~isempty(gfx))
 if(0)
 Z=abs(allcoeff); 
 Z=64*(Z/(max(max(Z))));
 subplot(1,1,1);
 d=zeros(1,n);d(1:(1+2^M))=1;
 image(0:(k+1), 1:n,[d'*64,Z,d'*64]);colorbar;
 title(gfx);
 xlabel('Permuted output');ylabel('Wavelets coefficients');
 %h=text(k+.75,n/2+10,'Selected Coefficients','Rotation',90);
 %bw=(63:-1:0)'*[1,1,1]/64;
 %colormap(bw)
 colormap(hot)
 figure(gcf);
'Press any key', pause
 end
for i=1:k
  subplot(floor(k/2+.5),2,i);
  plot(x(indexqper(:,i),i),y(indexqper(:,i)),'k.', ... 
       x(indexqper(:,i),i),yhat(:,i),'r-','LineWidth',2);
  xlabel(['x_{',num2str(i),'}']);ylabel('y');
%  plot(1:n,y(indexqper(:,i)),'.', ... 
%       1:n,yhat(:,i),'-','LineWidth',2);
%   if(dontshuffle)
%     xlabel(['Ranks x_{',num2str(i),'}']);
%   else
%      xlabel(['Shuffled ranks x_{',num2str(i),'}']);
%   end
%  ylabel('y');
    title(gfx);
 end
end
%%%
 if kk>1, Si=reshape(Si',k,kk)'; end
 return
end

function [coef,len]=builtinwdec(x,l,name)
 coef=feval(['dwt' name ],x);
 len=min(2^l,length(x));
end

function x=builtinwrec(coef,l,name)
 x=feval(['idwt' name ],coef);
end

%%
function debug_multiout
%%
nn=4096;
xx=randn(nn,3);
yy=xx*[1,0;1 2;0 2];

clear Si
for j=1:2
    Si(j,:)=wausi(xx,yy(:,j),3);
end
MSi=wausi(xx,yy,3);

Si
MSi
%%
end
