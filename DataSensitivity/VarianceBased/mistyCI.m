function [T,CI]=mistyCI(x,y)
% MISTYCI Total effects via minimal spanning trees, confidence intervals
[n,k]=size(x);
y=(y-mean(y))/std(y); % standardize for numerical stability
% means and moments vectors
EY4=mean(y.^4);
EY3=mean(y.^3);
EY2=mean(y.^2);
EY=mean(y);
T=zeros(3,k);
CI=zeros(1,k);

alfa=0.05;
R=10;D=3;
if(R~=0)
   EFjack=zeros(1,R);
   EF2jack=zeros(1,R);
   EY2Fjack=zeros(1,R);
   EYFjack=zeros(1,R);
end
psi=@(a,b,c)(a-b)./(a-c^2);
nablapsi=@(a,b,c)[(b-c.^2)/(a-c^2),-1,2*c*(a-b)/(a-c^2)]'./(a-c^2);
for i=1:k
    noti=1:k;noti(i)=[];
    [p,d]=mst_prim_alt(x(:,noti)); 
    p(1)=1; % fake parent for root node
  % means and moments vectors (conditional)
    EF=mean(y.*y(p))
    EF2=mean((y.*y(p)).^2);
    EY2F=mean(y.^3.*y(p));
    EYF=mean(y.^2.*y(p));
  % Jackknife for debiasing ?
  if R~=0
    for r=1:R
     ii=randperm(n,round((D-1)*n/D));
     q=mst_prim_alt(x(ii,noti)); q(1)=1;
     EFjack(r)=mean(y(ii).*y(ii(q)));
     EF2jack(r)=mean((y(ii).*y(p(ii))).^2);
     EY2Fjack(r)=mean(y(ii).^3.*y(p(ii)));
     EYFjack(r)=mean(y(ii).^2.*y(p(ii)));
    end
    EFjack
    EFj=(sqrt(D)*EF-sqrt(D-1)*mean(EFjack))/(sqrt(D)-sqrt(D-1));
    EF2j=(sqrt(D)*EF2-sqrt(D-1)*mean(EF2jack))/(sqrt(D)-sqrt(D-1));
    EY2Fj=(sqrt(D)*EY2F-sqrt(D-1)*mean(EY2Fjack))/(sqrt(D)-sqrt(D-1));
    EYFj=(sqrt(D)*EYF-sqrt(D-1)*mean(EYFjack))/(sqrt(D)-sqrt(D-1));
    T(2,i)=psi(EY2,mean(EFjack),EY);
    T(3,i)=psi(EY2,EFj,EY);
  end
  % Total effect 
    T(1,i)=psi(EY2,EF,EY);

  % Confidence: Covariance matrix
    G=[ EY4-EY2^2, EY2F-EY2*EF, EY3-EY*EY2;...
           EY2F-EY2*EF, EF2-EF^2, EYF-EY*EF;...
          EY3-EY*EY2, EYF-EY*EF, EY2-EY^2];
  % Delta-Method: derivative  
    g=nablapsi(EY2,EF,EY);
%    Psi=[g'*G*g, ((1-T(i))*( (1-T(i))*(EY4-EY2.^2)+4*T(i)*EY*(EY3-EY2*EY)...
%        -2*(EY2F-EY2*EF))+ EF2-EF^2+4*T(i)*EY*(T(i)*EY*(EY2-EY^2)-...
%        (EYF-EY*EF)))/(EY2-EY.^2).^2]
  % Two-sided 5%
    CI(1,i)=g'*G*g/sqrt(n)*tinv(1-alfa/2,n-1);
    % Confidence: Jackknifed version
       G=[ EY4-EY2^2, EY2Fj-EY2*EFj, EY3-EY*EY2;...
           EY2Fj-EY2*EFj, EF2j-EFj^2, EYFj-EY*EFj;...
          EY3-EY*EY2, EYFj-EY*EFj, EY2-EY^2];
    g=nablapsi(EY2,EFj,EY);
    CI(2,i)=g'*G*g/sqrt(n)*tinv(1-alfa/2,n-1);
end
end

function mistytest
%% minimal spanning tree vs. nearest neighbor
ishigami
n=4000;
R=50;
NSi=zeros(R,k);
NTi=zeros(R,k);
Ntim=zeros(R,1);
MEi=zeros(R,k);
MTi=zeros(R,k);
Mtim=zeros(R,1);
for r=1:R
    x=trafo(rand(n,k));y=model(x);
    tic;[S,T]=ninasi(x,y);NSi(r,:)=S;NTi(r,:)=T;Ntim(r)=toc;
    tic;[T,E]=misty(x,y);MTi(r,:)=T;MEi(r,:)=E;Mtim(r)=toc;
end
%%
boxplot([NTi,MTi])
%%

end   