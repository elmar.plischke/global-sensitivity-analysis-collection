function Si=easi(x,y,M,varargin)
%% EASI Calculation of sensitivity indices from given data
%
%     SI = EASI(X,Y) returns the sensitivity indices for input arguments X 
%     and output arguments Y (per line).
%     SI = COSI(X,Y,M) specifies the max. harmonic cutoff.
%     SI = COSI(X,Y,'Gfx Title') provides a figure.

%     Reference: 
%      E. Plischke, "An Effective Algorithm for Computing 
%       Global Sensitivity Indices (EASI)",
%       Reliability Engineering & Systems Safety, 95(4), 354-360, 2010
%%
%     Written by Elmar Plischke, elmar.plischke@tu-clausthal.de
 if (nargin==2) || (isempty(M))
  M=6;     % max. higher harmonic ( sum (-1)^k sin((2k+1)x)/ (2k+1)^2 )
 end
 [n,k]=size(x);
 [nn,kk]=size(y);
 if nn~=n, error('Input/output sizes mismatch!'), end
% default [graphics] options
opts=struct('GfxTitle','',...
            'GfxCols',min(k,2),...
            'Labels','',...
            'OutputLabel','y',...
            'ShowScatter',true,...
			'Unscaled',false,...
            'Debias',false,...
			'PlotOpts',{ {'-','LineWidth',2}},...
            'ScatterData',[]); 
%
if(nargin>=4) && ~isempty(varargin)
	if isstruct(varargin{1})
        opts_in=varargin{1};
    elseif length(varargin)==1
        opts_in=struct('GfxTitle',varargin{1});
      else
        opts_in=struct(varargin{:});
    end
    members=fieldnames(opts);
    for i=1:length(members)
            o=members{i};
            if isfield(opts_in,o)
                opts.(o)=opts_in.(o);
            end
    end
end 
%% historical undocumented behavior
 if(M<0), M=-M; opts.Unscaled=true; end
%%
 [xr,index]=sort(x);
%%
 const_in=xr(1,:)==xr(end,:);
 if(any(const_in)), disp('Constant input(s) detected.'); end
%%
 if mod(n,2)==0
% even no. of samples
    shuffle=[1:2:(n-1), n:-2:2];
 else
% odd no. of samples
    shuffle=[1:2:n, (n-1):-2:2];
 end
%% create quasi-periodic input of period 1
 indexqper=index(shuffle,:);

 if kk==1
% sort output
    yr=y(indexqper);
 else
    yr=zeros(n,k*kk);
    for i=1:kk
        z=y(:,i);
        yr(:,(i-1)*k+(1:k))=z(indexqper);
    end
 end
 %% look for resonances in the output
 F=fft(yr); % Save for use in gfx output
 spectrum=(abs(F)).^2/n;
 Vi=2*sum(spectrum(2:M+1,:));  
 V=sum(spectrum(2:end,:));
 if(opts.Unscaled)
  Si=Vi/(n-1);
 else
  Si=Vi./V;
  if(opts.Debias)
  % Kelley debiasing
   Si=1/(n-2*M)*(n*Si-2*M);
  end
 end
%%
 if kk>1
     Si=reshape(Si',k,kk)'; 
     Si(const_in,:)=0;
 else
     Si(const_in)=0;
 end
%%

%if(nargin==3), specshow(spectrum,[1],M); end
if ~isempty(opts.GfxTitle)
     G=zeros(n,k);
     % copy important coefficients
     G( [1+(0:M), n+1-(1:M)],:)=F( [1+(0:M), n+1-(1:M)], :); 
     z=real(ifft(G));
     for i=1:k
        if(k>1),subplot(ceil(k/opts.GfxCols),opts.GfxCols,i);;end
        if(opts.ShowScatter)
          plot(x(indexqper(:, i),i),yr(:,i),'.',...
            x(indexqper(:, i),i),z(:,i),opts.PlotOpts{:});
        else
            plot(x(indexqper(:, i),i),z(:,i),opts.PlotOpts{:});
        end
        if(~isempty(opts.Labels))
          xlabel(opts.Labels{i});
        else
          xlabel(['x_{',num2str(i),'}']);
        end
        ylabel(opts.OutputLabel)
        title(opts.GfxTitle);
     end
end
return
end