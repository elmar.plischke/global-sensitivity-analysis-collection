function [Sij,Si]=zissi(x,y,group,m,gfx)
%% ZISSI Computes sensitivity indices (first and higher effects) 
%       using the ping-pong curve
%   SIJ=ZISSI(X,Y,GROUP) computes the combined effects of the indices
%                        GROUP (using Cosine Transformation). 
%   SIJ=ZISSI(X,Y,GROUP,MODULI) additionally specifies the moduli for 
%                the construction of the ping-pong curve.
%   SIJ=ZISSI(X,Y,GROUP,[],'TITLE') creates a graphical output.
%   [SIJ,SI]=ZISSI(X,Y,GROUP) also returns the main effects for GROUP.

% Written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
[nn,kk]=size(y);
if nn~=n, error('Input/output sizes mismatch!'), end
if kk~=1, error('Only single output supported.'),end
if any((group<1) | (group>k)), error('Index out of range.'), end

l=length(group);
scale=1/6; % threshold for unimportant contributions
H=5; % max.harmonics
%% initialise "frequencies"
if(nargin<4) || isempty(m)
%%    
    m_defaults=[13 15 16 17 19];
    m=m_defaults(1:l);
%%
end
%% extended Euclidean algorithm: results could be precomputed/reused
 ms=prod(m);
 if(H*sum(m)>n), fprintf('Running low on samples.');end
 M=ms./m;
 [g,u,v]=gcd(m,M);
 if(any(g~=1)), error('Commensurate moduli'); end
%% rank-transform and scale the input
r=zeros(n,l);
rr=zeros(n,l); % test for sub-cube information
[xr,indxx] = sort(x(:,group));
for i=1:l
     rr(indxx(:,i),i)= (1:n)*m(i)/(n+1);
     r(indxx(:,i),i) = floor((1:n)*m(i)/(n+1));
end
%% make even
 idx=mod(r,2)==1;
 r(idx)=-r(idx)-1;
 rr(idx)=-rr(idx);
 zz=mean(rr-r,2); % remainder [0,1]
 
%% chinese remainder
 z= mod( r*(M.*v)', 2*ms)+zz; % plus sub-cube info
 idx=find(z>ms);     % handle large numbers
 z(idx)=2*ms-z(idx); % by flipping back
 %%
[~,indx]=sort(z);
F=dct(y(indx)); % save for regression curve
spectrum=abs(F.^2);
spectrum=spectrum/sum(spectrum(2:end));
%idx=find(spectrum(2:end/2)>=scale/sqrt(n));
scale/sqrt(n)
idx=find(spectrum(2:(H*sum(m)))>=scale/sqrt(n));
% bias correction
df=0;%df=length(idx);
Sij=(n*sum(spectrum(1+idx))-df)/(n-df); % sum of first and higher order effects
% First order effects
Si=zeros(1,l);
for i=1:l
    Si(i)=sum(spectrum(1+(1:H)*M(i)));
end
%% some graphics
if(nargin==5)
%%    
    subplot(3,1,1);
    plot(1:n,x(indx,group),'.'); %,'MarkerSize',3);
    title([gfx ' - Inputs (sorted)']);xlabel('Index');
    legend(cellstr([char(ones(l,1)*'x_{'),num2str(group'),char(ones(l,1)*'}')]));
    a=axis;a(2)=n;axis(a);
    subplot(3,1,2);
    plot(1:n,y(indx),'k.');
    title([gfx ' - Output (reordered)']);xlabel('Index');
    hold on;
    % regression curve
    G=zeros(n,1);freqsel=[1,1+idx',(n+1)-idx'];
    G(freqsel)=F(freqsel);
    plot(1:n,idct(G),'r');
    hold off;
    a=axis;a(2)=n;axis(a);
    subplot(3,1,3);
    specshow(spectrum,M,H,1); %m
    title([gfx ' - Power Spectrum of Output']);
    xlabel('Frequency');ylabel('Variance Contribution');
%%
end
%%
end
%%
function test
%%
ishigami
n=10000;
x=trafo(rand(n,k));
y=model(x);

for i=0:2:16
    [Sij,Si]=zissi(x,y,[1,3],i+[1 11],'Bla')
    pause(2);
    [Sij,Si]=zissi(x,y,[1,3],i+[11 1],'Bla')
    pause(2);
end
%% compare with fast design
xx=trafo(acos(cos(pi*((8+[1 121 11 101]).*(1:n)')/n))/pi);
yy=model(xx);

coef=dct(yy).^2;coef=coef/sum(coef(2:end));
specshow(coef,8+[1 11],5,1)
%%
end
