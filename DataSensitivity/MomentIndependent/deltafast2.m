function [d,s,p,h,bla]=deltafast2(x,y,M,gfx,varargin)
% DELTAFAST a quick method of computing delta moment independent measure
% DELTA=DELTAFAST(X,Y)
% [DELTA,THETA,LAMBDA]=DELTAFAST(X,Y) Kullback-Leibler/Shannon
% Information/L2 pdf density
%%
[n,k]=size(x);
%% Option Parsing
% defaults
Opts=struct('Kernel','epanechnikov','Cutoff',.7,'FringeFactor',.04,...
    'Stretch','none');
if~isempty(varargin)
    if isa(varargin{1},'struct')
        cmdOpts=varargin{1};
    else
        cmdOpts=struct(varargin{:});
    end
    members=fieldnames(Opts);
    for i=1:length(members)
     o=members{i};
     if isfield(cmdOpts,o), Opts.(o)=cmdOpts.(o);end
    end
end
%% Kernel selection
switch lower(Opts.Kernel)
% Box kernel
    case 'box', Kernel=@(x)(1-abs(x/sqrt(3))>0)/(2*sqrt(3));
% Epanechnikov with variance 1
    otherwise,  Kernel=@(x)3/(4*sqrt(5))*max(1-(x.^2/5),0);
end

% Number of partitions
if(nargin<3)||isempty(M), M=24; end
% Numerical noise cutoff (simple Kolmogorov-Smirnov)
Cutoff = Opts.Cutoff; % 0.7; % 1.31; % 1.5; % 0..2 % Missing Factor 2 ???

%% output stats

miny=min(y);
maxy=max(y);
fringe=Opts.FringeFactor; % .04;
L=miny-fringe*(maxy-miny);
U=maxy+fringe*(maxy-miny);

%% transform to unbounded support
switch lower(Opts.Stretch)
    case 'log'
% log trafo
Stretch=@(y,l,u)log(y-l)-log(u-y);
Squeeze=@(z,l,u)(exp(z)*u+l)./(exp(z)+1);
ty=Stretch(y,L,U); 
    case 'atanh'
% atanh trafo
Stretch=@(y,l,u)atanh((2*y-(l+u))/(u-l));
Squeeze=@(z,l,u)(tanh(z)*(u-l)+(l+u))/2;
ty=Stretch(y,L,U); 
    case 'probit'
% probit trafo
Stretch=@(y,l,u)norminv((y-l)/(u-l));
Squeeze=@(z,l,u)l+normcdf(z)*(u-l);
ty=Stretch(y,L,U); 
    case 'gauss'
% Transform to Gaussian
[ty,iy]=sort(y);yr(iy)=1:n;
ty(iy)=-sqrt(2)*erfinv(1-(2*(1:n)-1)'/n);        
Stretch=@(z,l,u)ty;
Squeeze=@(z,l,r)z;
L=-5;U=5;
    otherwise
        % no trafo
Stretch=@(y,l,u)y;
Squeeze=@(z,l,u)z;
end
ty=Stretch(y,L,U); 
% work with transformed data
medy=median(ty);
mady=median(abs(medy-ty)); % iqr via mad
% bandwidth estimate (rule of thumb)
stdy=min(std(ty),mady/0.675);
h=1.2*stdy*((4/(3*n))^(1/5)); % 1.2 compensates subset selection
%h=.01;
% construct interpolation points
z1=linspace(min(ty)-2*h,medy-mady, 25);
z2=linspace(medy-mady,medy+mady,52);
z3=linspace(medy+mady,max(ty)+2*h,25);
%z=[z1,z2(2:end-1),z3];
z=linspace(L,U,110);
l=length(z);
% back-trafo interpolation points for gfx
tz=Squeeze(z,L,U);
bla.y=tz;
%% kernel density matrix
%W=Kernel( (repmat(z,n,1)-repmat(ty,1,l))/h)/h;
W=Kernel( bsxfun(@minus,z,ty)/h)/h;
bla.W=W;
%% unconditional density 
densy=mean(W);

%% conditional densities for partitioned data
if(nargin>=4) & ~isempty(gfx),cols=jet(M);plotCols=ceil(sqrt(k));clf;end
Sm=zeros(k,M);
Tm=zeros(k,M);
Lm=zeros(k,M);
Hm=zeros(k,M);
nm=zeros(k,M);

%% keep only W from the partition
[xr,indxx]=sort(x);
for i=1:k
   xr(indxx(:,i),i)=1:n; % ranks (no ties)
end
%%
for j=1:M
   indx= ((j-1)*n/M <xr) & (xr <= j*n/M);
   nm(:,j)=sum(indx); % no ties: always same nr. of realizations
   for i=1:k
   % conditional density
   densc=mean(W(indx(:,i),:));
   bla.D{i,j}=densc;
   % L1 separation of densities (using Scheffe Thm)
   Sm(i,j)=trapz(z,max(densy-densc,0)); % all entries <1
   % Kullback Leibler
   tt=densc.*(log(densc)-log(densy));tt(densc==0)=0;
   Tm(i,j)=trapz(z,tt);
   %tt=(log(densc)-log(densy));tt(densc==0)=0;
   %Tm(i,j)=mean(tt);
   
   % Hellinger (squared)
   Hm(i,j)=trapz(z,(sqrt(densy)-sqrt(densc)).^2)/2;
   % Power score - L2 pdf density
   Lm(i,j)=trapz(z,(densy-densc).^2);

   if(nargin>=4) && ~isempty(gfx)
     subplot(ceil(k/plotCols)+1,plotCols,i);
     if(j==1)||(j==M),plot(tz,densy,'k','LineWidth',3);hold on;end
     plot(tz,densc,'Color',cols(j,:));
   end
   end
% Clear noise
Sm(Sm<Cutoff.*sqrt(1/n+1./nm))=0;
if(nargin>=4) & ~isempty(gfx) 
 subplot(ceil(k/plotCols)+1,1,ceil(k/plotCols)+1);
 %plot(1:M,Sm.*nm/n)
 plot(cumsum(nm(1,:))/n-1/(2*M),Sm);
 hold on
 plot(cumsum(nm(1,:))/n-1/(2*M),Tm,':');
 plot(cumsum(nm(1,:))/n-1/(2*M),2*sqrt(Lm),'--');
 hold off
 a=axis;a(1)=0;a(2)=1;axis(a);
end
 bla.nm=nm;
 bla.Sm=Sm;
 bla.Tm=Tm;
 bla.Lm=Lm;
 bla.Hm=Hm;
 d=sum(Sm.*nm,2)'/n;
 s=sum(Tm.*nm,2)'/n;
 p=sum(Lm.*nm,2)'/n;
 h=sum(Hm.*nm,2)'/n;
end
