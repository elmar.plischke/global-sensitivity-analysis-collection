function CMi=flavoursi(x,y,MP,gfx)
% FLAVOURSI The flavour method for computing CvM sensitivity
% G=FLAVOURSI(X,Y,MP,GFX) computes the Gini mean difference / Cramer von
%                         Mises sensitivities of Y given X. The optionla
%                         parameter MP controls the number of quantiles
%                         MP(1) and the max. harmonic MP(2) for the COSI first
%                         order sensitivity indices. The presence on GFX
%                         enables a graphical output.
%
% See also: GINIMIM, COPULASI, MMD2SI
%
% inspired by Gamboa, Klein, Lagnoux

% written by: elmar.plischke@tu-clausthal.de
[n,k]=size(x);
ys=sort(y);
%%
use_original_scale=true; % as requested by Vinzenz Brendler

if((nargin<=2)||isempty(MP))
P=40;
M=10;
else
    if(length(MP)>1)
        P=MP(2);
    else
        P=ceil(n/20);
    end
M=MP(1);
end
yy=[];
ps=linspace(1/2/P,1-1/2/P,P);
for p=ps % quantiles
    yy=[yy,y<ys(ceil(n*p))];
%    yy=[yy,y.*(y<ys(ceil(n*p/P)))];
end
try
 [ViT,V]=cosi(x,1.0*yy, -M); % ,'unscaled',true); % non-scaled, multi-output
catch
 % someone stole signal toolbox licenses ...
 [ViT,V]=easi(x,1.0*yy,-M); 
end
CMi=6*mean(ViT);       % normalization
%%
if(nargin>3)
 if(~use_original_scale)
    plot(ps,6*ViT);
    xlabel('Output Quantile');
 else
     % center the bins
   % plot(ys(ceil(n/(P+1)*(1:P))),6*ViT);
    plot(ys(ceil(n/(2*P)*(1:2:2*P-1))),6*ViT);
    xlabel('Output');
 end
 ylabel('Reliability Variance');
 title([ gfx ': Flavour Method for Gini/CvM']);
 legend(strcat('x_{',num2str((1:k)'),'}'))
end
%%
end

function test
%%
ishigami
n=8192;
x=trafo(rand(n,k));
y=model(x);
subplot(1,3,1);
flavoursi(x,y,[],'gfx')
subplot(1,3,2);
[g,~,~,~,~,gs]=ginimim(x,y);
g(2,:)
plot(linspace(0,1,size(gs,1)),gs*6)
xlabel('input quantile');ylabel('L1 cdf separation')
subplot(1,3,3);
q=.025:.05:.975;Q=quantsi(x,y,q);
mean(Q)
plot(q,Q)
xlabel('output quantile');ylabel('deviation of partial expectations')
%%
end
