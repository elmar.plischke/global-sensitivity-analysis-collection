function [D,D2]=deltawei(x,y,r,gfx)
% DELTAWEI Delta moment independent measure using Kernel Density MC
% D=DELTAWEI(X,Y) returns the Delta Sensitivity where X and Y are the
%                 input matrix and the output vector.
% D(1,:) using joint input MC
% D(2,:) using marginal input MC (randomized)
% D(3,:) using input range (uniform) MC
% 
% References:  Wei Lu Yuan(2013) 
%             Derennes Morio Simatos (2017)
% needs Botev's diffusion-based kde and kde2d

% written by elmar.plischke@tu-clausthal.de
%%
[n,k]=size(x);

if(nargin<3)
 r=1;
 GfxTitle='';
elseif (nargin<4)
 GfxTitle='';
else
 GfxTitle=gfx;
end
method='linear'; % 'nearest' 'linear' 'spline' 'cubic'
numplots=ceil(sqrt(k));
D=zeros(3,k);
D2=zeros(r,k);

[~,pdfy,ygrid]=kde(y);
fy=interp1(ygrid,pdfy,y,['*' method]);
%trapz(ygrid,pdfy)
%%
%colormap(flipud(gray(128)))
for i=1:k
    [~,pdfx,xgrid]=kde(x(:,i));             % instead of estimation this
    fx=interp1(xgrid,pdfx,x(:,i),['*' method]);   % should already be known
    [~,pdfxy,xygridx,xygridy]=kde2d([x(:,i),y]);
    pdfxy=max(pdfxy,0); % clear artefacts

    % MC with f_X,Y
    fxy=interp2(xygridx,xygridy,pdfxy,x(:,i),y,method);
    z= abs( fx.*fy./fxy -1);
    D(1,i)=mean( abs( z ))/2;

    % MC with f_X f_Y
    for s=1:r
    ii=randperm(n); % break the joint distribution
    fxy=interp2(xygridx,xygridy,pdfxy,x(:,i),y(ii),method);
    z= abs( fxy./(fx.*fy(ii)) -1);
    D2(s,i)=mean( abs( z ))/2;
    end
    D(2,i)=mean( D2(:,i)); 
  
    % MC with uniform grid 
    deltapdf=abs(pdfxy-bsxfun(@times,...
    interp1(xgrid,pdfx,xygridx(1,:),['*' method],0),...
    interp1(ygrid,pdfy,xygridy(:,1),['*' method],0)));
    
    D(3,i)=(xygridx(1,2)-xygridx(1,1))*(xygridy(2,1)'-xygridy(1,1)')*...
    sum(sum( deltapdf))/2;
    
 %   GfxTitle='xx';
    if(~isempty(GfxTitle))
    subplot(numplots,numplots,i);
%    contourf(xygridx(1,:),xygridy(:,1),pdfxy,...
%       linspace(-eps,max(max(pdfxy)),128),'LineColor','none');
    imagesc(xygridx(1,:),xygridy(:,1),deltapdf);
    set(gca,'YDir','normal');
    xlabel(['x_{' num2str(i) '}']);ylabel('y');
    title(GfxTitle);
    %plot3(x(:,i),y,z,'*');
    end
end
    