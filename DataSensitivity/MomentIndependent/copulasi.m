function l=copulasi(x,y,Ms,gfx)
% COPULASI Sensitivity Indices from Copula representation
%   L=COPULASI(X,Y) returns a list L of sensitivity measures for 
%   input X and output Y
%   L=COPULASI(X,Y,M,GFX) additionally sets partition size M and
%   offers graphical output

% Ref: Elmar Plischke, Emanuele Borgonovo: Copula Theory and Probabilistic 
%      Sensitivity Analysis: Is there a Connection?, European Journal of 
%      Operational Research 277(3), 1046-1059, 2019.
% written by elmar.plischke@tu-clausthal.de
%%
[n,k]=size(x);
uv=empcdf([x,y]);
%%
if(nargin<3)
%%
    Ms=[];
    gfx=0;
%%
elseif nargin<4
    gfx=0;
end
comoshow=1;
% default partition sizes: Ms(1) Bernstein polynomial order,
%   Ms(end): for external estimators
if(isempty(Ms)),Ms=[100,32]; end

%%
m=Ms(1);
u = linspace(1/(m+1),1,m);
%u = linspace(1/(2*m),1-1/(2*m),m);
P=u'*u;                          % Product copula
M=min(ones(m,1)*u,u'*ones(1,m)); % Maurice (comonotone Copula)
% Bernstein copula - out of loop initialisation
N=max(2*m,20); % Grid
t=linspace(1/(2*N),1-1/(2*N),N)';
% Build the Bernstein polynomial map
[F,F0]=bern(t,m); 
%
% [xr,xi]=sort(x);
c=zeros(n,1);
l=struct();
Z=zeros(m,m);
for kk=1:k
%%
  for i=1:m
    for j=1:m
        Z(j,i)=sum( (uv(:,kk)<u(i)) & uv(:,end)<u(j))/n; 
    end
  end
  D=diff(Z,[],2);DD=min(D*D'*(m),1);
  if(gfx)
   subplot(k,(3+comoshow),(3+comoshow)*kk-(2+comoshow));
   contourf(u,u,Z);hold on;
   contour(u,u,P,'w');hold off;
   if(comoshow)
   subplot(k,(3+comoshow),(3+comoshow)*kk-2);
   contourf(u,u,DD);hold on;
   contour(u,u,M,'w');hold off;
   end
  end
  l.block0= 'Estimates from Empirical Copula Grid';
  l.discrepancy(kk)=4*max(max(abs(Z-P)));
  l.schweizerwolf(kk)=12*mean(mean(abs(Z-P)));
  l.spearmanrho(kk)=12*mean(mean(Z-P));
  l.eta1(kk)=3*mean( (2*diff(mean(Z-P))*m).^2);
  % pre-sorting may improve this
  for i=1:n
      c(i)=sum(uv(:,kk)<=uv(i,kk) & uv(:,end)<=uv(i,end))/n;
  end
  if(gfx)
   subplot(k,(3+comoshow),(3+comoshow)*kk);plot3(uv(:,kk),uv(:,end),c,'.','MarkerSize',2);
   xlabel('u');ylabel('v');zlabel('C');grid on;
  end
  l.block1= 'Direct Estimates';
  l.spearmanrho1(kk)=-3+12*mean(uv(:,kk).*uv(:,end));
  l.tau(kk)=4*mean(c)-1;
  % italian tigers
  l.tigerrho(kk)=6*mean(c-uv(:,kk).*uv(:,end));
  l.tigersigma(kk)=6*mean(abs(c-uv(:,kk).*uv(:,end)));
  l.tigeromega(kk)=sqrt(30*mean((c-uv(:,kk).*uv(:,end)).^2));
%% Bernstein Copula density
    l.block2= 'Bernstein Copula Estimates';
    %%
    Z0=[0,zeros(1,m);zeros(m,1),Z];
    L=F*Z0*F'-t*t';  % Bernstein copula minus product copula 
    Q=F0*Z0*F0';     % Bernstein copula density
    R=F*Z0*F0';      % Bernstein conditional copula
    S=R-t*ones(1,N); % cdf difference
    if(gfx)
     subplot(k,(3+comoshow),(3+comoshow)*kk-1);contourf(t,t,min(Q,20),...
         length(colormap),'EdgeColor','none');colorbar
     %subplot(k,4,4*kk-1);plot(t,max(S)-min(S));
    end
    l.disc1(kk)=4*max(max(abs(L)));
    l.sw1(kk)=12*mean(mean(abs(L)));
    l.rho1(kk)=12*mean(mean(L));
    l.eta2(kk)=3*mean( (2*diff(mean(L))*N).^2);
    l.eta3(kk)=3*(sum((2*t'*Q/(N-1)-1).^2)/(N-1));
    l.eta5(kk)=12*mean( mean(R).^2) -3;
    l.cramer(kk)=sqrt(90*mean(mean( (L).^2)));
    l.shannon2(kk)=mean(real(nanmean(Q.*log(Q))))/2;
    l.delta(kk)=mean(mean(abs(Q-1)))/2;
    l.smirnov3(kk)=mean(max(abs(S)));
    l.kuiper3(kk)=mean(max(S)-min(S));
    l.gini2(kk)=6*mean(mean(S.^2));
    % Hellinger and Hellinger Correlation
    % G. Geenens and P. Lafaye de Micheaux. JASA (2020)
    B=mean(mean(sqrt(max(Q,0))));
    l.hellinger(kk)=1-B;
    l.hellingerCor(kk)=2/B^2*sqrt(B^4+sqrt(4-3*B^4)-2);
    % differ from shannon2 by factor 2*log2(exp(1));
    l.renyi2(kk)=1/(2-1)*log2(mean(mean(Q.^2)));
    l.renyi15(kk)=1/(1.5-1)*log2(mean(mean(max(Q,0).^1.5)));
    l.renyi11(kk)=1/(1.1-1)*log2(mean(mean(max(Q,0).^1.1)));
    l.renyi09(kk)=1/(.9-1)*log2(mean(mean(max(Q,0).^.9)));
    l.renyi05(kk)=1/(.5-1)*log2(mean(mean(max(Q,0).^.5)));
    % Kullback-Leibler via copula density MC
    [~,G0]=bern(uv(:,kk),m);
    [~,H0]=bern(uv(:,end),m);
    L=sum((H0*Z0).*G0,2);
%    size(L)
%    plot3(kk*ones(n,1)+uv(:,end),uv(:,kk),L,'.');hold on
%    a=gca;dar=a.DataAspectRatio;dar(1)=1;a.DataAspectRatio=dar;
    l.kullback(kk)=sum( log(L(L>0)) )/n;
    l.delta3(kk)=sum(abs(1/L(L>0)-1))/n; % the Wei et al. method
%% Titelei
if(gfx)
    if(comoshow)
 subplot(k,4,1);title('Empirical and Product Copula');
 subplot(k,4,2);title('C^T*C and Comonotonicity Copula');
 subplot(k,4,3);title('Bernstein Copula Density');
 subplot(k,4,4);title('Empirical Copula');
    else
         subplot(k,3,1);title('Empirical and Product Copula');
 subplot(k,3,2);title('Bernstein Copula Density');
 subplot(k,3,3);title('Empirical Copula');
    end
    
end
end
%%
%if(exist('betaKS2','file'))
%l.block3='External Direct Estimates';
%[l.smirnov,l.delta1,l.kuiper,l.eta,ww]=betaKS2(uv(:,1:end-1),uv(:,end),Ms(end));
%l.waldwolf=2*normcdf(ww)-1;
%[l.delta2,mutualinfo]=deltafast2(uv(:,1:end-1),uv(:,end),Ms(end));
%l.shannon=.5*mutualinfo;
%[gg,l.smirnov2,l.kuiper2,l.eta4,l.eta5]=ginimim(uv(:,1:end-1),uv(:,end),Ms(end));
%l.gini=gg(2,:);
%end

%%
end
function [b,B]=bern(t,n)
%BERN Bernstein base polynomials
 O=zeros(length(t),1);
 b=[1-t,t];
 for k=2:n
    T=repmat(t,1,k+1);
    B=b;
    b=(1-T).*[b,O]+T.*[O,b];
 end
 B=n*([O,B]-[B,O]); % derivative
end
function [ xc ] = empcdf( x )
%EMPCDF Computes the empirical cumulative distribution function
%   C=EMPCDF(X) assigns ranks (including ties)

[n,k]=size(x);
xc=zeros(n,k);
for j=1:k
 [xs,indj]=sort(x(:,j));
 xr=(1:n)';						 % ranks	
 tie_loc=[find(diff(xs)==0);n+2]; % stopper
 tie_next=diff(tie_loc);
 maxt=numel(tie_loc);
 i=1;while(i < maxt)
	run=tie_loc(i);len=1;
	while(tie_next(i)==1), i=i+1;len=len+1; end
	xr( run : run + len) = run+len/2;
	i=i+1;
 end	
 xc(indj,j)=(xr)/n;
end
end

%n=4096;xy=rand(n,2);
%l=copulasi([xy(:,2),xy(:,1)*[1,-1]],xy(:,1),32)