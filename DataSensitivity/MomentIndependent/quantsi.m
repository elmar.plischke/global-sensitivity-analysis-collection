function [Qi,Q,Ri,R]=quantsi(x,y,a,M,gfx)
% QUANTSI Quantile sensitivity using partial expectations.
% QI=QUANTSI(X,Y,ALFA)
% QI=QUANTSI(X,Y,ALFA,M)
% QI=QUANTSI(X,Y,ALFA,M,GFX)
% [Qi,Q]=QUANTSI(...) also returns conditional information 
% [~,~,R,RI]=QUANTSI(...) Klein/Rachdi Normalized Quantile Contrast Sensitivity   
% Ref.: Borgonovo, Jose, Plischke, Hazen: SA and Scoring rules
%       SAMO 2016 Talk Th. Browne

% Written by epl@tu-clausthal.de
if(nargin<4 || isempty(M))
M=32;
end
%%
if(nargin<3)
a=.95;
end
%% handle alfa vector
d=numel(a);
if(d>1)
    Qi=zeros(d,size(x,2));Q={};Ri=zeros(d,size(x,2));R={};
    if(isempty(gfx))
     for i=1:d
        [Qi(i,:),Q{i},Ri(i,:),R{i}]=quantsi(x,y,a(i),M);
     end
    else
     columns=min(d,3);
     for i=1:d
         %does not work with multivar output
         subplot(ceil(d/columns),columns,i);
        [Qi(i,:),Q{i},Ri(i,:),R{i}]=quantsi(x,y,a(i),M,...
            [gfx ' - ' num2str(round(100*a(i))) '%']);
        %hold on;
     end
     %hold off;
    end
    return
end
%%
[n,k]=size(x);
[m,l]=size(y);
%assert(m==n)
%assert(numel(a)==1)

[xs,xi]=sort(x);
[z,zi]=sort(y);
EY=mean(y);

% partial expectation
G=floor(n*a);g=n*a-G;
qtotal=(g*z(G+1,:)+sum(z(1:G,:)))/n;

Ms=round(linspace(0,n,M+1));
for j=1:l
Q=zeros(k,M);
R=zeros(k,M);
for m=1:M
    I=(xi((Ms(m)+1):Ms(m+1),:));
    nc=size(I,1);
    % prepare for interpolating conditional partial expectation
    G=floor(nc*a);g=nc*a-G;
    for i=1:k
        zc=sort(y(I(:,i),j)); % some clever index shuffling removes the sort
                            % but not today
        qcond=(g*zc(G+1)+sum(zc(1:G)))/nc;
        Q(i,m)=qcond-qtotal(j);
		R(i,m)=(qcond-qtotal(j))/(a*EY(j)-qtotal(j)); % Klein Fort Rachdi
    end
end
%
Qi(j,:)=sum(bsxfun(@times,Q,diff(Ms)/n),2)';
Ri(j,:)=sum(bsxfun(@times,R,diff(Ms)/n),2)';
%%
if(nargin>4)
  if(l>1),  subplot(ceil(sqrt(l)),round(sqrt(l)),j);end
  ms=linspace(0,1,M);
  plot(ms,(Q+qtotal(j))/a(j),'-',[0,1],qtotal(j)*[1,1]/a(j),'k--');
 if(l<8),  title(gfx);xlabel('input cdf');ylabel('quantile change'); end
end
end
end
