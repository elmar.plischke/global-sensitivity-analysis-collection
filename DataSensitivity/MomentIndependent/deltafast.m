function d=deltafast(x,y)
% DELTAFAST a quick method of computing delta moment independent measure
% DELTA=DELTAFAST(X,Y)
[n,k]=size(x);
% Epanechnikov with variance 1
Kernel=@(x)3/(4*sqrt(5))*max(1-(x.^2/5),0);
% Box kernel
%Kernel=@(x)(1-abs(x/sqrt(3))>0)/(2*sqrt(3));

% Numerical noise cutoff (simple Kolmogorov-Smirnov)
Cutoff = 0; % 0..1 (scale 0.5 from |.|-> max(.,0))

% output stats
%ys=sort(y);
miny=min(y);
maxy=max(y);
L=miny-.04*(maxy-miny);
U=maxy+.04*(maxy-miny);

% transform to unbounded support
% no trafo
Stretch=@(y,l,u)y;
Squeeze=@(z,l,u)z;
% log trafo
%Stretch=@(y,l,u)log(y-l)-log(u-y);
%Squeeze=@(z,l,u)(exp(z)*u+l)./(exp(z)+1);
% atanh trafo
%Stretch=@(y,l,u)atanh((2*y-(l+u))/(u-l));
%Squeeze=@(z,l,u)(tanh(z)*(u-l)+(l+u))/2;

ty=Stretch(y,L,U);  
% work with transformed data
medy=median(ty);
mady=median(abs(medy-ty)); % iqr via mad
stdy=min(std(ty),mady/0.675);
% bandwidth estimate (optimal for Gaussian data/Gaussian kernel)
h=stdy/(((3*n)/4)^(1/5));
% constuct interpolation points
z1=linspace(min(ty)-2*h,medy-mady, 25);
z2=linspace(medy-mady,medy+mady,52);
z3=linspace(medy+mady,max(ty)+2*h,25);
z=[z1,z2(2:end-1),z3];
l=length(z);
% back-trafo interpolation points for gfx
tz=Squeeze(z,L,U);
% kernel density matrix
W=Kernel( (repmat(z,n,1)-repmat(ty,1,l))/h)/h;
% unconditional density 
densy=mean(W);

% conditional densities for partitioned data
M=32;
m=linspace(0,n,M+1);
cols=jet(M);
Sm=zeros(k,M);
nm=zeros(k,M);
% keep only W from the partition
for i=1:k
 subplot(k+1,1,i);
 plot(tz,densy,'k','LineWidth',3);hold on

 [xr,indxx]=sort(x(:,i));
 xr(indxx)=1:n; % ranks (no ties)
 for j=1:M
   indx= (m(j)<xr) & (xr <= m(j+1));
   % conditional density
   densc=mean(W(indx,:));
   % L1 separation of densities (using Scheffe Thm)
   Sm(i,j)=trapz(z,max(densy-densc,0)); % all entries <.5
   nm(i,j)=sum(indx); % 1/M
   plot(tz,densc,'Color',cols(j,:));
  end
 plot(tz,densy,'k','LineWidth',3);hold off
end
% Clear noise
Sm(Sm<Cutoff.*sqrt(1/n+1./nm))=0;
 
 subplot(k+1,1,k+1);
 %plot(1:M,Sm.*nm/n)
 plot(cumsum(nm(1,:))/n-1/2/n,Sm*2)
 
 d=sum(Sm.*nm,2)'/n;
end
