function l=minna(x,y,MP,trafo,gfx)
  % MINNA Moment-independent Sensitivity using Nearest Neighbor Analyis
  
  % written by elmar.plischke@tu-clausthal.de
  [n,k]=size(x);
  if(nargin<3) || isempty(MP)
   M=32;  % outer points
   P=min(ceil(n/2),120); % conditional 
  else
   M=MP(1);P=MP(end);
  end
  % quadrature points
  x0=trafo(repmat(linspace(0,1,M)',1,k));
  % presort y
  [ys,iy]=sort(y);
  xs=x(iy,:);
  %
 % I=zeros(M,P,k);
  cols=jet(M);clf
  for i=1:M
    [~,ii]=sort(abs(bsxfun(@minus,x0(i,:),xs))); 
%    I(i,:,:)=ii(1:P,:);
%  end
%  for i=1:M
   c=zeros(n,k);
   for j=1:k,c(ii(1:P,j),j)=1/P; end
   dcdf=(1:n)/n-cumsum(c)';
%    for j=1:k
%         subplot(2,2,j)
%         plot(ys,dcdf(j,:),'Color',cols(i,:));
%         if(i==1)
%             hold on;
%             title(['Conditioning on factor ' num2str(j)]);
%             xlabel('Output');
%             ylabel('\Delta CDF');
%         elseif(i==M)
%             hold off
%         end
%    end
   %size(dcdf)
   KSis(i,:)=max(abs(dcdf),[],2)';
   Kuis(i,:)=max(dcdf,[],2)'-min(dcdf,[],2)';
   Gis(i,:)=mean(dcdf.^2,2)';
   CRis(i,:)=mean(ys(ii(1:P,:)));
  end
  
  %hold off
  if(nargin>=5) && ~isempty(gfx)
  subplot(1,4,1);
  plot((1:M)/M,KSis)
  title('Kolmogorov-Smirnov Separation');
  xlabel('Input CDF');
    subplot(1,4,2);
  plot((1:M)/M,Kuis)
  title('Kuiper Separation');
    xlabel('Input CDF');
    subplot(1,4,3);
  plot((1:M)/M,6*Gis);
  title('Gini Separation');
    xlabel('Input CDF');
    subplot(1,4,4);
 plot((1:M)/M,CRis);
  title('Regression Curves');
    xlabel('Input CDF');   
  end
  l=struct('Kolmogorov',mean(KSis),'Kuiper',mean(Kuis),'Gini',6*mean(Gis),...
      'PearsonCR',var(CRis)./var(y));
end
function testminn
  %%
  ishigami
  n=6000;
  x=trafo(rand(n,k));
  y=model(x);
  l=minna(x,y,[],trafo);
  %%
end
