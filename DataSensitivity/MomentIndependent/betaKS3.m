function [l]=betaKS3(x,y,MP,gfx)
% BETAKS Kolmogorov-Smirnov Sensitivity.
%L=BETAKS3(X,Y) returns a list of the following sensitivity indicators
%      B Kolmogorov-Smirnov Distance Beta
%      D Borgonovo Delta
%      K Kuiper Discrepancy Kappa
%      E Pearson Correlation Ratio Eta2
%      W Wald Wolfowitz number of runs (normalized)

%      Data X and Y must contain unique values per column

%      BM, DM, KM, EM, WM Conditional Shift/Separation Measurements

%      
% written by elmar.plischke@tu-clausthal.de
%function [b,d,t,e,w,bm,dm,tm,em,wm]=betaKS3(x,y,MP,gfx
[n,k]=size(x);
if(nargout>1), error('BETAKS: output is a now list'); end
if(size(y,1)~=n), error('BETAKS: size mismatch.'); end
plotdiff=0;
gfxrows=ceil(sqrt(k));

if(nargin<3), MP=[32,3];   end
%% multivariate output
l=size(y,2);
if(l>1)
    b=zeros(l,k);d=zeros(l,k);t=zeros(l,k);e=zeros(l,k);
    for i=1:l
        mims=betaKS3(x,y(:,i),MP);
        b(i,:)=mims.ks;d(i,:)=mims.borgonovo;t(i,:)=mims.kuiper;
        e(i,:)=mims.eta;g(i,:)=mims.gini;
    end
    l=struct('ks',b,'kuiper',t,'borgonovo',d,...
    'eta',e,'gini',g);
    return
end
%%
if(length(MP)>1)
    M=MP(1);P=MP(end);
else
    if(isempty(MP)), M=32; else M=MP(1); end
    P=3;
end
if(M==0)
    % test debiasing: predict value at partition size M=0
    l=struct();
    %[b1,d1,t1,e1]
    l1=betaKS3(x,y,32); 
    %[b2,d2,t2,e2]
    l2=betaKS3(x,y,64); % double partition
    l.borgonovo=2*(l1.borgonovo-.5*l2.bogonovo);
    l.ks=2*(l1.ks-.5*l2.k2);
    l.kuiper=2*(l1.kuiper-.5*l2.kuiper);
    l.eta=2*(l1.eta-.5*l2.eta);
    l.gini=2*(l1.gini-.5*l2.gini);
    return
end
% cumulative distribution: with ties
[ys,indx,rindx]=unique(y);
nn=size(ys,1);
if(nn==n)
 cdf=(1:n)/n;
elseif(nn==1)
    warning('Constant output detected.');
    z=zeros(1,k);
    l=struct('ks',z,'kuiper',z,'borgonovo',z,...
    'eta',z,'gini',z);
    return
else
 rle=zeros(1,nn); 
 % run length encoder
 for i=rindx'; rle(i)=rle(i)+1; end; 
 cdf_=cumsum(rle)/n;
 cdf= cdf_(sort(rindx));
 [ys,indx]=sort(y);  
end
xs=zeros(n,k);
for i=1:k
 xs(:,i)=x(indx,i);
end
try
    VY=nanvar(y,1);
catch
    VY=var(y,1);
end
%%
if(nargin<4), gfx=''; else cols=jet(M);end
m=linspace(0,1,M+1);
bm=zeros(k,M); % Kolmogorov
dm=zeros(k,M); % L1 Borgonovo
tm=zeros(k,M); % Kuiper
wm=zeros(k,M); % Wald Wolfowitz
em=ones(k,M)*VY/n; % Conditional Expectation
gm=zeros(k,M); % test for deltalternative
nm=zeros(k,M);
%% keep only values from the partition
[xr,indxx]=sort(xs);
%% test for constant input
factorlist=find(xr(1,:)~=xr(end,:));
if(any(xr(1,:)==xr(end,:)))
    disp('BETAKS: Constant input factors detected.');
end
%% 
for i=factorlist 
   xr(indxx(:,i),i)=cumsum([1;diff(xr(:,i))~=0]); % cheap tied ranks
end
xr=xr./(ones(n,1)*max(xr)); % scale to 1
%%
for j=1:M
   indx= (m(j)<xr) & (xr <= m(j+1));
   xtreme=xor(indx(2:end,:),indx(1:end-1,:));
   xtreme(2:end,:)=xtreme(2:end,:)|xtreme(1:end-1,:);
   %xtreme=indx; % test
   cdfc=cumsum(indx); 
   nm(:,j)=cdfc(end,:)'; % if no ties: always same nr. of realizations
   %% wenn y(1) nicht drin, dann y(1) mit 0 codieren, ebenso y(end) mit 1
   xtreme(1,:)=1;xtreme(n,:)=1;
   %%
   for i=factorlist %1:k
   % sum of conditionals 
   scc=nm(i,j);
   if ~scc, continue, end
   %difference of conditionals (smoothed)
   smoothdcdf=mollify(cdf'-cdfc(:,i)/scc,ceil(P*M))';
   xxtrem=xtreme(:,i)|[xtreme(2:end,i);0];
   critdcdf=smoothdcdf(xxtrem); 
   [mx,mxi]=max(critdcdf);[mn,mni]=min(critdcdf);
   % Kolmogorov Smirnov distance 
   bm(i,j)=max([0,mx,-mn]);
   % Kuiper dicrepancy 
   tm(i,j)=max([0,mx-mn]);
   % Gini flavour
   gm(i,j)=mean(smoothdcdf.^2);
   %%  find max in each positive run, min in each negative
   delta=0.0;
   sgn=0; exval=0.0;

 for val=critdcdf
    sg=sign(val);
    if sg==sgn
     exval=max([exval,sg*val]);
    else 
	 if(exval>0.5*sqrt(1/n+1/scc))
      delta=delta+exval;
	 end
	 sgn  =sg;
	 exval=sg*val;
	end %what if zero?
   end
   dm(i,j)=delta+exval;
   %% and more indicators
   % Wald-Wolfowitz number of runs
   xtreme=xor(indx(2:end,:),indx(1:end-1,:));
   mju=2*scc*(n-scc)/n-1;
   % +-1
   wm(i,j)=(mju-sum(xtreme(:,i))+1)/sqrt(mju*(mju+1)/(n-1));
   % Correlation ratio
   try
       em(i,j)=nanvar(ys(indx(:,i)),1);
   catch
       em(i,j)=var(ys(indx(:,i)),1);
   end
   if(~isempty(gfx))
    subplot(gfxrows,ceil(k/gfxrows),i)
    if( plotdiff)
     plot(ys(xxtrem),critdcdf,'Color',cols(j,:));hold on
    else
     plot(ys(xxtrem),cdf(xxtrem)-critdcdf,'Color',cols(j,:));hold on
    end
   end
   end % for
end
if(~isempty(gfx))
 for i=factorlist %1:k;
     subplot(gfxrows,ceil(k/gfxrows),i);
     title([ gfx ', conditioning on x_{' num2str(i) '}']);xlabel('y');
     if(plotdiff)
       ylabel('\Delta cdf');  
     else
       ylabel('cdf');
     end
     hold off;
 end
end
 b=sum(bm.*nm,2)'/n;
 d=sum(dm.*nm,2)'/n;
 t=sum(tm.*nm,2)'/n;
 w=sum(wm.*nm,2)'/n;
 g=6*sum(gm.*nm,2)'/n;
 f=1-sum(em.*nm,2)'./n/VY;
 e=zeros(1,k);
 e(factorlist)=f(factorlist);
% e=1-(n-M)/(n-1)*mean(em,2)'/var(y); % bias correction
l=struct('ks',b,'kuiper',t,'borgonovo',d,...
    'eta',e,'gini',g,'waldwolfowitz',w,...
    'kssep',bm,'kuipersep',tm,'borgonovosep',dm,...
    'etasep',em,'ginisep',gm,'waldwolfowitzsep',wm,'weights',nm/n);
end

function s=mollify(x,p)
% MOLLIFY moving average data smoother
[n,k]=size(x);
s=x;
p=min(p,floor(n/2-1));
if(p>0)
X=cumsum(x);
for i=1:n
% new 16.11.2020
    ihi=min(n,i+p);
   ilo=max(1,i-p-1);
   s(i,:)=(X(ihi,:)-X(ilo,:))/(ihi-ilo);
% s(i,:)=(X(min(end,i+p),:)-X(max(1,i-p-1),:))/(2*p+1);
end
end
end

function testbeta
%%
ishigami;
n=2^14-1;
x=trafo(sobolseq(n,k));y=model(x);
%%
bs=[];ds=[];ts=[];
Ds=[];Ts=[];
Ps=2:200;
h=waitbar(0,'Computation in progress');
for i=Ps;
    waitbar(sqrt(i*(i+1))/Ps(end),h);
    %if(mod(i,50)==0), fprintf('.'); drawnow('update'); end
    l=betaKS3(x,y,i);bs=[bs;l.ks];ds=[ds;l.borgonovo];ts=[ts;l.kuiper];
%    Ds=[Ds; deltabork(x,y,struct('PartitionSize',i,...
%        'KSLevel',0,'KDWidth','clone','QuadraturePoints',500))];
    Ts=[Ts; deltafast2(x,y,i)];
end
close(h);
%%
clf
plot(Ps,ts);hold on; 
plot(Ps,ds,'--');plot(Ps,Ts,'-.');hold off
%%

end
function test2betaks3
%% betaKS3 not working reliably in case of dirac deltas
n=4096;
x=sobolpoints(n,2);
y=norminv(.5+sign(x(:,1)-.5).*x(:,2)/2);
%
clf
t=linspace(0,1,32);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,0]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,0.25]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,0.5]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,1]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,2]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1);
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,3]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1)
[b,d,k,e,~,B,D]=betaKS3(x,y,[32,4]);d
plot(t,D);hold on;set(gca,'ColorOrderIndex',1)
%%
end