function [retval]=ximpl(x,y,nc,m)
% XIMPL XI importance measure for labeled output (classes).
%  Distances between conditional output 
%  probability mass functions (given a partition of the inputs) from the 
%  unconditional output probability mass function.
%  L=XIMPL(X,Y,NC,M) returns a list L of importances given different 
%  distance/divergence measures of the
%  discrete Y with respect to the inputs X assuming 
%  NC classes and partition size M
% 
% Reference: https://doi.org/10.1016/j.csda.2023.107701

% written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
if(nargin<3) || isempty(nc)
 classlist=unique(y)';
else
 classlist=1:nc; % starts with 1!
end
if(nargin<4) || isempty(m)
% default partitioning: n^1/2 samples in n^1/2 partitions
 m=ceil(sqrt(n)); %*ones(1,k);
% fprintf(' partition size %d\n',l); 
end
%if(isscalar(m))
% m=m*ones(1,k);
%end
%if(m<0)
% % requested number of realisations per subsample
%  m=ceil(n/-m);
%end
 nrmd=@(x)x./sum(x);
 pmf=@(y)nrmd(sum(bsxfun(@eq,y,classlist)));
 
 totalmass=pmf(y);
 if(any(totalmass==0)), warning('Empty class. Check labels'); end 
 %
 Bi=zeros(m,k);
 Di=zeros(m,k);
 Mi=zeros(m,k);
 Vi=zeros(m,k);
 Li=zeros(m,k);
 Ki=zeros(m,k);
 Hi=zeros(m,k);
 Wi=zeros(m,k);
 %
 warned=false;
 for j=1:k
  [xu,ic,ia]=unique(x(:,j));
  if~isscalar(m), mj = m(j); else mj=m; end
  if(length(xu)==n)
   xs=xu;
   ix=ic;
  elseif(mj~=0)
   if(~warned),warning('Ties in the input. Non-unique results');warned=true; end
   [xs,ix]=sort(x(:,j));
  end
  if(mj>0)
   indx=round(linspace(1,n,m+1));
  elseif(mj<0)
   indx=round(linspace(1,n,ceil(n/m)));
  else
  % discrete case
   mj=length(xu);
   ix=ic;
   indx=1:mj;
  end 
  for i=1:mj
   z=y(ix(indx(i):indx(i+1)));

   condmass=pmf(z(:));
   dmass=condmass-totalmass;
   Bi(i,j)=max(abs(dmass));
   Mi(i,j)=.5*(max(dmass)-min(dmass));
   Di(i,j)=sum(abs(dmass));
   % power divergences -2 [-1] -1/2 [0]  1
   lmbda=-2;
   mm=condmass.*((condmass./totalmass).^lmbda-1);
   mm(condmass==0)=0;
   Li(i,j)=1/(lmbda*(1+lmbda))*sum(mm);
   lmbda=1;
   Vi(i,j)=1/(lmbda*(1+lmbda))*sum(condmass.*((condmass./totalmass).^lmbda-1));
   % Kullback Leibler
   kl=condmass.*log(condmass./totalmass);
   kl(condmass==0)=0;
   Ki(i,j)=sum(kl);
   % Hellinger 
   Hi(i,j)=1-sum(sqrt(condmass.*totalmass));
   % Wasserstein W2 squared - unit costs if we change the label?
   Wi(i,j)=sum(dmass.^2);
  end
 end
 retval=struct('l1',mean(Bi),'Range',mean(Mi),'linf',mean(Di),'KullbackLeibler',mean(Ki),...
 'Hellinger',mean(Hi),'PowerDivergence1',mean(Li),'PowerDivergence2',mean(Vi),...
 'Wasserstein',sqrt(mean(Wi)));
end

function testximpl
  %%
  ishigami
  n=5120;
  x=trafo(rand(n,k));y=model(x);
  c=4;
  z=floor(c*(y-min(y))./(max(y)*(1+eps)-min(y)))+1;
  si1=ximpl(x,z,c,32)
  si2=pimf(x,z,c,32)
  %%
end
