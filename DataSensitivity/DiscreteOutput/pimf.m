function [retval]=pimf(x,y,nc,m)
% PIMF Importance measure using distance between conditional output 
% probability mass functions.
% PIMF(X,Y,NC,M) returns the sensitivity of the
%  discrete Y with respect to the inputs X assuming 
%  NC classes and partition size M

% written by elmar.plischke@tu-clausthal.de
[n,k]=size(x);
if(nargin<3) || isempty(nc)
 classlist=unique(y)';
else
 classlist=1:nc;
end
if(nargin<4) || isempty(m)
% default partitioning: n^1/2 samples in n^1/2 partitions
 m=ceil(sqrt(n)); 
% fprintf(' partition size %d\n',l); 
end
if(m<0)
 % requested number of realisations per subsample
  m=ceil(m/-l);
end
 nrmd=@(x)x./sum(x);
 pmf=@(y)nrmd(sum(bsxfun(@eq,y,classlist)));
 
 totalmass=pmf(y);
 
 %
 Bi=zeros(m,k);
 Di=zeros(m,k);
 Mi=zeros(m,k);
 Vi=zeros(m,k);
 Li=zeros(m,k);
 Ki=zeros(m,k);
 Hi=zeros(m,k);
 %
 [xs,ix]=sort(x);
 active=find(xs(1,:)~=xs(end,:)); % ignore constant inputs
 indx=round(linspace(1,n,m+1));
 for i=1:m
   z=y(ix(indx(i):indx(i+1),:));
   for j=active % 1:k
     condmass=pmf(z(:,j));
     
     dmass=condmass-totalmass;
     Bi(i,j)=max(abs(dmass));
     Mi(i,j)=.5*(max(dmass)-min(dmass));
     Di(i,j)=sum(abs(dmass));
     % power divergences -2 [-1] -1/2 [0]  1
     lmbda=-2;
     Li(i,j)=1/(lmbda*(1+lmbda))*sum(condmass.*((condmass./totalmass).^lmbda-1));
     lmbda=1;
     Vi(i,j)=1/(lmbda*(1+lmbda))*sum(condmass.*((condmass./totalmass).^lmbda-1));
     % Kullback Leibler
     kl=condmass.*log(condmass./totalmass);
     kl(condmass==0)=0;
     Ki(i,j)=sum(kl);
     % Hellinger 
     Hi(i,j)=1-sum(sqrt(condmass.*totalmass));
   end
 end
 B=mean(Bi);
 D=mean(Di);
 M=mean(Mi);
 L=mean(Li);
 retval=struct('l1',mean(Bi),'Range',mean(Mi),'linf',mean(Di),'KullbackLeibler',mean(Ki),...
 'Hellinger',mean(Hi),'PowerDivergence1',mean(Li),'PowerDivergence2',mean(Vi),'l1Sep',Bi);
end

function testmimpmf
  %%
  ishigami
  n=5120;
  x=trafo(rand(n,k));y=model(x);
  c=4;
  z=floor(c*(y-min(y))./(max(y)*(1+eps)-min(y)))+1;
  pimf(x,z,c,32)
  %%
end
