function [Shap,V,ST,more]=shapmobdep(k,n,model,trafo,C,rmode)
% SHAPMOBDEP Shapley values via M�bius transform.
% [SH,V]=SHAPMOBDEP(K,N,MODEL,TRAFO,C,RNDMODE) computes the Shapley Values
% SH and the variance V for the simulation MODEL with K inputs using the 
% marginal TRAFO and the rank correlation C for inputs, 
% given a basic sample block of N using random mode 'rand' (MC), 
% 'builtin' (QMC) or 'default' (QMC, external).

% Ref: Elmar Plischke, Giovanni Rabitti, and Emanuele Borgonovo:
% "Computing Shapley Effects for Sensitivity Analysis"
% written by elmar.plischke@tu-clausthal.de

%%
if(nargin<6), rmode='default';end
switch(lower(rmode))
    case {'rand','mc'}
    u=rand(n,2*k);
    case 'builtin'
    u=net(sobolset(2*k),2^4+n);u(1:2^4,:)=[];
    otherwise
    %u=sobolpoints(2^k+n,2*k);u(1:2^k,:)=[]; % Burn-in  
    u=sobolpoints(2^4+n,2*k);u(1:2^4,:)=[];   
end
S=chol(C);
na=norminv(u(:,1:k));       % normal A sample
xa=trafo(normcdf(na*S));
cb=norminv(u(:,k+1:end))*S; % correlated B sample
xb=trafo(normcdf(cb));
ya=model(xa);yb=model(xb); evals=2;
try % only windows
u=memory;
if(k>log2(u.MaxPossibleArrayBytes)-3), warning('Out of memory likely.'); end
end
if(k>=log2(flintmax)), warning('Precision (and patience) may be lost.'); end

if(k>11), pp=0;hh=waitbar(pp,'Progress'); else hh=0;pp=1; end
ell=2^k-1;H=zeros(1,ell);
%K=zeros(2,l);
sz=zeros(1,ell);
for i=1:ell-1
            ppnew=floor(100*i/ell)/200;
            if(ppnew>pp)  
                if(hh~=0),waitbar(ppnew,hh); end
                pp=ppnew;
            end    
% selection of input subset: bitmanipulation
% g=bitand(i,2.^(0:k-1))~=0; % lsb codes first index
 g=logical(bitget(i,1:k));
 m1=sum(g); % subset size
 sz(i)=m1;
 % normal ranks
 gc=~g; % g complement
 D=chol([C(g,g),C(g,gc);...
        C(gc,g),C(gc,gc)]);
 D11=D(m1+1:end,m1+1:end);
 D22=D(1:m1,1:m1);D21=D(1:m1,m1+1:end);
 ci=cb;
 ci(:,gc)=na(:,gc)*D11+cb(:,g)*(D22\D21); 
 xi=trafo(normcdf(ci));
 %
 %xi=xa; xi(:,g)=xb(:,g);
 yi=model(xi); evals=evals+1;
 H(i)=(yb'*(yi-ya))/n;
% if(i<l),K(:,l-i)=[mean((yi-yb).^2)/2;ya'*(yi-yb)/n]; end % dual
end
H(ell)=yb'*(yb-ya)/n; sz(ell)=k; % all b set
%% Shapley values via M�bius Trafo: 
% poset inclusion matrix is pascal triangle mod 2
mob=zeros(size(H));
sel=false(1,ell);
for i=1:ell
%    ii=find(sel);
    sel(1:i)=xor(sel(1:i),[true,sel(1:i-1)]);
    ii=find(sel(1:i));
    mob(:,i)=(H(:,ii)*(-1).^(sz(i)+sz(ii)'))/sz(i);
    ppnew=.5+floor(100*i/ell)/200;
    if(ppnew>pp)
       if(hh~=0),waitbar(ppnew,hh); end
       pp=ppnew;
    end
end
%% Owen/Grabisch formula (weights are already included)
Shap=ones(size(H,1),k);
for i=1:k
    Shap(:,i)=sum(mob(:,logical(bitand(1:ell,2^(i-1)))),2);
end
%% variance
V=H(:,end);
%% First and total effects
ST(2,:)=H(2.^(0:k-1));
ST(1,:)=V-H(2^k-1-2.^(0:k-1));
if(0) % test for Shapley-Owen second order effects
ShapOwen=zeros(k,k);
ShapOwen2=zeros(k,k);
 for i=1:(k-1)
  for j=(i+1):k
   ii=bitor(2^(i-1),2^(j-1));
   ShapOwen(j,i)=mob(ii)*sz(ii);
   ShapOwen2(j,i)=V-H(2^k-1-ii); % val(k)-val(~a)
   jj=logical(bitand(1:ell,ii));
   ShapOwen(i,i)=sum(mob(:,jj).*sz(jj)./(sz(jj)-1),2); % different weights
  end
 end
more.ShapOwen=ShapOwen;
more.ShapOwen2=ShapOwen2; 
end
more.MoebiusInv=mob;
more.ValueFun=H;
more.Sizes=sz;
more.BlockEvals= evals;
if(hh~=0),delete(hh);end
end