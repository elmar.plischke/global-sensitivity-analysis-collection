function retval=shapley(k, n, model,trafo) %Nv, Ni, No)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Authors: Eunhye Song, Barry L. Nelson, Jeremy Staum, Northwestern University
    % Questions/Comments: Please email Eunhye Song at 
    %                     eunhyesong2016 at u.northwestern.edu
    % 
    % Copyright 2015. Eunhye Song, Northwestern University
    % 
    % THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    % FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    % derivative works, such modified software should be clearly marked.
    % Additionally, this program is free software; you can redistribute it 
    % and/or modify it under the terms of the GNU General Public License as 
    % published by the Free Software Foundation; version 3.0 of the License. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This function implements Algorithm 1 to calculate the Shapley effects and
    % their standard errors by examining all permutations of inputs.
    %
    % List of inputs to this function:
    % k: number of inputs
    % Nv: MC sample size to estimate Var[Y]
    % Ni: inner MC sample size to estimate the cost function
    % No: output MC sample size to estimate the cost function
    %
    % This function requires R package "gtools." 
    %
    % The following functions are required from the user to execute this algorithm:
    % Xall(): sample a k-dimensional input vector
    % eta(X): returns output Y when input vector X is given
    % Xset(Sj, Sjc, xjc): sample an input vector corresponding to the indices in Sj
    %                     conditional on the input values xjc with the index set Sjc
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    Nv=n(1);Ni=n(2);No=n(end);
    
    Xall=@()trafo(rand(1,k));
    % Initialize Shapley value for all players
    
    Sh = zeros(1, k);
    Sh2 = zeros(1, k);

    % Estimate Var[Y] 
    Y=0;

    %for (i = 1:Nv)
    %    X=Xall();
    %    % Evaluate the objective function
    %    val = model(X);
    %    Y = [Y; val];
    %end
    X=trafo(rand(Nv,k));
    Y=model(X); evals=Nv;
    EY = mean(Y);
    VarY = var(Y);
    X0=X;
    % Generate all k! permutations
    perm = perms(1:k);
    
    % Estimate Shapley effects
    m = size(perm,1);
    for (p = 1:m)
        pis = perm(p,:);
        prevC = 0;
        for (j = 1:k)
            if (j == k)    
                Chat = VarY;
                del = Chat - prevC;
            else
                cVar = 0;
                Sj = pis(1:j); % set of the 1st-jth elements in pi 
                Sjc = pis(j+1:end); % set of the (j+1)th-kth elements in pi
                for (l = 1:No)
                    %Y=0;
                    %xjc = Xset(Sjc, 0, 0); % sampled values of the inputs in Sjc
                    %xjc = Xall();

                    xjc=trafo(rand(1,k));
                
                    if(Ni==Nv)
                        x=X0;
                    else
                        x=trafo(rand(Ni,k));
                    end
                    x(:,Sjc)=ones(Ni,1)*xjc(Sjc);
                    Y=model(x); evals=evals+Ni;
                    %for (h = 1:Ni)
                    %    % sample values of inputs in Sj conditional on xjc
                    %    %xj = Xset(Sj, Sjc, xjc);
                    %    x=Xall();x(Sjc)=xjc(Sjc);
                    %    %x = [xj, xjc];
                    %    %[~,pi_s] = sort(pis);
                    %    %[~,~,pi_s] = unique(pis);
                    %    val = model(x); %(pi_s));
                    %    Y = [Y;val];
                    %end
                    cVar = [cVar;var(Y)];
                end
                
                Chat = mean(cVar);
                del = Chat - prevC;
            end
            
            Sh(pis(j)) = Sh(pis(j)) + del;
            Sh2(pis(j)) = Sh2(pis(j)) + del^2;
            
            prevC = Chat;
        end
    end
    Sh = Sh/m;
    Sh2 = Sh2/m;
    
    retval=struct('Shapley', Sh, 'SEShapley', sqrt((Sh2 - Sh.^2)/m), 'VarY', VarY, 'EY', EY,'NEvals',evals);
end
