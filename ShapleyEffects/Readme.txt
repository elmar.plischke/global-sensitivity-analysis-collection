These are files from

Elmar Plischke, Giovanni Rabitti, and Emanuele Borgonovo: Computing Shapley Effects for Sensitivity Analysis. 
SIAM/ASA Journal on Uncertainty Quantification, Vol. 9, Iss. 4 (2021), 10.1137/19M1304738
  
https://doi.org/10.1137/19M1304738

All files compute Shapley effects, i.e. Shapley values using the conditional variance as the 
coalition-worth value function. They are all simulation-driven except for the nearest neighbor 
version.

shapley.m    is a stripped down version of the R implementation by Song et al.
myshapley.m  is an optimized version of the above
shapmob.m    uses Moebius inverses for computation
shapmobbs.m  uses Moebius inverses for computation and allows for bootstrapping
shapmobdep.m also handles dependent inputs
shapleybal.m utilizes the balanced value function (Myerson formula for Shapley values)
shapleyNN.m  uses nearest neighbor estimators for obtaining estimates of the value function

shapleygodadual.m is an implementation of Goda (2022) using randomized "crawling centipede" feature subset selection
