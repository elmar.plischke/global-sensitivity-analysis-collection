function [Shap,V,ST,mob,evals]=shapmobbs(k,n,model,trafo,randomsource)
% SHAPMOBBS Shapley values via Moebius transform, bootstrap.
%
% SH=SHAPMOBBS(K,N,MODEL,TRAFO) computes the Shapley values of the simulation
% model MODEL with inverse marginal cdfs TRAFO using K inputs and a basic 
% sample size of N. 
% SH=SHAPMOBBS(..., RNDSRC) specifies a random source (default Sobol' QMC)
% [SH,V]=SHAPMOBBS(...) also returns the variance estimate.
% Number of bootstrap replicates is fixed to 1500.
%%
R=1500;
% create 2 sets of samples
if(nargin>=5) && ~isempty(randomsource)
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('SHAPMOBBS: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use builtin QMC generator
    try
        u=net(sobolset(2*k),2^4+n);
    catch
        % for octave, port of Joe&Kuo 
        u=sobolpoints(2^4+n,2*k);
    end
    ua=u((2^4)+1:end,1:k);
    ub=u((2^4)+1:end,k+1:end);
%%
end
xa=trafo(ua);xb=trafo(ub);
ya=model(xa);yb=model(xb);evals=2;
try % only windows
u=memory;
if(k>log2(u.MaxPossibleArrayBytes)-3), warning('Out of memory likely.'); end
catch
end
if(k>=log2(flintmax)), warning('Precision (and patience) may be lost.'); end

if(k>11), pp=0;hh=waitbar(pp,'Progress'); else hh=0;pp=1; end
l=2^k-1;ls=2.^(0:k-1);
% % Bootstrap using strick breaker
%W=diff([zeros(1,R);sort(rand(n-1,R)); ones(1,R)],[],1);
% % Bootstrap using indices -- for Sobol
%I=[ (1:n) <=n/2 ; (1:n) > n/2 & (1:n) <= n*3/4; ...
%    (1:n) > n*3/4 & (1:n) <= n*7/8; (1:n) > n*7/8 & (1:n) <= n*15/16].*[2;4;8;16];
%I=[repmat(1:(n/2),1,2);repmat(n/2+(1:n/4),1,4);...
%   repmat(n*3/4+(1:n/8),1,8);repmat(n*7/8+(1:n/16),1,16);repmat(n*15/16+(1:n/32),1,32)];
%R=5;
I=round(rand(R,n)*n+.5);
H=zeros(2+R,l);
%K=zeros(2,l);
sz=zeros(1,l);
for i=1:l-1
            ppnew=floor(100*i/l)/200;
            if(ppnew>pp)  
                if(hh~=0),waitbar(ppnew,hh); end
                pp=ppnew;
            end    
% selection of input subset: bitmanipulation
 g=bitand(i,ls)~=0; g=logical(bitget(i,1:k)); % lsb codes first index
 sz(i)=sum(g); % subset size
 xi=xa; xi(:,g)=xb(:,g);
 yi=model(xi);evals=evals+1;
 H(1:2,i)=[mean((yi-ya).^2)/2;(yb'*(yi-ya))/n];
% Sobol'/Saltelli with weight
% H(3:end,i)=(yb.*W)'*(yi-ya);
% Bootstrapping Sobol'/Saltelli 
H(3:end,i)=mean(yb(I).*(yi(I)-ya(I)),2);
% Jansen
 % if(i<l),K(:,l-i)=[mean((yi-yb).^2)/2;ya'*(yi-yb)/n]; end % dual
end
H(1:2,l)=[mean((yb-ya).^2)/2;(yb'*(yb-ya))/n]; sz(l)=k; % all from b
% % Bootstrapping via weights from stick-breaking (Bayesian BS)
%H(3:end,l)=(yb.*W)'*(yb-ya);
% Bootstrapping Sobol'/Saltelli
H(3:end,l)=mean(yb(I).*(yb(I)-ya(I)),2);
%  % Bootstrapping Jensen
% H(3:end,l)=mean((yb(I)-ya(I)).^2,2)/2;
%% Shapley values via Mbius Trafo: 
% poset inclusion matrix is pascal triangle mod 2
mob=zeros(size(H));
sel=false(1,l);
for i=1:l
%    ii=find(sel);
    sel(1:i)=xor(sel(1:i),[true,sel(1:i-1)]);
    ii=find(sel(1:i));
    mob(:,i)=(H(:,ii)*(-1).^(sz(i)+sz(ii)'))/sz(i);
    ppnew=.5+floor(100*i/l)/200;
    if(ppnew>pp)
       if(hh~=0),waitbar(ppnew,hh); end
       pp=ppnew;
    end
end
%% Owen/Grabisch formula (weights are already included)
Shap=ones(size(H,1),k);
for i=1:k
    Shap(:,i)=sum(mob(:,logical(bitand(1:l,2^(i-1)))),2);
end
%% variance
V=H(:,end);
%% First and total effects
ST(1:2,:)= H(1:2,2.^(0:k-1));
ST(3:4,:)=-H(1:2,2^k-1-2.^(0:k-1));
if(hh~=0),delete(hh);end
end

%%
function testmob
%%
models % Oakley OHagan function OO
tic;[Sh1,V1]=shapmob(kOO,2048,modelOO,trafoOO);toc
tic;[Sh2,V2]=shapmob(kOO,2048,modelOO,trafoOO,@(n,k)mod(rand(1,1)+net(scramble(sobolset(k)),n),1));toc
tic;[Sh3,V3]=shapmob(kOO,2048,modelOO,trafoOO,@mhalton);toc
tic;[Sh4,V4]=shapmob(kOO,2048,modelOO,trafoOO,@rand);toc

Sh1./V1
Sh2./V2
Sh3./V3
Sh4./V4

%%
end

%% 
function testbstrp
%%
ns=[25,50,100,250,500,1000,2500,5000,10000]
Sh_pnt=[];
Sh_mean=[];
Sh_mx=[];
Sh_mn=[];
Sh_std=[];
Sh_evl=[];
Vh_pnt=[];
Vh_mean=[];
Vh_std=[];
Vh_mx=[];
Vh_mn=[];
for n=ns
    [Sh,V,~,~,evl]=shapmobbs(k,n,model,trafo,@rand);
    Sh_pnt(end+1,:)=Sh(1,:);
    
    L=sort(Sh(3:end,:));
    R=size(L,1);
    Sh_mn(end+1,:)=diag(L(ceil(+eps+normcdf(2*norminv(sum(L<Sh(1,:))/R)+norminv(.05))*R),1:k));
    Sh_mean(end+1,:)=L(round(R/2),:); % median
    Sh_mx(end+1,:)=diag(L(ceil(normcdf(2*norminv(sum(L<Sh(1,:))/R)-norminv(.05)-eps)*R),1:k));
    Sh_evl(end+1)=evl*n;
    Sh=Sh./V;
    L=sort(Sh(3:end,:));
    R=size(L,1);
    Vh_mn(end+1,:)=diag(L(ceil(+eps+normcdf(2*norminv(sum(L<Sh(1,:))/R)+norminv(.05))*R),1:k));
    Vh_mx(end+1,:)=diag(L(ceil(normcdf(2*norminv(sum(L<Sh(1,:))/R)-norminv(.05)-eps)*R),1:k));
    Vh_pnt(end+1,:)=Sh(1,:);
    %L=sort(Sh(3:end,:));
    Vh_mean(end+1,:)=L(round(R/2),:);
    %Vh_mn(end+1,:)=L(50,:); % ten percent
    %Vh_mx(end+1,:)=L(450,:);    
end
clf
subplot(1,2,1);
set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Sh_pnt, 'o');hold on 
 set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Sh_mean, '-'); set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Sh_mn,':'); set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Sh_mx,':'); set(gca,'ColorOrderIndex', 1);
subplot(1,2,2);
set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Vh_pnt, 'o');hold on 
 set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Vh_mean, '-'); set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Vh_mn,':'); set(gca,'ColorOrderIndex', 1);
semilogx(Sh_evl,Vh_mx,':'); set(gca,'ColorOrderIndex', 1);
%%
end
