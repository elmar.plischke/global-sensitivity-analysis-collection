function [phi,s_all,phi2,chi,chi2]=shapleygodadual(d,n,func,trafo,qrp,gfx)
%SHAPLEYGODA Shapley effects with error bars.
% [SH,V,SHV,T,TV]=SHAPLEYGODA(D,N,FUNC,TRAFO)
% N sample size, D input dimension of model FUNC, with marginal cdfs
% provided by TRAFO
% On return: SH Shaple effects (forward and backward path), 
% V variance, SHV estimator variance
% T Totals, TV totals estimator variance
%
% Required calls to FUNC N*(1+D)
%
% Source: T. Goda, A simple algorithm for global sensitivity 
%                  analysis with Shapley effects

% vectorized by elmar.plischke@tu-clausthal.de
if(nargin<6), gfx = ''; end % empty string for none
if(nargin<5), qrp =  0; end % use random permutations 
if isempty(qrp), qrp=0; end
switch(qrp)
 case 0 % MC/RP
  x = rand(n,d);               % Monte Carlo samples for x
  y = rand(n,d);               % Monte Carlo samples for y
  [~, pm] = sort(rand(n,d),2); % random permutation matrix
  case 1 %QMC/QRP
  burnin = 32; % skip first few realisations
  % QMC with scrambler
  allr = net(scramble(sobolset(3*d),'MatousekAffineOwen'),n+burnin);
  x = allr(burnin+1:end,1:d);
  y = allr(burnin+1:end,d+(1:d));
  [~,pm] = sort(allr(burnin+1:end,2*d+(1:d)),2);
  % invert permutation
%  for i=1:d
%   pm(pm(:,i),i)=(1:n)';
%  end

 case 2 % QMC/RP
       burnin = 32; % skip first few realisations
  % QMC with scrambler
  allr = net(scramble(sobolset(2*d),'MatousekAffineOwen'),n+burnin);
  x = allr(burnin+1:end,1:d);
  y = allr(burnin+1:end,d+(1:d));
    [~, pm] = sort(rand(n,d),2); % random permutation matrix
    case 3 % QMC/QRP direct
burnin = 32; % skip first few realisations
  % QMC with scrambler
  allr = net(scramble(sobolset(3*d),'MatousekAffineOwen'),n+burnin);
  x = allr(burnin+1:end,1:d);
  y = allr(burnin+1:end,d+(1:d));
  [~,pm] = sort(allr(burnin+1:end,2*d+(1:d)),2);
  case 4 % MC/QRP
   x = rand(n,d);               % Monte Carlo samples for x
  y = rand(n,d);               % Monte Carlo samples for y
  [~,pm] = sort(net(scramble(sobolset(d),'MatousekAffineOwen'),n),2);
end
 
 z = trafo(x);
 fz1 = func(z);
 fx = fz1;
 
 v = trafo(y); 
 fy = func(v);
 
 phi = zeros(1,d); phi2 = zeros(1,d); % initialization
 psi = zeros(1,d); psi2 = zeros(1,d); 
 chi =zeros(1,d); chi2=zeros(1,d); % for totals (experimental)
%%
 for j=1:d
%%  
    % activate indices from permutation matrix 
    ind = bsxfun(@eq,pm(:,j),1:d); % compare column with row
	if(j<d)
     z(ind) = v(ind); % copy over next pick'freeze dimension (per run)
	 fz2 = func(z);
    else
    % make last insertion explicit
     fz2 = fy;
    end
    % update
    fmarg = ((fx-fz1/2-fz2/2).*(fz1-fz2))';
    phi = phi + fmarg*ind/n;
    phi2 = phi2 + fmarg.^2*ind/n;
    % totals via elementary effects
    fmarg = (fz1-fz2).^2;
    chi = chi + fmarg'*ind/n;
    chi2 = chi2 + fmarg'.^2*ind/n; 
    % dual stuff ( x <-> y, fz1 <-> fz2 ind <-> ~ind )
    ind = ~ind;
    fmarg = ((fy-fz2/2-fz1/2).*(fz2-fz1))';
    psi = psi + fmarg*ind/n;
    psi2 = psi2 + fmarg.^2*ind/n;

	fz1 = fz2;
 end
 Vy = ((fx-fy)'*(fx-fy))/(2*n);
 %  correct dual by variance
 psi = Vy - psi;
 
 chi2 = (chi2 -chi.^2)/ (4*Vy.^2 *(n-1));
 chi = chi / (2*Vy);

%%
 s_all = sum(phi); 			% variance of function
 phi2 = (phi2-phi.^2)./(n-1);  % variance of Shapley estimates
 
 if(~isempty(gfx))
  subplot(1,2,1);
  bar(0:d,[s_all,phi]); % plot of estimates w/ confidence intervals
  hold on
  er = errorbar(0:d,[s_all,phi],[0,1.96*sqrt(phi2)],[0,1.96*sqrt(phi2)]);
  er.Color = [0 0 0];
  er.LineStyle = 'none';
  hold off
  ylabel('output variance');
  title('Shapley effects (95% confidence)');
 
  subplot(1,2,2)
  bar(1:d,chi); hold on
  errorbar(1:d,chi,2*sqrt(chi2),2*sqrt(chi2),'LineStyle','none','Color','k');
  hold off
  title('Total effects (95% confidence)');
  ylabel('variance contribution');
 end
 s_all=[s_all;sum(psi);Vy];phi=[phi;psi];phi2=[phi2;(psi2-psi.^2)/(n-1)];
end