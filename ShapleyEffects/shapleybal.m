function [Shap,Vy,evals]=shapleybal(k,n,model,trafo,randomsource)
% SHAPLEYBAL Shapley values via balanced worth formula.
% SH=SHAPLEYBAL(K,N,MODEL,TRAFO) computes Shapley values for MODEL
% with K inputs and marginal distributions from TRAFO
% using sample blocks of N realizations

% Ref: Elmar Plischke, Giovanni Rabitti, and Emanuele Borgonovo:
% "Computing Shapley Effects for Sensitivity Analysis"
% written by elmar.plischke@tu-clausthal.de

%% some odd parameter misuses ...
if(nargin==1)
% directly compute Shapley effects from provided value function vector 
% (of length 2^k-1)
    Shap=snapley_val(k);
    return
end
if(~isscalar(k) || k<0)
% use the full version, either with negative dimension or
% dimension set to a rank correlation matrix
    if(nargin<5)
        [Shap,Vy,evals]=shapleybalfull(k,n,model,trafo);
    else
        [Shap,Vy,evals]=shapleybalfull(k,n,model,trafo,randomsource);
    end
    return
end
%% end of parameter misuse
%%
% create 2 sets of samples
if(nargin<5) randomsource=[]; end
[ua,ub]=createsample(k,n,randomsource);

xa=trafo(ua);xb=trafo(ub);
ya=model(xa);yb=model(xb);evals=2;

if(k>=log2(flintmax)), warning('Precision (and patience) may be lost.'); end

if(k>11), pp=0;hh=waitbar(pp,'Progress'); else hh=0;pp=1; end
l=2^k-1;ls=2.^(0:k-1);
% precompute binomial weights
w=zeros(1,k);
for i=1:k
    w0=1; 
    for j=1:i-1
        w0=w0*(k-i+j)/j; % always integer
    end
    w(i)=w0;
end
% variance estimate
Vy=(yb-ya)'*(yb-ya)/n/2;
Shap=ones(1,k)*Vy; % all included: grand total
shft=(ya'*ya-yb'*yb)/n/2;
for i=1:2^(k-1)-1 % only half
    if(hh~=0)
        ppnew=floor(100*i/l)/50;
        if(ppnew>pp)  
            waitbar(ppnew,hh)
            pp=ppnew;
        end
    end
% selection of input subset: bitmanipulation
 g=logical(bitget(i,1:k)); % lsb codes first index
 sz=sum(g); % subset size
 xi=xa; xi(:,g)=xb(:,g);
 yi=model(xi);evals=evals+1;
 %% Shapley values via new duality formula 
 val=yi'*(yb-ya)/n + shft;
 Shap(g)=Shap(g)+val/w(sz);
 Shap(~g)=Shap(~g)-val/w(k-sz);
end
Shap=Shap/k; 
if(hh~=0),delete(hh);end
end

function Shi=snapley_val(val)
% SNAPLEY Fast Shapley Values for binary coded value function results.
n=size(val,1);
k=log2(n+1);
if(k~=round(k)), warning('Value vector should be power-of-2 less 1.'); end

ns=(1:n)';
len=sum(dec2bin(ns)-'0',2); % or bitget 
w=zeros(1,k);
% precompute binomial weights
for i=1:k, w(i)=prod(2:i-1)/prod((k-i+1):(k-1)); end
u=zeros(k,n);
for i=1:k
    ii=bitget(ns,i)==1;
    ww=w(len(ii));  % weight depends on active indices
    u(i,ii)=ww;
    u(i,~ii)=-ww(end-1:-1:1); % complements get negative weights
end

Shi=(u*val)/k;
end

function [Shap,Vy,evals]=shapleybalfull(k,n,model,trafo,randomsource)
% SHAPLEYBALFULL Shapley values via balanced worth formula (non-short cut computation).
% Called if k<0 or k is a matrix

%%
if(~isscalar(k)), S=k; C=chol(S); k=size(C,1); end
if(k<0), k=-k; C=[]; end

if(nargin<5), randomsource=[]; end

[ua,ub]=createsample(k,n,randomsource);

if(isempty(C))
    xa=trafo(ua);xb=trafo(ub);
else
    za=norminv(ua); % std normal
    zb=norminv(ub);
    na=za*C; % multivariate normal
    nb=zb*C;
    xa=trafo(normcdf(na)); % rank correlated
    xb=trafo(normcdf(nb));   
end
ya=model(xa);yb=model(xb);evals=2;

if(k>=log2(flintmax)), warning('Precision (and patience) may be lost.'); end

if(k>11), pp=0;hh=waitbar(pp,'Progress'); else hh=0;pp=1; end
l=2^k-1;
% precompute binomial weights
w=zeros(1,k);
for i=1:k
 w0=1; 
 for j=1:i-1
  w0=w0*(k-i+j)/j; % always integer
 end
 w(i)=w0;
end
% variance estimate
Vy=(yb-ya)'*(yb-ya)/n/2;
Shap=ones(1,k)*Vy; % all included: grand total

for i=1:2^(k-1)-1 % only half
%for i=1:(2^(k)-2)
 if(hh~=0)
  ppnew=floor(100*i/l)/50;
  if(ppnew>pp)  
   waitbar(ppnew,hh)
   pp=ppnew;
  end
 end
% selection of input subset: bitmanipulation
 g=logical(bitget(i,1:k)); % lsb codes first index
 sz=sum(g); % subset size
 if(isempty(C))
     % independent inputs
  xi=xa; xi(:,g)=xb(:,g);
  xj=xb; xj(:,g)=xa(:,g);
 else
%{
     %% something odd here
  ii=zeros(1,k);
  % work on complementary group for Jansen
  ii(~g)=1:(k-sz);
  ii( g)=(k-sz+1):k;
  T=S(:,ii); % reorder
  Z=chol(T(ii,:));
  Z(:,ii)=Z;Z(ii,:)=Z;
  % work on the original group for complementary Jansen
  ii( g)=1:sz;
  ii(~g)=(sz+1):k;
  T=S(:,ii); % reorder
  Q=chol(T(ii,:));
  Q(:,ii)=Q;Q(ii,:)=Q; 
  % back to std normal
  mj=na/Z;mi=na/Q;
  zj=zb; zj(:,~g)=mj(:,~g); xj=trafo(normcdf(zj*Z));
  zi=zb; zi(:,g)=mi(:,g); xi=trafo(normcdf(zi*Q));
%}
 D=chol([S(g,g),S(g,~g);S(~g,g),S(~g,~g)]);
 D11=D(sz+1:end,sz+1:end);
 D22=D(1:sz,1:sz);D21=D(1:sz,sz+1:end);
 ni=na;
 ni(:,~g)=zb(:,~g)*D11+na(:,g)*(D22\D21); 
 sz=k-sz; % complementary size
 E=chol([S(~g,~g),S(~g,g);S(g,~g),S(g,g)]);
 E11=E(sz+1:end,sz+1:end);
 E22=E(1:sz,1:sz);E21=E(1:sz,sz+1:end);
 nj=na;
 nj(:,g)=zb(:,g)*E11+na(:,~g)*(E22\E21);
 sz=k-sz; % reset sz
 xi=trafo(normcdf(ni));xj=trafo(normcdf(nj));
 end
 
 yi=model(xi);yj=model(xj);evals=evals+2;
 
 if(isempty(C))
 % Shapley values via balanced worth 
  bal=(yj-yi)'*(ya-yb)/(2*n);
else
 % formula for dependent inputs
  bal=(yj-yi)'*(yj+yi-2*ya)/(2*n);
 end
 Shap(g)=Shap(g)+bal/w(sz);
 Shap(~g)=Shap(~g)-bal/w(k-sz);
% disp([ bal Shap]/Vy)
end
Shap=Shap/k; 
if(hh~=0),delete(hh);end
end

function [ua,ub]=createsample(k,n,randomsource)
% create 2 sets of samples
if ~isempty(randomsource)
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('SHAPLEYBAL: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use builtin QMC generator
    try
        u=net(sobolset(2*k),2^4+n);
    catch
        % for octave, port of Joe&Kuo 
        u=sobolpoints(2^4+n,2*k);
    end
    ua=u((2^4)+1:end,1:k);
    ub=u((2^4)+1:end,k+1:end);
%%
end
end

