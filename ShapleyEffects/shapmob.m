function [Shap,V,ST,mob,evals]=shapmob(k,n,model,trafo,randomsource)
% SHAPLEY Shapley values via M�bius transform.
%
% SH=SHAPMOB(K,N,MODEL,TRAFO) computes the Shapley values of the simulation
% model MODEL with inverse marginal cdfs TRAFO using K inputs and a basic 
% sample size of N. 
% SH=SHAPMOB(..., RNDSRC) specifies a random source (default Sobol' QMC)
% [SH,V]=SHAPMOB(...) also returns the variance estimate.
%%
% create 2 sets of samples
if(nargin>=5) && ~isempty(randomsource)
    if ~isnumeric(randomsource)
        rsource=randomsource(n,2*k);
        ua=rsource(:,1:k);
        ub=rsource(:,(k+1):end);  
    else
        [nn,kk]=size(randomsource);
        if(n~=nn) || (k~=kk/2)
            error('LOSI: incompatible size of randomsource');
        end
        ua=randomsource(:,1:k);
        ub=randomsource(:,(k+1):end);
    end
else
%% use builtin QMC generator
    try
        u=net(sobolset(2*k),2^4+n);
    catch
        % for octave, port of Joe&Kuo 
        u=sobolpoints(2^4+n,2*k);
    end
    ua=u((2^4)+1:end,1:k);
    ub=u((2^4)+1:end,k+1:end);
%%
end
xa=trafo(ua);xb=trafo(ub);
ya=model(xa);yb=model(xb);evals=2;
try % only windows
u=memory;
if(k>log2(u.MaxPossibleArrayBytes)-3), warning('Out of memory likely.'); end
end
if(k>=log2(flintmax)), warning('Precision (and patience) may be lost.'); end

if(k>11), pp=0;hh=waitbar(pp,'Progress'); else hh=0;pp=1; end
l=2^k-1;ls=2.^(0:k-1);H=zeros(2,l);
%K=zeros(2,l);
sz=zeros(1,l);
for i=1:l-1
            ppnew=floor(100*i/l)/200;
            if(ppnew>pp)  
                if(hh~=0),waitbar(ppnew,hh); end
                pp=ppnew;
            end    
% selection of input subset: bitmanipulation
 g=bitand(i,ls)~=0; % lsb codes first index
 sz(i)=sum(g); % subset size
 xi=xa; xi(:,g)=xb(:,g);
 yi=model(xi);evals=evals+1;
 H(1:2,i)=[mean((yi-ya).^2)/2;(yb'*(yi-ya))/n];
 %try
 %    H(3,l-i)=yb'*(yb-yi)/n;
 %catch
 %    H(3,l)=yb'*(yb-ya)/n;
 %end
% if(i<l),K(:,l-i)=[mean((yi-yb).^2)/2;ya'*(yi-yb)/n]; end % dual
end
H(1:2,l)=[mean((yb-ya).^2)/2;(yb'*(yb-ya))/n]; sz(l)=k; % all from b
%% Shapley values via M�bius Trafo: 
% poset inclusion matrix is pascal triangle mod 2
mob=zeros(size(H));
sel=false(1,l);
for i=1:l
%    ii=find(sel);
    sel(1:i)=xor(sel(1:i),[true,sel(1:i-1)]);
    ii=find(sel(1:i));
    mob(:,i)=(H(:,ii)*(-1).^(sz(i)+sz(ii)'))/sz(i);
    ppnew=.5+floor(100*i/l)/200;
    if(ppnew>pp)
       if(hh~=0),waitbar(ppnew,hh); end
       pp=ppnew;
    end
end
%% Owen/Grabisch formula (weights are already included)
Shap=ones(size(H,1),k);
for i=1:k
    Shap(:,i)=sum(mob(:,logical(bitand(1:l,2^(i-1)))),2);
end
%% variance
V=H(:,end);
%% First and total effects
ST(1:2,:)= H(1:2,2.^(0:k-1));
ST(3:4,:)=-H(1:2,2^k-1-2.^(0:k-1));
if(hh~=0),delete(hh);end
end

%%
function testmob
%%
models % Oakley OHagan function OO
tic;[Sh1,V1]=shapmob(kOO,2048,modelOO,trafoOO);toc
tic;[Sh2,V2]=shapmob(kOO,2048,modelOO,trafoOO,@(n,k)mod(rand(1,1)+net(scramble(sobolset(k)),n),1));toc
tic;[Sh3,V3]=shapmob(kOO,2048,modelOO,trafoOO,@mhalton);toc
tic;[Sh4,V4]=shapmob(kOO,2048,modelOO,trafoOO,@rand);toc

Sh1./V1
Sh2./V2
Sh3./V3
Sh4./V4

%%
end