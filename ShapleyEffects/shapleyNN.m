function [Shap,S,T,H]=shapleyNN(x,y)
% SHAPLEYNN Shapley values via nearest neighbors.
[n,k]=size(x);
% Select all partitions
G=[0,1];  
for i=2:k
    G=[G,G;zeros(1,size(G,2)),ones(1,size(G,2))];
end
G=G(:,2:end); % remove zero
sz=sum(G);    % length / number of active indices
% Call nearest neighbor estimator with optional
% selection of dimensions 
[S,T,H]=ninasi(x,y,G');
H_end=H(end); % should be one, so fake it
H(end)=1;     % Old value might be used for quality checks

% Shapley values via M�bius Trafo: 
% implicit poset inclusion matrix
mob=zeros(size(H));
sel=false(1,size(H,2));
for i=1:size(H,2)
    sel(1:i)=xor(sel(1:i),[true,sel(1:i-1)]);
    ii=find(sel(1:i));
    mob(:,i)=(H(:,ii)*(-1).^(sz(i)+sz(ii)'))/sz(i);
end
% Owen / Grabisch formula
g=logical(G);
Shap=ones(1,k);
for i=1:k
    Shap(i)=sum(mob(g(i,:)));
end
end