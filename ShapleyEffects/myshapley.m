function [Shap,Shap2,evals]=myshapley(k,n,model,trafo,C,rmode)
%% MYSHAPLEY Shapley values.
% SH=SMYHAPLEY(K,N,MODEL,TRAFO) computes Shapley values for MODEL
% with K inputs and marginal distributions from TRAFO
% using sample blocks of N realizations, requiring
% 2^K * N model evaluations.
% [SH,SH2,EVALS]=SHAPLEY(...) also returns alternative estimation and the
% number of calls to MODEL.
% ...=SHAPLEY(,C) realizes a rank correlation structure C (SH2 is then the 
% output to consider).

% Written by elmar.plischke@tu-clausthal.de
% Ideas from Song, Nelson, Staum JUQ
unnormalized=k<0; k=abs(k);
if(nargin<6)
    rmode='qmc';
end
switch(lower(rmode))
    case {'rand','mc'}
        u=rand(n,2*k);
    case {'qmc','sobol'}
        u=sobolpoints(n+2,2*k);u(1:2,:)=[];
    case {'scramble'}
        u= net(scramble(sobolset(2*k),'MatousekAffineOwen'),n);
end
if(nargin>4) && ~isempty(C) && any(any(C~=eye(k)))
    inject=@(C,d,f,alfa,e,g)trafo(normcdf(inject_nora(C,f,alfa,g)));
    S=chol(C); %sqrtm(C);
    dependent=@(S,u,g)normcdf(g*S);
else
    inject=@(C,d,f,alfa,e,g)inject_plain(d,alfa,e);
    S=[];
    C=[];
    dependent=@(S,u,g)u;
end
ua=u(:,1:k);na=norminv(ua); % uncorr.
xa=trafo(dependent(S,ua,na));
ub=u(:,k+1:end);
nb=norminv(ub);
if(~isempty(C))
    db=nb*S; %corr.
else
    db=[];
end
xb=trafo(dependent(S,ub,nb));

ya=model(xa);yb=model(xb);evals=[2,0];
Shap=zeros(1,k); Shap2=zeros(1,k);
% Initialize coalition worth value storing database
valstore=struct(sprintf('x%d',1:k),...
    [mean((yb-ya).^2),yb'*(yb-ya)]);
%for p=perms(1:k)' % for each column

if(k>6), pp=0;hh=waitbar(pp,'Progress'); end
% Use Heap's Algorithm (iterative version) for
% computing all permutations

p=k:-1:1; % initial permutation

isodd=@(x)bitand(x,1);
% c is an encoding of the stack state.
c_heap=ones(k,1);
i_heap=0;
while(i_heap<=k)
    if i_heap==0 || c_heap(i_heap) < i_heap  % need short-circuit
            if isodd(i_heap)
                p([1,i_heap])= p([i_heap,1]);
            elseif i_heap>0
                p([c_heap(i_heap),i_heap])= p([i_heap,c_heap(i_heap)]);
            end
% end of first part of Heap            

            val  = 0; % of emptyset
            val2 = 0;
            xi   = xa;
            qs   = [];
            alfa=false(1,k);
            for q=p
                alfa(q)=true;
                qs = sort([qs,q]);
                key= sprintf('x%d',qs);
                if(~isfield(valstore,key)) % not in the database
%                    xi(:,alfa)=xb(:,alfa); % winding stairs
                    xi=inject(C,xb,db,alfa,xa,na);
                    assert(all(all(xi(:,alfa)==xb(:,alfa))),...
                        'Assertion for permutation %d failed.\n',alfa);
                    yi=model(xi);
                    evals(1)=evals(1)+1;
                    valstore.(key)=[mean((yi-ya).^2),yb'*(yi-ya)];
                else
                    evals(2)=evals(2)+1; % lookup counter
                end
                nval=valstore.(key)(1); % superset importance
                Shap(q)=Shap(q)+nval-val; % marginal contribution
                val=nval;
  
                nval2=valstore.(key)(2); % subset importance
                Shap2(q)=Shap2(q)+nval2-val2;
                val2=nval2; 
            end
            if(k>6)
                ppnew=floor(100*sum(evals)/(prod(1:k)*k))/100;
                if(ppnew~=pp)
                waitbar(ppnew,hh); 
                pp=ppnew;
                end
            end
% Second part of Heap permutation
% Swap has occurred
            if(i_heap>0)
                c_heap(i_heap)= c_heap(i_heap)+1; 
            end
            % Simulate recursive call reaching the base case
            i_heap = 1; 
    else
            % Reset the state and simulate popping the stack by incrementing the pointer.
            c_heap(i_heap) = 1;
            i_heap = i_heap+1;
    end
end
% 
if(~unnormalized)
Shap=Shap/2/var(yb)/prod(1:k);
Shap2=Shap2/n/var(yb)/prod(1:k);
else
    Shap=Shap/2/prod(1:k);
Shap2=Shap2/n/prod(1:k);
end
%%
if(k>6), delete(hh); end% waitbar
%%
end
function xi=inject_plain(xb,alfa,xa)
 xi=xa;xi(:,alfa)=xb(:,alfa);
end
function da=inject_nora(C,db,alfa,na)
% NORA Normal to ranks
beta=~alfa;
D=chol([C(alfa,alfa),C(alfa,beta);...
        C(beta,alfa),C(beta,beta)]);
m1=sum(alfa);
D11=D(m1+1:end,m1+1:end);
D22=D(1:m1,1:m1);D21=D(1:m1,m1+1:end);
%D12=D(m1+1:end,1:m1);
%all(all(D12==0))
da=db;
da(:,beta)=na(:,beta)*D11+db(:,alfa)*(D22\D21);
end

function testshapley
%% Ishigami with correlation between factors 1 and 3
ishigami
rhos=-.95:.05:.95;
S=[];T=[];S2=[];
for rho=rhos
    C=eye(k,k);C(1,3)=rho;C(3,1)=rho;
    [Sh,Sh2]=myshapley(k,1024,model,trafo,C);
    S(end+1,:)=Sh;
    T(end+1,:)=Sh2;
%    l=shapleydep(k,[1024,64,128],model,trafo,C);
%    S2(end+1,:)=l.Shapley/l.VarY;
end
subplot(1,2,1);
plot(rhos,T,'-','LineWidth',2); hold on
set(gca,'ColorOrderIndex',1);
plot(rhos,S,'-.','LineWidth',2);
%set(gca,'ColorOrderIndex',1);
%plot(rhos,S2,'--','LineWidth',2)
hold off
legend('x_1','x_2','x_3','x_4');title('Rank correlated Ishigami');
xlabel('Correlation');ylabel('Shapley Value (Partitioning One)')
S=[];T=[];S2=[];
for rho=rhos
    C=eye(k,k);C(1,3)=rho;C(3,1)=rho;
    [Sh,Sh2]=myshapley(-k,1024,model,trafo,C);
    S(end+1,:)=Sh;
    T(end+1,:)=Sh2;
%    l=shapleydep(k,[1024,64,128],model,trafo,C);
%    S2(end+1,:)=l.Shapley/l.VarY;
end
subplot(1,2,2);
plot(rhos,T,'-','LineWidth',2); hold on
set(gca,'ColorOrderIndex',1);
plot(rhos,S,'-.','LineWidth',2);
%set(gca,'ColorOrderIndex',1);
%plot(rhos,S2,'--','LineWidth',2)
hold off
legend('x_1','x_2','x_3','x_4');title('Rank correlated Ishigami');
xlabel('Correlation');ylabel('Shapley Value (Partitioning Variance)')
%%
end
